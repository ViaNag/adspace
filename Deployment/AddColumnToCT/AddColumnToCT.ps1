#add sharepoint cmdlets
if ( (Get-PSSnapin -Name Microsoft.SharePoint.PowerShell -ErrorAction SilentlyContinue) -eq $null )
{    
      Add-PsSnapin Microsoft.SharePoint.PowerShell
}

function Add-SPContentTypeField
{
    
 
    Param (
           [parameter(Mandatory=$true)][string]$ListName,
           [parameter(Mandatory=$true)][string]$FieldName,
           [parameter(Mandatory=$true)][string]$CTypeAddedTo,
           [parameter(Mandatory=$true)][string]$spWebUrl
           )
 

	# Script Variables
    $spWeb = Get-SPWeb $spWebUrl
    $list = $spWeb.Lists[$ListName]
 
    
        write-host "Checking site:"$spWeb.Title " list : "$list.Title -Foregroundcolor Green
 
            # Check to see if the library contains the content type we are looking for
            if (($list.ContentTypes | where { $_.Name -eq $CTypeAddedTo }) -eq $null)
            {
                Write-Host "The Content:" $CTypeAddedTo "does not exist in the library:" $list.Title -Foregroundcolor Red
            }
            else
            {
                # Check to see if the library contains the field we are looking for
                if (($list.Fields | where { $_.Title -eq $FieldName }) -eq $null)
                {
                    
                    # Get the column you want to copy
                    $siteCol = $spWeb.Fields.GetFieldByInternalName($FieldName)
                    # Get the content type you want to add the field to
                    $ct = $list.ContentTypes[$CTypeAddedTo]
                    
                    
                    # Add the column to the content type
                    $ct.Fieldlinks.Add($siteCol)
                    
                    $ct.Update()
                    Write-Host "The Field:" $siteCol.Title "has been successfully added to the library:" $list.Title -Foregroundcolor Cyan
                }
                else
                {
                    Write-Host "The Field:" $FieldName "exist in the library:" $list.Title -Foregroundcolor Yellow
                }
            }
        
    
    # Dispose of the site object
    $spWeb.Dispose()
}

[string] $currentLocation = Get-Location

[string] $configFileName = $currentLocation + "\Settings.xml"

# Check that the config file exists.
if (-not $(Test-Path -Path $configFileName -Type Leaf))
{
Write-Error -message ("Configuration file '" + $configFileName + "' does not exist.") -Red
}

$configXml = [xml]$(get-content $configFileName)
if( $? -eq $false ) 
{
Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red`
}

if ($configXml.Settings)
{
    foreach ($list in $configXml.Settings.Site)
    {
        Add-SPContentTypeField -ListName $list.ListName -FieldName $list.FieldName -CTypeAddedTo $list.CTypeAddedTo -spWebUrl $list.spWebUrl
    }
}


