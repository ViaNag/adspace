Add-PSSnapin Microsoft.SharePoint.Powershell

function Create-AllMetadataView
{
    
    Param (
           [parameter(Mandatory=$true)][string]$SiteUrl,
           [parameter(Mandatory=$true)][string]$SubSiteUrl,
           [parameter(Mandatory=$true)][string]$CSVPath
           )
 
    # Import CSV
    [array]$FolderList = Import-Csv $CSVPath
 
    # Script Variables
    $subSiteWeb = Get-SPWeb $SubSiteUrl
	
	#All metadata view
	$allMetadataViewName = "All Metadata"
	$defaultViewName = "Default View"

	#Add the column names from the ViewField property to a string collection
	$viewFields = New-Object System.Collections.Specialized.StringCollection
	$viewFields.Add("DocIcon") > $null
	$viewFields.Add("LinkFilename") > $null
	$viewFields.Add("Modified") > $null
	$viewFields.Add("Editor") > $null
	
	#Add the column names from the ViewField property to a string collection
	$allMetadataViewFields = New-Object System.Collections.Specialized.StringCollection
	$allMetadataViewFields.Add("DocIcon") > $null
	$allMetadataViewFields.Add("LinkFilename") > $null
	$allMetadataViewFields.Add("Modified") > $null
	$allMetadataViewFields.Add("Editor") > $null
	$allMetadataViewFields.Add("ViacomDepartment") > $null
	$allMetadataViewFields.Add("AdSpaceContentCategory") > $null
	$allMetadataViewFields.Add("Division") > $null
	$allMetadataViewFields.Add("LifecycleState") > $null
	$allMetadataViewFields.Add("DocMarketplace") > $null
	$allMetadataViewFields.Add("DocNetwork") > $null
	$allMetadataViewFields.Add("DocPlatform") > $null
	$allMetadataViewFields.Add("DocPublishingFrequency") > $null
	$allMetadataViewFields.Add("DocQuarter") > $null
	$allMetadataViewFields.Add("DocYear") > $null
	
	$viewQuery = "<Where></Where>"


	#DefaultView property
	$metadataViewDefaultView = $false
	$viewDefaultView = $true
	#RowLimit property
	$viewRowLimit = 50
	#Paged property
	$viewPaged = $true
	
    Foreach ($folderGroup in $FolderList) {
		
		if($folderGroup.LibraryName -ne '')
        {
            $docLib = $subSiteWeb.Lists[$folderGroup.LibraryName]
			
			$allMetadataView = $docLib.Views["All Metadata"]
			$defaultView = $docLib.Views["Default View"]
			
			if($allMetadataView -ne $null)
			{
				#Delete this view from the list
				$docLib.Views.Delete($allMetadataView.ID)
			}
			
			if($defaultView -ne $null)
			{
				#Delete this view from the list
				$docLib.Views.Delete($defaultView.ID)
			}
			#Create the view in the Navigation list

			$allMetadataView = $docLib.Views.Add($allMetadataViewName, $allMetadataViewFields, $viewQuery, $viewRowLimit, $viewPaged, $metadataViewDefaultView)
			$defaultView = $docLib.Views.Add($defaultViewName, $viewFields, $viewQuery, $viewRowLimit, $viewPaged, $viewDefaultView)
			
			Write-Host ("View '" + $allMetadataView.Title + "' created in list '" + $docLib.Title + "' on site " + $subSiteWeb.Url)
			Write-Host ("View '" + $defaultView.Title + "' created in list '" + $docLib.Title + "' on site " + $subSiteWeb.Url)
        }
        
    }
    $subSiteWeb.Dispose()
}


[string] $currentLocation = Get-Location

[string] $configFileName = $currentLocation + "\AllMetadataView.xml"

# Check that the config file exists.
if (-not $(Test-Path -Path $configFileName -Type Leaf))
{
	Write-Error -message ("Configuration file '" + $configFileName + "' does not exist.") -Red
}

$configXml = [xml]$(get-content $configFileName)
if( $? -eq $false ) 
{
	Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red`
}

if ($configXml.SubSiteFolders)
{
	foreach ($SubSite in $configXml.SubSiteFolders.Site)
	{
		Write-Host "Creating all metadata view for libraries in " + $SubSite.SubSiteUrl + " with " + $SubSite.CSVFile + "CSV file" -ForegroundColor Yellow

            $csvFullPath = $currentLocation + "\"+ $SubSite.CSVFile
            Create-AllMetadataView -SiteUrl $SubSite.SiteUrl -SubSiteUrl $SubSite.SubSiteUrl -CSVPath $csvFullPath

            Write-Host "Successfully Created navigation for " + $SubSite.SubSiteUrl + " with " + $SubSite.CSVFile + "CSV file" -ForegroundColor Green
    }
}
