﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddBookmark.aspx.cs" Inherits="Viacom.AdSpace.AppsWeb.Pages.AddBookmark" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Add Bookmark</title>
    <script type="text/javascript" src="../Scripts/js/jquery.js"></script>
    <script type="text/javascript" src="../Scripts/js/CustomJs/Bookmark.js"></script>
    <script src="../Scripts/js/iframeResizer.contentWindow.min.js"></script>
    <link href="../styles/CSS/Bookmark.css" rel="stylesheet" />
    <link href="http://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="frmBookmarks" runat="server">

<div id="dvMessage"  style="display:none">Bookmark Added !</div>
<div id="bookmark" >
    <div class="dvConatiner">
    <div class="leftsection" >Title</div>
    <div class="rightsection" > 
        <asp:TextBox ID="title" runat="server"></asp:TextBox>
        
    </div>
    </div>
    
    <div class="dvConatiner">
        <div class="leftsection urlID" id="urlTitle"  >Url</div>
        <div class="rightsection urlID" id="urlValue" > <asp:TextBox ID="url" runat="server"></asp:TextBox></div>
    </div>
    <div class="dvConatiner">
    <div class="leftsection" >Group Name</div>
    <div  class="rightsection" >   
        <div style="padding-bottom:10px;">
            <span title="Group Name: Choose Option" class="inputSpan">
                <input id="chkGrpName" name="chkGroupName" value="DropDownButton" checked="checked"  type="radio"/>
            </span>
            <select name="ddlGrpNAme"  id="ddlGrpNAme" title="Group Name: Choice Drop Down" ></select>
        </div> 
        
        <div>
            <span onclick="" class="inputSpan" title="Group Name: Specify your own value:">
                <input id="chkOther" name="chkGroupName" value="FillInButton" type="radio"/>
            </span>
            <input  runat="server" class="inputSpan"   name="txtGroupName" maxlength="255" id="txtOther" title="Group Name : Specify your own value:" type="text" style="width:120px"/>
            <label  style="font-size:11px;color:#ccc;float:left;">Specify your own value:</label>
        </div>
        
                          
    </div> 
        </div>    
    
      <div style='float:left' >
          <button class="button" onclick="javascript:Bookmark.Save();return false;" style="top:74%;position:absolute"> Save</button>

      </div>
    <div id="dvError"  style="display:none;color:red;" class="ErrorMessage"></div>
 
    
  </div>
    </form>
</body>
</html>
