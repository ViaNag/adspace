﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExternalNews.aspx.cs" Inherits="Viacom.AdSpace.AppsWeb.Pages.ExternalNews" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/CSS/News.css" rel="stylesheet" />
    <link href="http://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css" />
     <script src="../Scripts/js/iframeResizer.contentWindow.min.js"></script>
     <script type="text/javascript">
        // Set the style of the client web part page to be consistent with the host web.
        
        function setStyleSheet() {
            var hostUrl = ""
            if (document.URL.indexOf("?") != -1) {
                var params = document.URL.split("?")[1].split("&");
                for (var i = 0; i < params.length; i++) {
                    p = decodeURIComponent(params[i]);
                    if (/^SPHostUrl=/i.test(p)) {
                        hostUrl = p.split("=")[1];
                        document.write("<link rel=\"stylesheet\" href=\"" + hostUrl + "/_layouts/15/defaultcss.ashx\" />");
                        break;
                    }
                }
            }
            if (hostUrl == "") {
                document.write("<link rel=\"stylesheet\" href=\"/_layouts/15/1033/styles/themable/corev15.css\" />");
            }
        }
        setStyleSheet();
    </script>
   
 
   
</head>
<body>
    <form id="frmExtNews" runat="server">

     <asp:HiddenField ID="lblSenderExternal" runat="server"></asp:HiddenField>
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>   

<!--<asp:Label ID="lbl" runat="server" Text=""/>-->
        <div>
    <asp:UpdatePanel ID="upExternalNews" runat="server" UpdateMode="Conditional" >
     <Triggers> 
        <asp:AsyncPostBackTrigger ControlID="trvExternalNews" EventName="SelectedNodeChanged" /> 
    </Triggers>
    <ContentTemplate> 
        <table class="auto-style1 fullWidth">
            <tr>
                <td class="treeViewContainer" >
                    <asp:TreeView ID="trvExternalNews"  CssClass="arrow" ShowExpandCollapse="true" RootNodeStyle-HorizontalPadding="10px" RootNodeStyle-VerticalPadding="5px" LeafNodeStyle-HorizontalPadding="10px" LeafNodeStyle-VerticalPadding="5px"  ExpandImageUrl="../Images/RSSDown.png" CollapseImageUrl="../Images/RSSRight.png"  NodeWrap="true" NodeIndent="5" runat="server" OnSelectedNodeChanged="trvExternalNews_SelectedNodeChanged" ontreenodeexpanded="trvExternalNews_TreeNodeExpanded">
                           <RootNodeStyle CssClass="rssRootnodes"   />
                <LeafNodeStyle CssClass="rssLeafnodes" />                        
                         </asp:TreeView>
                    <br />
                  
                </td>
                
                <!--<td style="width:2%" class="tdpartition">-->
                   
                </td>
                <td id="tdExternalNews" class="auto-style6" runat="server">
                </td>
            </tr>
            </table>
   
        </ContentTemplate>
   
</asp:UpdatePanel>
            
    </div>
  </form>
   <!-- <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-50891440-1', 'adspaceviacom.com');
        ga('send', 'pageview');
        ga('set', '&uid',((USER_ID))); // Set the user ID using signed-in user_id.

</script>-->
</body>
</html>