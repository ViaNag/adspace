﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManagePermissionByUser.aspx.cs" Inherits="Viacom.AdSpace.AppsWeb.Pages.ManagePermissionByUser" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <title>:: Manage Permissions By User ::</title>
    <!-- Common Kendo UI Web CSS -->
    <link href="../styles/CSS/kendo.common.min.css" rel="stylesheet" />
    <link rel="Stylesheet" type="text/css" href="../Styles/peoplepickercontrol.css" />
    <link rel="stylesheet" href="../styles/CSS/jquery-ui.css" />
    <link href="http://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css" />
    <!-- Default Kendo UI Web theme CSS -->
    <link href="../styles/CSS/kendo.default.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="../styles/CSS/jquery-ui-1.10.4.custom.css" />
    <link rel="stylesheet" href="../styles/CSS/jquery.dataTables_themeroller.css" />
    <link rel="stylesheet" href="../styles/CSS/ManagePermissions.css" />
    <link rel="stylesheet" href="../styles/CSS/Access_request_style.css" type="text/css" />
    <link rel="stylesheet" href="../styles/CSS/ManagePermissionByUser.css" />
    <link href="../styles/CSS/chosen.css" rel="stylesheet" />
    <script src="../Scripts/js/jquery.js" type="text/javascript"></script>
    <script src="../Scripts/js/jquery-ui.js" type="text/javascript"></script>
    <script src="../Scripts/MicrosoftAjax.js"></script>
    <script src="../Scripts/peoplepickercontrol.js" type="text/javascript"></script>
    <script src="../Scripts/js/jquery.dataTables.js" type="text/javascript"></script>
    <script src="../Scripts/js/chosen.jquery.js" type="text/javascript"></script>
    <!-- Kendo UI Web combined JavaScript -->
    <script type="text/javascript" src="../Scripts/js/kendo.web.min.js"></script>
    <script src="../Scripts/js/CustomJs/AccessManagementHelper.js"></script>
    <script src="../Scripts/js/CustomJs/ManagePermissionByUser.js"></script>
   
    <style>
        .error {
            color: red;
        }
        .success {
            color: green;
        }
        #divError {
            margin-top: 10px;
            text-align: center;
        }
        .new_perm_error {
            color: red;
            left: 50%;
            right: 50%;
        }
        .div_to_user_val {
            display: inline;
        }

        .td_max_width_200 {
            max-width: 200px;
        }
      /*  .ui-autocomplete{
            z-index:999;
        }*/
      #ddMRUCopySelectFromUser_chosen,#ddMRUCopySelectToUser_chosen,#ddMRUTransSelectFromUser_chosen,#ddMRUTransSelectToUser_chosen,#ddMRUDeprovisionSelectUser_chosen
      {
          width : 250px!important;
      }

    </style>

      <script>  
(function( $ ) {
    $.widget( "custom.combobox", {
        _create: function() {
            this.wrapper = $( "<span>" )
            .addClass( "custom-combobox" )
            .insertAfter( this.element );
            this.element.hide();
            this._createAutocomplete();
            this._createShowAllButton();
        },
        _createAutocomplete: function() {
            var selected = this.element.children( ":selected" ),
            value = selected.val() ? selected.text() : "";
            this.input = $( "<input>" )
            .appendTo( this.wrapper )
            .val( value )
            .attr( "title", "" )
            .addClass( "custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left" )
            .autocomplete({
                delay: 0,
                minLength: 0,
                source: $.proxy( this, "_source" )
            })
            .tooltip({
                tooltipClass: "ui-state-highlight"
            });
            this._on( this.input, {
                autocompleteselect: function( event, ui ) {
                    ui.item.option.selected = true;
                    this._trigger( "select", event, {
                        item: ui.item.option
                    });
                },
                autocompletechange: "_removeIfInvalid"
            });
        },
        _createShowAllButton: function() {
            var input = this.input,
            wasOpen = false;
            $( "<a>" )
            .attr( "tabIndex", -1 )
            .attr( "title", "Show All Items" )
            .tooltip()
            .appendTo( this.wrapper )
            .button({
                icons: {
                    primary: "ui-icon-triangle-1-s"
                },
                text: false
            })
            .removeClass( "ui-corner-all" )
            .addClass( "custom-combobox-toggle ui-corner-right" )
            .mousedown(function() {
                wasOpen = input.autocomplete( "widget" ).is( ":visible" );
            })
            .click(function() {
                input.focus();
                // Close if already visible
                if ( wasOpen ) {
                    return;
                }
                // Pass empty string as value to search for, displaying all results
                input.autocomplete( "search", "" );
            });
        },
        _source: function( request, response ) {
            var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
            response( this.element.children( "option" ).map(function() {
                var text = $( this ).text();
                if ( this.value && ( !request.term || matcher.test(text) ) )
                    return {
                        label: text,
                        value: text,
                        option: this
                    };
            }) );
        },
        _removeIfInvalid: function( event, ui ) {
            // Selected an item, nothing to do
            if ( ui.item ) {
                return;
            }
            // Search for a match (case-insensitive)
            var value = this.input.val(),
            valueLowerCase = value.toLowerCase(),
            valid = false;
            this.element.children( "option" ).each(function() {
                if ( $( this ).text().toLowerCase() === valueLowerCase ) {
                    this.selected = valid = true;
                    return false;
                }
            });
            // Found a match, nothing to do
            if ( valid ) {
                return;
            }
            // Remove invalid value
            this.input
            .val( "" )
            .attr( "title", value + " didn't match any item" )
            .tooltip( "open" );
            this.element.val( "" );
            this._delay(function() {
                this.input.tooltip( "close" ).attr( "title", "" );
            }, 2500 );
            this.input.data( "ui-autocomplete" ).term = "";
        },
        _destroy: function() {
            this.wrapper.remove();
            this.element.show();
        }
    });
})( jQuery );
        $(function() {
            
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#ddSelectUser").chosen();
            $("#ddMRUCopySelectFromUser").chosen();
            $("#ddMRUCopySelectToUser").chosen();
            $("#ddMRUTransSelectFromUser").chosen();
            $("#ddMRUTransSelectToUser").chosen();
            $("#ddMRUDeprovisionSelectUser").chosen();
         //   SearchText();
        });

        
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div id="mainwrapper">
            <!--Top menu starts-->
            <div id="menu">
                <ul>
                    <li ><a href="" id="AccessRequest">Access Request</a></li>
                    <li class="active_menu"><a href="" id="ManageByUser">Manage Permissions by User</a></li>
                    <li><a href="" id="ManageByContent">Manage Permission by Content</a></li>
                    <li><a href="" id="RequestApproval">Request Approval</a></li>
                    <li><a href="" id="MyRequest">My Request</a></li>

                </ul>
            </div>
            <!--Top menu ends-->
            <div id="container">
                <div class="clr div_white"></div>
                <div id="errorDiv" class="main-section hide">
                    <label id="lblMsg" runat="server"></label>
                    <label id="lblErr" class="err" runat="server"></label>
                </div>

                <div id="divCopyPermissions" class="hide">
                    <div id="divCopyContent" style="z-index: 1000;">
                        <div class="div_blue" id="CopyPermissionFromDiv">
                            <label>Copy From: </label>
                            <div id="currentMRUCopyFromDiv" class="div_to_user_val">
                                <label id="lblMRUCopyFromUser"></label>
                                <input type="button" name="CopyPermissionChange" id="MPUCopyPermissionChangebtn" value="Change" />
                            </div>
                            <div id="newMRUCopyFromDiv" class="hide">
                              <asp:DropDownList ID="ddMRUCopySelectFromUser" style="max-width:200px;" runat="server" />
                                  <%--<asp:TextBox ID="txtCopySelectFromUser" Text="-- Select --" Width="200px" runat="server"></asp:TextBox>&nbsp;
                            <input type="button" id="copySelectFromUserCnclbtn" style="width:20px" onclick="javascript: clear_selectUser_CopyPermissionFrom()" value="X" />--%>
                                <%--<input type="text" id="txtMRUCopyFromUser" name="txtMRUCopyFromUser" style="width: 200px;" />&nbsp;&nbsp;<img src="../images/ico_search.png" width="16" height="16" alt="Search" align="absmiddle" />&nbsp;&nbsp;<img src="../images/ico_user_blue.png" width="16" height="16" alt="Click to search for user" align="absmiddle" />--%>
                                <input type="button" name="CopyPermissionNewCancel" id="MPUCopyPermissionNewCancelbtn" value="Cancel" />
                            </div>

                        </div>
                        <div class="div_grey" id="CopyPermissionToDiv">
                            <label>Copy To  &nbsp;&nbsp: </label>
                           <asp:DropDownList ID="ddMRUCopySelectToUser" style="max-width:200px;" runat="server" /> 
                           <%-- <asp:TextBox ID="txtCopySelectToUser" Text="-- Select --" Width="200px" runat="server"></asp:TextBox>&nbsp;
                            <input type="button" id="copySelectToUserCnclbtn" style="width:20px" onclick="javascript: clear_selectUser_transPermissionFrom()" value="X" />--%>
                            <%--<input type="text" id="txtMRUCopyToUser" name="txtMRUCopyToUser" style="width: 200px;" />&nbsp;&nbsp;<img src="../images/ico_search.png" width="16" height="16" alt="Search" align="absmiddle" />&nbsp;&nbsp;<img src="../images/ico_user_blue.png" width="16" height="16" alt="Click to search for user" align="absmiddle" />--%>
                        </div>
                        <div id="CopyPermissionError" class="hide">
                        </div>
                    </div>
                    <div id="CopyPermissionLoading" class="hide" style="margin-top: -11%; z-index: 1001">
                        <img id="imgCopyPermission" src="../images/ajaxloader2.gif" style="margin-left: 46%; float: left;" />
                    </div>

                </div>

                <div id="divTransferPermissions" class="hide">
                    <div id="divTransferConetent" style="z-index: 1000">
                        <div class="div_blue" id="TransferPermissionFromDiv">
                            <label>Transfer From: </label>
                            <div id="currentMRUTransferFromDiv" class="div_to_user_val">
                                <label id="lblMRUTransferFromUser"></label>
                                <input type="button" name="TransferPermissionChange" id="MPUTransferPermissionChangebtn" value="Change" />
                            </div>
                            <div id="newMRUTransferFromDiv" class="hide">
                             <asp:DropDownList ID="ddMRUTransSelectFromUser" style="max-width:200px;" runat="server" />
                                  <%--<asp:TextBox ID="txtTransSelectFromUser" Text="-- Select --" Width="200px" runat="server"></asp:TextBox>&nbsp;
                            <input type="button" id="transSelectFromUserCnclbtn" style="width:20px" onclick="javascript: clear_selectUser_transPermissionFrom()" value="X" />--%>
                                <%--<input type="text" id="txtMRUTransferFromUser" name="txtMRUTransferFromUser" style="width: 200px;" />&nbsp;&nbsp;<img src="../images/ico_search.png" width="16" height="16" alt="Search" align="absmiddle" />&nbsp;&nbsp;<img src="../images/ico_user_blue.png" width="16" height="16" alt="Click to search for user" align="absmiddle" />--%>
                                <input type="button" name="TransferPermissionNewCancel" id="MPUTransferPermissionNewCancelbtn" value="Cancel" />
                            </div>
                        </div>
                        <div class="div_grey" id="TransferPermissionToDiv">
                            <label>Transfer To: </label>
                          <asp:DropDownList ID="ddMRUTransSelectToUser" style="max-width:200px;" runat="server" />
                             <%--<asp:TextBox ID="txtTransSelectToUser" Text="-- Select --" Width="200px" runat="server"></asp:TextBox>&nbsp;
                            <input type="button" id="transSelectToUserCnclbtn" style="width:20px" onclick="javascript: clear_selectUser_transPermissionTo()" value="X" /> --%>
                            <%--<input type="text" id="txtMRUTransferToUser" name="txtMRUTransferToUser" style="width: 200px;" />&nbsp;&nbsp;<img src="../images/ico_search.png" width="16" height="16" alt="Search" align="absmiddle" />&nbsp;&nbsp;<img src="../images/ico_user_blue.png" width="16" height="16" alt="Click to search for user" align="absmiddle" />--%>
                        </div>
                        <div id="TransferPermissionNoteDiv">
                            <label>Please note that the user will be deprovisioned once the permissions have been copied.</label>
                        </div>
                        <div id="TransferPermissionError" class="hide">
                        </div>
                    </div>
                    <div id="TransferPermissionLoading" class="hide" style="margin-top: -11%; z-index: 1001">
                        <img id="imgTransferPermission" src="../images/ajaxloader2.gif" style="margin-left: 46%; float: left;" />
                    </div>

                </div>

                <div id="divDeprovisionUser" class="hide">
                    <div id="divDeprovisionUserSelectOuterDiv" style="z-index: 1000">
                        <div class="div_blue" id="DeprovisionSelectDiv">
                            <label>Select User: </label>
                          <asp:DropDownList ID="ddMRUDeprovisionSelectUser" style="max-width:200px;" runat="server" />
                            <%--<asp:TextBox ID="txtDeprovisionSelectUser" Text="-- Select --" Width="200px" runat="server"></asp:TextBox>&nbsp;
                            <input type="button" id="DeprovisionSelectUser" style="width:20px" onclick="javascript: clear_selectUser_deprovision()" value="X" />--%>
                        </div>
                    </div>
                    <div id="DeprovisionUserLoading" class="hide" style="margin-top: -6%; z-index: 1001">
                        <img id="imgDeprovisionUser" src="../images/ajaxloader2.gif" style="margin-left: 46%; float: left;" />
                    </div>
                    <div id="DeprovisionUserError" class="hide">
                    </div>
                </div>

                <div id="divSelectUser" style="width: 100px;" class="hide">
                    <div id="divFieldOwners">
                        
                        <div class="ms-core-form-line">
                            <div id="divAdministrators" class="cam-peoplepicker-userlookup ms-fullWidth">
                                <span id="spanAdministrators"></span>
                                <asp:TextBox ID="inputAdministrators" runat="server" CssClass="cam-peoplepicker-edit" Width="70"></asp:TextBox>
                            </div>
                            <div id="divAdministratorsSearch" class="cam-peoplepicker-usersearch ms-emphasisBorder"></div>
                            <asp:HiddenField ID="hdnAdministrators" runat="server" />
                        </div>
                    </div>
                    <div class="ms-core-form-line">
                        <asp:Label ID="lblEnteredData" runat="server" CssClass="ms-fullWidth"></asp:Label>
                    </div>
                </div>

                <div id="divManagePermissionUser">
                    <div class="div_white_border">
                        <div class="div_dark_grey">
                            <span class="spnDetails">Select User: &nbsp;</span>
                            <input type="hidden" id="hidManageByUserName" />
                            <asp:Label ID="lblManageByUserName" runat="server"></asp:Label>
                            <asp:DropDownList ID="ddSelectUser" style="max-width:200px;" runat="server" />
                         <%--    <asp:TextBox ID="txtSelectUser" Text="-- Select --" Width="200px" runat="server"></asp:TextBox>&nbsp;
                            <input type="button" id="reqUserClrBtn" style="width:20px" onclick="javascript:clear_selectUser()" value="X" />&nbsp;--%>
                            <input type="button" id="btnMPUGetComponents" runat="server" value="Get Permissions" class="button_primary" />
                            <input type="button" id="btnMPUCopyPerms" runat="server" value="Copy Permissions" class="button_primary" />
                            <input type="button" id="btnMPUTransferPerms" runat="server" value="Transfer Permissions" class="button_primary" />
                            <input type="button" id="btnMPUDeprovisionUser" runat="server" value="De-provision User" class="button_primary" />
                            <br />
                        </div>
                    </div>
                    <div id="divError" class="center hide">
                    </div>
                    <br />
                    <div id="mainContentOuterDiv" class="main_content">
                        <div id="mainContentDiv" class="div_white_border">
                            <!--left files structure here-->
                            <div id="file_structure" class="flt-lft">
                                <div id="sectionTreeDiv">
                                    <ul id="sectionTree" class="access_tree_scroll hide"></ul>
                                </div>
                                <div id="treeViewLoading">
                                    <img id="imgTreeViewLoading" src="../images/ajaxloader.gif" />
                                </div>
                            </div>

                            <!--right access list is here-->
                            <div id="access_structure" class="flt_right">
                                <div id="sectionWithAccessRights" class="access_rights_scroll">
                                    <%--<img id="accessRightsLoader" class="access_rights_loader hide" src="../img/ajaxloader.gif" />--%>
                                    <table id="tbSectionItemDetails" class="display hide">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Title</th>
                                                <th>Access</th>
                                                <th>Level</th>
                                                <th>Action</th>
                                            </tr>
                                            <tr id="trParentRow" class="tr_parent_row">
                                                <td id="tdParentType"></td>
                                                <td id="tdParentName"></td>
                                                <td id="tdParentAccessType" class="center"></td>
                                                <td id="tdParentAccessLevel" class="center"></td>
                                                <td id="tdParentAction" class="center"></td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                                <div id="sectionItemDetailsLoading">
                                    <img id="imgsectionItemLoading" src="../images/ajaxloader.gif" />
                                </div>
                                <div class="clr"></div>

                                <div class="clr"></div>
                                <div class="flt_left align_center">
                                </div>
                            </div>
                            <div class="clr div_white"></div>
                        </div>

                    </div>
                </div>
            </div>
            <div id="loadingUnderlay" class="ManagePermissionByUserUnderlay">
                <div class="ManagePermissionByUserLightBox" id="loadingDiv">
                    <img id="imgLoader" src="../images/ajaxloader2.gif" /><br />
                </div>
            </div>
        </div>

    </form>
</body>
</html>
