[string] $currentLocation = Get-Location

[string] $ConfigFileName = $currentLocation + "\Settings.xml"

Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
[string]$xmlpath = $ConfigFileName
	 
$settingsXML =  [xml](Get-Content ($xmlpath))
if( $? -eq $false ) 
{
	LogError "Could not read config file. Exiting ..."
	Stop-Transcript
	Stop-SPAssignment -Global
	Exit 0
}


$publicCertPath = $settingsXML.AppSettings.pathToCerFile
$NameRootAuthority = $settingsXML.AppSettings.rootAuthorityName
$TokenIssuerName = $settingsXML.AppSettings.tokenIssuerName 
$specificIssuerId = $settingsXML.AppSettings.appIssuerID

Write-Host "Success: Reading Configuration file: Process Completed ..." -ForegroundColor Green

Write-Host "Started Adding root authority..." -ForegroundColor Green 

$certificate = New-Object System.Security.Cryptography.X509Certificates.X509Certificate2($publicCertPath)

New-SPTrustedRootAuthority -Name $NameRootAuthority -Certificate $certificate 

Write-Host 'Added to root authority' -ForegroundColor Green 

Write-Host "Started Adding token issuer..." -ForegroundColor Green 

$realm = Get-SPAuthenticationRealm

$fullIssuerIdentifier = $specificIssuerId + '@' + $realm 

New-SPTrustedSecurityTokenIssuer -Name $TokenIssuerName -Certificate $certificate -RegisteredIssuerName $fullIssuerIdentifier –IsTrustBroker

Write-Host 'Added token issuer' -ForegroundColor Green 

iisreset 