function CreateGroup([String]$ConfigFileName = "")
{

	Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
	[string]$xmlpath = $ConfigFileName
	 
	$groupsXML =  [xml](Get-Content ($xmlpath))
	if( $? -eq $false ) 
	{
		LogError "Could not read config file. Exiting ..."
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	}
	Write-Host "Success: Reading Configuration file: Process Completed ..." -ForegroundColor Green 
	try
	{
	$siteurl=$groupsXML.Groups.site
	$Site= Get-SPSite -Identity $siteurl
	$SPWeb = $Site.RootWeb	
	$groupsXML.Groups.group |
	ForEach-Object {
					$GroupName=$_.name	
							
						 if ($SPWeb.SiteGroups[$SPWeb.Title +" " +$GroupName] -ne $null){
						  Write-Host "Group "$SPWeb.Title $GroupName" already exists!"			  
						 } 
						 else
						 {
						  $Description=$_.Description
						  $OwnerName=$_.Owner
						  $MemberName=$_.Member
						  $PermissionLevel=$_.PermissionLevel

						  # Check if Owner, Member and Permission Level is present
						  if ($OwnerName -ne "") { $owner = $SPWeb | Get-SPUser $OwnerName } else {$owner=$SPWeb.Site.Owner}
						  if ($MemberName -ne "") {  $member = $SPWeb | Get-SPUser $MemberName } else {$member=$SPWeb.Site.Owner}		
						  if ($PermissionLevel -eq "") {$PermissionLevel="Full Control"}
			  
						  #Adds the Group to the site and gives the permission to the group		
						  $SPWeb.SiteGroups.Add($SPWeb.Title +" " +$GroupName,$owner,$member,$Description)
						  $SPGroup = $SPWeb.SiteGroups[$SPWeb.Title +" " +$GroupName]
                          $roleAssigment = new-object Microsoft.SharePoint.SPRoleAssignment($SPGroup)

                          $PermissionLevel.Split(",") | ForEach {

                            $roleDefinition = $SPWeb.Site.RootWeb.RoleDefinitions[$_]
						  
						    $roleAssigment.RoleDefinitionBindings.Add($roleDefinition)

                          }

						  


						  $SPWeb.RoleAssignments.Add($roleAssigment)
						  Write-Host "Group-"$SPWeb.Title $GroupName "added to the site"		  			 
						 }
				$_.BreakRoleInheritence |ForEach-Object{ 			
					$_.List |ForEach-Object{
						if($_.Name)
						{
						# Break role inheritance of the list
						$web=Get-SPWeb $_.WebUrl
						$List = $web.Lists[$_.Name]
						$List.BreakRoleInheritance($true)
						# Give custom permissions on the list
						$roleDefinition = $SPWeb.Site.RootWeb.RoleDefinitions[$_.PermissionLevel]
						$roleAssigment = new-object Microsoft.SharePoint.SPRoleAssignment($SPGroup)					
						$roleAssigment.RoleDefinitionBindings.Add($roleDefinition)			
						$List.RoleAssignments.Add($roleAssigment) 
						$List.Update()
						}
				}
			}
			$SPWeb.Update()
			$SPWeb.Dispose()			
	}
	write-host "Successfully added groups"
	}
	catch
	{
		Write-Host "Exception while creating groups. Exiting script..." -ForegroundColor Red        
		Stop-Transcript
		Stop-SPAssignment -Global		
		Exit -1
	
	}


 
}

[string] $currentLocation = Get-Location

[string] $parameterfile = $currentLocation + "\Groups.xml"

#[string] $parameterfileForPublish = $currentLocation + "\PublishFile.xml"



CreateGroup $parameterfile

#PublishFile $parameterfileForPublish

