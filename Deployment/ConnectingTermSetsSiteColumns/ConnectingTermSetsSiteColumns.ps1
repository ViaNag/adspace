﻿$snapin = Get-PSSnapin | Where-Object {$_.Name -eq 'Microsoft.SharePoint.Powershell'} 
if ($snapin -eq $null) 
{    
	Write-Host "Loading SharePoint Powershell Snapin"    
	Add-PSSnapin "Microsoft.SharePoint.Powershell" 
}
#Name of MMS Proxy
$mmsServiceName = "Managed Metadata Service Proxy"
#Name of Groups
$grpNameArray=@("M and E Ad Sales")
 
Write-host "Enter Site Collection URL"
$siteUrl =  Read-Host
$site =Get-SPSite  $siteUrl
$web = $site.OpenWeb();


		#Declaring the Name of the Term Set for each groups.

#"Content categories","Department","Division","Lifecycle State","Network","Publishing Frequency","Year","CORPORATE REPORTS SUBSITE","INTERNATIONAL SUBSITE","M＆E SUBSITE","NICKELODEON SUBSITE"
#"DocContentCategory","ViacomDepartment","Division","LifecycleState","DocNetwork","DocPublishingFrequency","DocYear","CORPORATE REPORTS SUBSITE","INTERNATIONAL SUBSITE","M＆E SUBSITE","NICKELODEON SUBSITE"


        $termSets1=@("Client","Client Categories","Client Segment")
        #$termSets2=@("Departments","Divisions;Music ＆ Entertainment","Lifecycle State","Networks","Year")

        #Declaring the Metadata Columns
        $metaDataCol1=@("ClientName","ClientCategory","ClientSegment");
        #$metaDataCol2=@("ViacomDepartment","Division","LifecycleState","Network","Year");

		Write-Host "In the Site Collection"  $web.url
     foreach ($grpName in $grpNameArray) 
	  {       
       if($grpName -eq "M and E Ad Sales")
       {
        $k=0;
		foreach ($taxonomy in $termSets1) 
		{
			
			$spweb = $site.RootWeb		
			$session = new-object Microsoft.SharePoint.Taxonomy.TaxonomySession($site)
			$termStore = $Session.TermStores[$mmsServiceName];
			$group=$termStore.Groups[$grpName]
			
			Write-Host $group
			
            $termSet=""
            $term=""
            $associateToTerm=$false;
            if($taxonomy.Contains(";"))
            {
             $multiLevelTaxonomy=$taxonomy.Split(";")
             $m=0;
             foreach($tax in $multiLevelTaxonomy)
             {
              if($m -eq 0)
              {
              $termSet=$group.TermSets[$tax]
              $term=$group.TermSets[$tax]
              }
              else{
              $term=$term.Terms[$tax]
              $associateToTerm=$true
              }
              $m++;
             }
            }
            else{
			$termSet = $group.TermSets[$taxonomy]
             }					
			$targetField = [Microsoft.SharePoint.Taxonomy.TaxonomyField]$spweb.Fields.GetFieldByInternalName($metaDataCol1[$k])
			if($targetField -ne $null)
			{
				 Write-Host "Connecting with Term set!" $termSet.id " Namely"  $termSet.Name 
				$targetField.sspid = $termstore.id
				$targetField.termsetid = $termSet.id
                if($associateToTerm -eq $true){
                $targetField.AnchorId=$term.Id
                }
				$targetField.Update($true)								
			}
        $k++;	
        }
       }
        
			$spweb.Dispose()				
		}		
    	
	   

