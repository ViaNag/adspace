Add-PSSnapin Microsoft.SharePoint.Powershell -ErrorAction "SilentlyContinue"

function SetTermsRecursive ([Microsoft.SharePoint.Taxonomy.TermSetItem] $termsetitem, $parentnode)
{
	$parentnode.term |
		ForEach-Object {
			## create the term
			if($_ -ne $null)
			{
				$newterm = $termsetitem.CreateTerm($_.name, 1033)
				Write-Host -ForegroundColor Cyan "Added term :" $_.name "`n"
				SetTermsRecursive $newterm $_
			}
		}
}
#Do not modify anything in the script from here onwards
function Get-ScriptDirectory
{
	$Invocation = (Get-Variable MyInvocation -Scope 1).Value
	Split-Path $Invocation.MyCommand.Path
}

#Solutions to Deploy
$XMLName = "MMS_data.xml"
$XMLPath = Join-Path (Get-ScriptDirectory) $XMLName

echo "Extracting information from the $XMLPath"


Write-host "Enter Web-Application URL"
$input =  Read-Host
#Site Collection URL - Give your site collection url in quotation marks
$TaxonomySiteUrl = $input

#Access the TermStore data
[xml]$TermStoreData = Get-Content ($XMLPath)

$site = Get-SPSite $TaxonomySiteUrl
$session = new-object Microsoft.SharePoint.Taxonomy.TaxonomySession($site)
$termstore = $session.TermStores[$TermStoreData.termstore.name]
$TermStoreData.termstore.group |
	ForEach-Object {
		## create the group
		if ($termstore.Groups[$_.name] -eq $null)
		{
			$group = $termstore.CreateGroup($_.name);
			Write-Host -ForegroundColor Cyan "Added group :" $_.name "`n"
			$_.termset |
				ForEach-Object {
					## create the termset
					$termset = $group.CreateTermSet($_.name)
					Write-Host -ForegroundColor Cyan "Added termset : " $_.name "`n"
					SetTermsRecursive -termsetitem $termset -parentnode $_
				}
		}
	}

$termstore.CommitAll()