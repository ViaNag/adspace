
### Load SharePoint SnapIn   
if ((Get-PSSnapin "Microsoft.SharePoint.PowerShell" -ErrorAction SilentlyContinue) -eq $null)   
{   
    Add-PSSnapin Microsoft.SharePoint.PowerShell   
}   
### Load SharePoint Object Model   
[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SharePoint")   
  

 
function ShareColumn($web, $list, $contentType, $fieldToShare)
{

$list = $web.Lists[$list]   
### Get Document Set Content Type from list   
[Microsoft.SharePoint.SPContentType]$cType = $list.ContentTypes[$contentType]   
[Microsoft.Office.DocumentManagement.DocumentSets.DocumentSetTemplate]$newDocumentSetTemplate = [Microsoft.Office.DocumentManagement.DocumentSets.DocumentSetTemplate]::GetDocumentSetTemplate($cType);
$newDocumentSetTemplate.SharedFields.Add($cType.Fields[$fieldToShare]);
$newDocumentSetTemplate.AllowedContentTypes.Add($web.ContentTypes[$contentType].Id);
$newDocumentSetTemplate.Update($true);
$spFieldLink = New-Object Microsoft.SharePoint.SPFieldLink ($cType.Fields[$fieldToShare])
$cType.Update();
$web.Update();

}

    
  
# get the Web Application for the given Url 
Write-host -f White "`nEnter Site Collection URL"
$url =  Read-Host

$web = Get-SPWeb $url
ShareColumn $web "Team Site Shared Content" "AdSpace Client Documents" "Client Category"
ShareColumn $web "Team Site Shared Content" "AdSpace Client Documents" "Client Segment"
ShareColumn $web "Team Site Shared Content" "AdSpace Client Documents" "Client Name"

$web.Dispose()  






