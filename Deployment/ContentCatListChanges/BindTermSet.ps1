﻿$snapin = Get-PSSnapin | Where-Object {$_.Name -eq 'Microsoft.SharePoint.Powershell'} 
if ($snapin -eq $null) 
{    
	Write-Host "Loading SharePoint Powershell Snapin"    
	Add-PSSnapin "Microsoft.SharePoint.Powershell" 
}

function ChangeTypeOfListCol([String]$ConfigFileName = "")
{

	Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
	[string]$xmlpath = $ConfigFileName
	 
	$mmdXml =  [xml](Get-Content ($xmlpath))
	if( $? -eq $false ) 
	{
		LogError "Could not read config file. Exiting ..."
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	}
	Write-Host "Success: Reading Configuration file: Process Completed ..." -ForegroundColor Green 


    $site =Get-SPSite  $mmdXml.Config.SiteCollectionUrl

	$session = new-object Microsoft.SharePoint.Taxonomy.TaxonomySession($site)
	$termStore = $Session.TermStores[$mmdXml.Config.MMDServiceName];
	$group=$termStore.Groups[$mmdXml.Config.MMDGroupName]

    $mmdXml.Config.mapping |
	ForEach-Object {

        Write-Host "In the Site Collection"  $_.SiteUrl

        $spweb = Get-SPWeb $_.SiteUrl
        
		$list = $spweb.Lists[$_.ListName]

        $targetField = $list.Fields.GetField($_.ColumnName)

		#$targetField = $list.Fields[$_.ColumnName]



		if($targetField -ne $null)
		{
            $termSet = $group.TermSets[$_.MMDTermName]
                        
			Write-Host "Connecting with Term set!" $termSet.id " Namely"  $termSet.Name -ForegroundColor Yellow

		    $targetField.sspid = $termstore.id
		    $targetField.termsetid = $termSet.id
            
            if($associateToTerm -eq $true){
                $targetField.AnchorId=$term.Id
            }

		    $targetField.Update($true)								
				
            Write-Host "Updated Field" $_.ColumnName -ForegroundColor Green						
		}
        
    }
        
	$site.Dispose()				
}

[string] $currentLocation = Get-Location

[string] $parameterfile = $currentLocation + "\BindTermSet.xml"


ChangeTypeOfListCol $parameterfile