if ((Get-PSSnapin "Microsoft.SharePoint.PowerShell" -ErrorAction SilentlyContinue) -eq $null) 
{
    Add-PSSnapin "Microsoft.SharePoint.PowerShell"
}

Write-host "Enter Site Collection URL"
$siteUrl =  Read-Host
$site =Get-SPSite  $siteUrl
$web = $site.OpenWeb();

$list = $web.Lists["Device Channels"]

$itemTablet = $list.Items.Add()

$itemTablet["Name"] = "Tablet"
$itemTablet["Alias"] = "Tablet"
$itemTablet["Description"] = "Device Channel for Tablets"
$itemTablet["Device Inclusion Rules"] = "iPad"
$itemTablet["Active"] = $true

$itemTablet.Update()

$itemIPhone = $list.Items.Add()

$itemIPhone["Name"] = "iPhone"
$itemIPhone["Alias"] = "iPhone"
$itemIPhone["Description"] = "Device Channel for iPhone"
$itemIPhone["Device Inclusion Rules"] = "iPhone"
$itemIPhone["Active"] = $true

$itemIPhone.Update()

$itemAndroid = $list.Items.Add()

$itemAndroid["Name"] = "Android"
$itemAndroid["Alias"] = "Android"
$itemAndroid["Description"] = "Device Channel for Android"
$itemAndroid["Device Inclusion Rules"] = "Android"
$itemAndroid["Active"] = $true

$itemAndroid.Update()

$web.Dispose()