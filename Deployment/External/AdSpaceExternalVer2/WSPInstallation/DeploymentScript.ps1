


function Get-ScriptDirectory
{
	$Invocation = (Get-Variable MyInvocation -Scope 1).Value
	Split-Path $Invocation.MyCommand.Path
}


function UnInstallSolution([object]$solutionsConfiguration)
{	
    $AdminServiceName = "SPAdminV4"
	$IsAdminServiceWasRunning = $true;  
    [string] $SolutionName =""

    if ($(Get-Service $AdminServiceName).Status -eq "Stopped")
	   {
		    $IsAdminServiceWasRunning = $false;
		    Start-Service $AdminServiceName
		    Write-Host 'SERVICE WAS STOPPED, SO IT IS NOW STARTED'
	   }
    foreach($solutionConfiguration in $solutionsConfiguration.Solution)
    {
       
            $SolutionName = $solutionConfiguration.Name
	
	        Write-Host -f Yellow 'Uninstalling solution '$SolutionName

	        $Solution = Get-SPSolution | ? {($_.Name -eq $SolutionName) -and ($_.Deployed -eq $true)}

	        if($Solution.ContainsWebApplicationResource)
		    {
			    Uninstall-SPSolution $SolutionName -AllWebApplications -Confirm:$false
		    }
		    else
		    {
			    Uninstall-SPSolution $SolutionName -Confirm:$false
		    }

            while ($Solution.JobExists)
	         {

		            Start-Sleep 2
	         }
           #  WaitForJobToFinish($SolutionName)
	  	    if($Error.Count -eq 0)
		    {
              Write-Host -f Green 'Solution '$SolutionName' has been uninstalled successfully.'
            }
            else
            {
              Write-Host -f Red 'Error in uninstalling solution '$SolutionName
              $error.clear()
            }       
        }
}


function RemoveSolution([object]$solutionsConfiguration)
{	
    $AdminServiceName = "SPAdminV4"
	$IsAdminServiceWasRunning = $true;  
    [string] $SolutionName =""
    if ($(Get-Service $AdminServiceName).Status -eq "Stopped")
	   {
		    $IsAdminServiceWasRunning = $false;
		    Start-Service $AdminServiceName
		    Write-Host 'SERVICE WAS STOPPED, SO IT IS NOW STARTED'
	   }
    foreach($solutionConfiguration in $solutionsConfiguration.Solution)
    {
           $SolutionName = $solutionConfiguration.Name
	
	       Write-Host -f Yellow 'Removing solution '$SolutionName
           $Solution = Get-SPSolution | ? {($_.Name -eq $SolutionName) -and ($_.Deployed -eq $false)}

	        if($Solution -ne $null)
		    {
                Remove-SPSolution $SolutionName -Confirm:$false   
	        }

            while ($Solution.JobExists)
	        {
		      Start-Sleep 2
	        }
           # WaitForJobToFinish($SolutionName)
	  	    if($Error.Count -eq 0)
		    {
                Write-Host -f Green 'Solution '$SolutionName' has been removed successfully.'
            }
            else
            {
                Write-Host -f Red 'Error in removing solution '$SolutionName
                $error.clear()
            }       

        }
}




function AddSolution([object]$solutionsConfiguration)
{
	$AdminServiceName = "SPAdminV4"
	$IsAdminServiceWasRunning = $true;
   

	if ($(Get-Service $AdminServiceName).Status -eq "Stopped")
	{
		$IsAdminServiceWasRunning = $false;
		Start-Service $AdminServiceName
		Write-Host 'SERVICE WAS STOPPED, SO IT IS NOW STARTED'
	}

  foreach($solutionConfiguration in $solutionsConfiguration.Solution)				
    {
        $SolutionPath = $solutionConfiguration.Path
        $SolutionName = $solutionConfiguration.Name

	    #solutions to deploy
	    $curDir = Get-Location
        $curDirPath = $curDir.Path


        if($SolutionPath -ne "")
        {
            $SolutionPath = $SolutionPath.Trim()
            $SolutionPath = $SolutionPath.Trim("\/.")
      
        }

         if($SolutionPath -ne "")
        {	   

             $SolutionPath =  $curDirPath + "\"+ $SolutionPath +"\" + $SolutionName
        }

        else
        {
           $SolutionPath =  $curDirPath  +"\" + $SolutionName   
        }
		Write-Host -f yellow "Solution full path : "  $SolutionPath
	    
	   
        Write-Host  -f Yellow 'Adding solution '$SolutionName

	    Add-SPSolution $SolutionPath  | Out-Null
            
        if($Error.Count -eq 0)
		{
                Write-Host -f Green 'Solution '$SolutionName' has been added successfully.'
        }
        else
        {
                Write-Host -f Red 'Error: in adding  Solution '$SolutionName 
                Write-Host -f Red $Error
                $error.clear()
        }
    }
 

	if (-not $IsAdminServiceWasRunning)
	{
		Stop-Service $AdminServiceName
	}
}



function WaitForJobToFinish([string]$SolutionFileName)
{ 
	$JobName = "*solution-deployment*$SolutionFileName*"
	$job = Get-SPTimerJob | ?{ $_.Name -like $JobName }
	if ($job -eq $null) 
	{
		Write-Host 'Timer job not found'
	}
	else
	{
		$JobFullName = $job.Name
		Write-Host -NoNewLine "Waiting to finish job $JobFullName"
		while ((Get-SPTimerJob $JobFullName) -ne $null) 
		{
			Write-Host -NoNewLine .
			Start-Sleep -Seconds 2
		}
		Write-Host  "Finished waiting for job.."
	}

$error.clear()
}
	
function InstallSolution([object]$solutionsConfiguration)
{
	
    $AdminServiceName = "SPAdminV4"
	$IsAdminServiceWasRunning = $true;  
   [string]$SiteUrl  = ""
    [string] $SolutionName =""
 if ($(Get-Service $AdminServiceName).Status -eq "Stopped")
	    {
		    $IsAdminServiceWasRunning = $false;
		    Start-Service $AdminServiceName
		    Write-Host 'SERVICE WAS STOPPED, SO IT IS NOW STARTED'
	    }
    foreach($solutionConfiguration in $solutionsConfiguration.Solution)
    {
      $SiteUrl  = $solutionConfiguration.Url
      $SolutionName = $solutionConfiguration.Name

	

	
	Write-Host -f Yellow 'Installing solution '$SolutionName'...'

	$Solution = Get-SPSolution | ? {($_.Name -eq $SolutionName) -and ($_.Deployed -eq $false)}

	if(($Solution -ne $null) -and ($Solution.ContainsWebApplicationResource))
	{
		Install-SPSolution $SolutionName -WebApplication $SiteUrl -GACDeployment  -Force  -Confirm:$false
	}
	else
	{
		Install-SPSolution $SolutionName -GACDeployment -Force -Confirm:$false
	}

	WaitForJobToFinish($SolutionName)
	Write-Host -f Green 'Solution '$SolutionName' has been installed successfully  to webapplication ' $SiteUrl'.'
    $error.clear()
}
}


# =================================================================================
# FUNC: Create web application
# =================================================================================
function CreateWebApp([object] $webAppConfiguration) 
{
		# Set the WebApplicationBuilderObject and set it's properties from the config-file
		Write-Host "Creating Web Application: " $webAppConfiguration.name -ForegroundColor Green
		
		$ap = New-SPAuthenticationProvider –UseWindowsIntegratedAuthentication –DisableKerberos

         [string]$port= $webAppConfiguration.WebAppPort
         [string]$hostHeader= $webAppConfiguration.WebAppHostHeader
         [string]$appPool = $webAppConfiguration.WebAppPoolName
         [string]$name = $webAppConfiguration.WebAppDisplayName
         $appPoolAccount = (Get-SPManagedAccount $webAppConfiguration.WebAppPoolUsername)
         [string]$dbName=$webAppConfiguration.WebAppContentDBName
         [string]$dbServer=$webAppConfiguration.WebAppContentDBServer
         [string]$path=$webAppConfiguration.IISRootDirectory
         [boolean]$isSSlenabled= [bool]::Parse($webAppConfiguration.SSL)
         [boolean]$isAnonymousAccess= [bool]::Parse($webAppConfiguration.AllowAnonymousAccess)
		
		$webapp = New-SPWebApplication -Name $name  -Port $port -HostHeader $hostHeader –AuthenticationProvider $ap -ApplicationPool $appPool –ApplicationPoolAccount $appPoolAccount -DatabaseName $dbName  -DatabaseServer $dbServer  -Path $path  -SecureSocketsLayer:($isSSlenabled) -AllowAnonymousAccess:($isAnonymousAccess)
		
		if($Error.Count -eq 0)
		{
			Write-Host "Success: Creating Web Application: " $webAppConfiguration.name "." -ForegroundColor Green
			Write-Host "Provisioning " $webAppConfiguration.name	-ForegroundColor Green
		}
		else
		{
			Write-Host "Error: Creating Web Application: " $webAppConfiguration.name ". Exiting Script...." -ForegroundColor Red
			Write-Host $Error -ForegroundColor Red
            
            
		}
		
		# Provision the Web Application to all WFE's
		if($Error.Count -eq 0)
		{
			$webapp.ProvisionGlobally()
		}	

		#Managed Paths
		if($Error.Count -eq 0)
		{
			if ($webAppConfiguration.ManagedPaths -ne "")
			{
				foreach($ManagedPath in $webAppConfiguration.ManagedPaths.ManagedPath)
				{
					if($ManagedPath.RelativeURL -ne "")
                        { 
                           New-SPManagedPath -RelativeURL $ManagedPath.RelativeURL -WebApplication $webapp -Explicit
					        if($Error.Count -gt 0)
					        {
						        Write-Host $Error -ForegroundColor Red
                                  
                                 $error.clear()
						        Break
					        }
                        }
				}
			}
		
		
		#Web Level Solutions
		if($Error.Count -eq 0)
		{
			if ($webAppConfiguration.Solutions -ne "")
			{	
				foreach($solutionConfig in $webAppConfiguration.Solutions.Solution)
				{
					if($solutionConfig.name -ne "")
                    {
                        Install-SPSolution -Identity $solutionConfig.name -Force -WebApplication $webapp -GACDeployment
					    WaitForJobToFinish
					    IISReset
					    if($Error.Count -gt 0)
					    {
						    Write-Host $Error -ForegroundColor Red
  
                             $error.clear()
						    Break
					    }
                    }
				}
			}
			
		}
		
	}

 $error.clear()
}
	
	

# =================================================================================
# FUNC: Create site collection
# =================================================================================
function CreateSiteCollection([object] $siteCollectionConfiguration)
{
	
		if(!($siteCollectionConfiguration.Title.ToString() -eq ""))
		{
			Write-Host "Creating Site Collection:" $siteCollectionConfiguration.Title "(" $siteCollectionConfiguration.Path ")." -ForegroundColor Green
			$SiteCollection = New-SPSite -Url $siteCollectionConfiguration.Path -Name $siteCollectionConfiguration.Title -Description $siteCollectionConfiguration.Description -Language 1033 -Template $siteCollectionConfiguration.Template -OwnerAlias $siteCollectionConfiguration.AdminAccount
			if($Error.Count -eq 0)
			{
				Write-Host "Success: Creating Site Collection:" $siteCollectionConfiguration.Title "(" $siteCollectionConfiguration.Path ")." -ForegroundColor Green
			}
			else
			{
				Write-Host "Error: Creating Site Collection:" $siteCollectionConfiguration.Title "(" $siteCollectionConfiguration.Path "). Exiting script..." -foregroundcolor Red
				Write-Host $Error -ForegroundColor Red
                  
                $error.clear()
			}
			
			$primaryOwner = ""
 			$secondaryOwner = ""
  			if ($siteCollectionConfiguration.CreateDefaultGroups -eq "true")
			{
	 			Write-Host "Removing any existing visitors group for Site collection $($siteCollectionConfiguration.Path)" -foregroundcolor blue
	 			#This is here to fix the situation where a visitors group has already been assigned
	 			#$SiteCollection.RootWeb.AssociatedVisitorGroup = $null;
	 			$SiteCollection.RootWeb.Update();
				Write-Host "Creating Owners group for Site collection $($siteCollectionConfiguration.Path)" -foregroundcolor blue
	 			$SiteCollection.RootWeb.CreateDefaultAssociatedGroups($primaryOwner, $secondaryOwner, $siteCollectionConfiguration.Title)
	 			$SiteCollection.RootWeb.Update();
                  
                $error.clear()
			}
			
			# This section is added only to enable creation of search center subsite underneath Root site collection for AdSales
			if(!($siteCollectionConfiguration.Title.ToString() -eq "Root"))
			{
				Enable-SPFeature –identity "f6924d36-2fa8-4f0b-b16d-06b7250180fa" -URL $siteCollectionConfiguration.Path
			}
			
 		}	
}



# =================================================================================
# FUNC: CreateSite
# =================================================================================
function CreateSite([object] $sitesConfiguration)
{
	foreach($siteConfiguration in $sitesConfiguration.Site)
	{
		if(!($siteConfiguration.Path.ToString() -eq ""))
		{
			Write-Host "Creating Sites :" $siteConfiguration.Title "(" $siteConfiguration.Path ")." -ForegroundColor Green
			$Web = New-SPWeb -Url $siteConfiguration.Path -Name $siteConfiguration.Title -Description $siteConfiguration.Description -Language 1033 -Template $siteConfiguration.Template 
			if($Error.Count -eq 0)
			{
				Write-Host "Success: Creating Sites :" $siteConfiguration.Title "(" $siteConfiguration.Path ")." -ForegroundColor Green
			}
			else
			{
				Write-Host "Error: Creating Sites :" $siteConfiguration.Title "(" $siteConfiguration.Path "). Exiting script..." -foregroundcolor Red
				Write-Host $Error -ForegroundColor Red
  
                 $error.clear()
			}
			
 		}	
	}
}


# =================================================================================
# FUNC: Activate publishing web feature on all the site of site collection.
# =================================================================================

function EnableFeature([object] $featuresConfig)
{
   
        foreach($featureConfig in $featuresConfig.Feature)
	    {
		 
           if(!($featureConfig.Name.ToString() -eq ""))
	         { 
                $siteUrl = $featureConfig.Url
        
                $featureName = $featureConfig.Name
                $UpdateWebFeature = $null

                if($featureConfig.Type  -eq "Web")
                 {
                        
       
                       $web = Get-SPWeb -Identity  $siteUrl
		  
		               $UpdateWebFeature = $web.Features[$featureConfig.Name]
		  

                  }
		    
                 elseif($featureConfig.Type -eq "Site")
                  {
                    
                    $site = Get-SPSite -Identity  $siteUrl
		  
		            $UpdateWebFeature = $site.Features[$featureConfig.Name]
		  
                  }

		        if($UpdateWebFeature -eq $null )
		        {
			       # Write-Host -f red 'Feature '$featureName' is not activated to '$siteUrl'.'
		        }
		        else
		        {
			        Write-Host -f Yellow 'Deactivating feature '$featureConfig.Name 

			        Disable-SPFeature $featureName -Url $siteUrl -Force -Confirm:$false
			
			        Write-Host -f Green 'Feature '$feature.Name'  has been deactivated successfully to '$siteUrl'.'
		        }


			    Write-Host "Activating feature :" $featureConfig.Name "(" $siteConfiguration.Url ")." -ForegroundColor Green
           
                Enable-SPFeature –identity $featureConfig.Name -URL $featureConfig.Url	-Force		

			    if($Error.Count -eq 0)
			    {
				    Write-Host "Success: Actvated feature :" $featureConfig.Name "(" $featureConfig.Url ")." -ForegroundColor Green
			    }
			    else
			    {
				    Write-Host "Error: Actvating feature :" $featureConfig.Name "(" $featureConfig.Url "). Exiting script..." -foregroundcolor Red
				    Write-Host $Error -ForegroundColor Red
                    
                    $error.clear()
			    }
 		}	
    }

}







if ((Get-PSSnapin "Microsoft.SharePoint.PowerShell" -ErrorAction SilentlyContinue) -eq $null) 
{
    Add-PSSnapin "Microsoft.SharePoint.PowerShell"
}


[xml]$webAppFile = Get-Content Configuration.xml

[bool]$isCreateWebApplication = $false
[bool]$isCreateSiteCollection = $false
[bool]$isCreateSubSites = $false
[bool]$isAddSolutions = $false
[bool]$isInstallSolution = $false
[bool]$isActivateFeature = $false
[bool]$isRetractSolution = $false
[bool]$isUninstallSolution = $false

$error.clear()

$deploymentConfig = $webAppFile.DeploymentConfig


# Get the list for operation to perform
foreach($solutionConfiguration in $deploymentConfig.CreateWebApplication)
{
  $isCreateWebApplication =  [bool]::Parse($solutionConfiguration)
}

foreach($solutionConfiguration in $deploymentConfig.CreateSiteCollection)
{
  $isCreateSiteCollection =  [bool]::Parse($solutionConfiguration)
}

foreach($solutionConfiguration in $deploymentConfig.CreateSubSites)
{
  $isCreateSubSites =  [bool]::Parse($solutionConfiguration)
}

foreach($solutionConfiguration in $deploymentConfig.AddSolutionsPackage)
{
  $isAddSolutions =  [bool]::Parse($solutionConfiguration)
}

foreach($solutionConfiguration in $deploymentConfig.InstallSolutionsPackage)
{
  $isInstallSolution =  [bool]::Parse($solutionConfiguration)
}

foreach($solutionConfiguration in $deploymentConfig.ActivateFeatures)
{
  $isActivateFeature =  [bool]::Parse($solutionConfiguration)
}

foreach($solutionConfiguration in $deploymentConfig.RetractSolutionsPackage)
{
  $isRetractSolution =  [bool]::Parse($solutionConfiguration)
}

foreach($solutionConfiguration in $deploymentConfig.UninstallSolutionsPackage)
{
  $isUninstallSolution =  [bool]::Parse($solutionConfiguration)
}




# create the web application

if($isCreateWebApplication -eq $true)
{
    foreach($webAppConfiguration in $deploymentConfig.WebApplication)
    {

       CreateWebApp -webAppConfiguration $webAppConfiguration
    }
}


if($isCreateSiteCollection -eq $true)
{
  
    foreach($siteCollectionConfiguration in $deploymentConfig.SiteCollection)
    {

      CreateSiteCollection -siteCollectionConfiguration $siteCollectionConfiguration
    }
}

if($isCreateSubSites -eq $true)
{
    foreach($webConfiguration in $deploymentConfig.Sites)
    {
    
       CreateSite -sitesConfiguration $webConfiguration
    }
}


#Retract the solutions
if($isRetractSolution -eq $true)
{
    foreach($solutionConfiguration in $deploymentConfig.Solutions)
    {

       UnInstallSolution -solutionsConfiguration $solutionConfiguration
    }
}


#Uninstall the solutions
if($isUninstallSolution -eq $true)
{
    foreach($solutionConfiguration in $deploymentConfig.Solutions)
    {
    
       RemoveSolution -solutionsConfiguration $solutionConfiguration
    }
}


#Add the solution
if($isAddSolutions -eq $true)
{
    foreach($solutionConfiguration in $deploymentConfig.Solutions)
    {

       AddSolution -solutionsConfiguration $solutionConfiguration
    }
}



if($isInstallSolution -eq $true)
{
    foreach($solutionConfiguration in $deploymentConfig.InstallSolutions)
    {

       InstallSolution -solutionsConfiguration $solutionConfiguration
    }
}


if($isActivateFeature -eq $true)
{
    foreach($featureConfiguration in $deploymentConfig.Features)
    {
        
        EnableFeature -featuresConfig $featureConfiguration
    }
}


Remove-PsSnapin Microsoft.SharePoint.PowerShell