﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AccessRequestForm.aspx.cs" Inherits="Viacom.AdSpace.AppsWeb.Pages.AccessRequestForm" %>

<!doctype html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>:: Access Request ::</title>
    <link href="../styles/CSS/kendo.common.min.css" rel="stylesheet" />
    <!-- Default Kendo UI Web theme CSS -->
    <link href="../styles/CSS/kendo.default.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="../styles/CSS/jquery-ui.css" />
    <link rel="stylesheet" href="../styles/CSS/jquery-ui-1.10.4.custom.css" />
    <link rel="stylesheet" href="../styles/CSS/jquery.dataTables_themeroller.css" />

    <link rel="stylesheet" href="../styles/CSS/ManagePermissions.css" />
    <script type="text/javascript" src="../Scripts/js/jquery.js"></script>
    <script type="text/javascript" src="../Scripts/js/jquery-ui.js"></script>
    <script type="text/javascript" src="../Scripts/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="../Scripts/js/chosen.jquery.js"></script>
    <!-- Kendo UI Web combined JavaScript -->
    <link href="../styles/CSS/Access_request_style.css" rel="stylesheet" type="text/css">
    <script src="../Scripts/js/kendo.web.min.js"></script>
    <script src="../Scripts/js/MicrosoftAjax.js"></script>
    <script src="../Scripts/js/UXScript.js"></script>
    <script src="../Scripts/js/CustomJs/AccessRequestForm.js"></script>
    <link href="../styles/CSS/chosen.css" rel="stylesheet" />
    <link href="http://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(document).ready(function(){
            // SearchText();
            $("#ddRequestForUser").chosen();
            $("#ddCopyFromUser").chosen();
        });

        function SearchText() {
            $("#txtRequestForUser_autocomplete").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "AccessRequestForm.aspx/GetAutoCompleteData",
                        data: "{'userinfo':'" + $("#txtRequestForUser_autocomplete").val() + "'}",
                        dataType: "json",
                        success: function (data) {
                            response(data.d);
                        },
                        error: function (result) {
                            alert("Error");
                        }
                    });
                }
            });

            $("#txtCopyFromUser_autocomplete").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "AccessRequestForm.aspx/GetAutoCompleteData",
                        data: "{'userinfo':'" + $("#txtCopyFromUser_autocomplete").val() + "'}",
                        dataType: "json",
                        success: function (data) {
                            response(data.d);
                        },
                        error: function (result) {
                            alert("Error");
                        }
                    });
                }
            });
        }

        function clear_selection_requestUser()
        {
            $("#txtRequestForUser_autocomplete").val("");
        }

        function clear_selection_copyUser()
        {
            $("#txtCopyFromUser_autocomplete").val("");
        }

    </script>
</head>

<body>
    <form id="form1" runat="server">
        <asp:HiddenField ID="hidsiteUrl" runat="server" />
        <asp:HiddenField ID="hidRoleDefs" runat="server" />
        <div id="mainwrapper">
            <!--Top menu starts-->
            <div id="menu">
                <ul>
                    <li class="active_menu"><a href="" id="AccessRequest">Access Request</a></li>
                    <li><a href="" id="ManageByUser">Manage Permissions by User</a></li>
                    <li><a href="" id="ManageByContent">Manage Permission by Content</a></li>
                    <li><a href="" id="RequestApproval">Request Approval</a></li>
                    <li><a href="" id="MyRequest">My Request</a></li>
                </ul>
            </div>
            <!--Top menu ends-->
            <div id="container">
                <div class=" div_white flt_left">
                    <img src="../images/ico_page.png" class="img_RequestFor">&nbsp;Request access for&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 <asp:DropDownList ID="ddRequestForUser" runat="server"></asp:DropDownList>&nbsp;&nbsp;&nbsp;                
                  <%--<asp:TextBox ID="txtRequestForUser_autocomplete" runat="server" Width="300px" Text="-- Select --"></asp:TextBox>--%>
                    <%--<input type="button" id="btnRequestForUser" name="btnRequestForUser" value="Select User" />&nbsp;&nbsp;
                    <input type="text" id="txtRequestForUser" name="txtRequestForUser">&nbsp;&nbsp;<img src="../images/ico_search.png" class="img_Search">&nbsp;&nbsp;<img src="../images/ico_user_blue.png" class="Img_User">&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/ico_info.png">&nbsp;<span class="note">Access can be given to different user by searching</span>--%>            
                </div>

                <div class="clr div_white">
                    <div id="errorDiv">
                        <label id="lblError" class="err" runat="server"></label>
                    </div>
                </div>

                <%--<div class="div_white_border">--%>
                <div class="div_dark_grey">
                    <input type="radio" id="copyBtn" name="rblRequestAccess" value="Copy Permission" checked="checked" />&nbsp;<span class="spnDetails">Copy access from user</span>&nbsp;&nbsp;
                  <%--  <asp:TextBox ID="txtCopyFromUser_autocomplete" runat="server" Width="300px" Text="-- Select --"></asp:TextBox>--%>
                     <asp:DropDownList ID="ddCopyFromUser" runat="server"></asp:DropDownList>
                    <%--<input type="text" id="txtAccessCopyUser" name="txtAccessCopyUser">&nbsp;&nbsp;<img src="../images/ico_search.png" class="img_Search">&nbsp;&nbsp;<img src="../images/ico_user_blue.png" class="Img_User">&nbsp;&nbsp;--%>                    
                </div>

                <div class="div_dark_grey">
                    <input id="assignBtn" name="rblRequestAccess" type="radio" value="Grant Access" />&nbsp;<span class="spnDetails"> Assign Unique Permissions</span>&nbsp;&nbsp;
                </div>
                <br />
                <div class="div_white_border" style="float: left; width: inherit">
                    <div id="SectionUniquePermissions">
                        <!--left files structure here-->
                        <div id="file_structure">
                            <div id="sectionListDiv">
                                <asp:Table ID="tbSectionList" runat="server" CssClass="hide">
                                </asp:Table>
                                <ul id="sectionTreeView" class="access_tree_scroll hide" data-tree-loaded="false">
                                </ul>
                                <%--<table id="tbSectionList" style="border:1px solid black;"></table>--%>
                            </div>
                            <div id="treeViewLoading">
                                <img id="imgTreeViewLoading" src="../images/ajaxloader.gif" />
                            </div>
                        </div>
                        <!--right access list is here-->
                        <div id="access_structure">
                            <div class="clr"></div>
                            <div class="clr"></div>
                            <div class="clr"></div>
                            <div id="sectionWithAccessRights" class="access_rights_scroll">
                                <table id="tbSectionItemsList" class="display hide">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Type</th>
                                            <th>Title</th>
                                            <th>Access</th>
                                            <th>Permission</th>
                                            <th>Comments</th>
                                        </tr>
                                        <tr class="tr_parent_row">
                                            <td id="parentCheckBox"></td>
                                            <td id="parentTypeCell"></td>
                                            <td id="parentComponentCell"></td>
                                            <td id="parentInheritanceCell"></td>
                                            <td id="parentPermissionCell" class="left"></td>
                                            <td id="parentCommentCell" class="left"></td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            <div id="sectionItemDetailsLoading">
                                <img id="imgsectionItemLoading" src="../images/ajaxloader.gif" />
                            </div>
                            <div class="clr"></div>
                            <div class="clr"></div>
                            <div id="errorMessage"></div>
                            <div class="flt_left align_center hide" id="Btn_Operation">
                                <input type="button" id="ARUVSaveBtn" runat="server" value="Save" class="button_primary">&nbsp;
                                <input type="button" id="ARUVClearBtn" runat="server" value="Clear" class="button_secondary">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clr div_white"></div>
                <div id="User_Detail"></div>
                <div>
                    <div id="RequestedAccessItemsOuterDiv" class="div_white_border">
                        <div id="div_RequestedAccessItems" class="block">
                            <table id="tbRequestedAccessItems" class="display">
                                <thead>
                                    <tr>
                                        <%--   <th>ActionsType</th>
                                        <th>SourceUser</th>--%>
                                        <th>Type</th>
                                        <th>WebSite Title</th>
                                        <th>List Name</th>
                                        <th>Item Name</th>
                                        <th>Permission Level</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>

                        </div>
                    </div>
                    <div class="clr"></div>
                    <br />
                    <div class="block" id="div_SubmitBtn">
                        <div class="div_blue">
                            <input type='checkbox' id='chNotifyUser' runat='server' />&nbsp;&nbsp;Notify User&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='checkbox' id='chNotifyCreater' runat='server' />&nbsp;&nbsp;&nbsp;Notify Creator&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                        </div>
                        <div class="clr1"></div>
                        <div class="div_grey" id="GenericUserCommentDiv">
                            <label id="lblUserComments">&nbsp;&nbsp;User Comments&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</label>
                            <textarea id="txtUserComments" runat="server" cols="20" name="S1" rows="1"></textarea>
                        </div>
                        <div class="clr"></div>
                        <br />
                        &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;<input type="button" id="btnSubmitRequest" runat="server" value="Submit Request" class="button_primary" style="display: none">
                        <div class="clr"></div>
                        <div id="errorUser"></div>
                        <div id="successMessage"></div>
                        <br />
                    </div>

                </div>
                <div class="clr"></div>
            </div>

            <div id="loadingUnderlay" class="AccessRequestUnderlay">
                <div class="AccessRequestLightBox" id="loadingDiv">
                    <img id="imgLoader" src="../images/ajaxloader2.gif" /><br />
                </div>
            </div>
            <div id="loadingUnderlay" class="AccessRequestSubmitUnderlay">
                <div class="AccessRequestSubmitLightBox" id="loadingDiv">
                    <img id="imgLoader" src="../images/ajaxloader2.gif" /><br />
                    <label id="lblAccessRequestUpdatingMessage" style="text-align: center">Submitting...</label>
                </div>
            </div>
        </div>
    </form>
</body>
</html>

