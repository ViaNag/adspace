﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManagePermissionForm.aspx.cs" Inherits="Viacom.AdSpace.AppsWeb.Pages.ManagePermissionForm" %>

<!doctype html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>:: Audit Access ::</title>
    <link rel="stylesheet" href="../styles/CSS/jquery-ui.css" />
    <link rel="stylesheet" href="../styles/CSS/jquery-ui-1.10.4.custom.css" />
    <link rel="stylesheet" href="../styles/CSS/jquery.dataTables_themeroller.css" />
    <!-- Common Kendo UI Web CSS -->
    <link href="../styles/CSS/kendo.common.min.css" rel="stylesheet" />
    <!-- Default Kendo UI Web theme CSS -->
    <link href="../styles/CSS/kendo.default.min.css" rel="stylesheet" />
    <link href="http://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css" />
    <script src="../Scripts/js/jquery.js" type="text/javascript"></script>
    <script src="../Scripts/js/jquery-ui.js"></script>
    <script src="../Scripts/js/jquery.dataTables.js" type="text/javascript"></script>
   
    <!-- Kendo UI Web combined JavaScript -->
    <script src="../Scripts/js/kendo.web.min.js"></script>
    <link rel="stylesheet" href="../styles/CSS/ManagePermissions.css" />
    <link rel="stylesheet" href="../styles/CSS/Access_request_style.css" type="text/css">
    <script src="../Scripts/js/MicrosoftAjax.js"></script>
    <script src="../Scripts/js/UXScript.js"></script>
    <script src="../Scripts/js/CustomJs/ManagePermissionsForm.js"></script>
</head>

<body>
    <form id="form1" runat="server">
        <div id="mainwrapper">
            <asp:HiddenField ID="hidRoleDefs" runat="server" />
            <asp:HiddenField ID="hidSiteURL" runat="server" />
            <asp:HiddenField ID="hidListID" runat="server" />
            <asp:HiddenField ID="hidItemID" runat="server" />
            <asp:HiddenField ID="hidType" runat="server" />

            <!--Top menu starts-->
            <div id="menu">
                <ul>

                    <li><a href="" id="AccessRequest">Access Request</a></li>
                    <li><a href="" id="ManageByUser">Manage Permissions by User</a></li>
                    <li class="active_menu"><a href="" id="ManageByContent">Manage Permission by Content</a></li>
                    <li><a href="" id="RequestApproval">Request Approval</a></li>
                    <li><a href="" id="MyRequest">My Request</a></li>

                </ul>
            </div>
            <!--Top menu ends-->
            <div id="container">
                <div class="clr div_white"></div>
                <div id="errorDiv" class="main-section hide">
                    <label id="lblMsg" runat="server"></label>
                    <label id="lblError" class="err" runat="server"></label>
                </div>
                <%--<div class="div_white_border ">--%>
                <div class="clr div_white"></div>
                <div class="clr div_white"></div>
                <div class="clr div_white"></div>
                <div class="div_ManagePermission" id="ManagePermissionTabs">
                    <%--</div>--%>
                    <div class="div_white_border">
                        <!--left files structure here-->
                        <div id="file_structure" class="flt-lft">
                            <div id="sectionTreeDiv">
                                <ul id="sectionTree" class="access_tree_scroll hide"></ul>
                            </div>
                            <div id="treeViewLoading">
                                <img id="imgTreeViewLoading" src="../images/ajaxloader.gif" />
                            </div>
                        </div>
                        <!--right access list is here-->
                        <div id="access_structure" class="flt_right" style="height: 400px;">
                            <div class="clr"></div>
                            <div class="clr"></div>
                            <div class="clr"></div>

                            <div id="sectionWithAccessRights" class="access_rights_scroll">
                                <table id="tbSectionItemDetails" class="display">
                                    <thead>
                                        <tr>
                                            <th>Type</th>
                                            <th>Title</th>
                                            <th>Access</th>
                                            <th>Action</th>
                                        </tr>
                                        <tr class="tr_parent_row">
                                            <td id="parentTypeCell"></td>
                                            <td id="parentTitleCell"></td>
                                            <td id="parentInheritanceCell" class="center"></td>
                                            <td id="parentActionCell" class="center"></td>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                            <div id="sectionItemDetailsLoading">
                                <img id="imgsectionItemLoading" src="../images/ajaxloader.gif" />
                            </div>
                            <div class="clr"></div>

                            <div class="clr"></div>
                        </div>
                        <div class="clr div_white"></div>
                    </div>
                </div>
            </div>
        </div>

        <div id="ViewPermissions" title="View Permissions">
            <hr />
            <div id="ItemDetailsViewInformations">
                <div class="div_blue" id="ViewSiteName">
                    <asp:Label ID="lblViewSiteName" CssClass="div_siteName" runat="server">Site Name :</asp:Label>
                    <asp:Label ID="lblViewSiteNameResult" runat="server"></asp:Label>
                </div>
                <div class="div_grey" id="ViewListName">
                    <asp:Label ID="lblViewListName" CssClass="div_ListName" runat="server">List Name :</asp:Label>
                    <asp:Label ID="lblViewListNameResult" runat="server"></asp:Label>
                </div>
                <div class="div_blue" id="ViewItemName">
                    <asp:Label ID="lblViewItemName" CssClass="div_ItemName" runat="server">Item Name :</asp:Label>
                    <asp:Label ID="lblViewItemNameResult" runat="server"></asp:Label>
                </div>
            </div>
            <br />
            <div id="ViewItemPermission">
                <table id="tblViewItemPermission" class="display">
                    <thead>
                        <tr>
                            <th>User/Group</th>
                            <th>Type</th>
                            <th>Permission</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            <div id="ViewPermissionLoading">
                <img id="imgViewPermission" src="../images/ajaxloader.gif" />
            </div>
        </div>
        <div id="EditPermissions" title="Edit Permissions">
            <hr />
            <div id="ItemDetailsEditInformations">
                <div class="div_blue" id="EditSiteName">
                    <asp:Label ID="lblEditSiteName" CssClass="div_siteName" runat="server">Site Name :</asp:Label>
                    <asp:Label ID="lblEditSiteNameResult" runat="server"></asp:Label>
                </div>
                <div class="div_grey " id="EditListName">
                    <asp:Label ID="lblEditListName" CssClass="div_ListName" runat="server">List Name :</asp:Label>
                    <asp:Label ID="lblEditListNameResult" runat="server"></asp:Label>
                </div>
                <div class="div_blue " id="EditItemName">
                    <asp:Label ID="lblEditItemName" CssClass="div_ItemName" runat="server">Item Name :</asp:Label>
                    <asp:Label ID="lblEditItemNameResult" runat="server"></asp:Label>
                </div>
            </div>

            <br />
            <br />
            &nbsp;
            
            <div id="EditItemPermission">
                <table id="tblEditItemPermission" class="display">
                    <thead>
                        <tr>
                            <th>User/Group</th>
                            <th>Type</th>
                            <th>Permission</th>
                            <th>New Permission</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            <div id="ErrorDetailsInformation"></div>
            <div id="EditPermissionLoading">
                <img id="imgeditPermission" src="../images/ajaxloader.gif" /><br />
            </div>
            <div id="UpdatePermissionLoading">
                <img id="imgeditUpdatePermission" src="../images/ajaxloader.gif" /><br />
                <span id="updatingMessage">Submitting...</span>
            </div>
            <br />
        </div>

        <div id="loadingUnderlay" class="ManagePermissionUnderlay">
            <div class="ManagePermissionLightBox" id="loadingDiv">
                <img id="imgLoader" src="../images/ajaxloader2.gif" /><br />
            </div>
        </div>
    </form>

</body>
</html>
