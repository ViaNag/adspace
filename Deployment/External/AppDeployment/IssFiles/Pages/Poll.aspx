﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Poll.aspx.cs" Inherits="Viacom.AdSpace.AppsWeb.Pages.Poll" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="../Scripts/js/iframeResizer.contentWindow.min.js"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="../Scripts/js/jquery.js"></script>
    <script type="text/javascript" src="../Scripts/js/CustomJs/Poll.js"></script>
    <link href="../styles/CSS/Poll.css" rel="stylesheet" />
   <link href="http://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css" />
</head>

<body>
    <form id="frmPoll" runat="server">
        <div id="LeftPoll">
            <div id="CloseButton" class="hidden"><a class="aCloseBtn"  href="javascript:void(0)" onclick="document.getElementById('chart_div').style.display='none';document.getElementById('CloseButton').style.display='none';document.getElementById('pollContainer').style.display = 'inline';">
                <img class="imgClose" height="10" width="10"  src="../Images/CrossGreyBckG.png" /></a></div>
            <div id="pollContainer" >
                <div class="fullWidth">
                    <div>
                        <div id="Question"></div>
                    </div>
                    <div>
                        <div id="Answers">
                        </div>
                    </div>
                    <div>
                        <div id="Vote">
                        </div>
                    </div>
                    <div>
                        <div id="ShowResult">
                        </div>
                    </div>
                    <div>
                        <div>
                            <div id="Previous">
                                Loading..
                            </div>
                            <div id="Next">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="chart_div"></div>
        </div>
    </form>
</body>

</html>
