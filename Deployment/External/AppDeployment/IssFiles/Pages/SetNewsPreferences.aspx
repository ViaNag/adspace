﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SetNewsPreferences.aspx.cs" Inherits="Viacom.AdSpace.AppsWeb.SetNewsPreferences" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <script type="text/javascript" src="../Scripts/js/jquery.js"></script>
     <script src="../Scripts/js/iframeResizer.contentWindow.min.js"></script>
    <script src="../Scripts/js/CustomJs/News.js"></script>
    <script src="../Scripts/js/jquery.multi-select.js"></script>
    <link href="../styles/CSS/multi-select.css" rel="stylesheet" />
    <link href="../styles/CSS/News.css" rel="stylesheet" />
    <link href="http://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="frmNewsPreferences" runat="server">
    <div>
    <div id="dvMessage"  style="display:none">Your Preferences are Saved !</div>
<table class="fullWidth"  cellspacing="0" cellpadding="3" id="tblPreferences">
	
	<tr><td style='width:20%;' class="leftsection">External News : </td>
		<td style='width:80%;' class="rightsection"><select name="drpExternalNews" multiple="multiple" size="10"></select></td></tr>
		
	<tr><td style='width:20%;' class="leftsection">Viacom News : </td>
		<td style='width:80%;' class="rightsection"><select name="drpViacomNews" multiple="multiple" size="10"></select></td></tr>
	<tr>
		<td style='width:20%;'></td>
		<td class="btnContainer"  >
		<button class="button" onclick="javascript:News.Save();return false;"> Save</button>
	<button class="button" onclick="javascript:News.Cancel();return false;"> Cancel</button>
		</td>
	</tr>
		
</table>

    </div>
    </form>
</body>
</html>
