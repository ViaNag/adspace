if ((Get-PSSnapin "Microsoft.SharePoint.PowerShell" -ErrorAction SilentlyContinue) -eq $null) {
    Add-PSSnapin "Microsoft.SharePoint.PowerShell"
}

# Traverses all files in a folder and publish
function GetFiles($Folder,$list)
{ 
   Write-Host "+"$Folder.Name

    foreach($listitemfile in $Folder.files)
	{	
		try
		{
			Write-Host "`t" $listitemfile.Name
				$listitemfile.publish("");
		}
		catch
		{
		}
    
	 }
	 #Loop through all subfolders and call the function recursively
     foreach ($SubFolder in $Folder.SubFolders)
        {
		    if($SubFolder.Name -ne "Forms")
		    {  
			    Write-Host "`t" -NoNewline
				GetFiles($Subfolder,$list)
				 
			}
		}
 }

 # gets the list from site
function approveContent ($w, $listName,$folderName ) {
  $list = $w.Lists |? {$_.Title -eq $listName}
  if($folderName -eq $null -or $folderName -eq "")
  {
    $Folder = $list.RootFolder
  }
  else
  {
    $Folder = $list.RootFolder.SubFolders[$folderName]
  }
    
    GetFiles $Folder $list
 
}
 

# reads the configuration xml for publishing file
function PublishFile([String]$ConfigFileName = "")
{
	Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
	[string]$xmlpath = $ConfigFileName
	 
	$publishXML =  [xml](Get-Content ($xmlpath))
	if( $? -eq $false ) 
	{
		LogError "Could not read config file. Exiting ..."
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	}
	Write-Host "Success: Reading Configuration file: Process Completed ..." -ForegroundColor Green 
	   try
		{
		$siteurl=$publishXML.Publish.Site
		$site=  Get-SPSite -Identity $siteurl
		$web= $site.RootWeb 

		$publishXML.Publish.List|
		ForEach-Object {
		$ListName= $_.Name
		$FolderName=$_.Folder
		approveContent $web $ListName $FolderName

		}

		Write-Host $ListName  "Files Published Succesully" -ForegroundColor Green 
		}
		catch
		{
		Write-Host "Exception while creating groups. Exiting script..." -ForegroundColor Red        
		Stop-Transcript
		Stop-SPAssignment -Global		
		Exit -1
		}

}



function Import-SPDesignPackage {
    [CmdLetBinding(DefaultParameterSetName="Default")]
    param(
        [parameter(Mandatory=$true, Position=0, ParameterSetName="Default", ValueFromPipeline=$true, ValueFromPipelineByPropertyName=$true)]
        [string]
        $SiteUrl="",

        [parameter(Mandatory=$true, Position=0, ParameterSetName="Site", ValueFromPipeline=$true, ValueFromPipelineByPropertyName=$true)]
        [Microsoft.SharePoint.SPSite]
        $Site=$null,

        [parameter(Mandatory=$true, Position=1, ParameterSetName="Default", ValueFromPipeline=$true, ValueFromPipelineByPropertyName=$true)]
        [parameter(Mandatory=$true, Position=1, ParameterSetName="Site", ValueFromPipeline=$true, ValueFromPipelineByPropertyName=$true)]
        [string]
        $ImportFileName = "",

        [parameter(Mandatory=$true, Position=2, ParameterSetName="Default", ValueFromPipeline=$true, ValueFromPipelineByPropertyName=$true)]
        [parameter(Mandatory=$true, Position=2, ParameterSetName="Site", ValueFromPipeline=$true, ValueFromPipelineByPropertyName=$true)]
        [bool]
        $Apply = $false,

        [parameter(Mandatory=$false, Position=3, ParameterSetName="Default", ValueFromPipeline=$false, ValueFromPipelineByPropertyName=$true)]
        [parameter(Mandatory=$false, Position=3, ParameterSetName="Site", ValueFromPipeline=$false, ValueFromPipelineByPropertyName=$true)]
        [string]
        $PackageName = "",


        [parameter(Mandatory=$false, ParameterSetName="Default", ValueFromPipeline=$false, ValueFromPipelineByPropertyName=$true)]
        [parameter(Mandatory=$false, ParameterSetName="Site", ValueFromPipeline=$false, ValueFromPipelineByPropertyName=$true)]
        [int]
        $MajorVersion = 1,

        [parameter(Mandatory=$false, ParameterSetName="Default", ValueFromPipeline=$false, ValueFromPipelineByPropertyName=$true)]
        [parameter(Mandatory=$false, ParameterSetName="Site", ValueFromPipeline=$false, ValueFromPipelineByPropertyName=$true)]
        [int]
        $MinorVersion = 0
    )

    begin {
        [System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SharePoint.Publishing") | Out-Null
        [System.Reflection.Assembly]::LoadWithPartialName("System.Net") | Out-Null
    }

    process {
        $localSite = $null
        $localUrl = ""

        if( $PSCmdlet.ParameterSetName -like "Default" ) {
            $localSite = Get-SPSite $SiteUrl -ErrorAction 0
            $localUrl = $SiteUrl
        } else {
            $localSite = $site
            $localUrl = $site.Url
        }
        
        $resultObject = new-object System.Management.Automation.PSObject
        $resultObject | Add-Member -MemberType NoteProperty -Name "SiteUrl" -Value $localUrl -Force
        $resultObject | Add-Member -MemberType NoteProperty -Name "Success" -Value $false -Force

        try {
            if( !(Test-Path $ImportFileName) ) {
                $resultObject | Add-Member -MemberType NoteProperty -Name "InputFileFound" -Value $false -Force
                return
            } else {
                $resultObject | Add-Member -MemberType NoteProperty -Name "InputFileFound" -Value $true -Force
            }

            if( [System.IO.Path]::GetExtension($ImportFileName) -ne ".wsp" ) {
                $resultObject | Add-Member -MemberType NoteProperty -Name "InputFileExtensionValid" -Value $false -Force
                return
            } else {
                $resultObject | Add-Member -MemberType NoteProperty -Name "InputFileExtensionValid" -Value $true -Force
            }

            if($localSite -ne $null ) {
                $resultObject | Add-Member -MemberType NoteProperty -Name "SiteFound" -Value $true -Force

                $file = [System.IO.Path]::GetFileName($ImportFileName)

                if( [string]::IsNullOrEmpty($PackageName) ) {
                    $PackageName = [System.IO.Path]::GetFileNameWithoutExtension($ImportFileName)
                }

                $solutionFileName = "{0}-v{1}.{2}.wsp" -f ($PackageName, $MajorVersion, $MinorVersion)

                $webFile = $null
                $webFile = $localSite.RootWeb.GetFile("_catalogs/solutions/" + $solutionFileName)

                if( $webFile -ne $null -and $webFile.Exists ) {
                    $resultObject | Add-Member -MemberType NoteProperty -Name "PackageAlreadyExists" -Value $true -Force
                    return
                } else {
                    $resultObject | Add-Member -MemberType NoteProperty -Name "PackageAlreadyExists" -Value $false -Force
                }
                
                $package = new-object Microsoft.SharePoint.Publishing.DesignPackageInfo($file, [Guid]::Empty, $MajorVersion, $MinorVersion)

                $spfolder = $null
                $inputStream = $null

                $installDone = $false

                try {
                    $spfolder = $localSite.RootWeb.RootFolder.SubFolders.Add("tmp_importspdesignpackage_15494B80-89A0-44FF-BA6C-208CB6A053D0")
                    
                    $inputStream = [System.IO.File]::OpenRead($ImportFileName)
                    
                    $spfile = $spfolder.Files.Add($file, $inputStream, $true)
                    
                    $inputStream.Close()
                    $inputStream = $null

                    [Microsoft.SharePoint.Publishing.DesignPackage]::Install($localSite, $package, $spfile.Url)
                    $resultObject | Add-Member -MemberType NoteProperty -Name "InstallError" -Value [System.Exception]$null -Force
                    $installDone = $true
                } catch {
                    $resultObject | Add-Member -MemberType NoteProperty -Name "InstallError" -Value $_.Exception -Force
                } finally {
                    if( $spfolder -ne $null ) { $spfolder.Delete() }
                    if( $inputStream -ne $null ) { $inputStream.Close() }
                }
                
                if( $installDone ) {
                    if( $Apply ) {
                        try {
                            [Microsoft.SharePoint.Publishing.DesignPackage]::Apply($localSite, $package)
                            $resultObject | Add-Member -MemberType NoteProperty -Name "ApplyError" -Value [System.Exception]$null -Force
                            $resultObject.Success = $true
                        } catch {
                            $resultObject | Add-Member -MemberType NoteProperty -Name "ApplyError" -Value $_.Exception -Force
                        }
                    } else {
                        $resultObject.Success = $true
                    }
                }
            } else {
                $resultObject | Add-Member -MemberType NoteProperty -Name "SiteFound" -Value $false -Force
            }

        } finally {
            if( $PSCmdlet.ParameterSetName -like "Default*" ) {
                if( $localSite -ne $null ) {
                    $localSite.Dispose()
                }
            } else {
                if( $disposeSiteObject -eq $true -and $site -ne $null -and $site -is [Microsoft.SharePoint.SPSite] ) {
                    $site.Dispose()
                }
            }
            $resultObject
        }
    }

    end {}
}

function New-ObjectFromHashtable {
    [CmdletBinding()]
    param(
        [parameter(Mandatory=$true, Position=1, ValueFromPipeline=$true)]
        [Hashtable]
        $Hashtable
    )

    begin {
        $results = @()
    }

    process {
        $r = new-object System.Management.Automation.PSObject
        $Hashtable.Keys | % {
            $key = $_
            $value = $Hashtable[$key]
            $r | Add-Member -MemberType NoteProperty -Name $key -Value $value -Force
        }

        $results += $r
    }

    end {
        $results
    }

}

# ===================================================================================
# FUNC: ApplyDesignpackages
# DESC: Applies Design packages
# ===================================================================================
function ApplyDesignpackages([String]$ConfigFileName = "")
{

	# Check that the config file exists.
	if (-not $(Test-Path -Path $configFileName -Type Leaf))
	{
		Write-Error -message ("Configuration file '" + $configFileName + "' does not exist.") -Red
	}

	$configXml = [xml]$(get-content $configFileName)
	if( $? -eq $false ) 
	{
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red`
	}

	if ($configXml.designPackages)
	{
		foreach ($design in $configXml.designPackages.designPackage)
		{
			try
			{
				Write-Host "Applying Design Package " $design.ImportFileName " on " $design.siteUrl -ForegroundColor Yellow

				[string] $solutionFileName = Get-Location
				$solutionFileName = $solutionFileName + "\"+ $design.ImportFileName
				Import-SPDesignPackage -SiteUrl $design.siteUrl -ImportFileName $solutionFileName -PackageName $design.PackageName -Apply $true	

				if($Error.Count -gt 0)
				{
					Write-Host "Error Applying Design Package. Possible cause : Design package is already applied" -ForegroundColor Red
					$Error.Clear()	
				}
				else
				{
					Write-Host "Design Package applied" -ForegroundColor Green
				}
			}
			catch
			{
				Write-Host "Exception while Applying Design package." -ForegroundColor Red
			}
		}
	}
}

[string] $currentLocation = Get-Location

[string] $parameterfile = $currentLocation + "\DesignPackages.xml"

[string] $parameterfileForPublish = $currentLocation + "\PublishFile.xml"



ApplyDesignpackages $parameterfile

PublishFile $parameterfileForPublish
