Add-PSSnapin Microsoft.SharePoint.Powershell -ErrorAction "SilentlyContinue"

Write-host "Enter Site Collection URL"
$input =  Read-Host

$web = Get-SPWeb $input
$web.SiteLogoUrl = $web.Url + "/SiteAssets/images/AdSpace_logo_color_RGB.png"
$web.SiteLogoDescription = "Adspace logo"

$folder = $web.RootFolder
$folder.WelcomePage = "Pages/Home.aspx"

$folder.update();

$web.Update();