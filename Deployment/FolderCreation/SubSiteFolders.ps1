Add-PSSnapin Microsoft.SharePoint.Powershell

function Create-SPFoldersBulk
{
    
    Param (
           [parameter(Mandatory=$true)][string]$SiteUrl,
           [parameter(Mandatory=$true)][string]$SubSiteUrl,
           [parameter(Mandatory=$true)][string]$CSVPath
           )
 
    # Import CSV
    [array]$FolderList = Import-Csv $CSVPath
 
    # Script Variables
    $spWeb = Get-SPWeb $SiteUrl
    $navigationList = $spWeb.Lists["Navigation"]
	$subSiteWeb = Get-SPWeb $SubSiteUrl
	
    Foreach ($folderGroup in $FolderList) {
 
		$spList = $subSiteWeb.Lists[$folderGroup.LibraryName]
        
        <#
        $headerItems = $navigationList.Items | ?{$_.ContentType.Name -eq "Navigation Header" -and $_["HeaderTitle"] -eq $subSiteWeb.Title}
        
        if($headerItems.Count -lt 1)
        {
            $newHeaderItem = $navigationList.Items.Add()
 
            #Add properties to this list item
            $newHeaderItem["HeaderTitle"] = $subSiteWeb.Title

            $ofldurl= new-object Microsoft.SharePoint.SPFieldUrlValue($newHeaderItem["HeaderUrl"])
            $ofldurl.URL = $subSiteWeb.URL
            $subSiteUrl = $subSiteWeb.URL
            $subSitUrlDesc = $folderGroup.SiteName
			$ofldurl.Description = $folderGroup.SiteName

			$newHeaderItem["HeaderUrl"] = "$subSiteUrl , $subSitUrlDesc"
			
           
            $newHeaderItem["ContentType"] = "Navigation Header"
            #Update the object so it gets saved to the list
            $newHeaderItem.Update()
        }

        $categoryItems = $navigationList.Items | ?{$_.ContentType.Name -eq "Navigation Category" -and $_["CategoryTitle"] -eq $folderGroup.LibraryLinkName}
        
        if($categoryItems.Count -lt 1)
        {
            $newCategoryItem = $navigationList.Items.Add()
 
            #Add newCategoryItem to this list item
            $newCategoryItem["CategoryTitle"] = $folderGroup.LibraryLinkName
            $newCategoryItem["OrderCategories"] = $folderGroup.OrderCategories
            $listUrl = $spList.RootFolder.ServerRelativeUrl
            $listDesc = $spList.Title
            $newCategoryItem["LinkCategoryUrl"] = "$listUrl , $listDesc"

            $headerLookupItem = $navigationList.Items | where {$_["HeaderTitle"]  -eq $subSiteWeb.Title}
            $headerLookupValue = New-Object Microsoft.SharePoint.SPFieldLookupValue($headerLookupItem.ID,$headerLookupItem.ID.ToString());
            $newCategoryItem["LinkHeader"] = $headerLookupValue
            $newCategoryItem["ContentType"] = "Navigation Category"
            #Update the object so it gets saved to the list
            $newCategoryItem.Update()
        }
        #>

		<#
        # Create Parent Folder
        $parentFolder = $spList.Folders.Add("",[Microsoft.SharePoint.SPFileSystemObjectType]::Folder,$folderGroup.Root)
        $parentFolder.Update()
        Write-Host "The Parent folder" $folderGroup.Root "was successfully created" -foregroundcolor Green
		#>
		
 
        # Loop variables
        $i = 1
        $col = "Level"
        $colID = $col + $i
 
        if($folderGroup.$colID -ne $null)
        {
            
		    Create-SPFolder $folderGroup $folderGroup.$colID "" 1 $spWeb $spList $folderGroup.LibraryLinkName $navigationList $subSiteWeb
        }
        
    }
    $spWeb.Dispose()
    $subSiteWeb.Dispose()
}

function Create-SPFolder([object]$folderGroup,[string]$folderName,[string]$parentFolderRelativeUrl,[object]$level,[object]$spWeb,[object]$splist,[string]$LibraryLinkName,[object]$navigationList,[object]$subSiteWeb)
{
        if($level -eq 1)
        {
            $folderUrl = $splist.RootFolder.ServerRelativeUrl + "/$folderName"
        }
        else
        {
            $folderUrl = $parentFolderRelativeUrl + "/$folderName"
        }

        $targetFolder = $spWeb.GetFolder($folderUrl)

        
		if($targetFolder.Exists -eq $false)
		{
			$newSubfolder = $splist.Folders.Add($parentFolderRelativeUrl, [Microsoft.SharePoint.SPFileSystemObjectType]::Folder,$folderName)
				$newSubfolder.Update()
            $parentFolderTitle = $newSubfolder.DisplayName
            $parentFolderUrl = $newSubfolder.Folder.ServerRelativeUrl
				Write-Host "The subfolder" $folderGroup.$ColID "was successfully created" -foregroundcolor Cyan
            
            <#
            if($level -eq 1)
            {
                $linkItems = $navigationList.Items | ?{$_.ContentType.Name -eq "Navigation Link" -and $_["LinkName"] -eq $folderGroup.TopLevelFolder}
                
                if($linkItems.Count -lt 1)
                {
                    $newLinkItem = $navigationList.Items.Add()
 
                    #Add newCategoryItem to this list item
                    $newLinkItem["LinkName"] = $folderGroup.TopLevelFolder

                    $topFolderUrl = $parentFolderUrl
                    $topFolderDesc = $parentFolderTitle
                    $newLinkItem["LinkUrl"] = "$topFolderUrl , $topFolderDesc"

                    $everyOneUser = $spWeb.AllUsers["c:0(.s|true"];
                    $everyOneUserValue = New-Object Microsoft.SharePoint.SPFieldUserValue($spWeb, $everyOneUser.Id, $everyOneUser.DisplayName)
                    $newLinkItem["AccessTo"] = $everyOneUserValue;             
                    $headerLookupItem = $navigationList.Items | where {$_["HeaderTitle"]  -eq $subSiteWeb.Title}
                    $headerLookupValue = New-Object Microsoft.SharePoint.SPFieldLookupValue($headerLookupItem.ID,$headerLookupItem.ID.ToString());
                    $newLinkItem["LinkHeader"] = $headerLookupValue

                    $categoryLookupItem = $navigationList.Items | where {$_["CategoryTitle"]  -eq $LibraryLinkName}
                    $categoryLookupValue = New-Object Microsoft.SharePoint.SPFieldLookupValue($categoryLookupItem.ID,$categoryLookupItem.ID.ToString());
                    $newLinkItem["LinkCategory"] = $categoryLookupValue

                    $newLinkItem["ContentType"] = "Navigation Link"
                    #Update the object so it gets saved to the list
                    $newLinkItem.Update()
                }
            }
            #>
		}
        else
        {
            $parentFolderUrl = $targetFolder.ServerRelativeUrl
        }
			
		$nextLevel = $level + 1
        $col = "Level"
        $colID = $col + $nextLevel
		
        if($folderGroup.$colID -ne $null)
		{
            Create-SPFolder $folderGroup $folderGroup.$colID "$parentFolderUrl" $nextLevel $spWeb $spList "" $navigationList $subSiteWeb
		    
		}
		
}

[string] $currentLocation = Get-Location

[string] $configFileName = $currentLocation + "\SubSiteFolders.xml"

# Check that the config file exists.
if (-not $(Test-Path -Path $configFileName -Type Leaf))
{
	Write-Error -message ("Configuration file '" + $configFileName + "' does not exist.") -Red
}

$configXml = [xml]$(get-content $configFileName)
if( $? -eq $false ) 
{
	Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red`
}

if ($configXml.SubSiteFolders)
{
	foreach ($SubSite in $configXml.SubSiteFolders.Site)
	{
		Write-Host "Creating folders on " + $SubSite.SubSiteUrl + " with " + $SubSite.CSVFile + "CSV file" -ForegroundColor Yellow

            $csvFullPath = $currentLocation + "\"+ $SubSite.CSVFile
            Create-SPFoldersBulk -SiteUrl $SubSite.SiteUrl -SubSiteUrl $SubSite.SubSiteUrl -CSVPath $csvFullPath

            Write-Host "Successfully Created folders on " + $SubSite.SubSiteUrl + " with " + $SubSite.CSVFile + "CSV file" -ForegroundColor Green
        
		
    }
}
