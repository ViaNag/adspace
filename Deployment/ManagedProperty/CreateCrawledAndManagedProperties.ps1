#script reads from xml file to add crawled properties and managed properties
#add sharepoint cmdlets
if ( (Get-PSSnapin -Name Microsoft.SharePoint.PowerShell -ErrorAction SilentlyContinue) -eq $null )
{    
      Add-PsSnapin Microsoft.SharePoint.PowerShell
}

$currentDir=Convert-Path (Get-Location -PSProvider FileSystem)
$currentdatetime = get-date -format "yyyy-MM-d.hh-mm-ss"
$logname="Viacom_AdSpace_Search_CreateCrawledAndManagedProperties_"+$currentdatetime+".log"
$wspLogFile=$currentDir + "\" + $logname
Start-Transcript $wspLogFile

Write-Host "Before executing the script please ensure that Search Service Application is already created, running and name is correct in CreateCrawledAndManagedProperties.xml file."

#get the XML filea
[System.Xml.XmlDocument] $XmlDoc = new-object System.Xml.XmlDocument

#connect to the function library for this script
$FilePath ="CreateCrawledAndManagedProperties.xml"

$file = resolve-path($FilePath)
if (!$file)
{
        Write-Host "Could not find the configuration file specified. Aborting." -ForegroundColor red
         write-host "Press any key to continue"
$null=$Host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
        Break
}
write-host "Parsing file: " $file
$XmlDoc = [xml](Get-Content $file)
#get the node containing the name of the search service application where you want to add properties
$sa = $XmlDoc.SearchProperties.SSAName
$searchapp = Get-SPEnterpriseSearchServiceApplication $sa

#set enable for scoping true for manage property Content Type
Set-SPEnterpriseSearchMetadataManagedProperty -SearchApplication $searchapp -Identity ContentType -EnabledForScoping $true


#loop through crawled properties to check or add -- don't add if it already exists
$CrawledPropNodeList = $XmlDoc.SearchProperties.CrawledProperties
foreach ($CrawledPropNode in $CrawledPropNodeList.CrawledProperty)
{
    $SPCrawlProp = $CrawledPropNode.Name
    $SPCrawlPropType = $CrawledPropNode.Type
    
    #Create Crawled Property if it doesn't exist
    if (!(Get-SPEnterpriseSearchMetadataCrawledProperty -SearchApplication $searchapp -Name $SPCrawlProp -ea "silentlycontinue"))
    {
        switch ($SPCrawlPropType)
        {
        "Text" {$crawlprop = New-SPEnterpriseSearchMetadataCrawledProperty -SearchApplication $searchapp -Category SharePoint -VariantType 31 -Name $SPCrawlProp -IsNameEnum $false -PropSet "00130329-0000-0130-c000-000000131346"}
        "Integer" {$crawlprop = New-SPEnterpriseSearchMetadataCrawledProperty -SearchApplication $searchapp -Category SharePoint -VariantType 20 -Name $SPCrawlProp -IsNameEnum $false -PropSet "00130329-0000-0130-c000-000000131346"}  
        "Decimal" {$crawlprop = New-SPEnterpriseSearchMetadataCrawledProperty -SearchApplication $searchapp -Category SharePoint -VariantType 5 -Name $SPCrawlProp -IsNameEnum $false -PropSet "00130329-0000-0130-c000-000000131346"}  
        "DateTime" {$crawlprop = New-SPEnterpriseSearchMetadataCrawledProperty -SearchApplication $searchapp -Category SharePoint -VariantType 64 -Name $SPCrawlProp -IsNameEnum $false -PropSet "00130329-0000-0130-c000-000000131346"}
        "YesNo" {$crawlprop = New-SPEnterpriseSearchMetadataCrawledProperty -SearchApplication $searchapp -Category SharePoint -VariantType 11 -Name $SPCrawlProp -IsNameEnum $false -PropSet "00130329-0000-0130-c000-000000131346"}
        default {$crawlprop = New-SPEnterpriseSearchMetadataCrawledProperty -SearchApplication $searchapp -Category SharePoint -VariantType 31 -Name $SPCrawlProp -IsNameEnum $false -PropSet "00130329-0000-0130-c000-000000131346"}
        }
    }
}
#now that the crawled properties exist, loop through managed properties and add
$PropertyNodeList = $XmlDoc.SearchProperties.ManagedProperties
foreach ($PropertyNode in $PropertyNodeList.ManagedProperty)
{
    $SharePointProp = $PropertyNode.Name
    $SharePointPropType = $PropertyNode.Type
    $SharePointPropMapList = $PropertyNode.Map
    $MultValue = $PropertyNode.MultiValue
	$Refinable=$PropertyNode.Refinable
    #add managed property
    #remove it if it already exists
    if ($mp = Get-SPEnterpriseSearchMetadataManagedProperty -SearchApplication $searchapp -Identity $SharePointProp -ea "silentlycontinue")
    {
         #$mp | Remove-SPEnterpriseSearchMetadataManagedProperty -Confirm
        $mp.DeleteAllMappings()
        $mp.Delete()
        $searchapp.Update()
    }
    $prop=New-SPEnterpriseSearchMetadataManagedProperty -SearchApplication $searchapp -Name $SharePointProp -Type $SharePointPropType -EnabledForScoping $true 

     if($MultValue -eq "true")
{
    $prop.HasMultipleValues = $true
}
else
{
    $prop.HasMultipleValues = $false
}
if($Refinable -eq "true")
{
    $prop.Refinable = $true
}




    $prop.PutInPropertyBlob = 1
    $prop.Update()
    $mp = Get-SPEnterpriseSearchMetadataManagedProperty -SearchApplication $searchapp -Identity $SharePointProp
    #add multiple crawled property mappings
    foreach ($SharePointPropMap in $SharePointPropMapList)
    {
        $SPMapCat = $SharePointPropMap.Category
        $SPMapName = $SharePointPropMap.InnerText

        $cat = Get-SPEnterpriseSearchMetadataCategory –SearchApplication $searchapp –Identity $SPMapCat
        $prop = Get-SPEnterpriseSearchMetadataCrawledProperty -SearchApplication $searchapp -Category $cat -Name $SPMapName
        New-SPEnterpriseSearchMetadataMapping -SearchApplication $searchapp -CrawledProperty $prop -ManagedProperty $mp 
    }
}

write-host -f Green "Please perform full crawl."
write-host "Press any key to continue"
$null=$Host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
Stop-Transcript