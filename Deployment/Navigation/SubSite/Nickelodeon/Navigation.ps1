#add sharepoint cmdlets
if ( (Get-PSSnapin -Name Microsoft.SharePoint.PowerShell -ErrorAction SilentlyContinue) -eq $null )
{    
      Add-PsSnapin Microsoft.SharePoint.PowerShell
}

Write-Host "Enter the site collection url"
$spWebUrl = Read-Host 
[xml]$NavXml=GET-CONTENT "Navigation.xml"
foreach($webUrl in $NavXml.Navigation.WebUrl)
{
	Write-Host
	Write-Host  -f Yellow "-------------Creating a navigation on web "$webUrl.URL"----------------"
	Write-Host
	$SPWeb =Get-SPWeb -Identity $spWebUrl
	$pubWeb = [Microsoft.SharePoint.Publishing.PublishingWeb]::GetPublishingWeb($SPWeb)
	$nodes  = $pubWeb.Navigation.GlobalNavigationNodes
	$count = $pubweb.navigation.GlobalNavigationNodes.count
   
	#delete all Current Navigation Link
	for( $i=$count;$i-ge 0;$i--)
	{   
		  $node = $pubweb.navigation.GlobalNavigationNodes[$i]
		  if($node -ne $null)
		  {
			  $pubweb.navigation.GlobalNavigationNodes.delete($node)
		  }

	}
    
	#create Current Navigation Link
	$CreateSPNavigationNode = [Microsoft.SharePoint.Publishing.Navigation.SPNavigationSiteMapNode]::CreateSPNavigationNode
	foreach($heading in $webUrl.TopNav.Link)
	{
		$headingNode = $CreateSPNavigationNode.Invoke($heading.Title, $heading.URL, [Microsoft.SharePoint.Publishing.NodeTypes]::Heading, $nodes)
        $headingNode.Properties["Audience"] = $heading.TargetAudience
		$headingNode.Update()
		Write-Host -f White $heading.Title": heading  Added Successfully." 
	  
		if ($heading.SubLink -ne $null)
		{
			foreach($link in $heading.SubLink)
			{
					 $headingCollection = $headingNode.Children  
					 $linkNode = $CreateSPNavigationNode.Invoke($link.Title, $link.URL, [Microsoft.SharePoint.Publishing.NodeTypes]::AuthoredLinkPlain, $headingCollection)
                     $linkNode.Properties["Audience"] = $link.TargetAudience
					 $linkNode.Update()
					 Write-Host -f Green "        "$link.Title ":link Added Successfully."  
			}
		}  
	}
}
#quick nevigat
$qlNav = $pubweb.Navigation.CurrentNavigationNodes
    $qlNavCount=$pubweb.Navigation.CurrentNavigationNodes.Count

    for( $i=$qlNavCount;$i-ge 0;$i--)
	    {   
		      $node = $pubweb.navigation.CurrentNavigationNodes[$i]
		      if($node -ne $null)
		      {
			      $pubweb.navigation.CurrentNavigationNodes.delete($node)
		      }

	    }


#create Current Navigation Link
	$CreateSPNavigationNode = [Microsoft.SharePoint.Publishing.Navigation.SPNavigationSiteMapNode]::CreateSPNavigationNode
	foreach($heading in $webUrl.QuickNav.Link)
	{
		$headingNode = $CreateSPNavigationNode.Invoke($heading.Title, $heading.URL, [Microsoft.SharePoint.Publishing.NodeTypes]::Heading, $qlNav)
        $headingNode.Properties["Audience"] = $heading.TargetAudience
		$headingNode.Update()
		Write-Host -f White $heading.Title": heading  Added Successfully." 
	  
		if ($heading.SubLink -ne $null)
		{
			foreach($link in $heading.SubLink)
			{
					 $headingCollection = $headingNode.Children  
					 $linkNode = $CreateSPNavigationNode.Invoke($link.Title, $link.URL, [Microsoft.SharePoint.Publishing.NodeTypes]::AuthoredLinkPlain, $headingCollection)
                     $linkNode.Properties["Audience"] = $link.TargetAudience
					 $linkNode.Update()
					 Write-Host -f Green "        "$link.Title ":link Added Successfully."  
			}
		}  
	}
$WebNavSettings = New-Object Microsoft.SharePoint.Publishing.Navigation.WebNavigationSettings($SPWeb);
$WebNavSettings.GlobalNavigation.Source = "PortalProvider"
$WebNavSettings.CurrentNavigation.Source = "PortalProvider"
$SPWeb.AllProperties["__DisplayShowHideRibbonActionId"] = $false.ToString()
$SPWeb.Update()
$WebNavSettings.Update()
$pubWeb.Update()
$SPWeb.Dispose()