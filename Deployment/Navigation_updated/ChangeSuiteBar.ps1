if ((Get-PSSnapin "Microsoft.SharePoint.PowerShell" -ErrorAction SilentlyContinue) -eq $null) { Add-PSSnapin "Microsoft.SharePoint.PowerShell" }
$webApplicationURL = Read-Host "Web Application URL"
$webApp = Get-SPWebApplication $webApplicationURL
$CurrentPath = (get-Location).Path
$webApp.SuiteBarBrandingElementHtml = "<div class=""ms-core-brandingText"">Ad Sales</div>"
$webApp.Update()