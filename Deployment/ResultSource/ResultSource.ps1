﻿$ver = $host | select version 
if($Ver.version.major -gt 1) {$Host.Runspace.ThreadOptions = "ReuseThread"} 
if(!(Get-PSSnapin Microsoft.SharePoint.PowerShell -ea 0)) 
{ 
  Write-Progress -Activity "Loading Modules" -Status "Loading Microsoft.SharePoint.PowerShell" 
  Add-PSSnapin Microsoft.SharePoint.PowerShell 
} 

function CreateResultSource()
{
   # create manager instances
   $fedManager = New-Object Microsoft.Office.Server.Search.Administration.Query.FederationManager($sspApp)
   $searchOwner = New-Object Microsoft.Office.Server.Search.Administration.SearchObjectOwner([Microsoft.Office.Server.Search.Administration.SearchObjectLevel]::$scope, $site.RootWeb)

   # define the required QUERY
   $queryProperties = New-Object Microsoft.Office.Server.Search.Query.Rules.QueryTransformProperties
   
  
   # define custom sorting
   $sortCollection = New-Object Microsoft.Office.Server.Search.Query.SortCollection
    if($sortName -eq $null)
   {
   $sortCollection.Add("LastModifiedTime", [Microsoft.Office.Server.Search.Query.SortDirection]::Descending)
   $queryProperties["SortList"] = [Microsoft.Office.Server.Search.Query.SortCollection]$sortCollection
   }
   else
   {
   $sortCollection.Add($sortName, [Microsoft.Office.Server.Search.Query.SortDirection]::$sortOrder)
   }
   $queryProperties["SortList"] = [Microsoft.Office.Server.Search.Query.SortCollection]$sortCollection
 
 
   
   $source = $fedManager.GetSourceByName($resultSourceName, $searchOwner)
   if ($source -eq $null)
   {
	 # create result source
	 $resultSource = $fedManager.CreateSource($searchOwner)
	 $resultSource.Name = $resultSourceName
	 $resultSource.ProviderId = $fedManager.ListProviders()['Local SharePoint Provider'].Id
	 $resultSource.CreateQueryTransform($queryProperties, $query)
	 $resultSource.Commit()
   }
   else
   {
	 write-host "Result Source exists!" -f Yellow
   }
}

function CallCreateResultSource([string]$ConfigPath = "")
{
	$cfg = [xml](get-content $ConfigPath)
	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
	if($Error.Count -eq 0)
	{
		$webAppFile = $cfg
		$error.clear()
		$deploymentConfig = $webAppFile.DeploymentConfig
		foreach($resultsource in $deploymentConfig.ResultSources.ResultSource)	
		{
		# Get the list for operation to perform
		$resultSourceName = $resultsource.ResultSourceName
		$scope = $resultsource.ScopeName
		$SearchServiceName= $deploymentConfig.SearchServiceName
		$siteUrl= $resultsource.SiteUrl
		$query=$resultsource.Query
        $sortName=$resultsource.Sort
        $sortOrder=$resultsource.SortOrder
		# load Search assembly
		[void] [Reflection.Assembly]::LoadWithPartialName("Microsoft.Office.Server.Search")

		# get current search service aplication

		#**** Give the Search Service Name -> $SearchServiceName ****

		$sspApp = Get-SPEnterpriseSearchServiceApplication $SearchServiceName

		#****Give the site url where one want to create the result source***** 

		$site = get-spsite $siteUrl -WarningAction SilentlyContinue

		###Calling function
		CreateResultSource
		}


		foreach($contentsource in $deploymentConfig.ContentSources.ContentSource)
		{
		$SearchServiceName= $deploymentConfig.SearchServiceName
		$searchapp = Get-SPEnterpriseSearchServiceApplication $SearchServiceName
		New-SPEnterpriseSearchCrawlContentSource -SearchApplication $searchapp -Type $ContentSource.CType -name $ContentSource.ContentSourceName -StartAddresses $contentsource.SharedPath
		write-host "Content Source Created"
		}
	}
}




[string] $currentLocation = Get-Location

[string] $parameterfile = $currentLocation + "\ResultSourceConfiguration.xml"


CallCreateResultSource $parameterfile

