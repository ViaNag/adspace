﻿$snapin = Get-PSSnapin | Where-Object {$_.Name -eq 'Microsoft.SharePoint.Powershell'} 
if ($snapin -eq $null) 
{    
	Write-Host "Loading SharePoint Powershell Snapin"    
	Add-PSSnapin "Microsoft.SharePoint.Powershell" 
}

    [string] $currentLocation = Get-Location

[string] $configFileName = $currentLocation + "\ChangeDefaultValue.xml"

# Check that the config file exists.
if (-not $(Test-Path -Path $configFileName -Type Leaf))
{
	Write-Error -message ("Configuration file '" + $configFileName + "' does not exist.")
}

$configXml = [xml]$(get-content $configFileName)
if( $? -eq $false ) 
{
	Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
}

    $siteUrl =  $configXml.Config.siteCollectionUrl

    $site =Get-SPSite  $siteUrl

    $web = $site.OpenWeb();



    $spweb = $site.RootWeb		


	Write-Host "In the Site Collection"  $web.url
     

    $configXml.Config.mapping |
	ForEach-Object {
					
		$targetField = $spweb.Fields.GetFieldByInternalName($_.siteColumnName)
		if($targetField -ne $null)
		{
			Write-Host "Setting Default Value for " $_.siteColumnName

            $targetField.DefaultFormula =  $_.defaultValue;
			
			$targetField.Update($true)								
		}
        
    }
        
	$spweb.Dispose()				
