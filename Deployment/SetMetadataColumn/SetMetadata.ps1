$snapin = Get-PSSnapin | Where-Object {$_.Name -eq 'Microsoft.SharePoint.Powershell'}  
if ($snapin -eq $null)  
{     
    Write-Host "Loading SharePoint Powershell Snapin"     
    Add-PSSnapin "Microsoft.SharePoint.Powershell"  
}


[string] $Group = $null
[string] $TermSet = $null
[string] $MMDService = $null

function Set-Metadata
{
	Param (
           [parameter(Mandatory=$true)][string]$SiteUrl,
           [parameter(Mandatory=$true)][string]$SubSiteUrl,
           [parameter(Mandatory=$true)][string]$Column,
           [parameter(Mandatory=$true)][string]$Level,
		   [parameter(Mandatory=$true)][string]$LibraryName,
		   [parameter(Mandatory=$true)][string]$FolderUrl,
		   [parameter(Mandatory=$true)][string]$Value
           )
	
	# Script Variables
    $site = Get-SPSite $SiteUrl
    
    $subSiteWeb = Get-SPWeb $SubSiteUrl
	
	$docLib = $subSiteWeb.Lists[$LibraryName]

    if($docLib -ne $null)
    {
	
	    if($Level.ToLower() -eq 'library')
	    {
		    $folder = $docLib.RootFolder
	    }
	    else
	    {
            
		    $folderINTERNAL = $docLib.ParentWeb.GetFolder($FolderUrl);
            #$folderINTERNAL = $docLib.ParentWeb.GetFolder($docLib.RootFolder.Url+"/Account Plans");
            if($folderINTERNAL.Exists)
            {
			    $folder = $folderINTERNAL
		    }
	    }
	
	    if($folder -ne $null)
	    {
		    GetFiles $Column $Value $site $folder $docLib
	    }
    }
}


Function GetFiles($Column,$Value,$site,$folder,$docLib)
{ 
   Write-Host "+"$folder.Name
   foreach($file in $folder.Files)
   {
        Write-Host "`t" $file.Name
	    $managedmetadatafield= $docLib.Fields[$Column] -as [Microsoft.SharePoint.Taxonomy.TaxonomyField];  
	    $session = new-object Microsoft.SharePoint.Taxonomy.TaxonomySession($site) 
		$termStore = $session.TermStores[$MMDService] 
		$group = $termStore.Groups[$Group]
		$termSet = $group.TermSets[$TermSet]
		#$terms = $termSet.Terms
		
        $taxonomyTerms = $Value.split(';')

        if($managedmetadatafield.AllowMultipleValues -eq $true)
        {
            $termCollection = new-object Microsoft.SharePoint.Taxonomy.TaxonomyFieldValueCollection($managedmetadatafield);         
            foreach($t in $taxonomyTerms)            
            {            
             $terms = $termSet.GetTerms($t,$false);                        
             $term = $null;                        
             if($terms.Count -ne 0)                        
             {                        
              $term = $terms[0];                        
             }              
             $termValue = new-object Microsoft.SharePoint.Taxonomy.TaxonomyFieldValue($managedmetadatafield); 
             $termValue.TermGuid = $term.Id.ToString();            
             $termValue.Label = $term.Name;            
             $termCollection.Add($termValue);            
            }

            $managedmetadatafield.SetFieldValue($file.Item,$termCollection);
        }
        else
        {
            $termName = $taxonomyTerms[0]; 
            $terms = $termSet.GetTerms($termName,$false);            
            $term = $null; 
            if($terms.Count -ne 0)                        
            { 
                $term = $terms[0];
            }

            $managedmetadataField.SetFieldValue($file.Item,$term);
        }            
        $file.Item.Update();

        
   }

   # Use recursion to loop through all subfolders.
   foreach ($subFolder in $folder.SubFolders)
   {
       Write-Host "`t" -NoNewline
       GetFiles $Column $Value $site $folder $docLib
   }
 }
 
 
 
 [string] $currentLocation = Get-Location

[string] $configFileName = $currentLocation + "\SetMetadata.xml"

# Check that the config file exists.
if (-not $(Test-Path -Path $configFileName -Type Leaf))
{
	Write-Error -message ("Configuration file '" + $configFileName + "' does not exist.") -Red
}

$configXml = [xml]$(get-content $configFileName)
if( $? -eq $false ) 
{
	Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red`
}

if ($configXml.LocationBasedTagging)
{
	foreach ($mmd in $configXml.LocationBasedTagging.MMD)
	{
		Write-Host "Setting metadata for column " + $mmd.Column + " with value " + $mmd.Value -ForegroundColor Yellow
		
        $Group = $mmd.Group
        $TermSet = $mmd.TermSet
        $MMDService = $mmd.MMDService
        
		Set-Metadata -SiteUrl $mmd.SiteUrl -SubSiteUrl $mmd.SubSiteUrl -Column $mmd.Column -Level $mmd.Level -LibraryName $mmd.LibraryName -Folder $mmd.FolderUrl -Value $mmd.Value

        Write-Host "Successfully changed metadata value for " + $mmd.Column + " at level " + $mmd.Level -ForegroundColor Green
	}
	
}
 
 
 