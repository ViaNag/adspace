Add-PSSnapin Microsoft.SharePoint.Powershell


[string] $currentLocation = Get-Location

[string] $configFileName = $currentLocation + "\ResourcesList.xml"

# Check that the config file exists.
if (-not $(Test-Path -Path $configFileName -Type Leaf))
{
	Write-Error -message ("Configuration file '" + $configFileName + "' does not exist.")
}

$configXml = [xml]$(get-content $configFileName)
if( $? -eq $false ) 
{
	Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
}

Write-Host "Enter the site collection url"
$spWebUrl = Read-Host 


if ($configXml.Resources)
{
    $spWeb =Get-SPWeb -Identity $spWebUrl
    $resourcesList = $spWeb.Lists["Resources"]
    
    Write-Host "Add Items to Resources List " -ForegroundColor Yellow

	foreach ($Resource in $configXml.Resources.Resource)
	{
		
        
        $newResourceItem = $resourcesList.Items.Add()
 
                    #Add newCategoryItem to this list item
                    $newResourceItem["Title"] = $Resource.Title

                    $linkUrl = $Resource.LinkUrl
                    $linkDesc = $Resource.LinkDesc
                    $newResourceItem["Link"] = "$linkUrl , $linkDesc"
                    $newResourceItem["HeaderName"] = $Resource.Header    
	        
             #Update the object so it gets saved to the list
            $newResourceItem.Update()

            
        
		
    }
    Write-Host "Successfully added Items to Resources List " -ForegroundColor Green
    $spWeb.Dispose()
}
