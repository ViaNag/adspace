﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FortestingtimerJob
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
               
                    string internalSiteUrl = 
                    string extsiteUrl = appSettings.CurrentConfiguration.AppSettings.Settings["extsiteurl"].Value;

                    // Initialize the param object
                    ReadItemCollectionParams param = new ReadItemCollectionParams();
                    param.Siteurl = internalSiteUrl;
                    param.ExternalSiteUrl = extsiteUrl;

                    ExternalSharingFacade extSharingFacade = new ExternalSharingFacade();
                    extSharingFacade.DeleteExpiredContent(param);

                }
            }
            catch (Exception ex)
            {
            }
        }
    }
}
