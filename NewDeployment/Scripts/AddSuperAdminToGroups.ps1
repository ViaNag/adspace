function AddOwenerToGroups([String]$ConfigFileName = "")
{

	Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
	[string]$xmlpath = $ConfigFileName
	 
	$groupsXML =  [xml](Get-Content ($xmlpath))
	if( $? -eq $false ) 
	{
		LogError "Could not read config file. Exiting ..."
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	}
	Write-Host "Success: Reading Configuration file: Process Completed ..." -ForegroundColor Green 
	try
	{
    	$siteurl=$groupsXML.Groups.site
    	$Site= Get-SPSite -Identity $siteurl
    	$SPWeb = $Site.RootWeb
        	
        $SuperAdmin = $SPWeb.SiteGroups[$groupsXML.Groups.GroupName]
        
        foreach($groupSP in $SPWeb.SiteGroups)
        {
            $groupSP.Owner = $SuperAdmin
            
            $groupSP.Update()
        }
    			
    	$SPWeb.Update()
    	$SPWeb.Dispose()			
	
	   write-host "Successfully added super admin to groups"
	}
	catch
	{
		Write-Host "Exception while adding super admin to groups. Exiting script..." -ForegroundColor Red        
		Stop-Transcript
		Stop-SPAssignment -Global		
		Exit -1
	
	}
}




