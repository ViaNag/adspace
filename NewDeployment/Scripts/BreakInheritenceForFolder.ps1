Add-PSSnapin Microsoft.SharePoint.Powershell -ErrorAction "SilentlyContinue"
# ===================================================================================
# FUNC: Break inheritence
# DESC: Breaks inheritence
# ===================================================================================
function BreakInheritenceForFolder([String]$ConfigFileName = "")
{
    Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green

	# Check that the config file exists.
	if (-not $(Test-Path -Path $configFileName -Type Leaf))
	{
		Write-Error -message ("Configuration file '" + $configFileName + "' does not exist.") -Red
	}

	$configXml = [xml]$(get-content $configFileName)
	if( $? -eq $false ) 
	{
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red`
	}

	if ($configXml.BreakRoleInheritence)
	{
		foreach ($folder in $configXml.BreakRoleInheritence.Folder)
		{
			try
			{
				Write-Host "Breaking the inheritence for the list " $folder.Name " on " $folder.WebUrl -ForegroundColor Yellow

				if($folder.Name)
				{
					# Break role inheritance of the folder
					$web = Get-SPWeb $folder.WebUrl
					
                    Write-Host $folder.Name

					$FolderToBreak = $web.GetFolder($folder.Name)
					
					if($FolderToBreak)
					{
					    if($folder.InheritFromParent -eq "true")
						{
						    Write-Host "Inheritance true" -ForegroundColor Green
						}
						else
						{
						    Write-Host "Inheritance false on the list :" $list.Name " So we are reseting and breaking it again" -ForegroundColor Green
                            $FolderToBreak.Item.ResetRoleInheritance()
                            $FolderToBreak.Item.Update();
						}


                        $FolderToBreak.Item.BreakRoleInheritance($false)

                        foreach ($listGroup in $folder.Group)
		                {
                            # Give custom permissions on the list
						    

						    # Fetching the group
						    $groupName = $folder.RootWebName + " " + $listGroup.GroupName

						    foreach ($grp in $web.SiteGroups) 
						    {
							    if($grp.name -eq $groupName)
							    {
								    $group = $grp
								    break
							    }
						    }

						    $roleAssigment = new-object Microsoft.SharePoint.SPRoleAssignment($group)					
						    
                            

                            $listGroup.PermissionLevel.Split(",") | ForEach {

                            $roleDefinition = $web.Site.RootWeb.RoleDefinitions[$_]
						  
						    $roleAssigment.RoleDefinitionBindings.Add($roleDefinition)

                          }

						    $FolderToBreak.Item.RoleAssignments.Add($roleAssigment) 
						    $FolderToBreak.Item.Update()

						    Write-Host "New permissions applied on the site-" $web.Title", list-" $folder.Name". Group-" $groupName", given" $listGroup.PermissionLevel "access." -ForegroundColor Green
                        }
					}
					else
					{
						write-host "List-" $folder.Name ", not found on " $web.Title -ForegroundColor Yellow
					}
				}

				if($Error.Count -gt 0)
				{
					Write-Host "Error Breaking Inheritence. Cause : " $Error -ForegroundColor Red
					$Error.Clear()	
				}
				else
				{
					Write-Host "Process Complete. New permissions applied." -ForegroundColor Green
				}
			}
			catch
			{
				Write-Host "Exception while Breaking Inheritence." $Error -ForegroundColor Red
			}
		}
	}
}

