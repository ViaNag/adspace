
# ===================================================================================
# FUNC: Break inheritence
# DESC: Breaks inheritence
# ===================================================================================

Add-PSSnapin Microsoft.SharePoint.Powershell

function BreakInheritence([String]$ConfigFileName = "")
{

	# Check that the config file exists.
	if (-not $(Test-Path -Path $configFileName -Type Leaf))
	{
		Write-Error -message ("Configuration file '" + $configFileName + "' does not exist.") -Red
	}

	$configXml = [xml]$(get-content $configFileName)
	if( $? -eq $false ) 
	{
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red`
	}

	if ($configXml.BreakSiteRoleInheritence)
	{
		foreach ($siteToBreak in $configXml.BreakSiteRoleInheritence.Site)
		{
			try
			{
				Write-Host "Breaking the inheritence for web " $siteToBreak.WebUrl -ForegroundColor Yellow

				if($siteToBreak.WebUrl)
				{
					# Break role inheritance of the list
					$web = Get-SPWeb $siteToBreak.WebUrl
					
					if($web)
					{

					    If ( ($web.HasUniqueRoleAssignments ) -eq $false ) 
						{
                            Write-Host "Inheritance true" -ForegroundColor Green
						}
						else
						{

                            Write-Host "Inheritance false on the web :" $siteToBreak.WebUrl " So we are reseting and breaking it again" -ForegroundColor Green
                            $web.ResetRoleInheritance()
                            $web.Update();
						}
                        
                        Write-Host "Breaking the inheritence for web " $siteToBreak.WebUrl -ForegroundColor Yellow

                        $web.BreakRoleInheritance($false)
                        $web.Update();

                        foreach ($xmlGroup in $siteToBreak.Group)
		                {
			                try
			                {
                                $actualgroupName = $siteToBreak.RootWebName + " " + $xmlGroup.GroupName

                                Write-Host "Start Processing Group:" $actualgroupName " for web " $siteToBreak.WebUrl -ForegroundColor Green

                                if($xmlGroup.IsNew -eq 'true')
                                {
                                    Write-Host "This is new Group :" $actualgroupName " for web " $siteToBreak.WebUrl -ForegroundColor Green
                                    try
                                    {
                                        $GroupToDelete = $web.SiteGroups[$actualgroupName]
                            
                                        $web.SiteGroups.Remove($GroupToDelete)
                                        
                                         Write-Host "Removing Group:" $actualgroupName " for web " $siteToBreak.WebUrl -ForegroundColor Green

                                        $web.Update()

                                    }
                                    catch
                                    {
                                        Write-Host "Warning: Could not delete group." $Error -ForegroundColor Yello
                                    }

                                    Write-Host "Adding Group:" $actualgroupName " to site for web " $siteToBreak.WebUrl -ForegroundColor Yellow

						            $web.SiteGroups.Add($actualgroupName,$web.Site.Owner,$web.Site.Owner,$actualgroupName +"Site Group")
                                    $web.Update()
                                }

                                Write-Host "Associating Group:" $actualgroupName " to site for web " $siteToBreak.WebUrl -ForegroundColor Yellow

                                $SPGroup = $web.SiteGroups[$actualgroupName]

                                $roleAssigment = new-object Microsoft.SharePoint.SPRoleAssignment($SPGroup)

                                $PermissionLevel=$xmlGroup.PermissionLevel

                                $PermissionLevel.Split(",") | ForEach {
                                    
                                    $roleDefinition = $web.Site.RootWeb.RoleDefinitions[$_]
						  
						            $roleAssigment.RoleDefinitionBindings.Add($roleDefinition)

                                }

						        $web.RoleAssignments.Add($roleAssigment)

                                $web.Update()
                            }
                            catch
                            {
                                Write-Host "Exception while Adding Groups to site." $Error -ForegroundColor Red
                            }
                        }

                        Write-Host "Deleting Default Visitor Member and Owner Groups to web " $siteToBreak.WebUrl -ForegroundColor Yellow

                        try
                        {
                            
                            
                            $web.AssociatedOwnerGroup  = $null

                            $web.Update()

                        }
                        catch
                        {
                            Write-Host "Warning: Could not delete group." $Error -ForegroundColor Yello
                        }


                        try
                        {
                            
                            
                            $web.AssociatedMemberGroup  = $null

                            $web.Update()

                        }
                        catch
                        {
                            Write-Host "Warning: Could not delete group." $Error -ForegroundColor Yello
                        }
                        
                        try
                        {
                            
                            
                            $web.AssociatedVisitorGroup  = $null

                            $web.Update()

                        }
                        catch
                        {
                            Write-Host "Warning: Could not delete group." $Error -ForegroundColor Yello
                        }

                        
                        Write-Host "Create Default Visitor Member and Owner Groups to web " $siteToBreak.WebUrl -ForegroundColor Yellow

                        $web.CreateDefaultAssociatedGroups($web.Site.Owner ,"", $siteToBreak.DefaultGroupPrefix)
                        
					}
					else
					{
						write-host "Web -" $siteToBreak.WebUrl ", not found on " $web.Title -ForegroundColor Yellow
					}
				}

				if($Error.Count -gt 0)
				{
					Write-Host "Error Breaking Inheritence. Cause : " $Error -ForegroundColor Red
					$Error.Clear()	
				}
				else
				{
					Write-Host "Process Complete. New permissions applied." -ForegroundColor Green
				}
			}
			catch
			{
				Write-Host "Exception while Breaking Inheritence." $Error -ForegroundColor Red
			}
		}
	}
}








