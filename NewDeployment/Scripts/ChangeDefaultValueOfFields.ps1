﻿$snapin = Get-PSSnapin | Where-Object {$_.Name -eq 'Microsoft.SharePoint.Powershell'} 
if ($snapin -eq $null) 
{    
	Write-Host "Loading SharePoint Powershell Snapin"    
	Add-PSSnapin "Microsoft.SharePoint.Powershell" 
}

function ChangeDefaultValue([String]$ConfigFileName = "")
{

	Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
	[string]$xmlpath = $ConfigFileName
	 
	$mmdXml =  [xml](Get-Content ($xmlpath))
	if( $? -eq $false ) 
	{
		LogError "Could not read config file. Exiting ..."
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	}
	Write-Host "Success: Reading Configuration file: Process Completed ..." -ForegroundColor Green 

    $siteUrl =  $mmdXml.Config.siteCollectionUrl

    $site =Get-SPSite  $siteUrl

    $web = $site.OpenWeb();



    $spweb = $site.RootWeb		


	Write-Host "In the Site Collection"  $web.url
     

    $mmdXml.Config.mapping |
	ForEach-Object {
					
		$targetField = $spweb.Fields.GetFieldByInternalName($_.siteColumnName)
		if($targetField -ne $null)
		{
			Write-Host "Setting Defalut Value for " $_.siteColumnName

            $targetField.DefaultFormula =  $_.defaultValue;
			
			$targetField.Update($true)								
		}
        
    }
        
	$spweb.Dispose()				
}

function ChangeTypeOfListCol([String]$ConfigFileName = "")
{

	Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
	[string]$xmlpath = $ConfigFileName
	 
	$mmdXml =  [xml](Get-Content ($xmlpath))
	if( $? -eq $false ) 
	{
		LogError "Could not read config file. Exiting ..."
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	}
	Write-Host "Success: Reading Configuration file: Process Completed ..." -ForegroundColor Green 

    $siteUrl =  $mmdXml.Config.siteCollectionUrl

    $site =Get-SPSite  $siteUrl

    $web = $site.OpenWeb();



    $spweb = $site.RootWeb		


	Write-Host "In the Site Collection"  $web.url
     

    $mmdXml.Config.mapping |
	ForEach-Object {

		$list = $spweb.Lists[$_.ListName]

		$targetField = $list.Fields[$_.ColumnName]

		if($targetField -ne $null)
		{

            $schema = [xml]$targetField.SchemaXml;

            $schema.Field.Type  =  $_.Type;

            $schema.Field.List  =  $_.List;

            $schema.Field.ShowField  =  $_.ShowField;

            $schema.Field.Mult  =  $_.Mult;

            $targetField.SchemaXml = $schema.PSBase.OuterXml;

			
			$targetField.Update($true)								
		}
        
    }
        
	$spweb.Dispose()				
}