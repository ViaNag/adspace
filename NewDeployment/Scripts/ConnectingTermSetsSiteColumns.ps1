﻿$snapin = Get-PSSnapin | Where-Object {$_.Name -eq 'Microsoft.SharePoint.Powershell'} 
if ($snapin -eq $null) 
{    
	Write-Host "Loading SharePoint Powershell Snapin"    
	Add-PSSnapin "Microsoft.SharePoint.Powershell" 
}

function ConnectMMD([String]$ConfigFileName = "")
{

	Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
	[string]$xmlpath = $ConfigFileName
	 
	$mmdXml =  [xml](Get-Content ($xmlpath))
	if( $? -eq $false ) 
	{
		LogError "Could not read config file. Exiting ..."
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	}
	Write-Host "Success: Reading Configuration file: Process Completed ..." -ForegroundColor Green 

    #Name of MMS Proxy
    $mmsServiceName = $mmdXml.ManagedMetaData.serviceName


 

    $siteUrl =  $mmdXml.ManagedMetaData.siteCollectionUrl

    $site =Get-SPSite  $siteUrl

    $web = $site.OpenWeb();



    $spweb = $site.RootWeb		
	$session = new-object Microsoft.SharePoint.Taxonomy.TaxonomySession($site)
	$termStore = $Session.TermStores[$mmsServiceName];
	$group=$termStore.Groups[$mmdXml.ManagedMetaData.GroupName]

	Write-Host "In the Site Collection"  $web.url
     

    $mmdXml.ManagedMetaData.mapping |
	ForEach-Object {

		$taxonomy = $_.termNameInStore
			
        $termSet=""
        $term=""
        $associateToTerm=$false;
        if($taxonomy.Contains(";"))
        {
                $multiLevelTaxonomy=$taxonomy.Split(";")
                $m=0;
                foreach($tax in $multiLevelTaxonomy)
                {
                    if($m -eq 0)
                    {
                        $termSet=$group.TermSets[$tax]
                        $term=$group.TermSets[$tax]
                    }
                    else{
                        $term=$term.Terms[$tax]
                        $associateToTerm=$true
                    }
                    $m++;
                }
        }
        else{
			$termSet = $group.TermSets[$taxonomy]
        }					
		$targetField = [Microsoft.SharePoint.Taxonomy.TaxonomyField]$spweb.Fields.GetFieldByInternalName($_.siteColumnName)
		if($targetField -ne $null)
		{
				Write-Host "Connecting with Term set!" $termSet.id " Namely"  $termSet.Name 
			$targetField.sspid = $termstore.id
			$targetField.termsetid = $termSet.id
            if($associateToTerm -eq $true){
            $targetField.AnchorId=$term.Id
            }
			$targetField.Update($true)		
			
			if($_.ReadOnly -eq "true")
                {
                    $targetField.ShowInEditForm = $false
                    $targetField.ShowInNewForm = $false
					$targetField.ShowInDisplayForm = $false
					
					$targetField.Update($true)	
                }
                
                						
		}
        
    }
        
	$spweb.Dispose()				
}