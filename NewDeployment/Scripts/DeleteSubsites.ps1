# Completely deletes the specified Web (including all subsites)


function RemoveSPWebRecursively([Microsoft.SharePoint.SPWeb] $web)
{
    $subwebs = $web.GetSubwebsForCurrentUser()
    foreach($subweb in $subwebs)
    {
        if($subweb.Url -match "([a-zA-Z0-9]{4}-[0-9]{3})")
	    {
            RemoveSPWebRecursively($subweb)
	            $subweb.Dispose()
	    }
    }
    
    if($web.Url -match "([a-zA-Z0-9]{4}-[0-9]{3})")
	{
        Write-Host "Removing site ($($web.Url))..." -ForegroundColor Green
        Remove-SPWeb $web -Confirm:$false
    }
}


function DeleteSubsites([String]$ConfigFileName = "")
{
     Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
	
    $ConfigXML =  [xml](Get-Content ($ConfigFileName))
	if( $? -eq $false ) 
	{
		LogError "Could not read config file. Exiting ..."
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	}

	 Write-Host "Success: Accessing Configuration File." -ForegroundColor Green
     
     try
     {
        foreach ($webApp in $Configuration.Configuration.Farm.WebApplications.Webapplication)
		{
            $web = Get-SPWeb $webApp.SiteCollection.URL
            If ($web -ne $null)
            {
                RemoveSPWebRecursively($web)
                $web.Dispose()
            }
        }
        Write-Host "Success: Deleted all sub sites." -ForegroundColor Green
     }
     catch
     {
        Write-Host "Exception while deleting subsites. Exiting script..." -ForegroundColor Red
		Stop-Transcript
		Stop-SPAssignment -Global
		
		Exit -1
     }
}

