﻿#
# Add the SQL Server Provider.
#

$ErrorActionPreference = "Stop"

$sqlpsreg="HKLM:\SOFTWARE\Microsoft\PowerShell\1\ShellIds\Microsoft.SqlServer.Management.PowerShell.sqlps"

if (Get-ChildItem $sqlpsreg -ErrorAction "SilentlyContinue")
{
    throw "SQL Server Provider for Windows PowerShell is not installed."
}
else
{
    $item = Get-ItemProperty $sqlpsreg
    $sqlpsPath = [System.IO.Path]::GetDirectoryName($item.Path)
}


#
# Set mandatory variables for the SQL Server provider
#
Set-Variable -scope Global -name SqlServerMaximumChildItems -Value 0
Set-Variable -scope Global -name SqlServerConnectionTimeout -Value 30
Set-Variable -scope Global -name SqlServerIncludeSystemObjects -Value $false
Set-Variable -scope Global -name SqlServerMaximumTabCompletion -Value 1000

#
# Load the snapins, type data, format data
#
Push-Location
cd $sqlpsPath
Add-PSSnapin SqlServerCmdletSnapin100
Add-PSSnapin SqlServerProviderSnapin100
Update-TypeData -PrependPath SQLProvider.Types.ps1xml 
update-FormatData -prependpath SQLProvider.Format.ps1xml 
Pop-Location

function ExecuteSQLStatements([String]$ConfigFileName = "")
{

	Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
	[string]$xmlpath = $ConfigFileName
	 
	Write-Host "Success: Reading Configuration file: Process Completed ..." -ForegroundColor Green
	 
	try
	{
		Write-Host "Executing SQL Scripts : Process Starting ...." -ForegroundColor Green
	
		[string] $solutionFileName = Get-Location
		$solutionFileName = $solutionFileName + "\SqlScripts\Center" 
		
		foreach ($f in Get-ChildItem -path $solutionFileName -Filter *.sql | sort-object)
		{
			foreach ($webApp in $Configuration.Configuration.Farm.WebApplications.Webapplication)
			{
				Write-Host "Script -" $f.fullname " Executed on " $webApp.name " - " $webApp.CustomDatabase.DatabaseServer -ForegroundColor Yellow
				invoke-sqlcmd –ServerInstance $webApp.CustomDatabase.DatabaseServer -Database "AmsurgCenter" -InputFile $f.fullname
				Write-Host "Script Executed" -ForegroundColor Green
			}
		}

		[string] $solutionFileName2 = Get-Location
		$solutionFileName2 = $solutionFileName2 + "\SqlScripts\Person" 
	
		foreach ($f in Get-ChildItem -path $solutionFileName2 -Filter *.sql | sort-object)
		{
			foreach ($webApp in $Configuration.Configuration.Farm.WebApplications.Webapplication)
			{
				Write-Host "Script -" $f.fullname " Executed on " $webApp.name " - " $webApp.CustomDatabase.DatabaseServer -ForegroundColor Yellow
				invoke-sqlcmd –ServerInstance $webApp.CustomDatabase.DatabaseServer -Database "AmsurgPerson" -InputFile $f.fullname
				Write-Host "Script Executed" -ForegroundColor Green
			}
		}

		Write-Host "Success: Installing SQL." -ForegroundColor Green
	}
	catch
	{
		Write-Host "Exception while Installing SQL Script. Exiting script..." -ForegroundColor Red
		Stop-SPAssignment -Global
		
		Exit -1
	}
}