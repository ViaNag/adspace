
# ===================================================================================
# FUNC: EmptyList
# DESC: Delete all data from the list
# ===================================================================================
function EmptyList([String]$ConfigFileName = "")
{

	# Check that the config file exists.
	if (-not $(Test-Path -Path $configFileName -Type Leaf))
	{
		Write-Error -message ("Configuration file '" + $configFileName + "' does not exist.") -Red
	}

	$configXml = [xml]$(get-content $configFileName)
	if( $? -eq $false ) 
	{
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red`
	}

	if ($configXml.EmptyList)
	{
		foreach ($list in $configXml.EmptyList.List)
		{
			try
			{
				

				if($list.Name)
				{
					# Break role inheritance of the list
					$web = Get-SPWeb $list.WebUrl
					
					$ListToBreak = $web.Lists[$list.Name]
					
					if($ListToBreak)
					{
                        Write-Host "Deleting all items for the list " $list.Name " on " $list.WebUrl -ForegroundColor Yellow

                        Write-host "List $($ListToBreak.title) has $($ListToBreak.items.count) entries"

                        $items = $ListToBreak.items

                        foreach ($item in $items)
                        {
                            $ListToBreak.getitembyid($Item.id).Delete()
                        }
                        
						Write-Host "All Item deleted on the site-" $web.Title ", for list-" $list.Name -ForegroundColor Green
                        
					}
					else
					{
						write-host "List-" $list.Name ", not found on " $web.Title -ForegroundColor Yellow
					}

				}

				if($Error.Count -gt 0)
				{
					Write-Host "Error Deleting all items. Cause : " $Error -ForegroundColor Red
					$Error.Clear()	
				}
				else
				{
					Write-Host "Process Completed." -ForegroundColor Green
				}
			}
			catch
			{
				Write-Host "Exception while Breaking Inheritence." $Error -ForegroundColor Red
			}
		}
	}
}
