﻿function MinifyFiles([String]$ConfigFileName = "")
{

	Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
	[string]$xmlpath = $ConfigFileName
	 
	$minifyXML =  [xml](Get-Content ($xmlpath))
	if( $? -eq $false ) 
	{
		LogError "Could not read config file. Exiting ..."
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	}

	Write-Host "Success: Reading Configuration file: Process Completed ..." -ForegroundColor Green 
	try
	{
        $minifyXML.Minify.Files |
	    ForEach-Object {
                    $location = $_.Path
                    $fileType = $_.FileType
                    $tempFolderName = $_.FolderName

                    If(Test-Path -Path $Location\$tempFolderName)
                    {
                        Remove-Item -Recurse -Force $Location\$tempFolderName
                    }

                    New-Item -Path $Location -name $_.FolderName -ItemType "directory"

                    $files = gci $Location\*.$fileType

                    foreach ($file in $files)
                    { 
                        Move-Item $file $Location\$tempFolderName
                    }

                    $minifyCommand = "C:\Program Files (x86)\Microsoft\Microsoft Ajax Minifier\AjaxMin.exe"

                    foreach ($f in Get-ChildItem -path $location\$tempFolderName -Filter *.$fileType | sort-object)
		            {
			            Write-Host "Minifying file -" $f.fullname
                        & $minifyCommand $f.fullname -o $f.fullname.Replace("\" + $tempFolderName, "")
				        Write-Host "Script Executed" -ForegroundColor Green		
                    }			
            }	
    }
	catch
	{
		Write-Host "Exception while minifying files. Exiting script..." -ForegroundColor Red        
		Stop-Transcript
		Stop-SPAssignment -Global		
		Exit -1
	
	}


 
}
