if ( (Get-PSSnapin -Name Microsoft.SharePoint.PowerShell -ErrorAction SilentlyContinue) -eq $null )
{
    Add-PSSnapin Microsoft.SharePoint.PowerShell
}

function AddMyNavView([String]$ConfigFileName = "")
{

	Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
	[string]$xmlpath = $ConfigFileName
	 
	$mmdXml =  [xml](Get-Content ($xmlpath))
	if( $? -eq $false ) 
	{
		LogError "Could not read config file. Exiting ..."
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	}
	Write-Host "Success: Reading Configuration file: Process Completed ..." -ForegroundColor Green 

    $spWebUrl =  $mmdXml.Config.siteCollectionUrl

#Get destination site and list
$web = Get-SPWeb -Identity $spWebUrl
$list=$web.Lists["Navigation"]


$myNavigationViewName = "My Navigation"


#Add the column names from the ViewField property to a string collection
$viewFields = New-Object System.Collections.Specialized.StringCollection
$viewFields.Add("LinkName") > $null
$viewFields.Add("AccessTo") > $null
$viewFields.Add("LinkCategoryUrl") > $null
$viewFields.Add("OrderCategories") > $null
$viewFields.Add("CategoryTitle") > $null
$viewFields.Add("HeaderTitle") > $null
$viewFields.Add("ContentType") > $null
$viewFields.Add("LinkUrl") > $null
$viewFields.Add("HeaderUrl") > $null
$viewFields.Add("ID") > $null
$viewFields.Add("LinkCategory") > $null
$viewFields.Add("LinkHeader") > $null

$myNavigationViewQuery = "<Where><Or><Membership Type='CurrentUserGroups'><FieldRef Name='AccessTo' /></Membership><Eq><FieldRef Name='AccessTo' /><Value Type='Integer'><UserID Type='Integer'/></Value></Eq></Or></Where>"


#DefaultView property
$viewDefaultView = $false

$myNavigationView = $list.Views[$myNavigationViewName]
			
			if($myNavigationView -ne $null)
			{
				#Delete this view from the list
				$list.Views.Delete($myNavigationView.ID)
			}

#Create the view in the Navigation list

$myNavigationView = $list.Views.Add($myNavigationViewName, $viewFields, $myNavigationViewQuery, $viewRowLimit, $viewPaged, $viewDefaultView)

Write-Host ("View '" + $myNavigationView.Title + "' created in list '" + $list.Title + "' on site " + $web.Url)

$web.Dispose()

}