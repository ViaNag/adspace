#add sharepoint cmdlets
if ( (Get-PSSnapin -Name Microsoft.SharePoint.PowerShell -ErrorAction SilentlyContinue) -eq $null )
{    
      Add-PsSnapin Microsoft.SharePoint.PowerShell
}

function CreateNavigation([String]$ConfigFileName = "")
{

	Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
	[string]$xmlpath = $ConfigFileName
	 
	$NavXml =  [xml](Get-Content ($xmlpath))
	if( $? -eq $false ) 
	{
		LogError "Could not read config file. Exiting ..."
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	}
	Write-Host "Success: Reading Configuration file: Process Completed ..." -ForegroundColor Green 

    $spWebUrl = $NavXml.Navigation.siteUrl
    Write-Host $spWebUrl
    
    foreach($webUrl in $NavXml.Navigation.WebUrl)
                                                                                                                                                {
	Write-Host
	Write-Host  -f Yellow "-------------Creating a navigation on web "$webUrl.URL"----------------"
	Write-Host
	$SPWeb =Get-SPWeb -Identity $spWebUrl
	$pubWeb = [Microsoft.SharePoint.Publishing.PublishingWeb]::GetPublishingWeb($SPWeb)
	$nodes  = $pubWeb.Navigation.CurrentNavigationNodes
	$count = $pubweb.navigation.CurrentNavigationNodes.count

	#delete all Current Navigation Link
	for( $i=$count;$i-ge 0;$i--)
	{   
		  $node = $pubweb.navigation.CurrentNavigationNodes[$i]
		  if($node -ne $null)
		  {
			  $pubweb.navigation.CurrentNavigationNodes.delete($node)
		  }

	}

	#create Current Navigation Link
	$CreateSPNavigationNode = [Microsoft.SharePoint.Publishing.Navigation.SPNavigationSiteMapNode]::CreateSPNavigationNode
	foreach($heading in $webUrl.Link)
	{
		$headingNode = $CreateSPNavigationNode.Invoke($heading.Title, $heading.URL, [Microsoft.SharePoint.Publishing.NodeTypes]::Heading, $nodes)
		$headingNode.Update()
		Write-Host -f White $heading.Title": heading  Added Successfully." 
	  
		if ($heading.SubLink -ne $null)
		{
			foreach($link in $heading.SubLink)
			{
					 $headingCollection = $headingNode.Children  
					 $linkNode = $CreateSPNavigationNode.Invoke($link.Title, $link.URL, [Microsoft.SharePoint.Publishing.NodeTypes]::AuthoredLinkPlain, $headingCollection)
					 $linkNode.Update()
					 Write-Host -f Green "        "$link.Title ":link Added Successfully."  
			}
		}  
	}
    }
    $WebNavSettings = New-Object Microsoft.SharePoint.Publishing.Navigation.WebNavigationSettings($SPWeb);
    $WebNavSettings.GlobalNavigation.Source = "PortalProvider"
    $WebNavSettings.Update()
    $pubWeb.Update()
    $SPWeb.Dispose()

}