﻿$snapin = Get-PSSnapin | Where-Object {$_.Name -eq 'Microsoft.SharePoint.Powershell'} 
if ($snapin -eq $null) 
{    
	Write-Host "Loading SharePoint Powershell Snapin"    
	Add-PSSnapin "Microsoft.SharePoint.Powershell" 
}

function ChangeDefaultValue([String]$ConfigFileName = "")
{

	Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
	[string]$xmlpath = $ConfigFileName
	 
	$mmdXml =  [xml](Get-Content ($xmlpath))
	if( $? -eq $false ) 
	{
		LogError "Could not read config file. Exiting ..."
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	}
	Write-Host "Success: Reading Configuration file: Process Completed ..." -ForegroundColor Green 

    $siteUrl =  $mmdXml.Config.siteCollectionUrl

    $site =Get-SPSite  $siteUrl

    $web = $site.OpenWeb();



    $spweb = $site.RootWeb		


	Write-Host "In the Site Collection"  $web.url
     

    $mmdXml.Config.mapping |
	ForEach-Object {
					
		$targetField = $spweb.Fields.GetFieldByInternalName($_.siteColumnName)
		if($targetField -ne $null)
		{
			Write-Host "Setting Defalut Value for " $_.siteColumnName

            $targetField.DefaultFormula =  $_.defaultValue;
			
			$targetField.Update($true)								
		}
        
    }
        
	$spweb.Dispose()				
}

function ChangeTypeOfListCol([String]$ConfigFileName = "")
{

	Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
	[string]$xmlpath = $ConfigFileName
	 
	$mmdXml =  [xml](Get-Content ($xmlpath))
	if( $? -eq $false ) 
	{
		LogError "Could not read config file. Exiting ..."
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	}
	Write-Host "Success: Reading Configuration file: Process Completed ..." -ForegroundColor Green 

    $siteUrl =  $mmdXml.Config.siteCollectionUrl

    $site =Get-SPSite  $siteUrl

    $web = $site.OpenWeb();



    $spweb = $site.RootWeb		


	Write-Host "In the Site Collection"  $web.url
     

    $mmdXml.Config.mapping |
	ForEach-Object {

		$list = $spweb.Lists[$_.ListName]

		$targetField = $list.Fields[$_.ColumnName]

		if($targetField -ne $null)
		{
            Write-Host "Updating Field" $_.ColumnName -ForegroundColor Yellow

            $targetField.AllowMultipleValues = $true
			
			$targetField.Update($true)		

            Write-Host "Updated Field" $_.ColumnName -ForegroundColor Green						
		}
        
    }
        
	$spweb.Dispose()				
}


function DeleteColumnsFromAllDocLib([String]$ConfigFileName = "")
{

	Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
	[string]$xmlpath = $ConfigFileName
	 
	$mmdXml =  [xml](Get-Content ($xmlpath))
	if( $? -eq $false ) 
	{
		LogError "Could not read config file. Exiting ..."
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	}
	Write-Host "Success: Reading Configuration file: Process Completed ..." -ForegroundColor Green 

    $siteUrl =  $mmdXml.Config.siteCollectionUrl

    $site =Get-SPSite  $siteUrl

    $mmdXml.Config.mapping |
	ForEach-Object {

        for ($i = 0; $i -lt $site.AllWebs.Count; $i++)
        {
            $spweb = $site.AllWebs[$i]

            Write-Host "In the Site Collection" $spweb.Title -ForegroundColor Green

            for ($j = 0; $j -lt $spweb.Lists.Count; $j++)
            {
                $list = $spweb.Lists[$j]
            
                if($list.BaseType -eq 1)             
                {
                    Write-Host "In the Library" $list.Title -ForegroundColor Yellow 

		            $targetField = $list.Fields[$_.ColumnName]

		            if($targetField -ne $null -and $list.Title -ne $_.ExcludeList)
		            {
                        if($targetField.InternalName -eq $_.InternalName)
                        {
                            Write-Host "Deleting Field" $_.ColumnName -ForegroundColor Yellow

                            $list.Fields.Delete($targetField)

                            $list.update();

                            Write-Host "Deleting internal name " $targetField.Name -ForegroundColor DarkMagenta				
                        }
                        else
                        {
                            Write-Host "This is not the required field " $targetField.Name -ForegroundColor Red				
                        }
		            }
                    else
                    { 
                        if($list.Title -eq $_.ExcludeList)
                        {
                            Write-Host "Exclude list " $list.Title -ForegroundColor DarkMagenta						
                        }
                    }
                }
            }
        }
        
    }
        
	$spweb.Dispose()				
}


function DeleteColumnsFromCT([String]$ConfigFileName = "")
{

	Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
	[string]$xmlpath = $ConfigFileName
	 
	$mmdXml =  [xml](Get-Content ($xmlpath))
	if( $? -eq $false ) 
	{
		LogError "Could not read config file. Exiting ..."
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	}
	Write-Host "Success: Reading Configuration file: Process Completed ..." -ForegroundColor Green 

    $siteUrl =  $mmdXml.Config.siteCollectionUrl

    $ContentTypeName = $mmdXml.Config.ContentTypeName

    $web = Get-SPWeb $siteUrl

    $contentType = $web.ContentTypes[ $ContentTypeName ]

    $mmdXml.Config.mapping |
	ForEach-Object {

       

		$targetField = $web.Fields.GetFieldByInternalName($_.ColumnName)

		if($targetField -ne $null)
		{
            try
            {
                Write-Host "Deleting Content type Field" $_.ColumnName -ForegroundColor Yellow

                $contentType.FieldLinks.Delete( $targetField.Id )

                $contentType.Update( $true )

                Write-Host "Deleting  Content type internal name " $targetField.Name -ForegroundColor DarkMagenta				
            }
            catch 
            {
                Write-Error "Exception While Deleting Column" $_.ColumnName
            }
		}
                    
        
        
    }
        
	$web.Dispose()				
}

function AddColumnsToCT([String]$ConfigFileName = "")
{

	Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
	[string]$xmlpath = $ConfigFileName
	 
	$mmdXml =  [xml](Get-Content ($xmlpath))
	if( $? -eq $false ) 
	{
		LogError "Could not read config file. Exiting ..."
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	}
	Write-Host "Success: Reading Configuration file: Process Completed ..." -ForegroundColor Green 

    $siteUrl =  $mmdXml.Config.siteCollectionUrl

    $ContentTypeName = $mmdXml.Config.ContentTypeName

    $web = Get-SPWeb $siteUrl

    $contentType = $web.ContentTypes[ $ContentTypeName ]

    $mmdXml.Config.mapping |
	ForEach-Object {

       

		$targetField = $web.Fields.GetFieldByInternalName($_.ColumnName)

		if($targetField -ne $null)
		{
            try
            {
                Write-Host "Deleting Content type Field" $_.ColumnName -ForegroundColor Yellow
                
                $link = new-object Microsoft.SharePoint.SPFieldLink $targetField

                $contentType.FieldLinks.Add($link)

                $contentType.Update( $true )

                Write-Host "Deleting  Content type internal name " $targetField.Name -ForegroundColor DarkMagenta				
            }
            catch 
            {
                Write-Error "Exception While Deleting Column" $_.ColumnName
            }
		}
                    
        
        
    }
        
	$web.Dispose()				
}
