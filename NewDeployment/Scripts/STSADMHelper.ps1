
function SP-Run-STSADM-Command([string]$command, [string]$parameters, [string]$ignoreErrors)
{
	#stsadm -o execadmsvcjobs
	#SP-Check-STSADM-Result "execadmsvcjobs"
	$stsadmParams = "stsadm -o $command $parameters"
	Write-Host        $stsadmParams -ForegroundColor DarkCyan
	Invoke-Expression $stsadmParams
	if ($ignoreErrors -ne $true)
	{
		SP-Check-STSADM-Result $command
	}
	
	#stsadm -o execadmsvcjobs
	#SP-Check-STSADM-Result "execadmsvcjobs"
}

function SP-Run-STSADM-Command-NoCheck([string]$command, [string]$parameters, [string]$ignoreErrors)
{
	$stsadmParams = "stsadm -o $command $parameters"
	Write-Host        $stsadmParams -ForegroundColor DarkCyan
	Invoke-Expression $stsadmParams
	if ($ignoreErrors -ne $true)
	{
		SP-Check-STSADM-Result $command
	}
}

function SP-Run-STSADM-Command-NoError([string]$command, [string]$parameters, [string]$ignoreErrors)
{
	$stsadmParams = "stsadm -o $command $parameters"
	Write-Host        $stsadmParams -ForegroundColor DarkCyan
	Invoke-Expression $stsadmParams
	# stsadm -o execadmsvcjobs
	# SP-Check-STSADM-Result "execadmsvcjobs"
}

function SP-Check-STSADM-Result ([string]$command)
{
	if ($LASTEXITCODE -ne 0)
	{
		Throw "Error calling stsadm command '$command'"
	}
}
