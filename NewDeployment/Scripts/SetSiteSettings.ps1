Add-PSSnapin Microsoft.SharePoint.Powershell -ErrorAction "SilentlyContinue"

function SetWelcomeAndLogo([String]$ConfigFileName = "")
{

	Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
	[string]$xmlpath = $ConfigFileName
	 
	$homeAndLogoXML =  [xml](Get-Content ($xmlpath))
	if( $? -eq $false ) 
	{
		LogError "Could not read config file. Exiting ..."
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	}
	Write-Host "Success: Reading Configuration file: Process Completed ..." -ForegroundColor Green 
	
	$sitecolurl=$homeAndLogoXML.Sites.SiteUrl
	$Site= Get-SPSite -Identity $sitecolurl
	$RootSPWeb = $Site.RootWeb	
    $RootSPWeb.SiteLogoUrl = $RootSPWeb.Url + $homeAndLogoXML.Sites.LogoUrl
    $RootSPWeb.SiteLogoDescription = $homeAndLogoXML.Sites.LogoDesc

    $folder = $RootSPWeb.RootFolder
    $folder.WelcomePage = $homeAndLogoXML.Sites.Welcome

    $folder.update();

    $RootSPWeb.Update();
    Write-Host "Welcome Page and logo set for - "$RootSPWeb.Title

	$homeAndLogoXML.Sites.Site |
	ForEach-Object {
					$SubSiteName=$_.name
                    $SubSiteUrl = $_.url
                    $logoURl = $_.LogoUrl
                    $logoDesc = $_.LogoDesc	
                    $welcome = $_.Welcome
					$SPWeb = Get-SPweb $SubSiteUrl
                    $SPWeb.SiteLogoUrl = $RootSPWeb.Url + $logoURl
                    $SPWeb.SiteLogoDescription = $logoDesc

                    $folder = $SPWeb.RootFolder
                    $folder.WelcomePage = $welcome

                    $folder.update();

                    $SPWeb.Update();
					$SPWeb.Dispose()

						  


						  
						  Write-Host "Welcome Page and logo set for - "$SPWeb.Title 		  			 
						 }
				
			
						
	
	write-host "Successfully updated welcome page and logo"
	


 
}

