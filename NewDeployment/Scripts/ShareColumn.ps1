
### Load SharePoint SnapIn   
if ((Get-PSSnapin "Microsoft.SharePoint.PowerShell" -ErrorAction SilentlyContinue) -eq $null)   
{   
    Add-PSSnapin Microsoft.SharePoint.PowerShell   
}   
### Load SharePoint Object Model   
[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SharePoint")   
  

 
function ShareColumn($web, $list, $contentType, $fieldToShare)
{

$list = $web.Lists[$list]   
### Get Document Set Content Type from list   
[Microsoft.SharePoint.SPContentType]$cType = $list.ContentTypes[$contentType]   
[Microsoft.Office.DocumentManagement.DocumentSets.DocumentSetTemplate]$newDocumentSetTemplate = [Microsoft.Office.DocumentManagement.DocumentSets.DocumentSetTemplate]::GetDocumentSetTemplate($cType);
$newDocumentSetTemplate.SharedFields.Add($cType.Fields[$fieldToShare]);
$newDocumentSetTemplate.AllowedContentTypes.Add($web.ContentTypes[$contentType].Id);
$newDocumentSetTemplate.Update($true);
$spFieldLink = New-Object Microsoft.SharePoint.SPFieldLink ($cType.Fields[$fieldToShare])
$cType.Update();
$web.Update();

}

    
function ShareColumnForDocument([String]$ConfigFileName = "")
{

	Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
	[string]$xmlpath = $ConfigFileName
	 
	$NavXml =  [xml](Get-Content ($xmlpath))
	if( $? -eq $false ) 
	{
		LogError "Could not read config file. Exiting ..."
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	}
	Write-Host "Success: Reading Configuration file: Process Completed ..." -ForegroundColor Green 


    $url =  $NavXml.SiteCollection.url

    $web = Get-SPWeb $url
    ShareColumn $web "Team Site Shared Content" "AdSpace Client Documents" "Client Category"
    ShareColumn $web "Team Site Shared Content" "AdSpace Client Documents" "Client Segment"
    ShareColumn $web "Team Site Shared Content" "AdSpace Client Documents" "Client Name"

    $web.Dispose()  



}


