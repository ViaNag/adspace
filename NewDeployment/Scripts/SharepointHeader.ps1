function ChangeHeader([String]$ConfigFileName = "")
{
    Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
	[string]$xmlpath = $ConfigFileName
	 
	$configXML =  [xml](Get-Content ($xmlpath))
	if( $? -eq $false ) 
	{
		LogError "Could not read config file. Exiting ..."
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	}

	Write-Host "Success: Reading Configuration file: Process Completed ..." -ForegroundColor Green 


    try
	{
        foreach ($element in $configXML.SuiteBarBrandingElements.Element)
		{    
            Write-Host "changing header of" $element.Title -ForegroundColor Green 
            $webApp = Get-SPWebApplication -Identity $element.Url;
            $webApp.SuiteBarBrandingElementHtml = $element.HTML;
            $webApp.Update();
        }
    }
	catch
	{
		Write-Host "Exception while changing sharepoint header. Exiting script..." -ForegroundColor Red        
		Stop-Transcript
		Stop-SPAssignment -Global		
		Exit -1
	
	}
}