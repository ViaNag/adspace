function Addresources([String]$ConfigFileName = "")
{
# Check that the config file exists.
Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
	[string]$xmlpath = $ConfigFileName
	 
	$resourceXML =  [xml](Get-Content ($xmlpath))
	if( $? -eq $false ) 
	{
		LogError "Could not read config file. Exiting ..."
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	}
	Write-Host "Success: Reading Configuration file: Process Completed ..." -ForegroundColor Green 
foreach ($resources in $resourceXML.ResourceTab.Resources) 
{
Write-Host "Enter the sub site url"
$spWebUrl =$resources.WebUrl;
Write-Host $spWebUrl

if ($resources)
{
    $spWeb =Get-SPWeb  $spWebUrl
    $resourcesList = $spWeb.Lists["Resources"]
    
    Write-Host "Add Items to Resources List " -ForegroundColor Yellow

	foreach ($Resource in $resources.Resource)
	{
		
        
        $newResourceItem = $resourcesList.Items.Add()
 
                    #Add newCategoryItem to this list item
                    $newResourceItem["Title"] = $Resource.Title
                    
                    if($Resource.AppendUrl -eq 'true')
                    {
                        $linkUrl = $spWebUrl + $Resource.LinkUrl
                    }
                    else
                    {
                        $linkUrl = $Resource.LinkUrl
                    }
                    $linkDesc = $Resource.LinkDesc
                    $newResourceItem["Link"] = "$linkUrl , $linkDesc"
                    $newResourceItem["HeaderName"] = $Resource.Header    
	        
             #Update the object so it gets saved to the list
            $newResourceItem.Update()

         Write-Host "Successfully added Items to Resources List " -ForegroundColor Green
    $spWeb.Dispose()   
        
		
    }
    
}
}
}
