Add-PSSnapin Microsoft.SharePoint.Powershell

function Create-TopNavigation
{
    
    Param (
           [parameter(Mandatory=$true)][string]$SiteUrl,
           [parameter(Mandatory=$true)][string]$SubSiteUrl,
           [parameter(Mandatory=$true)][string]$CSVPath
           )
 
    # Import CSV
    [array]$FolderList = Import-Csv $CSVPath
 
    # Script Variables
    $spWeb = Get-SPWeb $SiteUrl
    $navigationList = $spWeb.Lists["Navigation"]
	$subSiteWeb = Get-SPWeb $SubSiteUrl
	
    Foreach ($folderGroup in $FolderList) {
 
		
        
        $headerItems = $navigationList.Items | ?{$_.ContentType.Name -eq "Navigation Header" -and $_["HeaderTitle"] -eq $subSiteWeb.Title}
        
        if($headerItems.Count -lt 1)
        {
            $newHeaderItem = $navigationList.Items.Add()
 
            #Add properties to this list item
            $newHeaderItem["HeaderTitle"] = $folderGroup.SiteName

            $ofldurl= new-object Microsoft.SharePoint.SPFieldUrlValue($newHeaderItem["HeaderUrl"])
            $ofldurl.URL = $subSiteWeb.URL
            $subSiteUrl = $subSiteWeb.URL
            $subSitUrlDesc = $folderGroup.SiteName
			$ofldurl.Description = $folderGroup.SiteName

			$newHeaderItem["HeaderUrl"] = "$subSiteUrl , $subSitUrlDesc"
			

            [Microsoft.SharePoint.SPFieldUserValueCollection] $ValueCollection = new-object Microsoft.SharePoint.SPFieldUserValueCollection

			Write-Host "Checking Role Defintion Bindings For Web Sites " -ForegroundColor Green

                $sObject = [Microsoft.SharePoint.SPSecurableObject]$subSiteWeb

                foreach ($roleAssignment in $sObject.RoleAssignments) 
                {
                    $member = $roleAssignment.Member
    
                    Write-Host "Member " $member.ID " Name: " $member.Name -ForegroundColor Yellow

                    if ($member -is [Microsoft.SharePoint.SPGroup] -and $member.Name -ne 'Style Resource Readers') 
                    {
                                
                        $SpGroup = $subSiteWeb.Groups[$member.Name]

                        $spFieldUserValue = New-Object Microsoft.SharePoint.SPFieldUserValue($subSiteWeb, $SpGroup.ID, $SpGroup.Name); 

                        $ValueCollection.Add($spFieldUserValue)
                    } 
                    else 
                    {
                        $spFieldUserValue = New-Object Microsoft.SharePoint.SPFieldUserValue($subSiteWeb, $member.ID, $member.LoginName); 

                        $ValueCollection.Add($spFieldUserValue)    
                    }
                }

                $newHeaderItem["AccessTo"] = $ValueCollection;  
           
            $newHeaderItem["ContentType"] = "Navigation Header"
            #Update the object so it gets saved to the list
            $newHeaderItem.Update()
        }


        if($folderGroup.LibraryName -ne '')
        {
            $spList = $subSiteWeb.Lists[$folderGroup.LibraryName]

            $categoryItems = $navigationList.Items | ?{$_.ContentType.Name -eq "Navigation Category" -and $_["CategoryTitle"] -eq $folderGroup.LibraryLinkName}
        
            if($categoryItems.Count -lt 1)
            {
                $newCategoryItem = $navigationList.Items.Add()
 
                #Add newCategoryItem to this list item
                $newCategoryItem["CategoryTitle"] = $folderGroup.LibraryLinkName
                $newCategoryItem["OrderCategories"] = $folderGroup.OrderCategories
                $listUrl = $spList.RootFolder.ServerRelativeUrl
                $listDesc = $spList.Title
                $newCategoryItem["LinkCategoryUrl"] = "$listUrl , $listDesc"

                $headerLookupItem = $navigationList.Items | where {$_["HeaderTitle"]  -eq $subSiteWeb.Title}


                
            [Microsoft.SharePoint.SPFieldUserValueCollection] $ValueCollection = new-object Microsoft.SharePoint.SPFieldUserValueCollection

			Write-Host "Checking Role Defintion Bindings For List " -ForegroundColor Green

                $sObject = [Microsoft.SharePoint.SPSecurableObject]$spList

                foreach ($roleAssignment in $sObject.RoleAssignments) 
                {
                    $member = $roleAssignment.Member
    
                    Write-Host "Member " $member.ID " Name: " $member.Name -ForegroundColor Yellow

                    if ($member -is [Microsoft.SharePoint.SPGroup] -and $member.Name -ne 'Style Resource Readers') 
                    {           
                        $SpGroup = $subSiteWeb.Groups[$member.Name]

                        $spFieldUserValue = New-Object Microsoft.SharePoint.SPFieldUserValue($subSiteWeb, $SpGroup.ID, $SpGroup.Name); 

                        $ValueCollection.Add($spFieldUserValue)
                    } 
                    else 
                    {
                        $spFieldUserValue = New-Object Microsoft.SharePoint.SPFieldUserValue($subSiteWeb, $member.ID, $member.LoginName); 

                        $ValueCollection.Add($spFieldUserValue)    
                    }
                }

                $newCategoryItem["AccessTo"] = $ValueCollection;  
                

                $headerLookupValue = New-Object Microsoft.SharePoint.SPFieldLookupValue($headerLookupItem.ID,$headerLookupItem.ID.ToString());
                $newCategoryItem["LinkHeader"] = $headerLookupValue
                $newCategoryItem["ContentType"] = "Navigation Category"
                #Update the object so it gets saved to the list
                $newCategoryItem.Update()
            }
            else
            {
                $categoryItemExist = $false
                foreach($categoryItem in $categoryItems)
                {
                   $headerLookupFieldValue = $categoryItem["LinkHeader"] -as [Microsoft.SharePoint.SPFieldLookupValue]
                   $headerLookupValue = $headerLookupFieldValue.LookupValue;

                   if($headerLookupValue -eq $subSiteWeb.Title)
                   {
                     $categoryItemExist = $true
                     break
                   }
                }

                if($categoryItemExist -eq $false)
                {
                    $newCategoryItem = $navigationList.Items.Add()
 
                    #Add newCategoryItem to this list item
                    $newCategoryItem["CategoryTitle"] = $folderGroup.LibraryLinkName
                    $newCategoryItem["OrderCategories"] = $folderGroup.OrderCategories
                    $listUrl = $spList.RootFolder.ServerRelativeUrl
                    $listDesc = $spList.Title
                    $newCategoryItem["LinkCategoryUrl"] = "$listUrl , $listDesc"

                    $headerLookupItem = $navigationList.Items | where {$_["HeaderTitle"]  -eq $subSiteWeb.Title}
                    $headerLookupValue = New-Object Microsoft.SharePoint.SPFieldLookupValue($headerLookupItem.ID,$headerLookupItem.ID.ToString());

                    [Microsoft.SharePoint.SPFieldUserValueCollection] $ValueCollection = new-object Microsoft.SharePoint.SPFieldUserValueCollection

			        Write-Host "Checking Role Defintion Bindings For List " -ForegroundColor Green

                    $sObject = [Microsoft.SharePoint.SPSecurableObject]$spList

                    foreach ($roleAssignment in $sObject.RoleAssignments) 
                    {
                        $member = $roleAssignment.Member
    
                        Write-Host "Member " $member.ID " Name: " $member.Name -ForegroundColor Yellow

                        if ($member -is [Microsoft.SharePoint.SPGroup] -and $member.Name -ne 'Style Resource Readers') 
                        {           
                            $SpGroup = $subSiteWeb.Groups[$member.Name]

                            $spFieldUserValue = New-Object Microsoft.SharePoint.SPFieldUserValue($subSiteWeb, $SpGroup.ID, $SpGroup.Name); 

                            $ValueCollection.Add($spFieldUserValue)
                        } 
                        else 
                        {
                            $spFieldUserValue = New-Object Microsoft.SharePoint.SPFieldUserValue($subSiteWeb, $member.ID, $member.LoginName); 

                            $ValueCollection.Add($spFieldUserValue)    
                        }
                    }

                    $newCategoryItem["AccessTo"] = $ValueCollection;

                    $newCategoryItem["LinkHeader"] = $headerLookupValue
                    $newCategoryItem["ContentType"] = "Navigation Category"
                    #Update the object so it gets saved to the list
                    $newCategoryItem.Update()
                }
            }
        }
        else
        {
            $categoryItems = $navigationList.Items | ?{$_.ContentType.Name -eq "Navigation Category" -and $_["CategoryTitle"] -eq $folderGroup.LibraryLinkName}
        
            if($categoryItems.Count -lt 1)
            {
                $newCategoryItem = $navigationList.Items.Add()
 
                #Add newCategoryItem to this list item
                $newCategoryItem["CategoryTitle"] = $folderGroup.LibraryLinkName
                $newCategoryItem["OrderCategories"] = $folderGroup.OrderCategories
                $listUrl = ''
                $listDesc = $folderGroup.LibraryLinkName
                #$newCategoryItem["LinkCategoryUrl"] = "$listUrl , $listDesc"


                $everyOneUser = $spWeb.AllUsers["c:0(.s|true"];
                $everyOneUserValue = New-Object Microsoft.SharePoint.SPFieldUserValue($spWeb, $everyOneUser.Id, $everyOneUser.DisplayName)
                $newCategoryItem["AccessTo"] = $everyOneUserValue; 


                $headerLookupItem = $navigationList.Items | where {$_["HeaderTitle"]  -eq $subSiteWeb.Title}
                $headerLookupValue = New-Object Microsoft.SharePoint.SPFieldLookupValue($headerLookupItem.ID,$headerLookupItem.ID.ToString());
                $newCategoryItem["LinkHeader"] = $headerLookupValue
                $newCategoryItem["ContentType"] = "Navigation Category"
                #Update the object so it gets saved to the list
                $newCategoryItem.Update()
            }
        }
		<#
        # Create Parent Folder
        $parentFolder = $spList.Folders.Add("",[Microsoft.SharePoint.SPFileSystemObjectType]::Folder,$folderGroup.Root)
        $parentFolder.Update()
        Write-Host "The Parent folder" $folderGroup.Root "was successfully created" -foregroundcolor Green
		#>
		
 
        if($folderGroup.Level1 -ne '')
        {
            
		    $linkItems = $navigationList.Items | ?{$_.ContentType.Name -eq "Navigation Link" -and $_["LinkName"] -eq $folderGroup.TopLevelFolder}
                
            if($linkItems.Count -lt 1)
            {
                $folderUrl = $splist.RootFolder.ServerRelativeUrl + "/" + $folderGroup.Level1
                $targetFolder = $subSiteWeb.GetFolder($folderUrl)

                $newLinkItem = $navigationList.Items.Add()
                if($targetFolder.Exists -eq $true)
		        {
                    $parentFolderUrl = $targetFolder.ServerRelativeUrl
                    $parentFolderTitle = $targetFolder.DisplayName
                    $newLinkItem["LinkUrl"] = "$parentFolderUrl , $parentFolderTitle"
                }
                

                $LibraryLinkName = $folderGroup.LibraryLinkName

                
 
                #Add newCategoryItem to this list item
                $newLinkItem["LinkName"] = $folderGroup.TopLevelFolder

                [Microsoft.SharePoint.SPFieldUserValueCollection] $ValueCollection = new-object Microsoft.SharePoint.SPFieldUserValueCollection

				Write-Host "Checking Role Defintion Bindings For Folders " -ForegroundColor Green

                    $sObject = [Microsoft.SharePoint.SPSecurableObject]$targetFolder.Item

                    foreach ($roleAssignment in $sObject.RoleAssignments) 
                    {
                        $member = $roleAssignment.Member
    
                        $OtherThanLimitedAccess = $false

                        foreach ($definition in $roleAssignment.RoleDefinitionBindings) {
         
                             if($definition.Name -ne "Limited Access")
                             {
                                $OtherThanLimitedAccess = $true

                                Write-Host "Role Defintion Bindings " $definition.Name -ForegroundColor Green

                                break
                             }
         
                        }
    
                        if($OtherThanLimitedAccess -eq $true)
                        {
                            Write-Host "Member " $member.ID " Name: " $member.Name -ForegroundColor Yellow

                            if ($member -is [Microsoft.SharePoint.SPGroup] -and $member.Name -ne 'Style Resource Readers') 
                            {
                                
                                $SpGroup = $subSiteWeb.Groups[$member.Name]

                                $spFieldUserValue = New-Object Microsoft.SharePoint.SPFieldUserValue($subSiteWeb, $SpGroup.ID, $SpGroup.Name); 

                                $ValueCollection.Add($spFieldUserValue)
                            } 
                            else 
                            {
                                $spFieldUserValue = New-Object Microsoft.SharePoint.SPFieldUserValue($subSiteWeb, $member.ID, $member.LoginName); 

                                $ValueCollection.Add($spFieldUserValue)    
                            }

                            
                        }
                    }

                    $newLinkItem["AccessTo"] = $ValueCollection;  
                            
                $headerLookupItem = $navigationList.Items | where {$_["HeaderTitle"]  -eq $subSiteWeb.Title}
                $headerLookupValue = New-Object Microsoft.SharePoint.SPFieldLookupValue($headerLookupItem.ID,$headerLookupItem.ID.ToString());
                $newLinkItem["LinkHeader"] = $headerLookupValue

                $categoryLookupItems = $navigationList.Items | where {$_["CategoryTitle"]  -eq $LibraryLinkName}

                if($categoryLookupItems.Count -gt 0)
                {
                    if($categoryLookupItems.Count -gt 1)
                    {
                        foreach($categoryLookupItem in $categoryLookupItems)
                        {
                           $headerLookupFieldValue = $categoryLookupItem["LinkHeader"] -as [Microsoft.SharePoint.SPFieldLookupValue]
                           $headerLookupValue = $headerLookupFieldValue.LookupValue;

                           if($headerLookupValue -eq $subSiteWeb.Title)
                           {
                             $categoryLookupValue = New-Object Microsoft.SharePoint.SPFieldLookupValue($categoryLookupItem.ID,$LibraryLinkName);
                             $newLinkItem["LinkCategory"] = $categoryLookupValue
                            
                           }
                        }

                
                    }
                    else
                    {

                        $categoryLookupValue = New-Object Microsoft.SharePoint.SPFieldLookupValue($categoryLookupItems.ID,$LibraryLinkName);
                        $newLinkItem["LinkCategory"] = $categoryLookupValue
                    }
                }

                $newLinkItem["ContentType"] = "Navigation Link"
                #Update the object so it gets saved to the list
                $newLinkItem.Update()
            }
            else
            {
                $linkItemExist = $false
                foreach($linkItem in $linkItems)
                {
                   $headerLookupFieldValue = $linkItem["LinkHeader"] -as [Microsoft.SharePoint.SPFieldLookupValue]
                   $headerLookupValue = $headerLookupFieldValue.LookupValue;

                   $categoryLookupFieldValue = $linkItem["LinkCategory"] -as [Microsoft.SharePoint.SPFieldLookupValue]
                   $categoryLookupValue = $categoryLookupFieldValue.LookupValue;

                   if(($headerLookupValue -eq $subSiteWeb.Title) -And ($categoryLookupValue -eq $folderGroup.LibraryLinkName))
                   {
                     $linkItemExist = $true
                     break
                   }
                }

                if($linkItemExist -eq $false)
                {
                    $folderUrl = $splist.RootFolder.ServerRelativeUrl + "/" + $folderGroup.Level1
                    $targetFolder = $subSiteWeb.GetFolder($folderUrl)

                    $newLinkItem = $navigationList.Items.Add()
                    if($targetFolder.Exists -eq $true)
		            {
                        $parentFolderUrl = $targetFolder.ServerRelativeUrl
                        $parentFolderTitle = $targetFolder.DisplayName
                        $newLinkItem["LinkUrl"] = "$parentFolderUrl , $parentFolderTitle"
                    }
                

                    $LibraryLinkName = $folderGroup.LibraryLinkName

                
 
                    #Add newCategoryItem to this list item
                    $newLinkItem["LinkName"] = $folderGroup.TopLevelFolder

					Write-Host "Checking Role Defintion Bindings For Folders " -ForegroundColor Green

                    [Microsoft.SharePoint.SPFieldUserValueCollection] $ValueCollection = new-object Microsoft.SharePoint.SPFieldUserValueCollection

                    $sObject = [Microsoft.SharePoint.SPSecurableObject]$targetFolder.Item

                    foreach ($roleAssignment in $sObject.RoleAssignments) 
                    {
                        $member = $roleAssignment.Member
    
                        $OtherThanLimitedAccess = $false

                        foreach ($definition in $roleAssignment.RoleDefinitionBindings) {
         
                             if($definition.Name -ne "Limited Access")
                             {
                                $OtherThanLimitedAccess = $true

                                Write-Host "Role Defintion Bindings " $definition.Name -ForegroundColor Green

                                break
                             }
         
                        }
    
                        if($OtherThanLimitedAccess -eq $true)
                        {
                            Write-Host "Member " $member.ID " Name: " $member.Name -ForegroundColor Yellow


                            if ($member -is [Microsoft.SharePoint.SPGroup] -and $member.Name -ne 'Style Resource Readers') 
                            {
                                $SpGroup = $subSiteWeb.Groups[$member.Name]

                                $spFieldUserValue = New-Object Microsoft.SharePoint.SPFieldUserValue($subSiteWeb, $SpGroup.ID, $SpGroup.Name); 

                                $ValueCollection.Add($spFieldUserValue)
                            } 
                            else 
                            {
                                $spFieldUserValue = New-Object Microsoft.SharePoint.SPFieldUserValue($subSiteWeb, $member.ID, $member.LoginName); 

                                $ValueCollection.Add($spFieldUserValue)    
                            }
                        }
                    }

                    $newLinkItem["AccessTo"] = $ValueCollection; 
                            
                    $headerLookupItem = $navigationList.Items | where {$_["HeaderTitle"]  -eq $subSiteWeb.Title}
                    $headerLookupValue = New-Object Microsoft.SharePoint.SPFieldLookupValue($headerLookupItem.ID,$headerLookupItem.ID.ToString());
                    $newLinkItem["LinkHeader"] = $headerLookupValue

                    $categoryLookupItems = $navigationList.Items | where {$_["CategoryTitle"]  -eq $LibraryLinkName}
                    if($categoryLookupItems.Count -gt 0)
                    {
                        if($categoryLookupItems.Count -gt 1)
                        {
                            foreach($categoryLookupItem in $categoryLookupItems)
                            {
                               $headerLookupFieldValue = $categoryLookupItem["LinkHeader"] -as [Microsoft.SharePoint.SPFieldLookupValue]
                               $headerLookupValue = $headerLookupFieldValue.LookupValue;

                               if($headerLookupValue -eq $subSiteWeb.Title)
                               {
                                 $categoryLookupValue = New-Object Microsoft.SharePoint.SPFieldLookupValue($categoryLookupItem.ID,$LibraryLinkName);
                            $newLinkItem["LinkCategory"] = $categoryLookupValue
                                 
                               }
                            }

                
                        }
                        else
                        {

                            $categoryLookupValue = New-Object Microsoft.SharePoint.SPFieldLookupValue($categoryLookupItems.ID,$LibraryLinkName);
                            $newLinkItem["LinkCategory"] = $categoryLookupValue
                        }
                    }

                    $newLinkItem["ContentType"] = "Navigation Link"
                    #Update the object so it gets saved to the list
                    $newLinkItem.Update()
                }
            }
        }
        
    }
    $spWeb.Dispose()
    $subSiteWeb.Dispose()
}

function CreateNavForFolders([String]$configFileName = "")
{

    [string] $currentLocation = Get-Location

	Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green

    
    # Check that the config file exists.
    if (-not $(Test-Path -Path $configFileName -Type Leaf))
    {
	    Write-Error -message ("Configuration file '" + $configFileName + "' does not exist.") -Red
    }

    $configXml = [xml]$(get-content $configFileName)
    if( $? -eq $false ) 
    {
	    Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red`
    }

    if ($configXml.SubSiteFolders)
    {
	    foreach ($SubSite in $configXml.SubSiteFolders.Site)
	    {
		    Write-Host "Creating navigation for " + $SubSite.SubSiteUrl + " with " + $SubSite.CSVFile + "CSV file" -ForegroundColor Yellow

                $csvFullPath = $currentLocation + "\DataFiles\nav\" + $SubSite.CSVFile
                Create-TopNavigation -SiteUrl $SubSite.SiteUrl -SubSiteUrl $SubSite.SubSiteUrl -CSVPath $csvFullPath

                Write-Host "Successfully Created navigation for " + $SubSite.SubSiteUrl + " with " + $SubSite.CSVFile + "CSV file" -ForegroundColor Green
        
		
        }
    }

}