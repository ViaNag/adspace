function GetHelp() {


$HelpText = @"

DESCRIPTION:
NAME: SP-WebAppBuilder
Creates Web Applications and Site Collections in Sharepoint 2010

PARAMETERS: 
-ConfigPath	 Path to the config xml-file containing the web Applications and Site Collections 
-webapplication	 Set to yes or no if you want to create Web Applications or not
-SiteCollection	 Set to yes or no if you want to create Site Collections or not	

-help		Displays the help topic

SYNTAX:

SP-WebAppBuilder -configpath config.xml -WebApplication yes -SiteCollection yes
Creates both Web Applications and the root site collection specified in the config xml-file.

SP-WebAppBuilder -configpath config.xml -WebApplication yes -SiteCollection no
Creates the Web Application but not the root site collections

SP-WebAppBuilder -help
Displays the help topic for the script

"@
$HelpText

}

if($help) { GetHelp; Continue }


# =================================================================================
#
#Main Function
#
# =================================================================================
function SP-WebAppSiteBuilder([string]$ConfigPath = ("Config.xml"))
{
 	$WebApplication = "yes"
	$SiteCollection = "yes"
	$cfg = [xml](get-content $ConfigPath)

	# Exit if config file is invalid
	if( $? -eq $false ) 
    {
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red
	}
	
	Write-Host "Sucessfully read config file $ConfigPath file" -ForegroundColor Green
	if($Error.Count -eq 0)
	{
		# Loop Through and create/Extend all web applications
		$OnlySiteCollections = $true
		foreach($webApplicationConfiguration in $cfg.Configuration.Farm.WebApplications.WebApplication)
		{
			if($WebApplication.ToLower() -eq "yes" -and $SiteCollection.ToLower() -eq "yes") 
		    { 
				createWebApp $webApplicationConfiguration $SiteCollection
		    }
			elseif($WebApplication.ToLower() -eq "yes" -and $SiteCollection.ToLower() -eq "no")
	    	{ 
				createWebApp $_ $SiteCollection.ToLower() $SiteCollection
			}
			elseif($WebApplication.ToLower() -eq "no" -and $SiteCollection.ToLower() -eq "yes")
		    {
				if($_.Type -eq "New")
		        {
					CreateSiteCollection $webApplicationConfiguration.Sites
			    }
			}
		}
	}
	else
	{
		Write-Host $Error -ForegroundColor Red
	}
}
# =================================================================================
#
# FUNC: CreateSiteCollection
# DESC: Creates the root site collection in each web application.
#
# =================================================================================
function CreateSiteCollection([object] $sitesConfiguration)
{
	foreach($siteConfiguration in $sitesConfiguration.Site)
	{
		if(!($siteConfiguration.Title.ToString() -eq ""))
		{
			Write-Host "Creating Site Collection:" $siteConfiguration.Title "(" $siteConfiguration.Path ")." -ForegroundColor Green
			$SiteCollection = New-SPSite -Url $siteConfiguration.Path -Name $siteConfiguration.Title -Description $siteConfiguration.Description -Language 1033 -Template $siteConfiguration.Template -OwnerAlias $siteConfiguration.AdminAccount
			if($Error.Count -eq 0)
			{
				Write-Host "Success: Creating Site Collection:" $siteConfiguration.Title "(" $siteConfiguration.Path ")." -ForegroundColor Green
			}
			else
			{
				Write-Host "Error: Creating Site Collection:" $siteConfiguration.Title "(" $siteConfiguration.Path "). Exiting script..." -foregroundcolor Red
				Write-Host $Error -ForegroundColor Red
			}
			
			$primaryOwner = ""
 			$secondaryOwner = ""
  			if ($siteConfiguration.CreateDefaultGroups -eq "true")
			{
	 			Write-Host "Removing any existing visitors group for Site collection $($SiteConfiguration.Path)" -foregroundcolor blue
	 			#This is here to fix the situation where a visitors group has already been assigned
	 			#$SiteCollection.RootWeb.AssociatedVisitorGroup = $null;
	 			$SiteCollection.RootWeb.Update();
				Write-Host "Creating Owners group for Site collection $($SiteConfiguration.Path)" -foregroundcolor blue
	 			$SiteCollection.RootWeb.CreateDefaultAssociatedGroups($primaryOwner, $secondaryOwner, $siteConfiguration.Title)
	 			$SiteCollection.RootWeb.Update();
			}
 		}	
	}
}

# =================================================================================
#
# FUNC: CreateSiteCollection
# DESC: Creates the root site collection in each web application.
#
# =================================================================================
function AddContentDatabase([object] $contentDBsConfiguration, $webAppURL)
{
	if($contentDBsConfiguration -ne $null)
	{
		foreach($contentDBConfiguration in $contentDBsConfiguration.ContentDatabase)
		{
			if(!($contentDBConfiguration.DatabaseName.ToString() -eq ""))
			{
				Write-Host "Adding Content Database: " $contentDBConfiguration.DatabaseName "." -ForegroundColor Green
				
				New-SPContentDatabase -DatabaseServer $contentDBConfiguration.DatabaseServer -Name $contentDBConfiguration.DatabaseName -WebApplication $webAppURL
				if($Error.Count -eq 0)
				{
					Write-Host "Success: Adding Content Database: " $contentDBConfiguration.DatabaseName "." -ForegroundColor Green
				}
				else
				{
					Write-Host "Error: Adding Content Database: " $contentDBConfiguration.DatabaseName "." -ForegroundColor Red
					Write-Host $Error -ForegroundColor Red
					Break 
				}			
			}	
		}
	}
}


# =================================================================================
#
# FUNC: createWebApp
# DESC: Creates and extends a web application.
#		It also creates a Root Site Collection if specified
#		Everyting needs to be specified in the object
#
# =================================================================================
function CreateWebApp([object] $webAppConfiguration, $CreateSite) 
{
	# Create New web application
	if( $webAppConfiguration.Type -eq "New")
	{
		# Set the WebApplicationBuilderObject and set it's properties from the config-file
		Write-Host "Creating Web Application: " $webAppConfiguration.name -ForegroundColor Green
		
		$ap = New-SPAuthenticationProvider –UseWindowsIntegratedAuthentication –DisableKerberos
		
		$webapp = New-SPWebApplication -Name $webAppConfiguration.WebAppDisplayName -Port $webAppConfiguration.WebAppPort -HostHeader $webAppConfiguration.WebAppHostHeader –AuthenticationProvider $ap -ApplicationPool $webAppConfiguration.WebAppPoolName –ApplicationPoolAccount (Get-SPManagedAccount $webAppConfiguration.WebAppPoolUsername) -DatabaseName $webAppConfiguration.WebAppContentDBName -DatabaseServer $Configuration.Configuration.Farm.Servers.DBServer.Name.value -Path $webAppConfiguration.IISRootDirectory -SecureSocketsLayer:([bool]::Parse($webAppConfiguration.SSL)) -AllowAnonymousAccess:([bool]::Parse($webAppConfiguration.AllowAnonymousAccess))
		
		if($Error.Count -eq 0)
		{
			Write-Host "Success: Creating Web Application: " $webAppConfiguration.name "." -ForegroundColor Green
			Write-Host "Provisioning " $webAppConfiguration.name	-ForegroundColor Green
		}
		else
		{
			Write-Host "Error: Creating Web Application: " $webAppConfiguration.name ". Exiting Script...." -ForegroundColor Red
			Write-Host $Error -ForegroundColor Red
		}
		
		# Provision the Web Application to all WFE's
		if($Error.Count -eq 0)
		{
			$webapp.ProvisionGlobally()
		}
		
		#Managed Paths
		if($Error.Count -eq 0)
		{
			if ($webAppConfiguration.ManagedPaths -ne "")
			{
				foreach($ManagedPath in $webAppConfiguration.ManagedPaths.ManagedPath)
				{
					New-SPManagedPath -RelativeURL $ManagedPath.RelativeURL -WebApplication $webapp -Explicit
					if($Error.Count -gt 0)
					{
						Write-Host $Error -ForegroundColor Red
						Break
					}

				}
			}
		}
		
		#Web Level Solutions
		if($Error.Count -eq 0)
		{
			if ($webAppConfiguration.Solutions -ne "")
			{	
				foreach($solutionConfig in $webAppConfiguration.Solutions.Solution)
				{
					Install-SPSolution -Identity $solutionConfig.name -Force -WebApplication $webapp -GACDeployment
					WaitForJobToFinish
					IISReset
					if($Error.Count -gt 0)
					{
						Write-Host $Error -ForegroundColor Red
						Break
					}
				}
			}
		}
		
		#Content Databases
		if($Error.Count -eq 0)
		{
			if($webAppConfiguration.ContentDatabases -ne "")
			{
				AddContentDatabase $webAppConfiguration.ContentDatabases $webAppConfiguration.LoadBalancerURL
			}
		}
		
		if($Error.Count -eq 0)
		{
			# Create The Root Site Collection
			if($CreateSite -eq "yes") 
			{
				CreateSiteCollection $webAppConfiguration.Sites
       		}
		}
	}
	elseif ( $webAppConfiguration.Type -eq "Extend")
	{
		if($Error.Count -eq 0)
		{
			# Extend the web application
			Write-Host "Extending web application" $webAppConfiguration.WebAppDisplayName -ForegroundColor Green
			if($webAppConfiguration.Authentication.Type -eq "ASPNET")
			{
	        	$ap = New-SPAuthenticationProvider -ASPNETMembershipProvider $webAppConfiguration.MembershipProvider -ASPNETRoleProviderName $webAppConfiguration.RoleManager
				Get-SPWebApplication –Identity $webAppConfiguration.Extend | New-SPWebApplicationExtension -Name $webAppConfiguration.WebAppDisplayName -HostHeader $webAppConfiguration.WebAppHostHeader -Port $webAppConfiguration.WebAppPort -Zone $webAppConfiguration.Zone -URL $webAppConfiguration.LoadBalancerURL.ToString() -AuthenticationProvider $ap -SignInRedirectURL $webAppConfiguration.SignInURL -Path $webAppConfiguration.IISRootDirectory -AllowAnonymousAccess:([bool]::Parse($webAppConfiguration.AllowAnonymousAccess))
			}
			elseif($webAppConfiguration.Authentication.Type -eq "ADFS")
			{
				$ap = Get-SPTrustedIdentityTokenIssuer $webAppConfiguration.Authentication.ADFSServer
				if($ap.ProviderRealms.ContainsKey($webAppConfiguration.LoadBalancerURL.ToString()) -eq $true)
				{
	    			$ap.ProviderRealms.Remove($webAppConfiguration.LoadBalancerURL.ToString())
				}
				$uri = new-object System.Uri($webAppConfiguration.LoadBalancerURL.ToString())
				$ap.ProviderRealms.Add($uri, $webAppConfiguration.Authentication.Realms)
				$ap.Update()
				Get-SPWebApplication -Identity $webAppConfiguration.Extend | New-SPWebApplicationExtension -Name $webAppConfiguration.WebAppDisplayName -HostHeader $webAppConfiguration.WebAppHostHeader -Zone $webAppConfiguration.Zone -URL $webAppConfiguration.LoadBalancerURL.ToString() -SecureSocketsLayer:([bool]::Parse($webAppConfiguration.SSL)) -Port $webAppConfiguration.WebAppPort -AuthenticationProvider $ap -Path $webAppConfiguration.IISRootDirectory -AllowAnonymousAccess:([bool]::Parse($webAppConfiguration.AllowAnonymousAccess))
			}
			elseif($webAppConfiguration.Authentication.Type -eq "Windows")
			{
					$ap = New-SPAuthenticationProvider –UseWindowsIntegratedAuthentication –DisableKerberos:([bool]::Parse($webAppConfiguration.Authentication.DisableKerberos))
					Get-SPWebApplication -Identity $webAppConfiguration.Extend | New-SPWebApplicationExtension -Name $webAppConfiguration.WebAppDisplayName -HostHeader $webAppConfiguration.WebAppHostHeader -Zone $webAppConfiguration.Zone -URL $webAppConfiguration.LoadBalancerURL.ToString() -SecureSocketsLayer:([bool]::Parse($webAppConfiguration.SSL)) -Port $webAppConfiguration.WebAppPort -AuthenticationProvider $ap -Path $webAppConfiguration.IISRootDirectory -AllowAnonymousAccess:([bool]::Parse($webAppConfiguration.AllowAnonymousAccess))
			}
		}
		if($Error.Count -eq 0)
		{
			Write-Host "Success: Creating Web application: " $webAppConfiguration.WebAppDisplayName "." -ForegroundColor Green
		}
		else
		{
			Write-Host "Error: While Creating Web application: " $webAppConfiguration.WebAppDisplayName ". Exiting Script..." -ForegroundColor Red
			
		}		
	}

	
}

function WaitForJobToFinish([string]$SolutionFileName)
{ 
    $JobName = "*solution-deployment*$SolutionFileName*"
    $job = Get-SPTimerJob | ?{ $_.Name -like $JobName }
    if ($job -eq $null) 
    {
        Write-Host "Timer job not found" -ForegroundColor Green
    }
    else
    {
        $JobFullName = $job.Name
        Write-Host -NoNewLine "Waiting to finish job $JobFullName" -ForegroundColor DarkBlue
        
        while ((Get-SPTimerJob $JobFullName) -ne $null) 
        {
            Write-Host -NoNewLine . -ForegroundColor DarkBlue
            Start-Sleep -Seconds 2
        }
        Write-Host  "Finished waiting for job.." -ForegroundColor Green
    }
}

