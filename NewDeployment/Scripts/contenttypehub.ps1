﻿
function Publish-ContentTypeHub {
    param
    (
        [parameter(mandatory=$true)][string]$CTHUrl,
        [parameter(mandatory=$true)][string]$Name
    )
 
    $site = Get-SPSite $CTHUrl
    if(!($site -eq $null))
    {
        $contentTypePublisher = New-Object Microsoft.SharePoint.Taxonomy.ContentTypeSync.ContentTypePublisher ($site)
        $site.RootWeb.ContentTypes | ? {$_.Name -match $Name} | % {
            $contentTypePublisher.Publish($_)
            write-host "Content type" $_.Name "has been republished" -foregroundcolor Green
        }
    }
}

function PublishContentTypes([string]$configFileName)
{
    # Check that the config file exists.
	if (-not $(Test-Path -Path $configFileName -Type Leaf))
	{
		Write-Error -message ("Configuration file '" + $configFileName + "' does not exist.") -Red
	}

	$configXml = [xml]$(get-content $configFileName)
	if( $? -eq $false ) 
	{
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red`
	}

	Write-Host "Creating sites" -ForegroundColor Green
    
	if ($configXml.Configuration.ContentTypes)
	{
		foreach ($contentType in $configXml.Configuration.ContentTypes.ContentType)
		{
            if(!($contentType.Path.ToString() -eq "" -or $contentType.Name.ToString() -eq ""))
		    {
                Write-Host "Publishing CT :" $contentType.Name "(" $contentType.Path ")." -ForegroundColor Green

                Set-SPMetadataServiceApplication -Identity $contentType.MMDServiceName -HubURI $contentType.Path
                # Consumes content types from the Content Type Gallery 
                $metadataserviceapplicationname = $contentType.MMDServiceName 
                $metadataserviceapplicationproxy = get-spmetadataserviceapplicationproxy $metadataserviceapplicationname 
                $metadataserviceapplicationproxy.Properties["IsNPContentTypeSyndicationEnabled"] = $true 
                $metadataserviceapplicationproxy.Update() 
                Write-Host "Property Updated successfully"
                Publish-ContentTypeHub $contentType.Path $contentType.Name
            }

        }
    }
}
