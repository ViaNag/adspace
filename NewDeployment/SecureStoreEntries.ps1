#Sharepoint cmdlets 
#.\CreateSecureStoreApplicationEntries.ps1 
# Set variables

# ===================================================================================
# LOAD SHAREPOINT POWERSHELL SNAPIN
# ===================================================================================
$snapin = Get-PSSnapin | Where-Object {$_.Name -eq 'Microsoft.SharePoint.Powershell'}
$ServiceContext = "string";
if ($snapin -eq $null) 
{
    Write-Host "Loading SharePoint Powershell Snapin"
    Add-PSSnapin "Microsoft.SharePoint.Powershell"
}

function Create-SecureStoreApplication(
    [string]$settingsFile = ".\Configuration\SecureStoreConfig.xml") {
    [xml]$config = Get-Content $settingsFile
    
    $config.SecureStoreApplications| ForEach-Object {
           Write-Host "Started Checking if Secure Store Application Exists : $($_.SSSApplicationName)..." 
           $ServiceContext = $_.ServiceContext;
           If ($_.SSSApplicationName -eq $null) {$secureStoreApplicationName = "State Service Application"}
            # Get the service instance
           $SecureStoreServiceInstances = Get-SPServiceInstance | ? {$_.GetType().Equals([Microsoft.Office.SecureStoreService.Server.SecureStoreServiceInstance])}
           $SecureStoreServiceInstance = $SecureStoreServiceInstances | ? {$_.Server.Address -eq $env:COMPUTERNAME}
            If (-not $?) { Throw " - Failed to find Secure Store service instance" }
            # Start Service instance
            If ($SecureStoreServiceInstance.Status -eq "Disabled")
            {
	           Write-Host  " - Starting Secure Store Service Instance..."
	           $SecureStoreServiceInstance.Provision()
	           If (-not $?) { Throw " - Failed to start Secure Store service instance" }
	           # Wait
	           Write-Host  " - Waiting for Secure Store service..." -NoNewline -foregroundcolor Yellow     
	           While ($SecureStoreServiceInstance.Status -ne "Online")
	           {
	               Write-Host  "." -NoNewline
		          Start-Sleep 1
		          $SecureStoreServiceInstances = Get-SPServiceInstance | ? {$_.GetType().ToString() -eq "Microsoft.Office.SecureStoreService.Server.SecureStoreServiceInstance"}
		          $SecureStoreServiceInstance = $SecureStoreServiceInstances | ? {$_.Server.Address -eq $env:COMPUTERNAME}
	           }
	           Write-Host  $($SecureStoreServiceInstance.Status)
            }
			# Create Service Application
			$GetSPSecureStoreServiceApplication = Get-SPServiceApplication | ? {$_.GetType().Equals([Microsoft.Office.SecureStoreService.Server.SecureStoreServiceApplication])}
			If ($GetSPSecureStoreServiceApplication -eq $Null)
			{
				Write-Host  " - Creating Secure Store Service Application..." -foregroundcolor Green     
				New-SPSecureStoreServiceApplication -Name $secureStoreApplicationName -PartitionMode:$false -Sharing:$false -DatabaseName $secureStoreDatabaseName -ApplicationPool $serviceApplicationPool -AuditingEnabled:$true -AuditLogMaxSize 30 | Out-Null
				Write-Host  " - Creating Secure Store Service Application Proxy..." -foregroundcolor Green     
				Get-SPServiceApplication | ? {$_.GetType().Equals([Microsoft.Office.SecureStoreService.Server.SecureStoreServiceApplication])} | New-SPSecureStoreServiceApplicationProxy -Name $secureStoreApplicationProxyName -DefaultProxyGroup | Out-Null			
				Write-Host  " - Done creating Secure Store Service Application." -foregroundcolor Green     
	
										
			}
			Else {Write-Host  " - Secure Store Service Application already provisioned." -foregroundcolor Green}      
			if($_.UpdateMasterKey -eq $true)
			{
				$secureStore = Get-SPServiceApplicationProxy | Where {$_.GetType().Equals([Microsoft.Office.SecureStoreService.Server.SecureStoreServiceApplicationProxy])}
				Start-Sleep 5
				Write-Host  " - Creating the Master Key..." -foregroundcolor Green     
				Update-SPSecureStoreMasterKey -ServiceApplicationProxy $secureStore.Id -Passphrase $_.passPhrase
				Start-Sleep 5
				Write-Host  " - Creating the Application Key..." -foregroundcolor Green     
				Update-SPSecureStoreApplicationServerKey -ServiceApplicationProxy $secureStore.Id -Passphrase $_.passPhrase -ErrorAction SilentlyContinue
			}
			else
			{
				Write-Host  " - Skipping Updating Master Key" -foregroundcolor Yellow     
			}

        }  
        
         foreach( $Node in $config.SecureStoreApplications.SecureStoreApplication) 
          { 
             Write-Host "Started Creating Application Entry for Application :  $($Node.ExternalApplicationName)..." -foregroundcolor Green     
             $ssApp = Get-SPSecureStoreApplication –ServiceContext $serviceContext –Name $Node.ExternalApplicationName -ErrorAction SilentlyContinue
            $adminClaim = New-SPClaimsPrincipal –Identity $Node.Administrator –IdentityType  $Node.IdentityTypeAdmin
            $userClaim = New-SPClaimsPrincipal –Identity $Node.MemeberName –IdentityType  $Node.IdentityTypeMember
            #$userClaim = New-SPClaimsPrincipal -ClaimValue "contoso\" –ClaimType role –TrustedIdentityTokenIssuer "SAML Provider" –IdentifierClaim Yes
            $fields = @()
            $credentialValues = @()
            foreach($ChildNode in $Node.SelectNodes('SecureStoreField'))
            {              
              $fields += New-SPSecureStoreApplicationField –Name $ChildNode.Name -Type $ChildNode.Type –Masked:$false            
              $credentialValues += ConvertTo-SecureString $ChildNode.Value –AsPlainText –Force            
               
            }
            
            if($ssApp -eq $Null)
            {

             Write-Host  " - Creating the External Application :$($Node.ExternalApplicationName) "	-foregroundcolor Green     
            $targetApp = new-spsecurestoretargetapplication -name $Node.ExternalApplicationName -friendlyname $Node.ExternalApplicationName -contactemail $Node.AdministratorEmail -applicationtype $Node.applicationtype -timeoutinminutes 3
            $ssApp = New-SPSecureStoreApplication –ServiceContext $serviceContext –TargetApplication $targetApp –Field $fields –Administrator $adminClaim -CredentialsOwnerGroup $userClaim
	
            }
            else
            {
                Write-Host  " - External Application Found :$($Node.ExternalApplicationName) and  updating credentials" -foregroundcolor Green     
            }
             Update-SPSecureStoreGroupCredentialMapping  –Identity $ssApp –Values $credentialValues
       } 
        
        Write-Host "Secure Store Application Entries  Created/updated"  -foregroundcolor Green     
     
	}
 
# Execute the script			
Create-SecureStoreApplication