﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;

namespace Viacom.AdSpace.Announcement.WF.Activity
{

    public sealed class CopyItemCodeActivity : CodeActivity
    {
        /// <summary>
        /// Gets or sets a value for ListId
        /// </summary>
        public InArgument<string> SiteUrl { get; set; }

        /// <summary>
        /// Gets or sets a value for ListId
        /// </summary>
        public InArgument<string> ListName { get; set; }

        /// <summary>
        /// Gets or sets a value for ListItem
        /// </summary>
        public InArgument<string> ItemId { get; set; }

        /// <summary>
        /// Gets or sets a value for ListItem
        /// </summary>
        public InArgument<string> FolderName { get; set; }

        /// <summary>
        /// Gets or sets a value for ListItem
        /// </summary>
        public OutArgument<string> OutputStatus { get; set; }

        // If your activity returns a value, derive from CodeActivity<TResult>
        // and return the value from the Execute method.
        protected override void Execute(CodeActivityContext context)
        {
            //// Obtain the runtime value of the Text input argument
            //string text = context.GetValue(this.Text);
            //this.TextOut.Set(context, "Return from WF");
            try
            {
                string siteUrl = context.GetValue<string>(this.SiteUrl);
                string listName = context.GetValue<string>(this.ListName);
                string itemID = context.GetValue<string>(this.ItemId);
                string folderName = context.GetValue<string>(this.FolderName);
                // validate
                if (siteUrl == null || listName == null || itemID == null || folderName == null)
                {
                    throw new Exception("Site url, list name, ItemID, or Folder Name cannot be null!", null);
                }
                else
                {
                    this.MoveSPListItemInsideSubFolder(siteUrl, listName, itemID, folderName);
                    this.OutputStatus.Set(context, "Item successfully moved.");
                }
            }
            catch (Exception ex)
            {
                this.OutputStatus.Set(context, "Item move failed: " + ex.Message);
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("AdSpace Workflow Error", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.StackTrace, null);
            }
        }

        /// <summary>
        /// Move item inside list folder
        /// </summary>
        /// <param name="siteUrl"></param>
        /// <param name="listName"></param>
        /// <param name="itemID"></param>
        /// <param name="folderName"></param>
        private void MoveSPListItemInsideSubFolder(string siteUrl, string listName, string itemID, string folderName)
        {
            using (SPSite site = new SPSite(siteUrl))
            {
                if (site != null)
                {
                    // Get the web site
                    using (SPWeb web = site.OpenWeb())
                    {
                        if (web != null)
                        {
                            classEventFiring objclassEventFiring = new classEventFiring();
                            objclassEventFiring.DisableHandleEventFiring();
                            // Get the list
                            SPList list = web.Lists[listName];
                            SPListItem item = list.GetItemById(Convert.ToInt32(itemID));
                            SPFile file = web.GetFile(item.Url);
                            if (!file.Url.Contains(folderName))
                            {
                                string NewDestinationUrl = file.Url.Replace(item.ID.ToString() + "_.000", folderName + "/" + item.ID.ToString() + "_.000");
                                file.MoveTo(NewDestinationUrl);
                                //if(!web.IsRootWeb)
                                //    item["AnnouncementSource"] = web.Title;
                                item.SystemUpdate();
                            }
                            objclassEventFiring.EnableHandleEventFiring();
                        }
                    }
                }
            }
        }
    }

    public class classEventFiring : SPItemEventReceiver
    {
        bool oldValue = true;
        public void DisableHandleEventFiring()
        {
            oldValue = this.EventFiringEnabled;
            this.EventFiringEnabled = false;
        }

        public void EnableHandleEventFiring()
        {
            this.EventFiringEnabled = oldValue;
        }
    }
}
