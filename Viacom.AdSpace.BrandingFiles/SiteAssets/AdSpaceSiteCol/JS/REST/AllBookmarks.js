﻿$(document).ready(function () {
    AllBookmarks.AdSpaceUserName = _spPageContextInfo.userId;
    AllBookmarks.getAllBookmarks()
});
var AllBookmarks = {
    AdSpaceUserName: "",
    callModal: function (e, t) {
        OpenPopUpPageWithTitle(e, RefreshOnDialogClose, 500, 250, "Bookmark")
    },
    getAllBookmarks: function () {
        $.ajax({
            type: "GET",
            url: _spPageContextInfo.siteAbsoluteUrl + "/_api/web/lists/getbytitle('Bookmark List')/Items" + "?$select=Title,Url,ID,GroupName,GroupNameCalc,AdSpaceUserNameId" + "&$filter=AdSpaceUserName eq '" + AllBookmarks.AdSpaceUserName + "'&$orderby=GroupName",
            headers: {
                accept: "application/json;odata=verbose"
            },
            async: false,
            success: function (e) {
                if (e.d.results) {
                    if (e.d.results.length == 0) $("#MyAllBookmarkGroup").append("There are no bookmarks.");
                    var t = [];
                    var n = "";
                    var r = "";
                    for (var i = 0; i < e.d.results.length; i++) {
                        var s = e.d.results[i].GroupName;
                        var o = e.d.results[i].ID;
                        var u = "";
                        var a;
                        var f = "";
                        if (t.indexOf(s) < 0) {
                            t.push(s);
                            var l = "../Pages/BookmarkGroupEdit.aspx?BookmarkValue=" + o;
                            var c = "../Pages/BookmarkGroupDelete.aspx?BookmarkValue=" + o;
                            $("#MyAllBookmarkGroup").append("<div id='outer_" + s + "' class='outerdiv'><div class='DivBookmarkGroup'>" + "<a class='aBookmarkGroup'  style='' id='AllBookmarkcallout" + i + "'>" + s + "</a>" + "<span style='margin-left:15px'><a class='aBookmarkEdit' style='' id='" + s + "' onclick=javascript:AllBookmarks.callModal('" + l + "','Edit') >" + "<img   src='../SiteAssets/images/EditPencil.png' style='height:13px;width:13px;'>" + "</a>" + "<a class='aBookmarkDelete' id='" + s + "' style='cursor:pointer' onclick=javascript:AllBookmarks.callModal('" + c + "','Delete')>" + "<img src='../SiteAssets/images/deleteCross.jpg' style='height:13px;width:13px;'>" + "</a></span>" + "</div></div>");
                            oDivIdForBookmark = "AllBookmarkcallout" + i;
                            targetElementForAllBookmark = document.getElementById(oDivIdForBookmark);
                            allGroupID = i;
                            allGroupName = s;
                            r = AllBookmarks.GetLinks(s);
                            AllBookmarks.CreateCallOutPopupForAllBookmark(targetElementForAllBookmark, allGroupID, allGroupName, r)
                        }
                    }
                }
            },
            error: function (e) {
                alert("Error:" + e.status + ": " + e.statusText)
            }
        })
    },
    GetLinks: function (e) {
        var t = "";
        e = e.replace(" ", "%20");
        e = e.replace("&", "%26");
        e = e.replace("'", "%27%27");
        $.ajax({
            type: "GET",
            url: _spPageContextInfo.siteAbsoluteUrl + "/_api/web/lists/getbytitle('Bookmark List')/Items" + "?$select=Title,Url,ID,GroupName,GroupNameCalc,AdSpaceUserNameId" + "&$filter=GroupName eq '" + e + "' and AdSpaceUserName eq '" + AllBookmarks.AdSpaceUserName + "'",
            headers: {
                accept: "application/json;odata=verbose"
            },
            async: false,
            success: function (e) {
                if (e.d.results) {
                    for (var n = 0; n < e.d.results.length; n++) {
                        var r = e.d.results[n].Title;
                        var i = e.d.results[n].URL;
                        var s = e.d.results[n].ID;
                        t += "<br/>";
                        var o = "../Pages/BookmarkItemEdit.aspx?BookmarkValue=" + s;
                        var u = "../Pages/BookmarkItemDelete.aspx?BookmarkValue=" + s;
                        t += "<a class='bookmarkName' style='cursor:pointer' target='_top' href='" + i + "'>" + r + "</a>" + "&nbsp;<span style=float:right>" + "<a class='aBookmarkEdit' style='cursor:pointer'   id='" + s + "' onclick=javascript:AllBookmarks.callModal('" + o + "','Edit')>" + "<img  style='height:13px;width:13px' src='../SiteAssets/images/EditPencil.png'> </a>" + "&nbsp;<a class='aBookmarkDelete' style='cursor:pointer'  id='" + s + "' onclick=javascript:AllBookmarks.callModal('" + u + "','Delete')>" + "<img  style='height:13px;width:13px' src='../SiteAssets/images/deleteCross.jpg'></a></span>"
                    }
                }
            },
            error: function (e) {
                alert("Error:" + e.status + ": " + e.statusText)
            }
        });
        return t
    },
    CreateCallOutPopupForAllBookmark: function (e, t, n, r) {
        var i = new CalloutOptions;
        i.ID = "bookmark" + t;
        i.launchPoint = e;
        i.beakOrientation = "topBottom";
        i.title = n;
        i.contentWidth = 300;
        i.content = '<div class="ms-soften" style="margin-top:13px;"><hr/>' + r + "</div>";
        var s = CalloutManager.createNewIfNecessary(i);
        s.set({
            openOptions: {
                event: "click"
            }
        })
    }
}