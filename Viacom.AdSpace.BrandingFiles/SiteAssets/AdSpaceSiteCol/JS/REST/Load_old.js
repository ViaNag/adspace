﻿/*********DECLARATIONS*****/
var titleB;
var URLB;
var LinkGroupB;
var structureB="";
var siteUrlB;
var linkCategoryB;
var linkCategoryTitleB;
var indexB;
var thisSiteB;
var useridB;
var spHostUrl="";

/**Page load**/
$(document).ready(function()
{
	spHostUrl=_spPageContextInfo.siteAbsoluteUrl;
	AdSpaceUserName =_spPageContextInfo.userId;
	getLinks();
	getResourcesLinks();
	hover();
	getBookmarks();
	
	 iFrameResize({
	    autoResize: true,
	    enablePublicMethods: true,
	    checkOrigin:false,
	    sizeWidth: false
	});
	 getCurrentUser();
	 ExecuteOrDelayUntilScriptLoaded(CheckIsSuperAdmin, "sp.js");
	 });
	 
	 /**Get logged in user group**/
 function getCurrentUserGroupColl(UserID)
 {

  $.ajax
  ({
  url: "/sites/AdSpaceViacom/_api/web/GetUserById("+UserID+")/Groups",
  type: "GET",
  headers: { "Accept": "application/json; odata=verbose","X-RequestDigest": $("#__REQUESTDIGEST").val() },
  dataType: "json",
  async: true,
   success: function(data){
   var results=data.Title;
  
   
      /* get all group's title of current user. */
      for(var i=0; i<data.d.results.length; i++)
      {
            if(data.d.results[i].Title=='AdSpace Owners')
            {
           	          //           Ribbon.Documents.New.NewDocument-Large
           }
          

      }
  }
  });
}


function getCurrentUser()
{

 $.ajax
  ({
  url: "/sites/AdSpaceViacom/_api/web/CurrentUser",
  type: "GET",
  headers: { "Accept": "application/json; odata=verbose","X-RequestDigest": $("#__REQUESTDIGEST").val() },
  dataType: "json",
  async: true,
  success: function(data){
    getCurrentUserGroupColl(data.d.Id);
  }
  });
} 

function getLinks()
{
 /*** Banner Links and Footer Links ***/
	    
    if(_spPageContextInfo.webTitle=='AdSpace')
    {
	     $('#bannerLinks').append("<iframe style='width:100%;' id='bannerIframe'  src='"+spHostUrl+"/_layouts/15/appredirect.aspx?redirect_uri="+getAppDetails('redirect_uri')+"%2FPages%2FLinks%2Easpx%3FSPHostUrl%3D"+spHostUrl+"%26SPHostTitle%3DAdSpace%26SPAppWebUrl%3D"+getAppDetails('SPAppWebUrl')+"%252FViacomAdSpaceApps%26SPLanguage%3Den%252DUS%26SPClientTag%3D"+getAppDetails('SPClientTag')+"%26SPProductNumber%3D"+getAppDetails('SPProductNumber')+"%26Level%3DTop%26ListName%3DLinks%26Section%3DBanner%26SenderId%3DE08EBD621&amp;client_id="+getAppDetails('client_id')+"' id='bannerIframe' frameborder='0' scrolling='no'></iframe>");
	     $('#footerLinks').append("<iframe style='width:100%' id='footerIframe' src='"+spHostUrl+"/_layouts/15/appredirect.aspx?redirect_uri="+getAppDetails('redirect_uri')+"%2FPages%2FLinks%2Easpx%3FSPHostUrl%3D"+spHostUrl+"%26SPHostTitle%3DAdSpace%26SPAppWebUrl%3D"+getAppDetails('SPAppWebUrl')+"%252FViacomAdSpaceApps%26SPLanguage%3Den%252DUS%26SPClientTag%3D"+getAppDetails('SPClientTag')+"%26SPProductNumber%3D"+getAppDetails('SPProductNumber')+"%26Level%3DTop%26ListName%3DLinks%26Section%3DFooter&amp;client_id="+getAppDetails('client_id')+"' frameborder='0' scrolling='no' ></iframe>");
	     $('#bannerLinks').append("<div style='float:right;padding-bottom:10px;padding-right:10px;'><a class='more' href='"+spHostUrl+"/Pages/AllLinks.aspx' target='_blank'>more</a></div>");
    }
	 else if(_spPageContextInfo.webTitle=='Nickelodeon')
	 {
		$('#bannerLinks').append("<iframe scrolling='no' style='width: 100% ! important; height: 100% ! important;' src='"+spHostUrl+"/International/Nickelodeon/_layouts/15/appredirect.aspx?redirect_uri="+getAppDetails('redirect_uri')+"%2FPages%2FLinks%2Easpx%3FSPHostUrl%3D"+spHostUrl+"%252FNickelodeon%26SPHostTitle%3DNickelodeon%26SPAppWebUrl%3D"+getAppDetails('SPAppWebUrl')+"%252FNickelodeon%252FViacomAdSpaceApps%26SPLanguage%3Den%252DUS%26SPClientTag%3D"+getAppDetails('SPClientTag')+"%26SPProductNumber%3D"+getAppDetails('SPProductNumber')+"%26Level%3DSubSite%26ListName%3DLinks%26Section%3DBanner&amp;client_id="+getAppDetails('client_id')+"' id='bannerIframe' frameborder='0' ></iframe>");
		$('#footerLinks').append("<iframe scrolling='no' style='width: 100% ! important; height: 100% ! important;' src='"+spHostUrl+"/Nickelodeon/_layouts/15/appredirect.aspx?redirect_uri="+getAppDetails('redirect_uri')+"%2FPages%2FLinks%2Easpx%3FSPHostUrl%3D"+spHostUrl+"%252FNickelodeon%26SPHostTitle%3DNickelodeon%26SPAppWebUrl%3D"+getAppDetails('SPAppWebUrl')+"%252FNickelodeon%252FViacomAdSpaceApps%26SPLanguage%3Den%252DUS%26SPClientTag%3D"+getAppDetails('SPClientTag')+"%26SPProductNumber%3D"+getAppDetails('SPProductNumber')+"%26Level%3DSubSite%26ListName%3DLinks%26Section%3DFooter&amp;client_id="+getAppDetails('client_id')+"' id='footerIframe' frameborder='0' ></iframe>");
		$('#bannerLinks').append("<div style='float:right;padding-bottom:10px;padding-right:10px;'><a class='more' href='"+spHostUrl+"/Nickelodeon/Pages/AllLinks.aspx' target='_blank'>more</a></div>");
	}
	else if(_spPageContextInfo.webTitle=='International')
	{
		$('#bannerLinks').append("<iframe scrolling='no' style='width: 100% ! important; height: 100% ! important;' src='"+spHostUrl+"/International/_layouts/15/appredirect.aspx?redirect_uri="+getAppDetails('redirect_uri')+"%2FPages%2FLinks%2Easpx%3FSPHostUrl%3D"+spHostUrl+"%252FInternational%26SPHostTitle%3DInternational%26SPAppWebUrl%3D"+getAppDetails('SPAppWebUrl')+"%252FInternational%252FViacomAdSpaceApps%26SPLanguage%3Den%252DUS%26SPClientTag%3D"+getAppDetails('SPClientTag')+"%26SPProductNumber%3D"+getAppDetails('SPProductNumber')+"%26Level%3DSubSite%26ListName%3DLinks%26Section%3DBanner&amp;client_id="+getAppDetails('client_id')+"' id='bannerIframe' frameborder='0' ></iframe>");
		$('#footerLinks').append("<iframe scrolling='no' style='width: 100% ! important; height: 100% ! important;' src='"+spHostUrl+"/International/_layouts/15/appredirect.aspx?redirect_uri="+getAppDetails('redirect_uri')+"%2FPages%2FLinks%2Easpx%3FSPHostUrl%3D"+spHostUrl+"%252FInternational%26SPHostTitle%3DInternational%26SPAppWebUrl%3D"+getAppDetails('SPAppWebUrl')+"%252FInternational%252FViacomAdSpaceApps%26SPLanguage%3Den%252DUS%26SPClientTag%3D"+getAppDetails('SPClientTag')+"%26SPProductNumber%3D"+getAppDetails('SPProductNumber')+"%26Level%3DSubSite%26ListName%3DLinks%26Section%3DFooter&amp;client_id="+getAppDetails('client_id')+"' id='footerIframe' frameborder='0' ></iframe>");
		$('#bannerLinks').append("<div style='float:right;padding-bottom:10px;padding-right:10px;'><a class='more' href='"+spHostUrl+"/International/Pages/AllLinks.aspx' target='_blank'>more</a></div>");
	}
	else if(_spPageContextInfo.webTitle=='MusicAndEntertainment' )
	{
	    $('#bannerLinks').append("<iframe scrolling='no' style='width: 100% ! important; height: 100% ! important;' src='"+spHostUrl+"/MusicAndEntertainment/_layouts/15/appredirect.aspx?redirect_uri="+getAppDetails('redirect_uri')+"%2FPages%2FLinks%2Easpx%3FSPHostUrl%3D"+spHostUrl+"%252FMusicAndEntertainment%26SPHostTitle%3DMusicAndEntertainment%26SPAppWebUrl%3D"+getAppDetails('SPAppWebUrl')+"%252FMusicAndEntertainment%252FViacomAdSpaceApps%26SPLanguage%3Den%252DUS%26SPClientTag%3D"+getAppDetails('SPClientTag')+"%26SPProductNumber%3D"+getAppDetails('SPProductNumber')+"%26Level%3DSubSite%26ListName%3DLinks%26Section%3DBanner&amp;client_id="+getAppDetails('client_id')+"' id='bannerIframe' frameborder='0' ></iframe>");                           	
	    $('#footerLinks').append("<iframe scrolling='no' style='width: 100% ! important; height: 100% ! important;' src='"+spHostUrl+"/MusicAndEntertainment/_layouts/15/appredirect.aspx?redirect_uri="+getAppDetails('redirect_uri')+"%2FPages%2FLinks%2Easpx%3FSPHostUrl%3D"+spHostUrl+"%252FMusicAndEntertainment%26SPHostTitle%3DMusicAndEntertainment%26SPAppWebUrl%3D"+getAppDetails('SPAppWebUrl')+"%252FMusicAndEntertainment%252FViacomAdSpaceApps%26SPLanguage%3Den%252DUS%26SPClientTag%3D"+getAppDetails('SPClientTag')+"%26SPProductNumber%3D"+getAppDetails('SPProductNumber')+"%26Level%3DSubSite%26ListName%3DLinks%26Section%3DFooter&amp;client_id="+getAppDetails('client_id')+"' id='footerIframe' frameborder='0' ></iframe>");
	    $('#bannerLinks').append("<div style='float:right;padding-bottom:10px;padding-right:10px;'><a class='more' href='"+spHostUrl+"/MusicAndEntertainment/Pages/AllLinks.aspx' target='_blank'>more</a></div>");
	}
	else if(_spPageContextInfo.webTitle=='CorporateReports' )
	{
	    $('#bannerLinks').append("<iframe scrolling='no' style='width: 100% ! important; height: 100% ! important;' src='"+spHostUrl+"/CorporateReports/_layouts/15/appredirect.aspx?redirect_uri="+getAppDetails('redirect_uri')+"%2FPages%2FLinks%2Easpx%3FSPHostUrl%3D"+spHostUrl+"%252FCorporateReports%26SPHostTitle%3DCorporateReports%26SPAppWebUrl%3D"+getAppDetails('SPAppWebUrl')+"%252FCorporateReports%252FViacomAdSpaceApps%26SPLanguage%3Den%252DUS%26SPClientTag%3D"+getAppDetails('SPClientTag')+"%26SPProductNumber%3D"+getAppDetails('SPProductNumber')+"%26Level%3DSubSite%26ListName%3DLinks%26Section%3DBanner&amp;client_id="+getAppDetails('client_id')+"' id='bannerIframe' frameborder='0' ></iframe>");                           	
	    $('#footerLinks').append("<iframe scrolling='no' style='width: 100% ! important; height: 100% ! important;' src='"+spHostUrl+"/CorporateReports/_layouts/15/appredirect.aspx?redirect_uri="+getAppDetails('redirect_uri')+"%2FPages%2FLinks%2Easpx%3FSPHostUrl%3D"+spHostUrl+"%252FCorporateReports%26SPHostTitle%3DCorporateReports%26SPAppWebUrl%3D"+getAppDetails('SPAppWebUrl')+"%252FCorporateReports%252FViacomAdSpaceApps%26SPLanguage%3Den%252DUS%26SPClientTag%3D"+getAppDetails('SPClientTag')+"%26SPProductNumber%3D"+getAppDetails('SPProductNumber')+"%26Level%3DSubSite%26ListName%3DLinks%26Section%3DFooter&amp;client_id="+getAppDetails('client_id')+"' id='footerIframe' frameborder='0' ></iframe>");
	    $('#bannerLinks').append("<div style='float:right;padding-bottom:10px;padding-right:10px;'><a class='more' href='"+spHostUrl+"/CorporateReports/Pages/AllLinks.aspx' target='_blank'>more</a></div>");
	}

}
/****** Fuction to check if poll already answer by user not********/
function searchStringInArrayPoll(str, strArray) {
    for (var j = 0; j < strArray.length; j++) {
    	
        if (strArray[j] === str) return 'found';
    }
    return -1;
}
/**Create structure for Bookmark group**/
function getBookmarks()
{
	$('#bannerBookmarks').append("<div id='BookmarHead' class='ulBookmarHead' ><p  class='pBookmarkHead'>My BookMarks</p></div><div class='liAddBookmark'><img src='/sites/adspaceviacom/SiteAssets/images/PlusSign.png'></img><a class='aAddBookmark' style='' onclick=javascript:AddBookmarkPageCall()>Add BookMark</a></div>");
	 	$.ajax({    
	        type: "GET",
	        url: _spPageContextInfo.siteAbsoluteUrl + "/_api/web/lists/getbytitle('Bookmark List')/Items" + "?$select=Title,URL,ID,GroupName,GroupNameCalc,UserNameId" + "&$filter=AdSpaceUserName eq '" + AdSpaceUserName + "'&$orderby=GroupName",
	        headers: {
	            "accept": "application/json;odata=verbose"
	        },
	        async:false,
	        success: function (data) {
	            if (data.d.results) {
	            var arr = new Array();
	            var structure = "";
	           
	            var Links="";
	            for (var i = 0; i < data.d.results.length; i++) 
	            {    	               
                    var category = data.d.results[i].GroupName; 	                     
                   	var ID=  data.d.results[i].ID; 
                    var targetElementForBookmark = "";
                    var GroupID ;
                    var GroupName = "";
	               
	                       /***new categories ***/   
	                 if(data.d.results.length < 20)//only 20 categories 
	                 {  
	             		var ArrayIndexvar =searchStringInArrayPoll(category,arr);
	             		
	                 if(ArrayIndexvar == '-1'){   
	                   	             
	                    	arr.push(category);   
	                     	var bookmarkEditUrl=spHostUrl+"/Pages/BookmarkGroupEdit.aspx?BookmarkValue="+ID;
	                     	var bookmarkDeletetUrl=spHostUrl+"/Pages/BookmarkGroupDelete.aspx?BookmarkValue="+ID;      
	                   		$('#bannerBookmarks').append("<div id='outer_" + category + "' class='outerdiv'><div class='DivBookmarkGroup'>" + "<a class='aBookmarkGroup'  style='' id='Bookmarkcallout" + i + "'>" + category + "</a>" + "&nbsp;<span style=margin-right:15px;><a class='aBookmarkEdit' style='' id='" + category + "' onclick=javascript:callModal('"+bookmarkEditUrl+"','Edit') >" + "<img   src='"+spHostUrl+"/SiteAssets/images/EditPencil.png' style='height:13px;width:13px;'>" + "</a>" + "<a class='aBookmarkDelete' id='" + category + "' style='cursor:pointer' onclick=javascript:callModal('"+bookmarkDeletetUrl+"','Delete')>" + "<img src='"+spHostUrl+"/SiteAssets/images/deleteCross.jpg' style='height:13px;width:13px;'>" + "</a></span>" + "</div></div>");
	                    	oDivIdForBookmark = "Bookmarkcallout" + i; // P element that holds announcement title                   
	                    	targetElementForBookmark = document.getElementById(oDivIdForBookmark);
	                   		GroupID = i;
	                    	GroupName = category;
	                    
	                     	Links =GetLinks(category);
	                       	CreateCallOutPopupForBookmark(targetElementForBookmark, GroupID, GroupName, Links);
	                     
	                     }//end if 
	                 }//end if 
	                                     
	            }//end for	        	        	              
	          }//end if 
	
	        },
	        error: function (xhr) {
	            alert("Error:"+xhr.status + ': ' + xhr.statusText);
	        }
	    });
}

/**Get links for bookmark**/
function GetLinks(category)
{
	var structure="";
   	category= category.replace(' ', '%20');
    category= category.replace('&', '%26');
    category= category.replace("'", "%27%27");

 	$.ajax({    
        type: "GET",
        url: _spPageContextInfo.siteAbsoluteUrl + "/_api/web/lists/getbytitle('Bookmark List')/Items" + "?$select=Title,URL,ID,GroupName,GroupNameCalc,UserNameId" + "&$filter=GroupName eq '" + category + "' and AdSpaceUserName eq '" + AdSpaceUserName + "'",
        headers: {
            "accept": "application/json;odata=verbose"
        },
        async:false,
        success: function (data) {
            if (data.d.results) {
         for (var i = 0; i < data.d.results.length; i++) {      
                  
                    var title = data.d.results[i].Title; 
                    var Url = data.d.results[i].URL; 
                    var ID=  data.d.results[i].ID; 
             structure+="<br/>"; 
             var bookmarkEditUrl=spHostUrl+"/Pages/BookmarkItemEdit.aspx?BookmarkValue="+ID;  
             var bookmarkDeletetUrl=spHostUrl+"/Pages/BookmarkItemDelete.aspx?BookmarkValue="+ID;       
            structure+="<a class='bookmarkName' style='cursor:pointer' target='_blank' href='" + Url  + "'>" + title + "</a>" + "<span style='float:right'>" + "<a class='aBookmarkEdit' style='cursor:pointer'   id='" + ID + "' onclick=javascript:callModal('"+bookmarkEditUrl+"','Edit')>" + "<img  style='height:13px;width:13px' src='"+spHostUrl+"/SiteAssets/images/EditPencil.png'> </a>" + "<a class='aBookmarkDelete' style='cursor:pointer'  id='" + ID + "' onclick=javascript:callModal('"+bookmarkDeletetUrl+"','Delete')>" + "<img  style='height:13px;width:13px' src='"+spHostUrl+"/SiteAssets/images/deleteCross.jpg'></a></span>" ;
            }
			}
			 },
        error: function (xhr) {
            alert("Error:"+xhr.status + ': ' + xhr.statusText);
        }
    });			
	return structure;
}

/**CAllout for  Bookmark**/
function CreateCallOutPopupForBookmark(strtargetElementForBookmark, strID, strGroup, strLinks) {

    var calloutOptionsForBookmark = new CalloutOptions();
    calloutOptionsForBookmark.ID = 'notification' + strID;
    calloutOptionsForBookmark.launchPoint = strtargetElementForBookmark;
    calloutOptionsForBookmark.beakOrientation = 'topBottom'; //'leftRight';topBottom
    calloutOptionsForBookmark.title = strGroup;
    calloutOptionsForBookmark.contentWidth = 500;
    calloutOptionsForBookmark.content = "<div class=\"ms-soften\" style=\"margin-top:13px;\"><hr/>" + strLinks + "</div>";
    var displayedPopupForBookmark = CalloutManager.createNewIfNecessary(calloutOptionsForBookmark);
    displayedPopupForBookmark.set({
        openOptions: {
            event: "click"
        }
    });
}

/**Show/hide link and resources tab on hover**/
function hover()
{
		$( "ul.ulBannerLinks").each(function() 
		{
		 	var count=0;
			var $this=$(this);
			/****check if ul has jst one child(p tag), if yes, remove the ul***/
		 	if($(this).children().length<=1)	 	
		 	{
		 		$(this).remove();
		 	}
		});
			  	
 		$('#links-container').click(function(e) 
	    {  	$('#bannerResourcesDiv').hide();
        	 $('#bannerLinksDiv').show();
        	});	
    	$('#bannerLinksDiv').mouseover(function(e) 
	    {  
        	 $('#bannerLinksDiv').show();
    	});
    	$('#bannerLinksDiv').mouseout(function(e){
  			$('#bannerLinksDiv').hide();
		});
		
	
		$('#resources-container').click(function(e) 
	    {  		    
	   		 $('#bannerLinksDiv').hide();
        	 $('#bannerResourcesDiv').show();
      	});	
    	$('#bannerResourcesDiv').mouseover(function(e) 
	    {  
        	 $('#bannerResourcesDiv').show();
    	});
    	$('#bannerResourcesDiv').mouseout(function(e){
  			$('#bannerResourcesDiv').hide();
		}); 
}

/**Get resources link**/
function getResourcesLinks()
{
	var resourcesStructure="";
	
	if(_spPageContextInfo.webTitle=='Search Center')
	{
		var siteabsoluteUrl=_spPageContextInfo.siteAbsoluteUrl;
		$("#resources-container").hide();
	}
	else
	{
	var siteabsoluteUrl=_spPageContextInfo.webAbsoluteUrl;
	}
	$.ajax({
  		url: siteabsoluteUrl + "/_api/web/lists/getbytitle('Resources')/items?$select=Title,Link",
        method: "GET",
        async: false,
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {
        if(data.d.results.length>0)
        {
        	resourcesStructure="<ul class='ulRecources' >";
            for(var i=0; i<data.d.results.length;i++)
            {                                    
				resourcesStructure+="<li class='liResources'><a class='aResources' target='_blank'  href='"+data.d.results[i].Link.Url+"'>"+data.d.results[i].Title+"</a></li>";                                    									
            }                                                                 

        	resourcesStructure+="</ul>";
        	$("#bannerResources").append(resourcesStructure);
          }                          	                                    
        },
        error: function (jqXHR, textStatus, errorThrown) {
             alert(jqXHR.responseText );
        }
        });

}

/**Check whether logged in user is Super Administrator**/
function CheckIsSuperAdmin() {
    var userId = _spPageContextInfo.userId;
    var isAdmin=0;
      try {
        $.ajax({
            url: _spPageContextInfo.siteAbsoluteUrl+ "/_api/web/sitegroups()?$select=id&$filter=(Title eq 'AdSpace Owners')",
            method: "GET",
            async: false,
            headers: {
                "accept": "application/json; odata=verbose"
            },
            success: function (groupData) {
                var listResults = groupData.d.results;
                if (listResults.length > 0) {
              
                    $.ajax({
                        url: _spPageContextInfo.siteAbsoluteUrl+ "/_api/Web/SiteGroups/GetById(" + listResults[0].Id + ")/Users?$select=Id,Title&$filter=(Id eq " + userId + ")",
                        method: "GET",
                          async: false,

                        headers: {
                            "accept": "application/json; odata=verbose"
                        },
                        success: function (userData) {
                            if (userData.d.results.length > 0) 
                              isAdmin=1;  
                                							                     
                           		$("#Ribbon\.Documents\.New\.NewDocument-Large").hide();
                        },
                        error: function (err) {
                            // Error
                            alert(JSON.stringify(err));
                        }
                    });
                }
            },
            error: function (err) {
                // Error
                alert(JSON.stringify(err));
            }
        });
    }
    catch (err) {
        alert(JSON.stringify(err));
    }

    return isAdmin;
}

/**Open AddBookmark Page in Popup**/
function AddBookmarkPageCall() {

    var currentURL = document.location.href;   
    //currentURL =currentURL.split('/sites');
	//currentURL ="/sites"+currentURL[1];
    url = currentURL.substring(currentURL.lastIndexOf('/') + 1, currentURL.length);
    var BoookMarkURL =spHostUrl+"/Pages/AddBookmark.aspx?fileUrl="+currentURL ;    
    var BookmarkTitle = 'Add bookmark for - ' + url
    OpenPopUpPageWithTitle(BoookMarkURL, RefreshOnDialogClose, null, null, 'Add Bookmark')


}

/**Open Popup**/
function callModal(url,type)
{  


    OpenPopUpPageWithTitle(url , RefreshOnDialogClose, 500,250, 'Bookmark');

}

