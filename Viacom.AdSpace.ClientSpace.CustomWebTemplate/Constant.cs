﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Viacom.AdSpace.ClientSpace.CustomWebTemplate
{
    class Constant
    {
        
        //Header and link constant
        public const string CLIENT_SUBSITE_HOME = "Home";
        public const string CLIENT_SUBSITE_CLIENTPRESENTATION = "Client Presentation";
        public const string CLIENT_SUBSITE_CLIENTPLANNING = "Client Planning";
        public const string CLIENT_SUBSITE_CUSTOM_RESEARCH = "Custom Research";
        public const string CLIENT_SUBSITE_MARKETING_CASESTUDIES = "Marketing Case Studies";
        public const string CLIENT_SUBSITE_SHARED_CONTENT = "My Shared Content";
        public const string CLIENT_SUBSITE_EXT_ADMIN_VIEW = "External Share Admin Console";

        //ClientSpace library name
        public const string LIBRARY_TITLE_CLIENTPRESENTATION = "Client Presentations";
        public const string LIBRARY_TITLE_CLIENTPLANNING = "Client Planning";
        public const string LIBRARY_TITLE_CUSTOM_RESEARCH = "Custom Research";
        public const string LIBRARY_TITLE_MARKETING_CASESTUDIES = "Marketing Case Studies";

    }
}
