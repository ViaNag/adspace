using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Navigation;

namespace Viacom.AdSpace.ClientSpace.CustomWebTemplate
{
    /// <summary>
    /// This class handles events raised during feature activation, deactivation, installation, uninstallation, and upgrade.
    /// </summary>
    /// <remarks>
    /// The GUID attached to this class may be used during packaging and should not be modified.
    /// </remarks>

    [Guid("dcfaed08-2a87-43f4-8cf6-ef0bf47114e1")]
    public class Feature2EventReceiver : SPFeatureReceiver
    {
        public override void FeatureActivated(SPFeatureReceiverProperties properties)
        {
            SPWeb web = properties.Feature.Parent as SPWeb;

            // Add Everything Item to Search Drop Down List
            SPNavigationNode allAdSpaceNode = new SPNavigationNode("All AdSpace", "/SearchCenter/Pages/AllAdSpace.aspx", true);
            web.Navigation.AddToSearchNav(allAdSpaceNode);

            // Add This Site Item to Search Drop Down List
            SPNavigationNode thisSiteNode = new SPNavigationNode("This Site", "/SearchCenter/Pages/AdSpaceSubsite.aspx?u={ContextUrl}&ud={ContextUrl}", true);
            web.Navigation.AddToSearchNav(thisSiteNode);

            // Add People Item to Search Drop Down List
            SPNavigationNode peopleNode = new SPNavigationNode("People", "/SearchCenter/Pages/peopleresults.aspx", true);
            web.Navigation.AddToSearchNav(peopleNode);


            web.Update();
        }


        // Uncomment the method below to handle the event raised before a feature is deactivated.

        public override void FeatureDeactivating(SPFeatureReceiverProperties properties)
        {
            SPWeb web = properties.Feature.Parent as SPWeb;
            SPNavigationNodeCollection searchNodes = web.Navigation.SearchNav;
            int searchNodeCount = searchNodes.Count;
            for (int i = 0; i < searchNodeCount; i++)
            {
                bool unsafeUpdates = web.AllowUnsafeUpdates;
                web.AllowUnsafeUpdates = true;
                var searchNode = searchNodes[0];
                searchNode.Delete();
                web.Update();
                web.AllowUnsafeUpdates = unsafeUpdates;
            }
        }

        // Uncomment the method below to handle the event raised after a feature has been installed.

        //public override void FeatureInstalled(SPFeatureReceiverProperties properties)
        //{
        //}


        // Uncomment the method below to handle the event raised before a feature is uninstalled.

        //public override void FeatureUninstalling(SPFeatureReceiverProperties properties)
        //{
        //}

        // Uncomment the method below to handle the event raised when a feature is upgrading.

        //public override void FeatureUpgrading(SPFeatureReceiverProperties properties, string upgradeActionName, System.Collections.Generic.IDictionary<string, string> parameters)
        //{
        //}
    }
}
