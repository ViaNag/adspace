using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Taxonomy;
using Viacom.AdSpace.SiteProvisioning.BLL;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using Microsoft.SharePoint.Administration;
using System.Text;

namespace Viacom.AdSpace.ClientSpace.CustomWebTemplate.Features.Viacom.AdSpace.ClientListDefaultValues
{
    /// <summary>
    /// This class handles events raised during feature activation, deactivation, installation, uninstallation, and upgrade.
    /// </summary>
    /// <remarks>
    /// The GUID attached to this class may be used during packaging and should not be modified.
    /// </remarks>

    [Guid("aab71953-dd73-48d2-a841-cf8bcb6b47ab")]
    public class ViacomAdSpaceEventReceiver : SPFeatureReceiver
    {
        public override void FeatureActivated(SPFeatureReceiverProperties properties)
        {
            SPWeb currentWeb = null;
            try
            {
                currentWeb = (SPWeb)properties.Feature.Parent;
                string webUrl = currentWeb.ServerRelativeUrl;
                List<string[]> clientListDefaultValues = GetFieldsDefaultValues(currentWeb, webUrl, currentWeb.Title);
                bool allowUnsafeupdates = currentWeb.AllowUnsafeUpdates;
                currentWeb.AllowUnsafeUpdates = true;
                SetListDefaultValues(clientListDefaultValues, currentWeb);
                currentWeb.AllowUnsafeUpdates = allowUnsafeupdates;
                currentWeb.Update();
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Ad Sales Client feature activated", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                throw ex;
            }
        }

        public string SetListDefaultValues(List<string[]> clientListDefaultValues, SPWeb oWeb)
        {
            try
            {
                string sucessMsg = string.Empty;
                string[] clientLibNames = { Constants.CLIENT_LIBRARY_NAME_CLIENTPLANNING, Constants.CLIENT_LIBRARY_NAME_PRESENTATION, Constants.CLIENT_LIBRARY_NAME_CUSTOMRESEARCH, Constants.CLIENT_LIBRARY_NAME_MKTCASESTUDIES };
                try
                {
                    foreach (string libName in clientLibNames)
                    {
                        SPList list = oWeb.Lists.TryGetList(libName);
                        TermSet termSet = null;
                        TaxonomySession txs = new TaxonomySession(oWeb.Site);
                        TermStore termStore = txs.DefaultSiteCollectionTermStore;
                        for (int i = 0; i < clientListDefaultValues.Count; i++)
                        {
                            try
                            {
                                SPField fieldType = list.Fields.GetFieldByInternalName(clientListDefaultValues[i][0]);
                                if (fieldType.TypeDisplayName == Constants.FLD_TYPE_TAX)
                                {
                                    TaxonomyField field = list.Fields.GetFieldByInternalName(clientListDefaultValues[i][0]) as TaxonomyField;
                                    if (field != null)
                                    {
                                        if (!(string.IsNullOrEmpty(clientListDefaultValues[i][1])) && clientListDefaultValues[i][1].Contains('|'))
                                        {
                                            termSet = txs.DefaultSiteCollectionTermStore.GetTermSet(field.TermSetId);
                                            Term termVal = termStore.GetTerm(new Guid(clientListDefaultValues[i][1].Split(new char[] { '|' })[1]));
                                            Term term = termSet.GetTerms(termVal.Name, false).FirstOrDefault();
                                            int[] wssId = TaxonomyField.GetWssIdsOfTerm(oWeb.Site, txs.DefaultSiteCollectionTermStore.Id, termSet.Id, term.Id, false, 10);
                                            if (wssId.Length > 0)
                                            {
                                                field.DefaultValue = Convert.ToString(wssId[0]) + ";#" + term.Name + "|" + Convert.ToString(term.Id).ToLower();
                                            }
                                            else
                                            {
                                                int id = AddGuidToHiddenList(oWeb.Site, term, false);
                                                field.DefaultValue = Convert.ToString(id) + ";#" + term.Name + "|" + Convert.ToString(term.Id).ToLower();
                                            }
                                            field.ShowInEditForm = false;
                                            fieldType.ShowInNewForm = false;
                                            field.Update();
                                        }
                                    }
                                }
                                else
                                {
                                    if (!(string.IsNullOrEmpty(clientListDefaultValues[i][1])))
                                    {
                                        fieldType.DefaultValue = clientListDefaultValues[i][1];
                                    }
                                    fieldType.ShowInEditForm = false;
                                    fieldType.ShowInNewForm = false;
                                    fieldType.Update();
                                }
                            }
                            catch (Exception ex)
                            {
                                StringBuilder sb = new StringBuilder("Exception in Ad Space ClientListDefaultValue.");
                                sb.AppendFormat(" Error in getting library {0} in client web", libName);
                                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory(Convert.ToString(sb), TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                            }
                            list.Update();
                        }
                    }
                    return Constants.SUCESS_MSG;
                }
                catch (Exception ex)
                {
                    SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Ad Space Site Provisioning-DefaultValueFeature.cs-SetListDefaultValues", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                    StringBuilder sbFailMsg = new StringBuilder();
                    return Convert.ToString(sbFailMsg.AppendFormat("{0}{1}{2}{3}", Constants.FAIL_MSG, "SetListDefaultValues", Constants.FAIL_EX_MSG, ex.Message));
                }
                return sucessMsg;
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Ad Space Site Provisioning-DefaultValueFeature.cs-SetListDefaultValues", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                StringBuilder sbFailMsg = new StringBuilder();
                return null;
            }
        }

        /// <summary>
        /// Get Default values for fields to set in libraries
        /// </summary>
        /// <returns></returns>
        internal List<string[]> GetFieldsDefaultValues(SPWeb web, string webUrl, string clientTitle)
        {
            try
            {
                List<string[]> ClientSiteFIeldsDefaultValue = new List<string[]>();

                SPList oList = web.Lists.TryGetList(Constants.CLIENT_PROVISION_LIST_NAME);
                        if (oList != null)
                        {
                            string clntName = string.Empty;
                            SPQuery query = new SPQuery();
                            query.Query = "";
                            SPListItemCollection itemColl = oList.GetItems(query);
                            foreach (SPItem oListItem in itemColl)
                            {
                                clntName = Convert.ToString(oListItem[Constants.CLIENT_DATA_CLIENTNAME]);
                                string clntNm = clntName.Split(new char[] { '|' }).First();
                                if (clntNm.ToLower().Equals(clientTitle.ToLower()))
                                {
                                    ClientSiteFIeldsDefaultValue.Add(new string[] { Constants.CLIENT_DATA_CLIENTNAME, Convert.ToString(oListItem[Constants.CLIENT_DATA_CLIENTNAME]) });
                                    ClientSiteFIeldsDefaultValue.Add(new string[] { Constants.CLIENT_DATA_SEGMENT, Convert.ToString(oListItem[Constants.CLIENT_DATA_SEGMENT]) });
                                    ClientSiteFIeldsDefaultValue.Add(new string[] { Constants.CLIENT_DATA_CATEGORY, Convert.ToString(oListItem[Constants.CLIENT_DATA_CATEGORY]) });
                                    ClientSiteFIeldsDefaultValue.Add(new string[] { Constants.CLIENT_DATA_GABRIELID, Convert.ToString(oListItem[Constants.CLIENT_DATA_GABRIELID]) });
                                    ClientSiteFIeldsDefaultValue.Add(new string[] { Constants.CLIENT_DATA_ADFRONTID, Convert.ToString(oListItem[Constants.CLIENT_DATA_ADFRONTID]) });
                                    ClientSiteFIeldsDefaultValue.Add(new string[] { Constants.CLIENT_DATA_RDMID, Convert.ToString(oListItem[Constants.CLIENT_DATA_RDMID]) });
                                    ClientSiteFIeldsDefaultValue.Add(new string[] { Constants.CLIENT_DATA_WIDEID, Convert.ToString(oListItem[Constants.CLIENT_DATA_WIDEID]) });
                                    return ClientSiteFIeldsDefaultValue;
                                }
                            }
                        }
                return ClientSiteFIeldsDefaultValue;
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Ad Space Site Provisioning-DefaultValueFeature.cs-GetFieldsDefaultValues", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                StringBuilder sbFailMsg = new StringBuilder();
                return null;
            }
        }

        /// <summary>
        /// Function to handle taxonomy values to Taxonomy hidden list at site collection level.
        /// </summary>
        /// <param name="newsite">Current client site</param>
        /// <param name="newTerm">New term which will be adding at site collection level</param>
        /// <param name="isKeywordField">False for Term and True for Keyword</param>
        /// <returns></returns>
        public int AddGuidToHiddenList(SPSite newsite, Term newTerm, bool isKeywordField)
        {
            int id = -1;
            try
            {
                Type txFieldType = typeof(TaxonomyField);
                MethodInfo methodInfoTaxonomyGuid = txFieldType.GetMethod("AddTaxonomyGuidToWss", BindingFlags.NonPublic | BindingFlags.Static, null, new Type[3] { 
								        typeof(SPSite), 
								        typeof(Term), 
								        typeof(bool) }, null);

                if (methodInfoTaxonomyGuid != null)
                {
                    id = (int)methodInfoTaxonomyGuid.Invoke(null, new object[3] { newsite, newTerm, isKeywordField });
                }
                return id;
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Ad Space Client hiddenlist", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                throw ex;
                //return id;
            }

        }
    }
}
