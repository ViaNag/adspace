using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Microsoft.SharePoint;

namespace Viacom.AdSpace.ClientSpace.CustomWebTemplate
{
    /// <summary>
    /// This class handles events raised during feature activation, deactivation, installation, uninstallation, and upgrade.
    /// </summary>
    /// <remarks>
    /// The GUID attached to this class may be used during packaging and should not be modified.
    /// </remarks>

    [Guid("6f9537d0-8cdc-4a4e-9945-42d40220ee64")]
    public class ViacomAdSpaceClientSpaceEventReceiver : SPFeatureReceiver
    {
        public const string CLIENT_PLANNING = "Client Planning";
        public const string CLIENT_PRESENTATION_KEY = "Client Presentations";
        public const string CUSTOM_RESEARCH_KEY = "Custom Research";
        public const string MARKETIN_CASE_STUDIES_KEY = "Marketing Case Studies";

        public override void FeatureActivated(SPFeatureReceiverProperties properties)
        {
            // Create library in client palnning 
            CreateLibraryFolder(properties, CLIENT_PLANNING);

            // Create library in client presentation 
            CreateLibraryFolder(properties, CLIENT_PRESENTATION_KEY);

            // Create library in custom research 
            CreateLibraryFolder(properties, CUSTOM_RESEARCH_KEY);

            // Create library in Marketing Case Studies
            CreateLibraryFolder(properties, MARKETIN_CASE_STUDIES_KEY);

        }



        private void CreateLibraryFolder(SPFeatureReceiverProperties properties, string listTitle)
        {

            string value = properties.Feature.Properties[listTitle].Value;

            if (!string.IsNullOrEmpty(value))
            {
                string[] folders = value.Split(',');
                SPList list = ((SPWeb)properties.Feature.Parent).Lists.TryGetList(listTitle);

                if (list != null)
                {
                    foreach (string folderPath in folders)
                    {
                        string folderName = folderPath.Split('/')[folderPath.Split('/').Length - 1];
                        string path = folderPath.Replace(folderName, string.Empty);
                        CreateFolder(((SPWeb)properties.Feature.Parent), list, path, folderName);

                    }
                }
            }

        }

        private void CreateFolder(SPWeb web, SPList spList, string folderPath, string folderName)
        {
            SPFolder parentFolder = web.GetFolder(spList.RootFolder.ServerRelativeUrl + "/" + folderPath);

            if (parentFolder.Exists)
            {         
                
                if (!(web.GetFolder(parentFolder.ServerRelativeUrl + "/" + folderName)).Exists)
                {

                    try
                    {

                        web.AllowUnsafeUpdates = true;
                        var folder = spList.Items.Add(parentFolder.ServerRelativeUrl, SPFileSystemObjectType.Folder, folderName);


                        folder.Update();
                        web.AllowUnsafeUpdates = false;
                    }
                    catch (Exception)
                    {
                        throw;
                    }

                }
            }
        }


    }
}
