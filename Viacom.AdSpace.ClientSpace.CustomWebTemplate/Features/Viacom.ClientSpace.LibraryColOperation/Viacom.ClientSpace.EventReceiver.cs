using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Taxonomy;
using Viacom.AdSpace.SiteProvisioning.BLL;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using Microsoft.SharePoint.Administration;
using System.Text;

namespace Viacom.AdSpace.ClientSpace.CustomWebTemplate.Features.Viacom.ClientSpace.LibraryColOperation
{
    /// <summary>
    /// This class handles events raised during feature activation, deactivation, installation, uninstallation, and upgrade.
    /// </summary>
    /// <remarks>
    /// The GUID attached to this class may be used during packaging and should not be modified.
    /// </remarks>

    [Guid("36d420d2-a5e5-49cf-bf9f-af114cd097e6")]
    public class ViacomClientSpaceEventReceiver : SPFeatureReceiver
    {
        // Uncomment the method below to handle the event raised after a feature has been activated.
        public override void FeatureActivated(SPFeatureReceiverProperties properties)
        {
            SPWeb currentWeb = null;
            try
            {
                currentWeb = (SPWeb)properties.Feature.Parent;
                currentWeb.AllowUnsafeUpdates = true;
                
                // Change Column order in views; call this method if Key - Value pair exists
                UpdateViewColumnOrder(currentWeb);
                currentWeb.AllowUnsafeUpdates = false;

            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("ClientSpace Library Column Operation Feature Activated", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                throw ex;
            }
        }

        /// <summary>
        /// This method updates the column order in a view
        /// </summary>
        /// <param name="currentWeb"></param>
        public void UpdateViewColumnOrder(SPWeb currentWeb)
        {
            string listKey = "KeyClientSiteLibraries";
            string viewKey = "KeyClientSiteLibViews";
            string listValue = string.Empty;
            string viewValue = string.Empty;

            // Lists format: List1|List2|List3
            listValue = GetValueFromKey(currentWeb, listKey);

            // View-Columns format: View Name1|Columns#View Name2|Columns
            viewValue = GetValueFromKey(currentWeb, viewKey);

            // If values are not blank
            if (listValue != string.Empty && viewValue != string.Empty)
            {
                using (currentWeb)
                {
                    // Get list names 
                    string[] lists = listValue.Split('|');
                    foreach (string listName in lists)
                    {
                        // Read lists
                        SPList list = currentWeb.Lists.TryGetList(listName);
                        if (list != null)
                        {
                            // Get View-Columns combination
                            string[] viewColumns = viewValue.Split('#');
                            foreach (string viewColumn in viewColumns)
                            {
                                string viewName = viewColumn.Split('|')[0];
                                string columns = viewColumn.Split('|')[1];

                                // Get the view
                                SPView listView = list.Views[viewName];

                                if (listView != null)
                                {
                                    // Delete all fields from the view
                                    listView.ViewFields.DeleteAll();

                                    // Add fields to the view
                                    string[] columnNames = columns.Split(';');
                                    foreach (string columnName in columnNames)
                                    {
                                        listView.ViewFields.Add(columnName);
                                    }

                                    // Update the view
                                    listView.Update();

                                    // Update the List
                                    list.Update();
                                }

                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// This method gets the value based on key from Config list
        /// </summary>
        /// <param name="currentWeb"></param>
        /// <param name="listKey"></param>
        /// <returns></returns>
        private static string GetValueFromKey(SPWeb currentWeb, string Key)
        {
            string value = string.Empty;
            SPWeb rootWeb = currentWeb.Site.RootWeb;
            
            // Get config list
            SPList configList = rootWeb.Lists["FieldsConfigList"];
            SPQuery configQuery = new SPQuery();
            configQuery.Query = "<Where><Eq><FieldRef Name='Key'/><Value Type='Text'>" + Key + "</Value></Eq></Where>";
            SPListItemCollection configColl = configList.GetItems(configQuery);
            if (configColl != null && configColl.Count > 0)
            {
                // Get value based on Key
                value = Convert.ToString(configColl[0]["Value1"]);
            }
            else
            {
                throw new System.ArgumentException("The record with key : " + Key + "not found.", "Config Key");
            }

            return value;
        }
    }
}

         
        