﻿using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Viacom.AdSpace.ExpiryNotification
{
    class Program
    {
        static string siteCollURL = ConfigurationSettings.AppSettings["SiteUrl"];
        static string UserName = ConfigurationSettings.AppSettings["UserName"];
        static string Domain = ConfigurationSettings.AppSettings["Domain"];
        static string Password = ConfigurationSettings.AppSettings["Password"];
        static string groupname = ConfigurationSettings.AppSettings["groupname"];
        static string viacomemail = ConfigurationSettings.AppSettings["viacomemail"];
        static string viacomrelay = ConfigurationSettings.AppSettings["viacomrelay"];
        static string redirecturl = ConfigurationSettings.AppSettings["redirecturl"];
        static void Main(string[] args)
        {
            ClientContext clientContext = new ClientContext(siteCollURL);
            clientContext.Credentials = new NetworkCredential(UserName, Password, Domain);
            var web = clientContext.Web;
            Microsoft.SharePoint.Client.Group appgroup = web.SiteGroups.GetByName(groupname);
            UserCollection collUser = appgroup.Users;
            clientContext.Load(collUser);
            clientContext.ExecuteQuery();
            foreach (User mailUser in collUser)
            {
                if (mailUser.Email.ToString() != "")
                {

                    MailMessage objMailMsg = new MailMessage();
                    objMailMsg.IsBodyHtml = true;
                    objMailMsg.From = new MailAddress(viacomemail);
                    string emailAddress = mailUser.Email;
                    objMailMsg.To.Add(emailAddress);
                    string strSubject = "Upcoming Content Scheduled to Expire";
                    objMailMsg.Subject = strSubject;
                    string messageBody = string.Empty;
                    messageBody = "<br> Hello, <br>";
                    messageBody += "<h4>Please find upcoming content expiring in the next 4 weeks.<br></h4> <a href="+redirecturl+">Click here</a><br><br>";
                    messageBody += "<h4>Thanks & Regards,<h4>";
                    messageBody += "AdSpace Team<br><br>";
                    messageBody += "This is system generated e-mail.Please do not reply";
                    objMailMsg.Body = messageBody;
                    objMailMsg.IsBodyHtml = true;
                    objMailMsg.Priority = MailPriority.Normal;
                    SmtpClient objSmtpClient = new SmtpClient(viacomrelay);
                    objSmtpClient.Send(objMailMsg);
                }
            }
        }
    }
}
