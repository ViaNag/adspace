(function () {
    var statusFieldCtx = {};

  
    statusFieldCtx.Templates = {};
    statusFieldCtx.Templates.Fields = {
        "Status": {
            "View": StatusFieldViewTemplate
        }
    };

    SPClientTemplates.TemplateManager.RegisterTemplateOverrides(
        statusFieldCtx
        );
})();

function StatusFieldViewTemplate(ctx) {

    var Status = ctx.CurrentItem.Status;

     if (Status == 'Pending')
     {
        return "<input type="checkbox"  class="checks"/> ";
     }
    
     if (Status == 'Approved')
     {
        return "<input type="checkbox"  class="checks"/>";
     } 

}