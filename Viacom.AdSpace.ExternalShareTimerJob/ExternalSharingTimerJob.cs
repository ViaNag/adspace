﻿using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web.Configuration;
using Viacom.AdSpace.ExternalSharing;
using Viacom.AdSpace.ExternalSharing.Entities;

namespace Viacom.AdSpace.ExternalShareTimerJob
{
    /// <summary>
    /// Class to implement timer job execute method
    /// </summary>
    public class ExternalSharingTimerJob : SPJobDefinition
    {
        #region Timer job Constructors

        public ExternalSharingTimerJob()
            : base()
        {
        }

        public ExternalSharingTimerJob(string jobName, SPService service) :
            base(jobName, service, null, SPJobLockType.Job)
        {
            this.Title = Constants.JobTitle;
        }

        public ExternalSharingTimerJob(string jobName, SPWebApplication webapp) :
            base(jobName, webapp, null, SPJobLockType.Job)
        {
            this.Title = Constants.JobTitle;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// overriden method of timer service to execute
        /// </summary>
        /// <param name="targetInstanceId"></param>
        public override void Execute(Guid targetInstanceId)
        {
            Configuration config = null;
            try
            {
                SPWebApplication webApp = this.Parent as SPWebApplication;
                if (webApp != null)
                {
                    config = WebConfigurationManager.OpenWebConfiguration("/", webApp.Name);
                }
                if (config != null)
                {
                    // Get appsetting section of web config 
                    AppSettingsSection appSettings = config.AppSettings;
                    // Initialize the param object
                    ReadItemCollectionParams param = new ReadItemCollectionParams();
                    // Set parm properties from appsettings value
                    param.Siteurl = appSettings.CurrentConfiguration.AppSettings.Settings[Constants.InternalSite].Value; ;
                    param.ExternalSiteUrl = appSettings.CurrentConfiguration.AppSettings.Settings[Constants.ExternalSite].Value;
                    param.ResetPasswordUrl = appSettings.CurrentConfiguration.AppSettings.Settings[Constants.ResetPassword].Value;
                    param.TemplateWebUrl = appSettings.CurrentConfiguration.AppSettings.Settings[Constants.TemplateWebUrl].Value;
                    param.FbaUserId = appSettings.CurrentConfiguration.AppSettings.Settings[Constants.FbaUserId].Value;
                    ExternalSharingFacade extSharingFacade = new ExternalSharingFacade();
                    extSharingFacade.DeleteExpiredContent(param);
                    extSharingFacade.ShareContent(param);
                }
            }
            catch (Exception ex)
            {
                Logging.LogException(Constants.ExceptionLocation, ex);
            }
        }
    }
        #endregion
}



