using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using System;
using System.Runtime.InteropServices;

namespace Viacom.AdSpace.ExternalShareTimerJob
{
    /// <summary>
    /// This class handles events raised during feature activation, deactivation, installation, uninstallation, and upgrade.
    /// </summary>
    /// <remarks>
    /// The GUID attached to this class may be used during packaging and should not be modified.
    /// </remarks>

    [Guid("2805dc5e-d3dd-4d14-8adc-c21c813cadfc")]
    public class ViacomAdSpaceEventReceiver : SPFeatureReceiver
    {
        String TimerJobName = "LockInActiveUserTimerJob";
        // Uncomment the method below to handle the event raised after a feature has been activated.

        public override void FeatureActivated(SPFeatureReceiverProperties properties)
        {
            try
            {
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    SPWebApplication parentWebApp = (SPWebApplication)properties.Feature.Parent;
                    SPSite site = properties.Feature.Parent as SPSite;
                    DeleteUserTimerJob(TimerJobName, parentWebApp);
                    CreateTimerJob(parentWebApp);
                });
            }
            catch (Exception ex)
            {
                throw ex;
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory(TimerJobName, TraceSeverity.Medium, EventSeverity.Information), TraceSeverity.Medium, ex.ToString());
            }
        }

        /// <summary>
        /// Create Remove user Timer job
        /// </summary>
        /// <param name="site"></param>
        /// <returns></returns>
        private bool CreateTimerJob(SPWebApplication site)
        {
            bool jobCreated = false;
            try
            {
                LockInActiveUserTimerJob job = new LockInActiveUserTimerJob(TimerJobName, site);

                SPDailySchedule schedule = new SPDailySchedule();
                schedule.BeginHour = 2;
                schedule.EndHour = 3;
                job.Schedule = schedule;

                job.Update();
            }
            catch (Exception ex)
            {
                return jobCreated;
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory(TimerJobName, TraceSeverity.Medium, EventSeverity.Information), TraceSeverity.Medium, ex.ToString());
            }
            return jobCreated;
        }

        /// <summary>
        /// Method to delete remove user Timer Job
        /// </summary>
        /// <param name="jobName"></param>
        /// <param name="site"></param>
        /// <returns></returns>
        public bool DeleteUserTimerJob(string jobName, SPWebApplication site)
        {
            bool jobDeleted = false;
            try
            {
                foreach (SPJobDefinition job in site.JobDefinitions)
                {
                    if (job.Name == jobName)
                    {
                        job.Delete();
                        jobDeleted = true;
                    }
                }
            }
            catch (Exception)
            {
                return jobDeleted;
            }
            return jobDeleted;
        }

    }
}
