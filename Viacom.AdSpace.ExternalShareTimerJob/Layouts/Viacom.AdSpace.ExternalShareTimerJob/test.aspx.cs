﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using Microsoft.SharePoint.Administration;

namespace ExternalShareTimerJob.Layouts.Viacom.AdSpace.ExternalShareTimerJob
{
    public partial class test : LayoutsPageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            removepermission_extuser();
            grantpermission_extuser();

        }

        //to grant user permission on the basis of allow download value specified at the time of sharing
        public void grantpermission_extuser()
        {
            try
            {

                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    var web = SPContext.Current.Web;
                    SPList extlist = web.Lists["Shared Content"];
                    SPQuery camlQuery = new SPQuery();
                    camlQuery.ViewXml = "<View/>";
                    SPListItemCollection extlistItems = extlist.GetItems(camlQuery);

                    web.AllowUnsafeUpdates = true;
                    foreach (SPListItem extItem in extlistItems)
                    {

                        string shareds = extItem["SharedWith"].ToString();

                        string perm_stat = extItem["PermissionStatus"].ToString();
                        //string allow_download = extItem["AllowDownload"].ToString();

                        if (perm_stat == "Pending")
                        {

                            string permissionLevel = "View Only";
                            string permissionLevel1 = "Read";

                            string[] selPpl = shareds.Split(',');
                            foreach (string sname in selPpl)
                            {

                                string snamecon = "i:0#.f|fbamembership|" + sname;
                                SPUser user = web.EnsureUser(snamecon);

                                // SPUser user = web.CurrentUser;

                                SPRoleDefinition roleDef = null;
                                foreach (SPRoleDefinition oSPRoleDefinition in web.RoleDefinitions)
                                {
                                    if (oSPRoleDefinition.Name == permissionLevel)
                                    {
                                        roleDef = oSPRoleDefinition;
                                        break;
                                    }
                                }
                                //SPRoleDefinitionBindingCollection collRoleDefinitionBinding = new SPRoleDefinitionBindingCollection();
                                //collRoleDefinitionBinding.Add(roleDef);

                                SPRoleDefinition roleDef1 = null;

                                foreach (SPRoleDefinition oSPRoleDefinition in web.RoleDefinitions)
                                {
                                    if (oSPRoleDefinition.Name == permissionLevel1)
                                    {
                                        roleDef1 = oSPRoleDefinition;
                                        break;
                                    }
                                }


                                //SPRoleDefinitionBindingCollection collRoleDefinitionBinding1 = new SPRoleDefinitionBindingCollection();
                                //collRoleDefinitionBinding1.Add(roleDef1);

                                SPRoleAssignment oSPRoleAssignment = new SPRoleAssignment(user);

                                try
                                {

                                    //if (!extItem.HasUniqueRoleAssignments)
                                    //{
                                    //    extItem.BreakRoleInheritance(false, true);
                                    //}
                                    //if (allow_download == "Yes")
                                    //{
                                    //     oSPRoleAssignment.RoleDefinitionBindings.Add(roleDef1);

                                    //}
                                    //else
                                    //{
                                    oSPRoleAssignment.RoleDefinitionBindings.Add(roleDef1);
                                    //}

                                    extItem.RoleAssignments.Add(oSPRoleAssignment);
                                    extItem["PermissionStatus"] = "Completed";
                                    extItem.Update();

                                }
                                catch (Exception ex)
                                {
                                    SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("ExternalSharingTimerJob", TraceSeverity.Medium, EventSeverity.Information), TraceSeverity.Medium, ex.ToString());

                                }
                            }

                        }
                    }

                });


            }
            catch (Exception ex)
            {

            }
        }

        // to remove all permission [group or people] for the particular item in the external library
        public void removepermission_extuser()
        {

            SPSecurity.RunWithElevatedPrivileges(delegate()
            {
                var web = SPContext.Current.Web;
                web.AllowUnsafeUpdates = true;
                SPList extlist = web.Lists["Shared Content"];
                //SPList extlist = web.Lists[library_title];
                SPQuery oSPQuery = new SPQuery();
                oSPQuery.Query = "<View/>";
                SPListItemCollection extlistItems = extlist.GetItems(oSPQuery);

                foreach (SPListItem extItem in extlistItems)
                {

                    string perm_stat = extItem["PermissionStatus"].ToString();
                    if (perm_stat == "Pending")
                    {
                        try
                        {

                            extItem.BreakRoleInheritance(true, false);

                            SPRoleAssignmentCollection roleassigncoll = extItem.RoleAssignments;
                            foreach (SPRoleAssignment roleassign in roleassigncoll)
                            {
                                roleassign.RoleDefinitionBindings.RemoveAll();
                                roleassign.Update();


                            }


                        }
                        catch (Exception ex)
                        {


                        }

                    }

                }
                web.AllowUnsafeUpdates = false;

            });
        }
    }
}
