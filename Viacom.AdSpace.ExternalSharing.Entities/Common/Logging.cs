﻿using Microsoft.SharePoint.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Viacom.AdSpace.ExternalSharing.Entities
{
    /// <summary>
    /// /Class contains static method to log message & error
    /// </summary>
    public class Logging
    {
        /// <summary>
        /// Log exception
        /// </summary>
        /// <param name="exceptionLocation"></param>
        /// <param name="ex"></param>
        public static void LogException(string exceptionLocation, Exception ex)
        {
            if (ex.InnerException != null)
                LogException(exceptionLocation, ex.InnerException);
            Log(exceptionLocation, ex.Message, ex.StackTrace);
        }
       
        /// <summary>
        /// Log message
        /// </summary>
        /// <param name="exceptionLocation"></param>
        /// <param name="Message"></param>
        /// <param name="StackTrace"></param>
        public static void Log(string exceptionLocation, string Message, string StackTrace)
        {
            //log to ULS
            SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory(exceptionLocation, TraceSeverity.High, EventSeverity.ErrorCritical), TraceSeverity.Unexpected, Message, StackTrace);
        }

    }
}
