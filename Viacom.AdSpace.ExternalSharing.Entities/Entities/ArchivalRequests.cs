﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Viacom.AdSpace.ExternalSharing.Entities.Entities
{
    public class ArchivalRequests : IOutcome, IOperationParam
    {
        /// <summary>
        ///  Implementing IOperationParam property
        /// </summary>
        public string Query { get; set; }

        /// <summary>
        /// Implementing IOperationParam property
        /// </summary>
        public string Siteurl { get; set; }

        /// <summary>
        /// Implementing IOperationParam property
        /// </summary>
        public string ListTitle { get; set; }

        /// <summary>
        /// Implementing Ioutcome property
        /// </summary>
        public bool IsSuccess { get; set; }

        /// <summary>
        /// Implementing Ioutcome property
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets item url
        /// </summary>
        public string ItemURL { get; set; }

        /// <summary>
        /// Gets or sets Job Status
        /// </summary>
        public string JobStatus { get; set; }

        /// <summary>
        /// Gets or sets Created date
        /// </summary>
        public DateTime Created { get; set; }

        /// <summary>
        /// Gets or sets Job Message
        /// </summary>
        public string JobMessage { get; set; }

        /// <summary>
        /// Gets or sets Id 
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Implementing Ioutcome property
        /// </summary>
        public bool ArchivalEnabled { get; set; }

        /// <summary>
        /// Gets or set the value for the Item Type
        /// </summary>
        public string SharedItemType { get; set; }

        /// <summary>
        /// Gets or sets Dictionary
        /// </summary>
        public Dictionary<string, IOperationParam> folderSettingDict { get; set; }
    }
}
