﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Viacom.AdSpace.ExternalSharing.Entities.Entities;

namespace Viacom.AdSpace.ExternalSharing.Entities.Interface
{
    public interface IArchivalRequests
    {

        //// <summary>
        /// Gets or sets folder url
        /// </summary>
        IOperationParam ParentRequest { get; set; }
    }
}
