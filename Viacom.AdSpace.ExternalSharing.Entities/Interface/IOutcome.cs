﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Viacom.AdSpace.ExternalSharing.Entities
{
    /// <summary>
    /// This interface is to type cast the outcome of an action.
    /// </summary>
    public interface IOutcome
    {
        /// <summary>
        /// Gets or sets a value indicating wheather the outcome was a success or not
        /// </summary>
        bool IsSuccess { get; set; }

        /// <summary>
        /// Gets or sets message
        /// </summary>
        string Message { get; set; }

        /// <summary>
        /// Gets or sets Dictionary
        /// </summary>
        Dictionary<string, IOperationParam> folderSettingDict { get; set; }
    }
}
