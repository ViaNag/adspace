﻿using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Viacom.AdSpace.ExternalSharing.Entities;

namespace Viacom.AdSpace.ExternalSharing
{
    /// <summary>
    /// This is the action to copy files to a new doc set
    /// </summary>
    class CopyFilesAction : IExternalSharingAction
    {


        public IOutcome PerformAction(IOperationParam param)
        {
            // TODO: Copy the items from source to dest and get the new item details and update the item in the item collection with the information. Id and destination path. All the information that was being set in the old job should be set here as well.
            // Initialize the outcome of this action
            ActionOutcome result = new ActionOutcome();

            // Type casting ioutcome type outcome to  ExternalSharingItemCollection
            ExternalSharingItemCollection input = param as ExternalSharingItemCollection;
            string id = input.CurentItem[Constants.itemId].ToString();
            string srcLibrary = input.CurentItem[Constants.ListName].ToString();
            string srcSite = input.CurentItem[Constants.SiteURL].ToString();

            SPList srcList;
            using (SPSite site = new SPSite(srcSite))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    srcList = web.Lists[srcLibrary];
                }
            }
            try
            {
                using (SPSite oSiteCollection = new SPSite(input.ExternalSiteUrl))
                {
                    using (SPWeb web = oSiteCollection.RootWeb)
                    {
                        SPListItemCollection srcItems = null;
                        // get the source library
                        if (srcList != null)
                        {
                            SPQuery srcQuery = new SPQuery();
                            //srcQuery.Query = @"<Where><Eq><FieldRef Name='ID' /><Value Type='Number'>" + id + "</Value></Eq></Where>";
                            srcQuery.Query = string.Format(Constants.Query, Constants.IDField, Constants.NumberType, id);
                            srcQuery.ViewAttributes = Constants.QueryRecursiveScope;
                            srcItems = srcList.GetItems(srcQuery);
                        }

                        //get destination list
                        SPList list = web.Lists.TryGetList(input.ListTitle);
                        if (list != null)
                        {
                            //SPQuery query = new SPQuery();
                            //query.Query = input.Query;
                            //SPListItemCollection items = list.GetItems(query);
                            if (srcItems != null && srcItems.Count == 1)
                            {
                                SPFile file = srcItems[0].File;
                                string nLocation = web.ServerRelativeUrl.TrimEnd('/') + "/" + input.ListTitle + "/" + input.CurrentContentGroup;
                                SPFolder destFolder = web.GetFolder(nLocation);
                                web.AllowUnsafeUpdates = true;
                                SPFile fileItem = destFolder.Files.Add(file.Name, file.OpenBinary(), true);
                                destFolder.Update();

                                // update the mediasilo propert for video files
                                SPListItem itemFile = fileItem.Item;
                                bool isVideo = srcItems[0][Constants.IsVideo] != null ? Convert.ToBoolean(srcItems[0][Constants.IsVideo]) : false;
                                if (isVideo)
                                {
                                    string mideaSiloColumns = Constants.MideaSiloColumns;
                                    string columns = string.Empty;
                                    string siteUrl = input.Siteurl;

                                    using (SPSite site = new SPSite(siteUrl))
                                    {
                                        using (SPWeb internalWeb = site.OpenWeb())
                                        {
                                            SPList configList = internalWeb.Lists[Constants.ConfigurationList];
                                            SPQuery configQuery = new SPQuery();
                                            //configQuery.Query = "<Where><Eq><FieldRef Name='Key'/><Value Type='Text'>" + mideaSiloColumns + "</Value></Eq></Where>";
                                            configQuery.Query = string.Format(Constants.Query, Constants.ConfigKeyField, Constants.TextTypeField, mideaSiloColumns);
                                            SPListItemCollection configListItems = configList.GetItems(configQuery);
                                            if (configListItems != null && configListItems.Count > 0)
                                            {
                                                SPListItem item = configListItems[0];
                                                mideaSiloColumns = item[Constants.Value1] != null ? Convert.ToString(item[Constants.Value1]) : string.Empty;
                                            }
                                        }
                                    }
                                    string[] columnCollection = mideaSiloColumns.Split(',');
                                    foreach (string column in columnCollection)
                                    {
                                        if (column == Constants.DownloadServer || column == Constants.DownloadProxy)
                                        {
                                            if (input.ListTitle == Constants.DownloadableContent)
                                            {
                                                itemFile[column] = Convert.ToString(srcItems[0][Constants.AssetID]);
                                            }
                                        }
                                        else
                                        {
                                            itemFile[column] = srcItems[0][Constants.AssetID];
                                        }
                                    }
                                    itemFile.Update();
                                    fileItem.Update();
                                }
                                SPFolder dsFolder = web.GetFolder(web.ServerRelativeUrl.TrimEnd('/') + "/" + input.ListTitle + "/" + input.CurrentContentGroup);
                                SPListItem dsItem = dsFolder.Item;
                                dsItem[Constants.SharedWith] = input.CurentItem[Constants.SharedWith];
                                dsItem[Constants.ExpiryDate] = input.CurentItem[Constants.ExpiresOn];
                                dsItem[Constants.PermissionStatus] = Constants.CompletedStatus;
                                dsItem.Update();
                                web.AllowUnsafeUpdates = false;
                                result.IsSuccess = true;
                                result.Message = destFolder.ServerRelativeUrl;
                            }
                            else
                            {
                                result.IsSuccess = false;
                            }
                        }
                        else
                        {
                            result.IsSuccess = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Message = ex.Message;
            }
            return result;
        }
    }
}
