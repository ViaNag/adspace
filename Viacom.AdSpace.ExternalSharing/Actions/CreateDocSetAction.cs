﻿using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Viacom.AdSpace.ExternalSharing.Entities;

namespace Viacom.AdSpace.ExternalSharing
{
    /// <summary>
    /// This is the action to craete a new doc set
    /// </summary>
    class CreateDocSetAction : IExternalSharingAction
    {
        public IOutcome PerformAction(IOperationParam outcome)
        {
            // Initialize the outcome of this action
            ActionOutcome result = new ActionOutcome();

            // Type casting ioutcome type outcome to  ReadItemCollectionParams
            ExternalSharingItemCollection input = outcome as ExternalSharingItemCollection;
            try
            {
                using (SPSite oSiteCollection = new SPSite(input.ExternalSiteUrl))
                {
                    using (SPWeb web = oSiteCollection.RootWeb)
                    {
                        SPList list = web.Lists.TryGetList(input.ListTitle);
                        if (list != null)
                        {
                            SPFolderCollection folderColl = list.RootFolder.SubFolders;
                            SPFolder newFolder = folderColl.Add(input.CurrentContentGroup);
                            result.IsSuccess = true;
                            result.Message = newFolder.ServerRelativeUrl;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Message = ex.Message;
            }
            return result;
        }
    }
}
