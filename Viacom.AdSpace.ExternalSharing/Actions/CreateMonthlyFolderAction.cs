﻿using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Viacom.AdSpace.ExternalSharing.Entities;

namespace Viacom.AdSpace.ExternalSharing
{
    class CreateMonthlyFolderAction : IExternalSharingAction
    {
        /// <summary>
        /// This method performs the underlying action
        /// </summary>
        /// <param name="param">Outcome of the read operations is expected</param>
        /// <returns>Outcome of the action</returns>
        public IOutcome PerformAction(IOperationParam param)
        {
            // Initialize the outcome of this action
            ActionOutcome result = new ActionOutcome();

            // Type casting ioutcome type outcome to  ReadItemCollectionParams
            ReadItemCollectionParams input = param as ReadItemCollectionParams;

            string folderName = string.Empty;

            // create folder name in the format of MMM_YYYY
            folderName = DateTime.Now.ToString(Constants.ArchiveFolderFormat);

            try
            {
                // Open the external web.
                using (SPSite site = new SPSite(input.Siteurl))
                {
                    using (SPWeb web = site.RootWeb)
                    {
                        SPList list = web.Lists.TryGetList(input.ListTitle);
                        SPFolder folder = web.GetFolder(string.Format(Constants.FolderRelativeUrl, list.RootFolder.ServerRelativeUrl, folderName));
                        if (folder.Exists)
                        {
                            result.IsSuccess = true;
                            result.Message = folderName;
                        }
                        else
                        {
                            SPListItem newFolder = list.Items.Add(list.RootFolder.ServerRelativeUrl, SPFileSystemObjectType.Folder);

                            newFolder[Constants.TitleField] = folderName;
                            newFolder.Update();
                            list.Update();
                            result.IsSuccess = true;
                            result.Message = folderName;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                Logging.LogException(Constants.ExceptionLocation, ex);
            }
            return result;
        }
    }
}
