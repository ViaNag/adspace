﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Viacom.AdSpace.ExternalSharing.Entities;
using Microsoft.SharePoint;
namespace Viacom.AdSpace.ExternalSharing
{
    /// <summary>
    /// This is the class to get email body
    /// </summary>
    class GetMailContent : IExternalSharingAction
    {
        public IOutcome PerformAction(IOperationParam param)
        {
            string fileName = string.Empty;
            bool isDownloadable = false;
            string listName = string.Empty;
            string fileDetails = string.Empty;
            MailDetails mailItemDetails = param as MailDetails;

            try
            {
                if (mailItemDetails.ContentGroupItems != null && mailItemDetails.ContentGroupItems.Count > 0)
                {
                    mailItemDetails.MailMessage = Convert.ToString(mailItemDetails.ContentGroupItems[0][Constants.MessageField]);
                    fileDetails = Constants.StartULtag;
                    for (int index = 0; index < mailItemDetails.ContentGroupItems.Count; index++)
                    {
                        SPListItem listItem = mailItemDetails.ContentGroupItems[index];
                        fileName = Convert.ToString(listItem[Constants.FileNameField]);
                        isDownloadable = Convert.ToBoolean(listItem[Constants.AllowDownloadField]);
                        listName = isDownloadable ? Constants.DownloadableContentListTitle : Constants.SharedContentListTitle;
                        string sharedEmails = Convert.ToString(listItem[Constants.SharedWith]);
                        string[] mails = sharedEmails.Split(',');
                        mailItemDetails.MailToUsers = new List<string>();
                        foreach (string mail in mails)
                        {
                            mailItemDetails.MailToUsers.Add(mail);
                        }
                        string fName = Convert.ToString(listItem[Constants.AdSpaceName]);
                        fileDetails = fileDetails + string.Format(Constants.fileListing, fileName);
                    }
                    fileDetails = fileDetails + Constants.EndULtag;
                    mailItemDetails.MailBody = fileDetails;
                }
            }
            catch (Exception ex)
            {
                mailItemDetails.IsSuccess = false;
                Logging.LogException(Constants.ExceptionLocation, ex);
            }
            return mailItemDetails;
        }
    }
}
