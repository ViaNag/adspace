﻿using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Viacom.AdSpace.ExternalSharing.Entities;

namespace Viacom.AdSpace.ExternalSharing
{
    class GetMailTemplates : IExternalSharingAction
    {
        public IOutcome PerformAction(IOperationParam param)
        {
            // Initilize mail details
            MailDetails mailItemDetails = new MailDetails();

            try
            {
                using (SPSite site = new SPSite(param.Siteurl))
                {
                    using (SPWeb web = site.OpenWeb())
                    {
                        SPList list = web.Lists.TryGetList(param.ListTitle);
                        SPQuery query = new SPQuery();
                        query.Query = param.Query;
                        SPListItemCollection items = list.GetItems(query);
                        if (items != null && items.Count > 0)
                        {
                            SPListItem item = items[0];
                            mailItemDetails.MailTemplate = item[Constants.EmailBodyField] != null ? Convert.ToString(item[Constants.EmailBodyField]) : string.Empty;
                            mailItemDetails.MailSubject = item[Constants.EmailSubjectField] != null ? Convert.ToString(item[Constants.EmailSubjectField]) : string.Empty;
                            mailItemDetails.IsSuccess = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                mailItemDetails.IsSuccess = false;
                Logging.LogException(Constants.ExceptionLocation, ex);

            }
            return mailItemDetails;
        }
    }
}
