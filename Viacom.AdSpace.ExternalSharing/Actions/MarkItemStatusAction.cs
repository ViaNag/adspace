﻿using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Viacom.AdSpace.ExternalSharing.Entities;

namespace Viacom.AdSpace.ExternalSharing
{
    /// <summary>
    /// This is the action to mark items are deleted in the list
    /// </summary>
    class MarkItemStatusAction : IExternalSharingAction
    {
        /// <summary>
        /// This method performs the underlying action
        /// </summary>
        /// <param name="outcome">Outcome of the read operations is expected</param>
        /// <returns>Outcome of the action</returns>
        public IOutcome PerformAction(IOperationParam outcome)
        {
            // TODO: All the items should be marked as status deleted.
            //Arguments Passing Reiming : Listname , item status
            ExternalSharingItemCollection esItemCollection = outcome as ExternalSharingItemCollection;
            ActionOutcome actionOutcome = new ActionOutcome();
            try
            {
                foreach (SPListItem esItem in esItemCollection.ItemCollection)
                {
                    esItem[esItemCollection.ColumnName] = esItemCollection.Status;
                    esItem.Update();
                    actionOutcome.IsSuccess = true;
                }                
            }
            catch(Exception ex)
            {
                actionOutcome.IsSuccess = false;
                actionOutcome.Message = ex.Message;
            }
            return actionOutcome;
        }
    }
}
