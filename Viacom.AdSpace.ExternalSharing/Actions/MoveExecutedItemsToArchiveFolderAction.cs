﻿using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Viacom.AdSpace.ExternalSharing.Entities;

namespace Viacom.AdSpace.ExternalSharing
{
    /// <summary>
    /// Class to perform the Move executed items to the archived folder action
    /// </summary>
    class MoveExecutedItemsToArchiveFolderAction : IExternalSharingAction
    {
        /// <summary>
        /// This method performs the underlying action
        /// </summary>
        /// <param name="param">Outcome of the read operations is expected</param>
        /// <returns>Outcome of the action</returns>
        public IOutcome PerformAction(IOperationParam param)
        {
            // Get all the items to be deleted
            ExternalSharingItemCollection itemsToBeArchieved = param as ExternalSharingItemCollection;
            // Initialize the outcome of this action
            ActionOutcome result = new ActionOutcome();
            try
            {
                if (itemsToBeArchieved != null)
                {
                    SPList parentList = itemsToBeArchieved.ItemCollection[0].ParentList;
                    foreach (SPListItem item in itemsToBeArchieved.ItemCollection)
                    {
                        try
                        {
                            SPFile file = item.Web.GetFile(item.Url);
                            string NewDestinationUrl = file.Url.Replace(string.Format(Constants.ItemUrlToMove, item.ID.ToString()), string.Format(Constants.ItemUrlAftreMove, itemsToBeArchieved.FolderPath, item.ID.ToString()));
                            file.MoveTo(NewDestinationUrl);
                            item[Constants.ExternallySharedContentStatusField] = Constants.DeletedStatus;
                            item.SystemUpdate();
                        }
                        catch (Exception ex)
                        {
                            Logging.Log(Constants.ExceptionLocation, string.Format(Constants.ItemMoveErrorMsg, item.Title), ex.StackTrace);
                        }
                    }
                    parentList.Update();
                    result.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                Logging.LogException(Constants.ExceptionLocation, ex);
            }
            return result;
        }
    }
}
