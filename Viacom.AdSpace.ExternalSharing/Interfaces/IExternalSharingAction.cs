﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Viacom.AdSpace.ExternalSharing.Entities;

namespace Viacom.AdSpace.ExternalSharing
{
    /// <summary>
    /// This abstract class is to used for type casting the Account type actions
    /// </summary>
    interface IExternalSharingAction
    {
        /// <summary>
        /// This method is a declaration for the Perform action method for the external sharing actions.
        /// </summary>
        /// <param name="outcome">The outcome of the Read items action is expected</param>
        /// <returns>Outcome of the Action</returns>
        IOutcome PerformAction(IOperationParam outcome);
    }
}
