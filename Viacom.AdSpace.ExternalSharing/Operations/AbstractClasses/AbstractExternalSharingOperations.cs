﻿using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using System;
using System.Configuration;
using System.Web.Configuration;
using Viacom.AdSpace.ExternalSharing.Entities;

namespace Viacom.AdSpace.ExternalSharing
{
    abstract class AbstractExternalSharingOperations
    {

        /// <summary>
        /// This method is used for performing the External Sharing operations operation.
        /// </summary>
        /// <returns>Outcome of the operation</returns>
        public abstract IOutcome Execute(IOperationParam srcParam);

        /// <summary>
        /// This method get the items from the AdSpace list based on the passed query
        /// </summary>
        /// <param name="param">Ioperation type</param>
        /// <returns>Collection of external shared list items</returns>
        public IOutcome GetItemCollectionOnStatus(IOperationParam param)
        {
            ReadItemCollectionParams input = param as ReadItemCollectionParams;
            ExternalSharingItemCollection outcome = new ExternalSharingItemCollection();
            try
            {
                using (SPSite site = new SPSite(input.Siteurl))
                {
                    using (SPWeb web = site.RootWeb)
                    {
                        SPList list = web.Lists.TryGetList(input.ListTitle);
                        if (list != null)
                        {
                            SPQuery query = new SPQuery();
                            query.Query = input.Query;
                            if (input.ListTitle == Constants.ExtShareConfigurationListName)
                            {
                                query.ViewAttributes = Constants.QueryRecursiveScope;
                            }
                            if (input.ListTitle == Constants.ArchivalRequestsListName)
                            {
                                query.ViewAttributes = Constants.QueryRecursiveScope;
                            }
                            SPListItemCollection items = list.GetItems(query);
                            outcome.ItemCollection = items;
                            outcome.IsSuccess = true;
                        }
                        else
                        {
                            outcome.IsSuccess = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                outcome.IsSuccess = false;
                outcome.Message = ex.Message;
                Logging.LogException(Constants.GetItemCollectionOnStatusLocation, ex);
            }
            return outcome;
        }
    }
}
