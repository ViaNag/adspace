﻿using Microsoft.Office.DocumentManagement;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Viacom.AdSpace.ExternalSharing.Actions;
using Viacom.AdSpace.ExternalSharing.Entities;
using Viacom.AdSpace.ExternalSharing.Entities.Entities;

namespace Viacom.AdSpace.ExternalSharing
{
    class BulkArchivalOperation : AbstractExternalSharingOperations
    {
        List<ArchivalRequests> archivalRequestObject = new List<ArchivalRequests>();
        ActionOutcome operationOutcome = new ActionOutcome();
        
        string siteUrl = "";
        public override IOutcome Execute(IOperationParam param)
        {
            siteUrl = param.Siteurl;
            param.Query = string.Format(Constants.ExtShareConfigListQuery, Constants.JobStatus, Constants.ChoiceType, Constants.NewJobStatus, Constants.InProgressJobStatus, Constants.CreatedField);
            param.ListTitle = Constants.ArchivalRequestsListName;
            ExternalSharingItemCollection esItemCollection = this.GetItemCollectionOnStatus(param) as ExternalSharingItemCollection;
            // Check if collection in the read outcome is not null and item count is great than 0
            if (esItemCollection != null && esItemCollection.IsSuccess && esItemCollection.ItemCollection.Count > 0)
            {
                LoadListItemIntoCollection(esItemCollection.ItemCollection);
            }
            return null;
        }

        private void LoadListItemIntoCollection(SPListItemCollection configListItems)
        {
            foreach (SPListItem configListItem in configListItems)
            {
                ArchivalRequests archivalRequestConfigItem = new ArchivalRequests();
                archivalRequestConfigItem.SharedItemType = Convert.ToString(configListItem[Constants.SharedItemType]);
                archivalRequestConfigItem.ArchivalEnabled = Convert.ToBoolean(configListItem[Constants.ArchivalEnabled]);
                archivalRequestConfigItem.ListTitle = Convert.ToString(configListItem[Constants.LibraryName]);
                archivalRequestConfigItem.Siteurl = Convert.ToString(configListItem[Constants.SiteURL]);
                archivalRequestConfigItem.ItemURL = Convert.ToString(configListItem[Constants.ItemURL]);
                archivalRequestConfigItem.JobMessage = Convert.ToString(configListItem[Constants.JobMessage]);
                archivalRequestConfigItem.JobStatus = Convert.ToString(configListItem[Constants.JobStatus]);
                archivalRequestConfigItem.Created = Convert.ToDateTime(configListItem[Constants.CreatedField]);
                archivalRequestConfigItem.ID = Convert.ToInt32(configListItem[Constants.IDField]);
                archivalRequestObject.Add(archivalRequestConfigItem);
            }
            IExternalSharingAction markFolderAction = new MarkFolders();
            ActionOutcome consolidatedOutcome = new ActionOutcome();
            consolidatedOutcome.folderSettingDict = new Dictionary<string, IOperationParam>();
            IOperationParam dictValue = new FolderSettings();
            foreach (ArchivalRequests archivalRequestConfigItem in archivalRequestObject)
            {
                FolderSettings folderSetting = new FolderSettings();
                folderSetting.FolderURL = archivalRequestConfigItem.ItemURL;
                folderSetting.LibraryName = archivalRequestConfigItem.ListTitle;
                folderSetting.SiteURL = archivalRequestConfigItem.Siteurl;
                folderSetting.SettingDateTime = archivalRequestConfigItem.Created;
                folderSetting.ParentRequest = archivalRequestConfigItem;
                ActionOutcome outcome = new MarkFolders().PerformAction(folderSetting) as ActionOutcome;
                if (outcome != null && outcome.IsSuccess)
                {
                    foreach (KeyValuePair<string, IOperationParam> folders in outcome.folderSettingDict)
                    {
                        if (consolidatedOutcome.folderSettingDict.TryGetValue(folders.Key, out dictValue))
                        {
                            FolderSettings consolidatedOutcomeValue = dictValue as FolderSettings;
                            FolderSettings outcomeValue = folders.Value as FolderSettings;
                            int result = DateTime.Compare(outcomeValue.SettingDateTime, consolidatedOutcomeValue.SettingDateTime);
                            if (result > 0)
                            {
                                consolidatedOutcome.folderSettingDict.Remove(folders.Key);
                                consolidatedOutcome.folderSettingDict.Add(folders.Key, folders.Value);
                            }

                        }
                        else
                        {
                            //key not found in dictionary, add folderSetting to dictionary with key as folderurl
                            consolidatedOutcome.folderSettingDict.Add(folders.Key, folders.Value);
                        }
                    }
                }
            }
            if (consolidatedOutcome != null && consolidatedOutcome.folderSettingDict.Count > 0)
            {
                GetFoldersFromDictionary(consolidatedOutcome);
            }
        }

        private void GetFoldersFromDictionary(ActionOutcome outcome)
        {
            ReadItemCollectionParams param = new ReadItemCollectionParams();
            param.Query = String.Format(Constants.Query, Constants.ConfigKeyField, Constants.TextTypeField, Constants.ArchivalBatchLimit);
            param.Siteurl = siteUrl;
            param.ListTitle = Constants.ConfigurationList;
            ExternalSharingItemCollection configListItems = this.GetItemCollectionOnStatus(param) as ExternalSharingItemCollection;
            // Check if collection in the read outcome is not null and item count is great than 0
            if (configListItems != null && configListItems.IsSuccess && configListItems.ItemCollection.Count > 0)
            {
                SPListItem item = configListItems.ItemCollection[0];
                uint ArchivalBatchLimit = item[Constants.Value1] != null ? Convert.ToUInt32(item[Constants.Value1]) : 0;
                if (ArchivalBatchLimit > 0)
                {
                    foreach (KeyValuePair<string, IOperationParam> folders in outcome.folderSettingDict)
                    {
                        UpdateExternalSharingMetadata(folders.Value as FolderSettings, ArchivalBatchLimit);
                    }
                }
            }
            UpdateArchiveConfigListItem();
        }

        private void UpdateExternalSharingMetadata(FolderSettings folders, uint ArchivalBatchLimit)
        {
            int count = 0;
            ArchivalRequests parentRequest = (folders.ParentRequest) as ArchivalRequests;
            // do something with entry.Value or entry.Key
            bool isArchiveEnabled = parentRequest.ArchivalEnabled;
            DateTime archivalUpdateDate = folders.SettingDateTime;
            string listTitle = folders.LibraryName;
            string subSiteUrl = folders.SiteURL;
            string updateTime = SPUtility.CreateISO8601DateTimeFromSystemDateTime(archivalUpdateDate);
                string arch="1";
            if(isArchiveEnabled){
arch="1";
            }
            else{
                arch="0";
            }
            string query = String.Format(Constants.ArchiveMetadataQuery, updateTime, arch);
            string itemType = parentRequest.SharedItemType;
            string itemURL = parentRequest.ItemURL;
            try
            {
                using (SPSite site = new SPSite(subSiteUrl))
                {
                    using (SPWeb web = site.OpenWeb())
                    {
                        SPFolder folder = null;
                        SPList oList = web.Lists.TryGetList(listTitle);
                        if (oList != null)
                        {
                            SPQuery listQuery = new SPQuery();
                            listQuery.Query = query;

                           // if (itemType == Constants.FolderItemType) // Condition commented with respect to a bug TFS#19137
                           // {
                                folder = web.GetFolder(folders.FolderURL);
                                listQuery.Folder = folder;  // This should restrict the query to the subfolder
                           // }
                            listQuery.RowLimit = (ArchivalBatchLimit + 1);
                            SPListItemCollection filesCollection = oList.GetItems(listQuery);
                            if (filesCollection != null && filesCollection.Count > 0)
                            {
                                foreach (SPListItem file in filesCollection)
                                {
                                    count++;
                                    if (count < ArchivalBatchLimit)
                                    {
                                        if (file.Folder != null)
                                        {
                                            MetadataDefaults columnDefaults = new MetadataDefaults(oList);                                          
                                            SetDefaultColumnValue(web, columnDefaults, file.Folder, isArchiveEnabled);
                                            SetIsArchivedColumn(web, file.Folder, isArchiveEnabled);
                                        }
                                        else
                                        {
                                            UpdateColumn(web, file, isArchiveEnabled, archivalUpdateDate);
                                        }

                                    }
                                    else
                                    {
                                        parentRequest.JobStatus = Constants.InProgressJobStatus;
                                        parentRequest.JobMessage = Constants.InProgressJobMessage;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                parentRequest.JobMessage = ex.Message;
                operationOutcome.Message = ex.Message;
                operationOutcome.IsSuccess = false;
                Logging.LogException(Constants.ExceptionLocation, ex);
            }
        }

        /// <summary>
        /// updates list item column value
        /// </summary>
        /// <param name="web"></param>
        /// <param name="item"></param>
        /// <param name="action"></param>
        /// <param name="expiryDate"></param>
        private void UpdateColumn(SPWeb web, SPListItem item, bool isArchiveEnabled, DateTime archivalUpdateDate)
        {
            try
            {
                bool isNonArchived = false;
                if (item[Constants.FileType] != null)
                {
                    string extension = item[Constants.FileType].ToString();
                    extension = extension.ToLower();
                    if ((extension.IndexOf("_z") >= 0) || (extension.IndexOf("aspx") >= 0))
                    {
                        isNonArchived = true;
                    }
                }
                if (!isNonArchived)
                {
                    if (isArchiveEnabled)
                    {
                        item[Constants.ArchievedField] = 1;
                        item[Constants.ArchivalSettingUpdateDateTime] = archivalUpdateDate;
                    }
                    else
                    {
                        item[Constants.ArchievedField] = 0;
                        item[Constants.ArchivalSettingUpdateDateTime] = null;
                    }
                    bool allowUnsafeUpdate = web.AllowUnsafeUpdates;
                    try
                    {                       
                        web.AllowUnsafeUpdates = true;
                        item.SystemUpdate();
                    }
                    catch (Exception ex)
                    {
                        Logging.LogException(Constants.ExceptionLocation, ex);
                    }
                    finally
                    {
                        web.AllowUnsafeUpdates = allowUnsafeUpdate;
                    }

                }
            }
            catch (Exception ex)
            {
                Logging.LogException(Constants.ExceptionLocation, ex);
            }

        }

        /// <summary>
        /// set is archived column for folders
        /// </summary>
        /// <param name="oWeb"></param>
        /// <param name="folder"></param>
        /// <param name="columnName"></param>
        private static void SetIsArchivedColumn(SPWeb oWeb, SPFolder folder, bool isArchiveEnabled)
        {
            if (folder != null)
            {
                SPListItem spitem = folder.Item;
                if (spitem != null)
                {
                    if (isArchiveEnabled)
                    {
                        spitem[Constants.ArchievedField] = 1;
                    }
                    else
                    {
                        spitem[Constants.ArchievedField] = 0;
                    }
                    bool unsafeUpdate = oWeb.AllowUnsafeUpdates;
                    oWeb.AllowUnsafeUpdates = true;
                    spitem.SystemUpdate();
                    oWeb.AllowUnsafeUpdates = unsafeUpdate;
                }
            }
        }

        /// <summary>
        /// Set location based defaults for library folders
        /// </summary>
        /// <param name="web"></param>
        /// <param name="columnDefaults"></param>
        /// <param name="folder"></param>
        /// <param name="action"></param>
        /// <param name="expiryDate"></param>
        private void SetDefaultColumnValue(SPWeb web, MetadataDefaults columnDefaults, SPFolder folder, bool isArchiveEnabled)
        {
            if (isArchiveEnabled)
            {
                columnDefaults.SetFieldDefault(folder, "IsArchived", "1");
            }
            else
            {
                columnDefaults.SetFieldDefault(folder, "IsArchived", "0");
            }
            web.AllowUnsafeUpdates = true;
            columnDefaults.Update();
            web.AllowUnsafeUpdates = false;
        }

        /// <summary>
        /// Set location based defaults for library folders
        /// </summary>
        /// <param name="web"></param>
        /// <param name="columnDefaults"></param>
        /// <param name="folder"></param>
        /// <param name="action"></param>
        /// <param name="expiryDate"></param>
        private void UpdateArchiveConfigListItem()
        {
            try
            {
                using (SPSite osite = new SPSite(siteUrl))
                {
                    using (SPWeb oWeb = osite.RootWeb)
                    {
                        oWeb.AllowUnsafeUpdates = true;
                        SPList oList = oWeb.Lists.TryGetList(Constants.ArchivalRequestsListName);
                        if (oList != null)
                        {
                            foreach (ArchivalRequests archiveConfigItem in archivalRequestObject)
                            {
                                SPListItem oListItem = oList.GetItemById(archiveConfigItem.ID);
                                if (archiveConfigItem.JobStatus != Constants.InProgressStatus)
                                {
                                    oListItem[Constants.JobStatus] = Constants.CompletedStatus;
                                    oListItem[Constants.JobMessage] = Constants.JobMessageText;
                                }
                                else
                                {
                                    oListItem[Constants.JobStatus] = archiveConfigItem.JobStatus;
                                    oListItem[Constants.JobMessage] = archiveConfigItem.JobMessage;
                                }
                                oListItem.SystemUpdate();
                            }
                        }
                        oWeb.AllowUnsafeUpdates = false;
                    }
                }
            }
            catch (Exception ex)
            {
                operationOutcome.Message = ex.Message;
                operationOutcome.IsSuccess = false;
                Logging.LogException(Constants.ExceptionLocation, ex);
            }
        }
    }
}
