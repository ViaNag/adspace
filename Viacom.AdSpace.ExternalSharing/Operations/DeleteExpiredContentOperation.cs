﻿using Microsoft.SharePoint.Administration;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Viacom.AdSpace.ExternalSharing.Entities;

namespace Viacom.AdSpace.ExternalSharing
{
    /// <summary>
    /// This class will outline the actions to be performed to complete the delete expired content operations
    /// </summary>
    class DeleteExpiredContentOperation : AbstractExternalSharingOperations
    {
        /// <summary>
        /// This method will perform the Delete expired content operation
        /// </summary>
        /// <returns>Outcome of the operations will be returned</returns>
        public override IOutcome Execute(IOperationParam param)
        {
            // TODO: Log the failure points in the operation below

            // Initialize the Action Outcome object
            ActionOutcome operationOutcome = new ActionOutcome();

            // Assign value to param related to internal web extrenal list
            param.ListTitle = Constants.ExternalSharedTitle;
            param.Query = string.Format(Constants.Query, Constants.ExternallySharedContentStatusField, Constants.ChoiceType, Constants.ToBeDeletedStatus);

            // Get the items for which have expiry date has passed and perform the delete operation on them
            ExternalSharingItemCollection esItemCollection = this.GetItemCollectionOnStatus(param) as ExternalSharingItemCollection;

            // Check if collection in the read outcome is not null and item count is great than 0
            if (esItemCollection != null && esItemCollection.IsSuccess && esItemCollection.ItemCollection.Count > 0)
            {

                // Assign external site url
                esItemCollection.Siteurl = (param as ReadItemCollectionParams).ExternalSiteUrl;

                // Delete the expired content from the external site one item at a time
                ActionOutcome outcome = new DeleteExpiredItemsAction().PerformAction(esItemCollection) as ActionOutcome;

                if (outcome != null && outcome.IsSuccess)
                {

                    // Clearing param query property
                    param.Query = string.Empty;

                    // Create a monthly folder if not already present else get the folder location in the outcome's message variable
                    outcome = new CreateMonthlyFolderAction().PerformAction(param) as ActionOutcome;

                    if (outcome != null && outcome.IsSuccess)
                    {
                        esItemCollection.FolderPath = outcome.Message;

                        // Move the items to the monthly folder and update the status of item to deleted
                        outcome = new MoveExecutedItemsToArchiveFolderAction().PerformAction(esItemCollection) as ActionOutcome;

                        if (outcome != null && outcome.IsSuccess)
                        {
                            operationOutcome.IsSuccess = true;
                        }
                        else
                        {
                            operationOutcome.IsSuccess = false;
                            Logging.Log(Constants.ExceptionLocation, Constants.MoveExecutedItemsToArchiveFolderActionFailed, null);
                        }
                    }
                    else
                    {
                        operationOutcome.IsSuccess = false;
                        Logging.Log(Constants.ExceptionLocation, Constants.CreateMonthlyFolderActionFailed, null);
                    }
                }
                else
                {
                    operationOutcome.IsSuccess = false;
                    Logging.Log(Constants.ExceptionLocation, Constants.DeleteExpiredItemsActionFailed, null);
                }
            }
            else
            {
                operationOutcome.IsSuccess = false;
                Logging.Log(Constants.ExceptionLocation, Constants.ExternalSharingItemCollectionFailed, null);
            }

            return null;
        }
    }
}
