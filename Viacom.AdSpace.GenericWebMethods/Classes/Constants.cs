﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Viacom.AdSpace.GenericWebMethods.Classes
{
    class Constants
    {
        public const string SiteUrl = "siteUrl";
        public const string ListName = "listName";
        public const string ItemId = "itemId";
        public const string Title = "title";
        public const string URL = "URL";
        public const string IsFeaturedLink = "isFeaturedLink";
        public const string AccessTo = "accessTo";
        public const string Category = "category";
        public const string IsHeaderLink = "isHeaderLink";
        public const string IsPromoted = "isPromoted";
        public const string Source = "source";
    }
}
