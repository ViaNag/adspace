﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint;
using Viacom.AdSpace.Invoice.BAL;
using Viacom.AdSpace.Invoice.BAL.Entities;
using Viacom.AdSpace.Invoice.BAL.Data;
using Viacom.AdSpace.Invoice.BAL.Configuration;
using Viacom.AdSpace.Invoice.BAL.Workflows;

namespace Viacom.AdSpace.Invoice.BAL
{
    public interface IInvoiceRequest
    {
        IInvoiceMethodResponse<Dictionary<string, string>> GetConfigurationItems();
        IInvoiceMethodResponse<SPListItemCollection> GetUserMappingByNetworknStageGrp(SPWeb web, string networkName, string nextStageGroupColumn);
        IInvoiceMethodResponse<List<Network>> GetNetworksList();
        IInvoiceMethodResponse<bool> IsCurrentUserInRole(string Role, string NetWork);
        IInvoiceMethodResponse<List<IRole>> GetCurrentUserRoles(string NetWork);
        IInvoiceMethodResponse<Dictionary<string, bool>> IsFormEditable(string itemID);
        IInvoiceMethodResponse<CurrentStatus> GetCurrentRequestStatus(string itemID);
        IInvoiceMethodResponse<bool> IsBillingVisible(string itemID);
        IInvoiceMethodResponse<bool> SaveApproverComment(string itemID, string approvalStatus, string comments, bool isBillingSelected);
        IInvoiceMethodResponse<bool> SaveInvoiceChanges(string itemID, List<KeyValuePair<string, string>> kvpList);
        IInvoiceMethodResponse<List<Brand>> GetBrandsForNetwork(string Network);
        IInvoiceMethodResponse<List<string>> GetInvoicesForBrand(string network, string brand);
        IInvoiceMethodResponse<InvoiceDetails> GetInvoiceDetails(string invoiceID);


    }
}
