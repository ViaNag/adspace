﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using Viacom.AdSpace.Invoice.BAL.Workflows;
using Viacom.AdSpace.Invoice.BAL.Configuration;

namespace Viacom.AdSpace.Invoice.BAL
{
    public abstract class Base
    {

        public virtual Logging.ULSLogger InvoiceLogger
        {
            get
            {
                return new Logging.ULSLogger();
            }
        }

        public virtual Dictionary<string, string> Configuration
        {
            get
            {
                ListConfigurationSource lstConfig = new ListConfigurationSource(SPContext.Current.Web.Url, Constants.CONFIGLIST, Constants.CONFIGKEYFIELD, Constants.CONFIGVALUEFIELD);
                return lstConfig.ConfigItems;
            }
        }
        
        
        public virtual SPListItemCollection GetListItems(SPWeb web, string listname, string querystring, string viewstring)
        {
            SPListItemCollection items = null;
            // Build a query.
            try
            {
                if (!string.IsNullOrEmpty(listname) && !string.IsNullOrEmpty(querystring))
                {
                    SPQuery query = new SPQuery();
                    query.Query = querystring;
                    if (!string.IsNullOrEmpty(viewstring))
                    {
                        query.ViewFields = viewstring;

                        query.ViewFieldsOnly = true;
                    }
                    // Get data from a list.
                    string listUrl = web.ServerRelativeUrl + "/lists/" + listname;
                    SPList list = web.GetList(listUrl);
                    items = list.GetItems(query);
                }
            }
            catch (Exception ex)
            {
                InvoiceLogger.LogError("GetListItems: Internal Error ", ex);
                throw new Exception(ex.Message, ex);
            }
            return items;
        }
        public virtual string getNextStageGroupColumn(string currentStage, double adjAmt)
        {
            string nextStageGroupColumn = null;
            string nextStage = null;
            List<InvoiceAdjustmentWorkflowStage> stages = new InvoiceAdjustmentInvoiceAdjustmentWorkflowStageSource().Stages;
            foreach (InvoiceAdjustmentWorkflowStage stage in stages)
            {
                if (stage.ApprovalStage.Equals(currentStage))
                {
                    if (stage.ThresholdAmount > 0 && stage.ThresholdAmount <= adjAmt)
                    {
                        nextStage = stage.ThresholdBreachNextStage;
                    }
                    else
                    {
                        nextStage = stage.NextApprovalStage;
                    }
                    break;
                }
            }
            foreach (InvoiceAdjustmentWorkflowStage stage in stages)
            {
                if (stage.ApprovalStage.Equals(nextStage))
                {
                    nextStageGroupColumn = stage.StageGroupColumn;
                    break;
                }
            }
            return nextStageGroupColumn;
        }
        public virtual SPListItem GetRequestItemByID(SPWeb web, string itemID)
        {
            SPListItem requestItems = null;
            try
            {
                SPList list = web.Lists[Configuration[Constants.FORMSLISTNAMEKEY]];
                requestItems = list.GetItemById(Int32.Parse(itemID));
            }
            catch (Exception ex)
            {
                InvoiceLogger.LogError("GetRequestItemByID: Internal Error ", ex);
                throw new Exception(ex.Message, ex);
            }
            return requestItems;
        }
        public virtual string getStringDataPart(string text, char delimiter, int subStringPartNumber)
        {
            string result = null;
            result = text.Split(delimiter)[subStringPartNumber];
            return result;
        }
    }
}
