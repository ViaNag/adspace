﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InvoiceBase = Viacom.AdSpace.Invoice.BAL;
using Microsoft.SharePoint;

namespace Viacom.AdSpace.Invoice.BAL.Configuration
{
    public class ListConfigurationSource : IConfigurationSource
    {
        string WebUrl;
        string ConfigList;
        string _KeyColumn;
        string _ValueColumn;

        public List<string> Keys
        {
            get 
            {
                return ConfigItems.Keys.ToList<string>(); 
            }
        }

        Dictionary<string, string> _ConfigItems;
        public Dictionary<string, string> ConfigItems
        {
            get
            {
               _ConfigItems = new Dictionary<string, string>();
               using (SPSite site = new SPSite(WebUrl))
               {
                   using (SPWeb web = site.OpenWeb())
                   {
                       SPList lst = web.Lists[ConfigList];
                       foreach (SPListItem itm in lst.Items)
                       {
                           string key = (string)itm[_KeyColumn];
                           string value = (string)itm[_ValueColumn];

                           _ConfigItems.Add(key, value);
                       }
                   }
               }
               return _ConfigItems;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public ListConfigurationSource(string weburl, string list,string KeyColumn,string ValueColumn)
        {
            WebUrl = weburl;
            ConfigList = list;
            _KeyColumn = KeyColumn;
            _ValueColumn = ValueColumn;
        }
    }
}
