﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Microsoft.SharePoint;
using System.Security.Principal;

using InvoiceBase = Viacom.AdSpace.Invoice.BAL;

namespace Viacom.AdSpace.Invoice.BAL.Data
{
    public class SQLDataSource : InvoiceBase.IDataSource<DataRow>
    {
        string _connectionstring;

        public SQLDataSource(string webconfigconnectionkey)
        {
           // _connectionstring = connectionstring;
            _connectionstring = Convert.ToString(ConfigurationManager.ConnectionStrings[webconfigconnectionkey]);

            
        }

        public SQLDataSource(string webconfigconnectionkey, string temp)
        {
            //_connectionstring = ConfigurationManager.ConnectionStrings[webconfigconnectionkey].ConnectionString;
            _connectionstring = Convert.ToString(ConfigurationManager.ConnectionStrings[webconfigconnectionkey]);
        }

        SqlConnection _dataconnectiont;
        SqlConnection _dataconnection
        {
            get 
            {
                if (_dataconnectiont == null) _dataconnectiont = new SqlConnection(_connectionstring);
                return _dataconnectiont;
            }
        }


        public List<DataRow> GetData(IDataOperationParam param)
        {
            DataTable dt = null;
            SPSecurity.RunWithElevatedPrivileges(delegate()
            {
                WindowsImpersonationContext impersonatedContext = null;
                SQLDataOperationParam dataopparam = param as SQLDataOperationParam;
                try
                {
                    WindowsIdentity spUserIdentity = WindowsIdentity.GetCurrent();
                    impersonatedContext = spUserIdentity.Impersonate();
                    _dataconnection.Open();
                    SqlDataAdapter adp = new SqlDataAdapter(dataopparam.GetCommand(_dataconnection));
                    DataSet ds = new DataSet();
                    adp.Fill(ds);
                    dt = ds.Tables[0];                    
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally 
                { 
                    _dataconnection.Close();
                    if (impersonatedContext != null) { impersonatedContext.Undo(); }
                }
            });

            if (dt != null)
            {
                return dt.AsEnumerable().ToList();
            }
            return null;
        }

        //public List<DataRow> GetData(IDataOperationParam param)
        //{
        //    SQLDataOperationParam dataopparam = param as SQLDataOperationParam;
        //    try
        //    {
        //        _dataconnection.Open();
        //        SqlDataAdapter adp = new SqlDataAdapter(dataopparam.GetCommand(_dataconnection));
        //        DataSet ds = new DataSet();
        //        adp.Fill(ds);
        //        DataTable dt = ds.Tables[0];
        //        return dt.AsEnumerable().ToList();
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
        //    finally { _dataconnection.Close(); }
        //}


        
    }

   
   
}
