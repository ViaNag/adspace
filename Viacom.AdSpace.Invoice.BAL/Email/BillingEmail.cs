﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Net;
using System.Net.Mail;
using Viacom.AdSpace.Invoice.BAL;
using Viacom.AdSpace.Invoice.BAL.Workflows;
using Viacom.AdSpace.Invoice.BAL.Entities;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Text.RegularExpressions;

namespace Viacom.AdSpace.Invoice.BAL.Email
{
    public class BillingEmail : Base, IBillingEmail
    {
        private Dictionary<string, string> EMAILSUBJECTKEYS
        {
            get
            {
                Dictionary<string, string> _emailSubjectKeys = new Dictionary<string, string>();
                _emailSubjectKeys.Add("Network", Constants.NETWORKFIELD);
                _emailSubjectKeys.Add("Advertiser", Constants.ADVERTISERSFIELD);
                _emailSubjectKeys.Add("InvoiceNo", Constants.INVOICENOFIELD);
                _emailSubjectKeys.Add("Deal", Constants.DEALFIELD);
                _emailSubjectKeys.Add("Agency", Constants.AGENCYFIELD);
                return _emailSubjectKeys;
            }
        }
        public string EmailTo { get; set; }
        public string EmailCC { get; set; }
        public string EmailSubject { get; set; }
        public string EmailBody { get; set; }
        public string EmailSentResponse { get; set; }

        public BillingEmail() { EmailSentResponse = null; }

        public IInvoiceMethodResponse<BillingEmail> GetBillingEmail(string itemID)
        {
            InvoiceMethodResponse<BillingEmail> actionresponse = new InvoiceMethodResponse<BillingEmail>();
            try
            {
                PopulateBillingEmailBodynSubject();

                if (this.EmailBody != null || this.EmailSubject != null)
                {
                    SPWeb web = SPContext.Current.Web;

                    SPListItem item = GetRequestItemByID(web, itemID);
                    if (item != null)
                    {
                        if (item[Constants.REQUESTSUBMITTOR] != null)
                        {
                            SPFieldUserValue requestor = new SPFieldUserValue(web, item[Constants.REQUESTSUBMITTOR].ToString());
                            EmailTo += requestor.User.Email;
                        }
                        if (item[Constants.SENDBILLINGATTACHMENTFIELD].ToString() == true.ToString())
                        {
                            if (item[Constants.AGENCYCONTACTEMAILFIELD] != null)
                            {
                                if (EmailTo.Length > 0)
                                {
                                    EmailTo += ";";
                                }
                                EmailTo += item[Constants.AGENCYCONTACTEMAILFIELD].ToString();
                            }
                            if (item[Constants.BUYEREMAILFIELD] != null)
                            {
                                if (EmailTo.Length > 0)
                                {
                                    EmailTo += ";";
                                }
                                EmailTo += item[Constants.BUYEREMAILFIELD].ToString();
                            }
                            if (item[Constants.ACCOUNTEXECEMAILFIELD] != null)
                            {
                                if (EmailTo.Length > 0)
                                {
                                    EmailTo += ";";
                                }
                                EmailTo += item[Constants.ACCOUNTEXECEMAILFIELD].ToString();
                            }
                            if (item[Constants.SSEEMAILFIELD] != null)
                            {
                                if (EmailTo.Length > 0)
                                {
                                    EmailTo += ";";
                                }
                                EmailTo += item[Constants.SSEEMAILFIELD].ToString();
                            }
                            if (item[Constants.SALESASSISTANTEMAILFIELD] != null)
                            {
                                if (EmailTo.Length > 0)
                                {
                                    EmailTo += ";";
                                }
                                EmailTo += item[Constants.SALESASSISTANTEMAILFIELD].ToString();
                            }
                        }

                        foreach (KeyValuePair<string, string> entry in EMAILSUBJECTKEYS)
                        {
                            string entryValue = null;
                            if (EmailSubject.Contains(entry.Key) && item[entry.Value] != null)
                            {
                                if (item[entry.Value].ToString().Contains('#'))
                                {
                                    entryValue = getStringDataPart(item[entry.Value].ToString(), '#', 1);
                                }
                                else
                                {
                                    entryValue = item[entry.Value].ToString();
                                }
                                EmailSubject = EmailSubject.Replace(entry.Key.ToString(), entryValue);
                            }
                        }
                    }
                    SPFieldUserValueCollection approverValues = (SPFieldUserValueCollection)item[Constants.APPROVERSFIELD];
                    if (approverValues != null)
                    {
                        for (int i = 0; i < approverValues.Count; i++)
                        {
                            if (approverValues[i].User.Email != null)
                            {
                                if (EmailCC != null && EmailCC.Length > 0)
                                {
                                    EmailCC += ";";
                                }
                                EmailCC += approverValues[i].User.Email;
                            }
                        }
                    }
                    string querystring = string.Concat(
                                                   "<Where><Or>",
                                                   "<Eq>",
                                                      "<FieldRef Name='", Constants.CONFIGKEYFIELD, "'/>",
                                                      "<Value Type='Text'>", Constants.FINALSTAGECCGROUPKEY, "</Value>",
                                                   "</Eq>",
                                                   "<Or>",
                                                       "<Eq>",
                                                          "<FieldRef Name='", Constants.CONFIGKEYFIELD, "'/>",
                                                          "<Value Type='Text'>", Constants.WORKFLOWCCUSERSKEY, "</Value>",
                                                       "</Eq>",
                                                       "<Eq>",
                                                          "<FieldRef Name='", Constants.CONFIGKEYFIELD, "'/>",
                                                          "<Value Type='Text'>", Constants.WORKFLOWTOUSERSKEY, "</Value>",
                                                       "</Eq>",
                                                     "</Or>",
                                                   "</Or></Where>");
                    SPListItemCollection items = GetListItems(web, Constants.CONFIGLIST, querystring, null);
                    if (items != null)
                    {
                        foreach (SPListItem configItem in items)
                        {
                            //if (configItem[Constants.CONFIGKEYFIELD] != null && configItem[Constants.CONFIGKEYFIELD].ToString().Equals(Constants.FINALSTAGECCUSERSKEY))
                            //{
                            //    if (configItem[Constants.CONFIGVALUEFIELD].ToString().Contains(','))
                            //    {
                            //        string[] userEmailCollection = configItem[Constants.CONFIGVALUEFIELD].ToString().Split(',');
                            //        foreach (string userEmail in userEmailCollection)
                            //        {
                            //            if (EmailCC != null && EmailCC.Length > 0)
                            //            {
                            //                EmailCC += ";";
                            //            }
                            //            EmailCC += userEmail;
                            //        }
                            //    }
                            //}
                            if (configItem[Constants.CONFIGKEYFIELD] != null && configItem[Constants.CONFIGKEYFIELD].ToString().Equals(Constants.WORKFLOWTOUSERSKEY))
                            {

                                SPFieldUserValueCollection objUserFieldValueCol = new SPFieldUserValueCollection(web, configItem[Constants.CONFIGUSERTYPEVALUEFIELD].ToString());
                                for (int i = 0; i < objUserFieldValueCol.Count; i++)
                                {
                                    SPFieldUserValue singlevalue = objUserFieldValueCol[i];
                                    if (singlevalue.User == null)
                                    {
                                        SPGroup group = web.Groups[singlevalue.LookupValue];
                                        foreach (SPUser user in group.Users)
                                        {
                                            if (EmailCC != null && EmailCC.Length > 0)
                                            {
                                                EmailCC += ";";
                                            }
                                            EmailCC += user.Email;
                                        }
                                    }
                                }
                            }
                            if (configItem[Constants.CONFIGKEYFIELD] != null && configItem[Constants.CONFIGKEYFIELD].ToString().Equals(Constants.WORKFLOWCCUSERSKEY))
                            {

                                SPFieldUserValueCollection objUserFieldValueCol = new SPFieldUserValueCollection(web, configItem[Constants.CONFIGUSERTYPEVALUEFIELD].ToString());
                                for (int i = 0; i < objUserFieldValueCol.Count; i++)
                                {
                                    SPFieldUserValue singlevalue = objUserFieldValueCol[i];
                                    if (singlevalue.User == null)
                                    {
                                        SPGroup group = web.Groups[singlevalue.LookupValue];
                                        foreach (SPUser user in group.Users)
                                        {
                                            if (EmailCC != null && EmailCC.Length > 0)
                                            {
                                                EmailCC += ";";
                                            }
                                            EmailCC += user.Email;
                                        }
                                    }
                                }
                            }
                            if (configItem[Constants.CONFIGKEYFIELD] != null && configItem[Constants.CONFIGKEYFIELD].ToString().Equals(Constants.FINALSTAGECCGROUPKEY))
                            {

                                SPFieldUserValueCollection objUserFieldValueCol = new SPFieldUserValueCollection(web, configItem[Constants.CONFIGUSERTYPEVALUEFIELD].ToString());
                                for (int i = 0; i < objUserFieldValueCol.Count; i++)
                                {
                                    SPFieldUserValue singlevalue = objUserFieldValueCol[i];
                                    if (singlevalue.User == null)
                                    {
                                        SPGroup group = web.Groups[singlevalue.LookupValue];
                                        foreach (SPUser user in group.Users)
                                        {
                                            if (EmailCC != null && EmailCC.Length > 0)
                                            {
                                                EmailCC += ";";
                                            }
                                            EmailCC += user.Email;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    actionresponse.ActionSuccessful = true;
                }
                actionresponse.Data = this;
            }
            catch (Exception ex)
            {
                actionresponse.ActionSuccessful = false;
                actionresponse.ExceptionIfAny = ex;
            }
            return actionresponse;
        }
        public IInvoiceMethodResponse<BillingEmail> SendBillingEmail(string itemID)
        {
            IInvoiceMethodResponse<BillingEmail> actionresponse = new InvoiceMethodResponse<BillingEmail>();
            try
            {
                SPWeb web = SPContext.Current.Web;
                SPListItem item = GetRequestItemByID(web, itemID);
                string SSVIACOMServiceUrl = string.Empty;
                string SSVIACOMFromAddress = string.Empty;
                string SSVIACOMFromDisplay = string.Empty;

                if (item != null)
                {
                    if (item[Constants.FORMSTATUSFIELD] != null)
                    {
                        List<InvoiceAdjustmentWorkflowStage> Stages = new InvoiceAdjustmentInvoiceAdjustmentWorkflowStageSource().Stages;
                        foreach (InvoiceAdjustmentWorkflowStage stage in Stages)
                        {
                            if (stage.ApprovalStage.Equals(item[Constants.FORMSTATUSFIELD].ToString()))
                            {
                                if (stage.IsFinalStage && stage.IsBillingVisible)
                                {
                                    actionresponse = GetBillingEmail(itemID);
                                    SPFile billingdoc = null;
                                    billingdoc = GetBillingDocument(itemID);

                                    string querystring = string.Concat(
                                                                           "<Where><Or><Eq>",
                                                                              "<FieldRef Name='", Constants.CONFIGKEYFIELD, "'/>",
                                                                              "<Value Type='Text'>", Constants.SSVIACOMServiceUrl, "</Value></Eq>",
                                    "<Or><Eq>",
                                                                              "<FieldRef Name='", Constants.CONFIGKEYFIELD, "'/>",
                                                                              "<Value Type='Text'>", Constants.SSVIACOMFromAddress, "</Value>",
                                                                           "</Eq>",
                                                                                "<Eq>",
                                                                                "<FieldRef Name='", Constants.CONFIGKEYFIELD, "'/>",
                                                                                "<Value Type='Text'>", Constants.SSVIACOMFromDisplay, "</Value>",
                                                                                "</Eq></Or></Or></Where>");

                                    //Get configuration details from subsite
                                    SPListItemCollection items = GetListItems(web, Constants.CONFIGLIST, querystring, null);
                                    if (items != null)
                                    {
                                        foreach (SPListItem configItem in items)
                                        {
                                            if (configItem[Constants.CONFIGKEYFIELD] != null && Convert.ToString(configItem[Constants.CONFIGKEYFIELD]).Equals(Constants.SSVIACOMServiceUrl))
                                            {
                                                SSVIACOMServiceUrl = Convert.ToString(configItem[Constants.CONFIGVALUEFIELD]);
                                            }

                                            if (configItem[Constants.CONFIGKEYFIELD] != null && Convert.ToString(configItem[Constants.CONFIGKEYFIELD]).Equals(Constants.SSVIACOMFromAddress))
                                            {
                                                SSVIACOMFromAddress = Convert.ToString(configItem[Constants.CONFIGVALUEFIELD]);
                                            }

                                            if (configItem[Constants.CONFIGKEYFIELD] != null && Convert.ToString(configItem[Constants.CONFIGKEYFIELD]).Equals(Constants.SSVIACOMFromDisplay))
                                            {
                                                SSVIACOMFromDisplay = Convert.ToString(configItem[Constants.CONFIGVALUEFIELD]);
                                            }

                                        }
                                    }

                                    if (String.IsNullOrEmpty(SSVIACOMServiceUrl))
                                    {
                                        InvoiceLogger.LogInfo("Configuration missing from site - " + web.ServerRelativeUrl + " SSVIACOMServiceUrl");
                                    }

                                    if (String.IsNullOrEmpty(SSVIACOMFromAddress))
                                    {
                                        InvoiceLogger.LogInfo("Configuration missing from site - " + web.ServerRelativeUrl + " SSVIACOMFromAddress");
                                    }

                                    if (String.IsNullOrEmpty(SSVIACOMFromDisplay))
                                    {
                                        InvoiceLogger.LogInfo("Configuration missing from site - " + web.ServerRelativeUrl + " SSVIACOMFromDisplay");
                                    }

                                    string response = SendEmail(billingdoc, SSVIACOMServiceUrl, SSVIACOMFromAddress, SSVIACOMFromDisplay);
                                    actionresponse.Data = this;
                                    actionresponse.ActionSuccessful = true;
                                    break;
                                }
                            }
                        }
                        if (EmailSentResponse == null || String.IsNullOrEmpty(EmailSentResponse))
                        {
                            EmailSentResponse = "Failure|Current Stage is not final or billing section is not enabled";
                            actionresponse.Data = this;
                            actionresponse.ActionSuccessful = true;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                actionresponse.ActionSuccessful = false;
                actionresponse.ExceptionIfAny = ex;
                InvoiceLogger.LogError("Invoice:SendBillingEmail - ", ex);
            }
            return actionresponse;
        }


        /// <summary>
        /// Method to send email from SSViacom service
        /// </summary>
        /// <param name="file"></param>
        /// <param name="SSVIACOMServiceUrl"></param>
        /// <param name="SSVIACOMFromAddress"></param>
        /// <returns></returns>
        private string SendEmail(SPFile file, string SSVIACOMServiceUrl, string SSVIACOMFromAddress, string SSVIACOMFromDisplay)
        {
            string response = string.Empty;
            MailDetails details = new MailDetails();

            MailAddressCollection TO_addressList = new MailAddressCollection();
            if (EmailTo != null)
            {
                foreach (var curr_address in EmailTo.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    MailAddress toAddress = new MailAddress(curr_address);
                    TO_addressList.Add(toAddress);
                }
            }
            MailAddressCollection CC_addressList = new MailAddressCollection();
            if (EmailCC != null)
            {
                foreach (var curr_address in EmailCC.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    MailAddress CCAddress = new MailAddress(curr_address);
                    CC_addressList.Add(CCAddress);
                }
            }

            if (TO_addressList.Count == 0 && CC_addressList.Count == 0)
            {
                EmailSentResponse = "Failure|No recipients available for billing email.";
            }
            else
            {
                details.MailBody = EmailBody;
                details.Subject = EmailSubject;
                details.MailCC = EmailCC;
                details.MailTo = EmailTo;
                details.MailFromDisplay = SSVIACOMFromDisplay;
                details.MailFrom = SSVIACOMFromAddress;

                details.MailAttachmentName = file.Name;
                details.MailAttachmentByteArray = file.OpenBinary();

                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(MailDetails));
                MemoryStream mem = new MemoryStream();
                ser.WriteObject(mem, details);
                string data = Encoding.UTF8.GetString(mem.ToArray(), 0, (int)mem.Length);

                string sendEmailResponse = string.Empty;
                WebClient webClient = new WebClient();
                webClient.Headers["Content-type"] = "application/json";
                webClient.Encoding = Encoding.UTF8;

                sendEmailResponse = webClient.UploadString(SSVIACOMServiceUrl + "/api/SendEmail", "POST", data);

                if (sendEmailResponse.Trim('"').Equals("Mail Sent Successfully", StringComparison.InvariantCultureIgnoreCase))
                {
                    EmailSentResponse = "Success|Email Sent with attachment";
                }
                else
                {
                    EmailSentResponse = "Failure|Mail not sent - No billing document attached";
                }
            }
            return response;
        }

        private void PopulateBillingEmailBodynSubject()
        {
            try
            {
                SPWeb web = SPContext.Current.Web;
                string querystring = string.Concat(
                                                   "<Where><Eq>",
                                                      "<FieldRef Name='", Constants.KEYFIELD, "'/>",
                                                      "<Value Type='Text'>", Constants.BILLINGEMAILTEMPLATEKEY, "</Value>",
                                                   "</Eq></Where>");
                SPListItemCollection items = GetListItems(web, Constants.EMAILTEMPLATELIST, querystring, null);
                if (items != null)
                {
                    SPListItem item = items[0];
                    if (item != null)
                    {
                        EmailSubject = item[Constants.EMAILSUBJECTFIELD].ToString();
                        EmailBody = item[Constants.EMAILBODYFIELD].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private SPFile GetBillingDocument(string itemID)
        {
            try
            {
                SPWeb web = SPContext.Current.Web;
                string querystring = string.Concat(
                                                       "<Where><Eq>",
                                                          "<FieldRef Name='", Constants.CONFIGKEYFIELD, "'/>",
                                                          "<Value Type='Text'>", Constants.FORMDOCLIBNAMEKEY, "</Value>",
                                                       "</Eq></Where>");
                SPListItemCollection items = GetListItems(web, Constants.CONFIGLIST, querystring, null);
                string docLibName = null;
                if (items != null)
                {
                    SPListItem item = items[0];
                    if (item != null)
                    {
                        docLibName = item[Constants.CONFIGVALUEFIELD].ToString();
                        if (docLibName != null)
                        {
                            SPList lib = web.Lists[docLibName];
                            string libfolderurl = lib.RootFolder.ServerRelativeUrl + "/" + itemID;
                            SPFolder folder = web.GetFolder(libfolderurl);
                            SPFileCollection files = folder.Files;
                            SPFile billingFile = null;
                            bool fileFound = false;
                            foreach (SPFile file in files)
                            {
                                if (file.Item[Constants.ATTACHMENTTYPEKEY].ToString().Equals(Constants.BILLINGATTACHTYPE))
                                {
                                    billingFile = file;
                                    fileFound = true;
                                    break;
                                }
                            }
                            if (fileFound)
                            {
                                return billingFile;
                            }
                            else
                            {
                                return null;
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return null;
        }

        private void PopulateFinalEmailBodynSubject(string emailkey)
        {
            try
            {
                SPWeb web = SPContext.Current.Web;
                string querystring = string.Concat(
                                                   "<Where><Eq>",
                                                      "<FieldRef Name='", Constants.KEYFIELD, "'/>",
                                                      "<Value Type='Text'>", emailkey, "</Value>",
                                                   "</Eq></Where>");
                SPListItemCollection items = GetListItems(web, Constants.EMAILTEMPLATELIST, querystring, null);
                if (items != null)
                {
                    SPListItem item = items[0];
                    if (item != null)
                    {
                        EmailSubject = item[Constants.EMAILSUBJECTFIELD].ToString();
                        EmailBody = item[Constants.EMAILBODYFIELD].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IInvoiceMethodResponse<BillingEmail> GetFinallyApprovedEmail(string itemID, string dispFormPath)
        {
            InvoiceMethodResponse<BillingEmail> actionresponse = new InvoiceMethodResponse<BillingEmail>();
            try {
                SPWeb web = SPContext.Current.Web;

                SPListItem item = GetRequestItemByID(web, itemID);
                if (item != null)
                {
                    if (item[Constants.FORMSTATUSFIELD] != null)
                    {
                        string querystring = string.Concat(
                                                       "<Where><Eq>",
                                                          "<FieldRef Name='", Constants.WFCURRENTSTAGEKEYFIELD, "'/>",
                                                          "<Value Type='Text'>", item[Constants.FORMSTATUSFIELD].ToString(), "</Value>",
                                                       "</Eq></Where>");
                        string approvalstglistname = Regex.Replace(Constants.IAAPPROVALSTAGECONFIGLIST, @"\s+", "");
                        SPListItemCollection approvalitems = GetListItems(web, approvalstglistname, querystring, null);
                        if (approvalitems != null && approvalitems.Count > 0)
                        {
                            SPListItem approvalitem = approvalitems[0];
                            string emaillookup = null;
                            string currentSTGPersonField = null;
                            if (approvalitem[Constants.EMAILTEMPLATEKEYFIELD] != null)
                            {
                                emaillookup = getStringDataPart(approvalitem[Constants.EMAILTEMPLATEKEYFIELD].ToString(), '#', 1);
                            }
                            else
                            {
                                actionresponse.ActionSuccessful = false;
                                Exception ex = new Exception("Could not find Email template value for the current Stage");
                                actionresponse.ExceptionIfAny = ex;
                                InvoiceLogger.LogError("Error - GetFinallyApprovedEmail", ex);
                                throw ex;
                            }
                            if (approvalitem[Constants.CURRENTSTGPERSONFIELD] != null)
                                currentSTGPersonField = approvalitem[Constants.CURRENTSTGPERSONFIELD].ToString();
                            else
                            {
                                actionresponse.ActionSuccessful = false;
                                Exception ex = new Exception("Could not find Current Stage Person/Group field value for the current Stage");
                                actionresponse.ExceptionIfAny = ex;
                                InvoiceLogger.LogError("Error - GetFinallyApprovedEmail", ex);
                                throw ex;
                            }
                            PopulateFinalEmailBodynSubject(emaillookup);
                            if (this.EmailSubject != null && this.EmailBody != null)
                            {
                                foreach (KeyValuePair<string, string> entry in EMAILSUBJECTKEYS)
                                {
                                    string entryValue = null;
                                    if (EmailSubject.Contains(entry.Key) && item[entry.Value] != null)
                                    {
                                        if (item[entry.Value].ToString().Contains('#'))
                                        {
                                            entryValue = getStringDataPart(item[entry.Value].ToString(), '#', 1);
                                        }
                                        else
                                        {
                                            entryValue = item[entry.Value].ToString();
                                        }
                                        EmailSubject = EmailSubject.Replace(entry.Key.ToString(), entryValue);
                                    }
                                }
                                EmailBody = EmailBody.Replace(Constants._ITEMIDKEY, itemID);
                                if (item[Constants.REQUESTSUBMITTOR] != null)
                                {
                                    SPFieldUserValue requestor = new SPFieldUserValue(web, item[Constants.REQUESTSUBMITTOR].ToString());
                                    EmailBody = EmailBody.Replace(Constants.ITEMCREATEDBYKEY, requestor.User.Name);
                                    EmailTo = requestor.User.Email;
                                }
                                EmailBody = EmailBody.Replace(Constants.SITEPATHKEY, Constants.SITEPATHVALUE);
                                EmailBody = EmailBody.Replace(Constants.ITEMLINKKEY, web.Url + dispFormPath + Constants.ITEMLINKQUERYSTRING + itemID);
                                if (item[currentSTGPersonField] != null) {
                                    SPFieldUserValue finaluser = new SPFieldUserValue(web, item[currentSTGPersonField].ToString());
                                    EmailBody = EmailBody.Replace(Constants.ITEMMODIFIEDBYKEY, finaluser.User.Name);
                                }
                                querystring = string.Concat(
                                                                           "<Where><Or><Eq>",
                                                                              "<FieldRef Name='", Constants.CONFIGKEYFIELD, "'/>",
                                                                              "<Value Type='Text'>", Constants.SSVIACOMServiceUrl, "</Value></Eq>",
                                    "<Or><Eq>",
                                                                              "<FieldRef Name='", Constants.CONFIGKEYFIELD, "'/>",
                                                                              "<Value Type='Text'>", Constants.SSVIACOMFromAddress, "</Value>",
                                                                           "</Eq>",
                                                                                "<Eq>",
                                                                                "<FieldRef Name='", Constants.CONFIGKEYFIELD, "'/>",
                                                                                "<Value Type='Text'>", Constants.SSVIACOMFromDisplay, "</Value>",
                                                                                "</Eq></Or></Or></Where>");

                                //Get configuration details from subsite
                                SPListItemCollection configItems = GetListItems(web, Constants.CONFIGLIST, querystring, null);
                                string SSVIACOMServiceUrl = null;
                                string SSVIACOMFromAddress = null;
                                string SSVIACOMFromDisplay = null;
                                if (configItems != null)
                                {
                                    foreach (SPListItem configItem in configItems)
                                    {
                                        if (configItem[Constants.CONFIGKEYFIELD] != null && Convert.ToString(configItem[Constants.CONFIGKEYFIELD]).Equals(Constants.SSVIACOMServiceUrl))
                                        {
                                            SSVIACOMServiceUrl = Convert.ToString(configItem[Constants.CONFIGVALUEFIELD]);
                                        }

                                        if (configItem[Constants.CONFIGKEYFIELD] != null && Convert.ToString(configItem[Constants.CONFIGKEYFIELD]).Equals(Constants.SSVIACOMFromAddress))
                                        {
                                            SSVIACOMFromAddress = Convert.ToString(configItem[Constants.CONFIGVALUEFIELD]);
                                        }

                                        if (configItem[Constants.CONFIGKEYFIELD] != null && Convert.ToString(configItem[Constants.CONFIGKEYFIELD]).Equals(Constants.SSVIACOMFromDisplay))
                                        {
                                            SSVIACOMFromDisplay = Convert.ToString(configItem[Constants.CONFIGVALUEFIELD]);
                                        }

                                    }
                                }

                                if (String.IsNullOrEmpty(SSVIACOMServiceUrl))
                                {
                                    InvoiceLogger.LogInfo("Configuration missing from site - " + web.ServerRelativeUrl + " SSVIACOMServiceUrl");
                                }

                                if (String.IsNullOrEmpty(SSVIACOMFromAddress))
                                {
                                    InvoiceLogger.LogInfo("Configuration missing from site - " + web.ServerRelativeUrl + " SSVIACOMFromAddress");
                                }

                                if (String.IsNullOrEmpty(SSVIACOMFromDisplay))
                                {
                                    InvoiceLogger.LogInfo("Configuration missing from site - " + web.ServerRelativeUrl + " SSVIACOMFromDisplay");
                                }

                                string response = SendEmail(SSVIACOMServiceUrl, SSVIACOMFromAddress, SSVIACOMFromDisplay);
                                actionresponse.Data = this;
                                actionresponse.ActionSuccessful = true;
                            }
                        }
                        else
                        {
                            actionresponse.ActionSuccessful = false;
                            Exception ex = new Exception("Could not find Stage with title " + item[Constants.FORMSTATUSFIELD].ToString());
                            actionresponse.ExceptionIfAny = ex;
                            InvoiceLogger.LogError("Error - GetFinallyApprovedEmail", ex);
                            throw ex;
                        }
                    }
                    else
                    {
                        actionresponse.ActionSuccessful = false;
                        Exception ex = new Exception("Could not find form status associated with the current item");
                        actionresponse.ExceptionIfAny = ex;
                        InvoiceLogger.LogError("Error - GetFinallyApprovedEmail", ex);
                        throw ex;
                    }
                }    
            }
            catch (Exception ex)
            {
                actionresponse.ActionSuccessful = false;
                actionresponse.ExceptionIfAny = ex;
                InvoiceLogger.LogError("Invoice:GetFinallyApprovedEmail - ", ex);
            }
            return actionresponse;
        }
        /// <summary>
        /// Method to send email from SSViacom service without attachment
        /// </summary>
        /// <param name="file"></param>
        /// <param name="SSVIACOMServiceUrl"></param>
        /// <param name="SSVIACOMFromAddress"></param>
        /// <returns></returns>
        private string SendEmail(string SSVIACOMServiceUrl, string SSVIACOMFromAddress, string SSVIACOMFromDisplay)
        {
            string response = string.Empty;
            MailDetails details = new MailDetails();

            MailAddressCollection TO_addressList = new MailAddressCollection();
            if (EmailTo != null)
            {
                foreach (var curr_address in EmailTo.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    MailAddress toAddress = new MailAddress(curr_address);
                    TO_addressList.Add(toAddress);
                }
            }
            MailAddressCollection CC_addressList = new MailAddressCollection();
            if (EmailCC != null)
            {
                foreach (var curr_address in EmailCC.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    MailAddress CCAddress = new MailAddress(curr_address);
                    CC_addressList.Add(CCAddress);
                }
            }

            if (TO_addressList.Count == 0 && CC_addressList.Count == 0)
            {
                EmailSentResponse = "Failure|No recipients available for billing email.";
            }
            else
            {
                details.MailBody = EmailBody;
                details.Subject = EmailSubject;
                details.MailCC = EmailCC;
                details.MailTo = EmailTo;
                details.MailFromDisplay = SSVIACOMFromDisplay;
                details.MailFrom = SSVIACOMFromAddress;

                details.MailAttachmentName = null;
                details.MailAttachmentByteArray = null;

                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(MailDetails));
                MemoryStream mem = new MemoryStream();
                ser.WriteObject(mem, details);
                string data = Encoding.UTF8.GetString(mem.ToArray(), 0, (int)mem.Length);

                string sendEmailResponse = string.Empty;
                WebClient webClient = new WebClient();
                webClient.Headers["Content-type"] = "application/json";
                webClient.Encoding = Encoding.UTF8;

                sendEmailResponse = webClient.UploadString(SSVIACOMServiceUrl + "/api/SendEmail", "POST", data);

                if (sendEmailResponse.Trim('"').Equals("Mail Sent Successfully", StringComparison.InvariantCultureIgnoreCase))
                {
                    EmailSentResponse = "Success|Email was sent by the system for the item";
                }
                else
                {
                    EmailSentResponse = "Failure|Mail not sent by the system. Please contact the site adminstrator";
                }
            }
            return response;
        }
    }
}