﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint;

namespace Viacom.AdSpace.Invoice.BAL.Email
{
    interface IBillingEmail
    {
        string EmailTo { get; set; }
        string EmailCC { get; set; }
        string EmailSubject { get; set; }
        string EmailBody { get; set; }
        IInvoiceMethodResponse<BillingEmail> GetBillingEmail(string itemID);
    }
}
