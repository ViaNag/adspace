﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InvoiceBase = Viacom.AdSpace.Invoice.BAL;

namespace Viacom.AdSpace.Invoice.BAL.Entities
{
    public class MailDetails
    {
        public string Subject { get; set; }
        public string MailBody { get; set; }
        public string MailTo { get; set; }
        public string MailCC { get; set; }
        public string MailBCC { get; set; }
        public string MailFrom { get; set; }
        public string MailFromDisplay { get; set; }
        public string MailAttachment { get; set; }
        public string MailAttachmentName { get; set; }
        public byte[] MailAttachmentByteArray { get; set; }
    }

    public class Brand: InvoiceBase.Interfaces.IBrand
    {
        public string BrandValue { get; set; }        
    }

    public class CurrentStatus: InvoiceBase.Interfaces.ICurrentStatus
    {
        public string ItemID { get; set; }
        public string CurrentFormStatus { get; set; }
        public string NextFormStatus { get; set; }
        public string NextRequestChangeFormStatus { get; set; }
        public string CurrentStateGroup { get; set; }
        public string NextStateGroup { get; set; }
        public bool IsUserMemberofNextApproverGroup { get; set; }
        public string IARequestCurrentStateListCol { get; set; }
        public string IARequestCurrentStateListDateCol { get; set; }
        public string IARequestNextStateListCol { get; set; }
        public string IARequestNextStateListDateCol { get; set; }
        public string IARequestNextChangeStateListCol { get; set; }
        public string IARequestNextChangeStateListDateCol { get; set; }
        public double ThresholdAmount { get; set; }
        public bool IsBillingVisibleForNextStage { get; set; }
        public bool IsNextStageLastStage { get; set; }
        public string CurrentStatusMessage { get; set; }
    }

    public class InvoiceDetails: InvoiceBase.Interfaces.IInvoiceDetails
    {
        public string Brand_Year { get; set; }
        public string Network_Call_Sign { get; set; }
        public string Agency_Current_Invoicing { get; set; }
        public string Advertiser_Current { get; set; }
        public string Traffic_Deal_Code { get; set; }
        public string Invoice_ID { get; set; }
        public string Invoice_Rev_No { get; set; }
        public string Brand { get; set; }
        public string Traffic_Deal_Start_Date { get; set; }
        public string Traffic_Deal_End_Date { get; set; }
    }

    public class InvoiceID: InvoiceBase.Interfaces.IInvoiceID
    {
        public string InvoiceIdValue { get; set; }
    }

    public class Network: InvoiceBase.Interfaces.INetwork
    {
        public string ID { get; set; }
        public string NetworkValue { get; set; }
    }
    public class TrafficDeal: InvoiceBase.Interfaces.ITrafficDeal
    {
        public string TrafficDealValue { get; set; }
    }

    public class TrafficDealDetails: InvoiceBase.Interfaces.ITrafficDealDetails
    {
        public string Agency { get; set; }
        public string Advertiser { get; set; }
    }
}
