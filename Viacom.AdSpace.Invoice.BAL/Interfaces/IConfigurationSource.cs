﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Viacom.AdSpace.Invoice.BAL
{
    public interface IConfigurationSource
    {
        List<string> Keys { get;}
        Dictionary<string, string> ConfigItems { get; set; }
    }

}
