﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Viacom.AdSpace.Invoice.BAL
{
    public interface IDataSource
    {
        List<T> GetData<T>(IDataOperationParam opparam);
    }

    public interface IDataSource<T>
    {
        List<T> GetData(IDataOperationParam opparam);
    }
}
