﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Viacom.AdSpace.Invoice.BAL
{
    public interface IRoleSource
    {
        List<IRole> Roles { get; }        
    }
}
