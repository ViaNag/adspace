﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Viacom.AdSpace.Invoice.BAL
{
    public interface IWorkflowStageSource
    {
        List<IWorkflowStage> Stages { get; }
    }

    public interface IWorkflowStageSource<T> where T:IWorkflowStage
    {
        List<T> Stages { get; }
        InvoiceMethodResponse<string> getCurrentStatusMessage(string stageName, string adjAmt);
    }
}
