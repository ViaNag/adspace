﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using Newtonsoft.Json.Linq;
using Viacom.AdSpace.Invoice.BAL;
using Viacom.AdSpace.Invoice.BAL.Roles;
using Viacom.AdSpace.Invoice.BAL.Entities;
using Viacom.AdSpace.Invoice.BAL.Data;
using Viacom.AdSpace.Invoice.BAL.Workflows;
using Viacom.AdSpace.Invoice.BAL.Configuration;

namespace Viacom.AdSpace.Invoice.BAL.InvoiceAdjustment
{
    public class InvoiceAdjustmentRequest : Base, IInvoiceRequest
    {
        
        public IInvoiceMethodResponse<SPListItemCollection> GetUserMappingByNetworknStageGrp(SPWeb web, string networkName, string nextStageGroupColumn)
        {
            SPListItemCollection usermappingItems = null;
            InvoiceMethodResponse<SPListItemCollection> actionresponse = new InvoiceMethodResponse<SPListItemCollection>();
            try
            {
                string querystring = string.Concat(
                                                   "<Where><Eq>",
                                                      "<FieldRef Name='", Constants.NETWORKNAMEFIELD, "'/>",
                                                      "<Value Type='Text'>", networkName, "</Value>",
                                                   "</Eq></Where>");

                if (!string.IsNullOrEmpty(nextStageGroupColumn))
                {
                    string viewstring = string.Concat(
                                                     "<FieldRef Name='", nextStageGroupColumn, "' />");
                    usermappingItems = GetListItems(web, Constants.USERSMAPPINGLIST, querystring, viewstring);
                    actionresponse.Data = usermappingItems;
                }
                actionresponse.ActionSuccessful = true;

            }
            catch (Exception ex)
            {
                InvoiceLogger.LogError("GetUserMappingByNetworknStageGrp: Internal Error", ex);
                actionresponse.ActionSuccessful = false;
                actionresponse.ExceptionIfAny = ex;
            }
            return actionresponse;
        }

        public IInvoiceMethodResponse<Dictionary<string, string>> GetConfigurationItems()
        {
            InvoiceMethodResponse<Dictionary<string, string>> actionresponse = new InvoiceMethodResponse<Dictionary<string, string>>();
            ListConfigurationSource lstConfig = new ListConfigurationSource(SPContext.Current.Web.Url, Constants.CONFIGLIST, Constants.CONFIGKEYFIELD, Constants.CONFIGVALUEFIELD);
            actionresponse.Data = lstConfig.ConfigItems;
            return actionresponse;
        }

        public IInvoiceMethodResponse<List<Network>> GetNetworksList()
        {
            InvoiceMethodResponse<List<Network>> actionresponse = new InvoiceMethodResponse<List<Network>>();
            try
            {
                List<Network> networks = null;
                SPView view = null;
                SPList list = null;
                SPWeb web = SPContext.Current.Web;
                list = web.Lists[Constants.USERSMAPPINGLIST];
                if (list != null)
                {
                    view = list.Views[Constants.USERNETWORKVIEW];
                    if (view != null)
                    {
                        SPListItemCollection items = list.GetItems(view);

                        networks = new List<Network>();
                        foreach (SPListItem item in items)
                        {
                            networks.Add(
                                new Network
                                {
                                    ID = item[Constants.IDFIELD].ToString(),
                                    NetworkValue = item[Constants.NETWORKNAMEFIELD].ToString()
                                });
                        }
                    }

                }
                actionresponse.ActionSuccessful = true;
                actionresponse.Data = networks;
            }
            catch (Exception ex)
            {
                InvoiceLogger.LogError("GetNetworksList: Internal Error", ex);
                actionresponse.ActionSuccessful = false;
                actionresponse.ExceptionIfAny = ex;
            }

            return actionresponse;
        }

        public IInvoiceMethodResponse<bool> IsCurrentUserInRole(string Role, string NetWork)
        {
            InvoiceMethodResponse<bool> actionresponse = new InvoiceMethodResponse<bool>();
            try
            {
                RoleSource rolesrc = new RoleSource();
                Role rl = rolesrc.GetRoleByName(Role);
                if (rl != null)
                {
                    SPWeb web = SPContext.Current.Web;


                    string querystring = string.Concat(
                                        "<Where><Eq>",
                                           "<FieldRef Name='", Constants.NETWORKNAMEFIELD, "'/>",
                                           "<Value Type='Text'>", NetWork, "</Value>",
                                        "</Eq></Where>");
                    string viewstring = string.Concat(
                                        "<FieldRef Name='", rl.AssociatedListColumn, "' />");
                    SPListItemCollection items = GetListItems(web, Constants.USERSMAPPINGLIST, querystring, viewstring);
                    if (items.Count > 0)
                    {
                        SPListItem item = items[0];
                        string grpName = "";
                        SPGroup grp = null;
                        grpName = getStringDataPart(item[Constants.SAFIELD].ToString(), '#', 1);
                        grp = web.SiteGroups[grpName];
                        if (grp != null)
                        {
                            actionresponse.Data = web.IsCurrentUserMemberOfGroup(grp.ID);
                        }
                    }


                }

                actionresponse.ActionSuccessful = true;
            }
            catch (Exception ex)
            {
                InvoiceLogger.LogError("IsCurrentUserInRole: Internal Error", ex);
                actionresponse.ActionSuccessful = false;
                actionresponse.ExceptionIfAny = ex;
            }
            return actionresponse;
        }

        public IInvoiceMethodResponse<bool> IsRequestor(string network)
        {

            return IsCurrentUserInRole("SA", network);
        }


        public IInvoiceMethodResponse<bool> IsApprover(string itemID)
        {
            InvoiceMethodResponse<bool> actionresponse = new InvoiceMethodResponse<bool>();
            actionresponse.Data = false;

            try
            {
                SPWeb web = SPContext.Current.Web;


                SPListItem requestItem = GetRequestItemByID(web, itemID);
                if (requestItem != null)
                {                    
                    string networkName = null;
                    networkName = getStringDataPart(requestItem[Constants.NETWORKFIELD].ToString(), '#', 1);
                    string formStatus = requestItem[Constants.FORMSTATUSFIELD].ToString();
                    double adjAmt = Double.Parse(requestItem[Constants.ADJUSTMENTAMTFIELD].ToString());
                    adjAmt = Math.Abs(adjAmt);
                    string nextStageGroupColumn = getNextStageGroupColumn(formStatus, adjAmt);
                    SPListItemCollection usermappingItems = GetUserMappingByNetworknStageGrp(web, networkName, nextStageGroupColumn).Data;
                    if (usermappingItems.Count > 0)
                    {
                        SPListItem item = usermappingItems[0];
                        string grpName = "";
                        SPGroup grp = null;
                        grpName = getStringDataPart(item[nextStageGroupColumn].ToString(), '#', 1);
                        grp = web.SiteGroups[grpName];
                        if (grp != null)
                        {
                            actionresponse.Data = web.IsCurrentUserMemberOfGroup(grp.ID);
                        }
                    }
                }



                actionresponse.ActionSuccessful = true;

            }
            catch (Exception ex)
            {
                InvoiceLogger.LogError("IsApprover: Internal Error", ex);
                actionresponse.ActionSuccessful = false;
                actionresponse.ExceptionIfAny = ex;
            }
            return actionresponse;
        }

        public IInvoiceMethodResponse<Dictionary<string,bool>> IsFormEditable(string itemID)
        {
            InvoiceMethodResponse<Dictionary<string, bool>> actionresponse = new InvoiceMethodResponse<Dictionary<string, bool>>();
            actionresponse.Data = new Dictionary<string,bool>();
            try
            {
                SPWeb web = SPContext.Current.Web;

                SPListItem requestItem = GetRequestItemByID(web, itemID);
                if (requestItem != null)
                {
                    string networkName = null;
                    string previousApprovalResponse = null;
                    networkName = getStringDataPart(requestItem[Constants.NETWORKFIELD].ToString(), '#', 1);
                    if (requestItem[Constants.PREVAPPROVALRESPONSEFIELD] != null)
                    {
                        previousApprovalResponse = requestItem[Constants.PREVAPPROVALRESPONSEFIELD].ToString();
                    }
                    if (IsRequestor(networkName).Data)
                    {
                        actionresponse.Data.Add(Constants.ISREQUESTOR, true);
                    }
                    else 
                    {
                        actionresponse.Data.Add(Constants.ISREQUESTOR, false);
                    }
                    if (!String.IsNullOrEmpty(previousApprovalResponse))
                    {
                        if (previousApprovalResponse.Equals(Constants.REQCHANGESTATUS))
                        {
                            actionresponse.Data.Add(Constants.ISCHANGEREQUESTED, true);
                        }
                    }
                    else 
                    {
                        actionresponse.Data.Add(Constants.ISCHANGEREQUESTED, false);
                        InvoiceLogger.LogError("IsFormEditable: Previous Approval Response is Empty");
                    }
                }

                actionresponse.ActionSuccessful = true;

            }
            catch (Exception ex)
            {
                InvoiceLogger.LogError("IsFormEditable: Internal Error", ex);
                actionresponse.ActionSuccessful = false;
                actionresponse.ExceptionIfAny = ex;
                //throw new Exception(ex.Message, ex);
            }

            return actionresponse;
        }

        public IInvoiceMethodResponse<CurrentStatus> GetCurrentRequestStatus(string itemID)
        {
            InvoiceMethodResponse<CurrentStatus> actionresponse = new InvoiceMethodResponse<CurrentStatus>();
            try
            {
                CurrentStatus state;
                SPWeb web = SPContext.Current.Web;

                state = new CurrentStatus();
                state.ItemID = itemID;
                SPListItem requestItem = GetRequestItemByID(web, itemID);
                if (requestItem != null)
                {
                    string networkName = null;
                    networkName = getStringDataPart(requestItem[Constants.NETWORKFIELD].ToString(), '#', 1);
                    string formStatus = requestItem[Constants.FORMSTATUSFIELD].ToString();
                    double adjustmentAmt = Double.Parse(requestItem[Constants.ADJUSTMENTAMTFIELD].ToString());
                    adjustmentAmt = Math.Abs(adjustmentAmt);
                    string prevApproverResponse = null;
                    var prevApproverResponseObj = requestItem[Constants.PREVAPPROVALRESPONSEFIELD];
                    if (prevApproverResponseObj != null)
                    {
                        prevApproverResponse = prevApproverResponseObj.ToString();
                    }
                    state.CurrentFormStatus = formStatus;
                    List<InvoiceAdjustmentWorkflowStage> wfStages = new InvoiceAdjustmentInvoiceAdjustmentWorkflowStageSource().Stages;//new InvoiceAdjustmentWorkflowStage().getWorkflowStages();
                    string nextStageGroupColumn = null;
                    string currentStageGroupColumn = null;
                    string currentstatusMessageStage = null;
                    string grpName = null;
                    bool IsThresholdBreached = false;

                    foreach (InvoiceAdjustmentWorkflowStage wfstage in wfStages)
                    {
                        if (wfstage.ApprovalStage.Equals(formStatus, StringComparison.CurrentCultureIgnoreCase))
                        {

                            currentStageGroupColumn = wfstage.StageGroupColumn;
                            state.IARequestCurrentStateListCol = wfstage.IARequestListCol;
                            state.IARequestCurrentStateListDateCol = wfstage.IARequestListDateCol;
                            state.ThresholdAmount = wfstage.ThresholdAmount;
                            if (state.ThresholdAmount > 0 && state.ThresholdAmount <= adjustmentAmt)
                            {
                                state.NextFormStatus = wfstage.ThresholdBreachNextStage;
                            }
                            else
                            {
                                state.NextFormStatus = wfstage.NextApprovalStage;
                            }
                            
                            break;
                        }
                        else if (wfstage.ChangeRequestNextStage != null && wfstage.ChangeRequestNextStage.Equals(formStatus, StringComparison.CurrentCultureIgnoreCase))
                        {
                           
                            string firstStage = new FirstStage().FirstStageValue;

                            if (firstStage != null)
                            {
                                state.NextFormStatus = firstStage;
                                state.IARequestCurrentStateListCol = wfstage.ChangeRequestIARListCol;
                                state.IARequestCurrentStateListDateCol = wfstage.ChangeRequestIARListDateCol;
                                state.ThresholdAmount = 0;

                            }
                            else
                            {

                                throw new Exception("First Stage can not be null or empty");
                            }
                            break;
                        }
                    }
                    foreach (InvoiceAdjustmentWorkflowStage wfstage in wfStages)
                    {
                        if (wfstage.ApprovalStage.Equals(state.NextFormStatus, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (currentStageGroupColumn == null)
                            {
                                currentStageGroupColumn = wfstage.StageGroupColumn;
                            }
                            if(wfstage.ThresholdAmount > 0 && wfstage.ThresholdAmount <= adjustmentAmt){
                                currentstatusMessageStage = wfstage.ThresholdBreachNextStage;
                            }
                            else currentstatusMessageStage = wfstage.NextApprovalStage;
                            
                            nextStageGroupColumn = wfstage.StageGroupColumn;
                            state.IARequestNextStateListCol = wfstage.IARequestListCol;
                            state.IARequestNextStateListDateCol = wfstage.IARequestListDateCol;
                            state.NextRequestChangeFormStatus = wfstage.ChangeRequestNextStage;
                            state.IARequestNextChangeStateListCol = wfstage.ChangeRequestIARListCol;
                            state.IARequestNextChangeStateListDateCol = wfstage.ChangeRequestIARListDateCol;
                            state.IsBillingVisibleForNextStage = wfstage.IsBillingVisible;
                            state.IsNextStageLastStage = wfstage.IsFinalStage;
                            if (wfstage.IsFinalStage)
                            {
                                state.CurrentStatusMessage = Constants.APPROVEDSTATUS;
                            }                       
                            break;
                        }
                    }
                    if (state.CurrentStatusMessage == null)
                    {
                        foreach (InvoiceAdjustmentWorkflowStage wfstage in wfStages)
                        {
                            if (wfstage.ApprovalStage.Equals(currentstatusMessageStage, StringComparison.CurrentCultureIgnoreCase))
                            {
                                if (wfstage.CurrentStatusMessage != null)
                                {
                                    state.CurrentStatusMessage = wfstage.CurrentStatusMessage;
                                }
                                break;
                            }
                        }
                    }
                    SPListItem item = null;
                    SPGroup grp = null;
                    SPListItemCollection usermappingItems = null;
                    if (currentStageGroupColumn != null && nextStageGroupColumn != null)
                    {
                        string querystring = string.Concat(
                                               "<Where><Eq>",
                                                  "<FieldRef Name='", Constants.NETWORKNAMEFIELD, "'/>",
                                                  "<Value Type='Text'>", networkName, "</Value>",
                                               "</Eq></Where>");
                        string viewstring = string.Concat(
                                                             "<FieldRef Name='", currentStageGroupColumn, "' />",
                                                             "<FieldRef Name='", nextStageGroupColumn, "' />");

                        usermappingItems = GetListItems(web, Constants.USERSMAPPINGLIST, querystring, viewstring);
                        if (usermappingItems.Count > 0)
                        {
                            item = usermappingItems[0];
                            grpName = getStringDataPart(item[currentStageGroupColumn].ToString(), '#', 1);
                            state.CurrentStateGroup = grpName;
                            grpName = getStringDataPart(item[nextStageGroupColumn].ToString(), '#', 1);
                            state.NextStateGroup = grpName;
                            grp = web.SiteGroups[grpName];
                            if (grp != null)
                            {
                                if (prevApproverResponse != null && prevApproverResponse.Equals(Constants.REQCHANGESTATUS))
                                {
                                    state.IsUserMemberofNextApproverGroup = false;
                                }
                                else
                                {
                                    state.IsUserMemberofNextApproverGroup = web.IsCurrentUserMemberOfGroup(grp.ID);
                                }
                            }
                        }
                    }
                }

                actionresponse.ActionSuccessful = true;
                actionresponse.Data = state;
            }
            catch (Exception ex)
            {
                InvoiceLogger.LogError("GetCurrentRequestStatus: Internal Error", ex);
                actionresponse.ActionSuccessful = false;
                actionresponse.ExceptionIfAny = ex;
            }
            return actionresponse;
        }

        public IInvoiceMethodResponse<bool> IsBillingVisible(string itemID)
        {
            InvoiceMethodResponse<bool> actionresponse = new InvoiceMethodResponse<bool>();
            actionresponse.Data = false;
            try
            {
                CurrentStatus currentStatus = GetCurrentRequestStatus(itemID).Data;
                if (currentStatus.IsBillingVisibleForNextStage)
                {
                    actionresponse.Data = true;
                }
                actionresponse.ActionSuccessful = true;
            }
            catch (Exception ex)
            {
                InvoiceLogger.LogError("IsBillingVisible: Internal Error", ex);
                actionresponse.ActionSuccessful = false;
                actionresponse.ExceptionIfAny = ex;
            }
            return actionresponse;
        }

        public IInvoiceMethodResponse<bool> SaveApproverComment(string itemID, string approvalStatus, string comments, bool isBillingSelected)
        {
            InvoiceMethodResponse<bool> actionresponse = new InvoiceMethodResponse<bool>();
            actionresponse.Data = false;
            string listName = string.Empty;
            try
            {
                
                if (approvalStatus.Equals(Constants.APPROVEDSTATUS) ||
                    approvalStatus.Equals(Constants.REJECTSTATUS) ||
                    approvalStatus.Equals(Constants.REQCHANGESTATUS))
                {
                    bool isApprover = IsApprover(itemID).Data;
                    if (isApprover)
                    {
                        CurrentStatus currentStatus = GetCurrentRequestStatus(itemID).Data;
                        SPWeb web = SPContext.Current.Web;

                        SPList list = web.Lists[Configuration[Constants.FORMSLISTNAMEKEY]];
                        // Update the List item by ID  
                        SPListItem itemToUpdate = list.GetItemById(Int32.Parse(itemID));
                        if (approvalStatus.Equals(Constants.APPROVEDSTATUS))
                        {
                            itemToUpdate[Constants.FORMSTATUSFIELD] = currentStatus.NextFormStatus;
                            itemToUpdate[currentStatus.IARequestNextStateListCol] = web.CurrentUser;
                            itemToUpdate[currentStatus.IARequestNextStateListDateCol] = DateTime.Now;
                            if (currentStatus.CurrentStatusMessage != null)
                            {
                                itemToUpdate[Constants.CURRENTSTATUSMESSAGEFIELD] = currentStatus.CurrentStatusMessage;
                            }
                            if (list.Fields.ContainsField(Constants.SENDBILLINGATTACHMENTFIELD))
                            {
                                if (isBillingSelected)
                                {
                                    itemToUpdate[Constants.SENDBILLINGATTACHMENTFIELD] = true;
                                }
                                else
                                {
                                    itemToUpdate[Constants.SENDBILLINGATTACHMENTFIELD] = false;
                                }
                            }
                        }
                        if (approvalStatus.Equals(Constants.REJECTSTATUS))
                        {
                            itemToUpdate[Constants.FORMSTATUSFIELD] = Constants.REJECTSTATUS;
                            itemToUpdate[currentStatus.IARequestNextStateListCol] = web.CurrentUser;
                            itemToUpdate[currentStatus.IARequestNextStateListDateCol] = DateTime.Now;
                            itemToUpdate[Constants.CURRENTSTATUSMESSAGEFIELD] = Constants.REJECTSTATUS;
                        }
                        if (approvalStatus.Equals(Constants.REQCHANGESTATUS))
                        {
                            itemToUpdate[Constants.FORMSTATUSFIELD] = currentStatus.NextRequestChangeFormStatus;
                            itemToUpdate[currentStatus.IARequestNextChangeStateListCol] = web.CurrentUser;
                            itemToUpdate[currentStatus.IARequestNextChangeStateListDateCol] = DateTime.Now;
                            if (itemToUpdate[Constants.CURRENTSTATUSMESSAGEFIELD] != null)
                            {
                                string currentStatusMsg = itemToUpdate[Constants.CURRENTSTATUSMESSAGEFIELD].ToString();
                                currentStatusMsg += " " + Constants.REQUESTFORCHANGEMESSAGE;
                                itemToUpdate[Constants.CURRENTSTATUSMESSAGEFIELD] = currentStatusMsg;
                            }
                        }
                        SPFieldUserValueCollection approverValues = (SPFieldUserValueCollection)itemToUpdate[Constants.APPROVERSFIELD];
                        if (approverValues != null)
                        {
                            approverValues.Add(new SPFieldUserValue(web, web.CurrentUser.ID, web.CurrentUser.Name));
                            itemToUpdate[Constants.APPROVERSFIELD] = approverValues;
                        }
                        else
                        {
                            itemToUpdate[Constants.APPROVERSFIELD] = web.CurrentUser;
                        }
                        itemToUpdate[Constants.PREVAPPROVALRESPONSEFIELD] = approvalStatus;
                        itemToUpdate[Constants.FORMPREVSTATUSFIELD] = currentStatus.CurrentFormStatus;
                        
                        comments = comments.Replace(Constants.COMMENTDATEKEY, DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fff"));
                        itemToUpdate[Constants.COMMENTSFIELD] = comments;
                        itemToUpdate[Constants.MODIFYBYFIELD] = web.CurrentUser;
                        itemToUpdate[Constants.ISRESUBMITTEDFIELD] = false;
                        string network = getStringDataPart(itemToUpdate[Constants.NETWORKFIELD].ToString(), '#', 1);
                        List<IRole> userroles = GetCurrentUserRoles(network).Data;
                        if (userroles.Count > 0)
                        {
                            itemToUpdate[Constants.INVOICEADJ_EMAILTEXT] = userroles[0].Title;
                            itemToUpdate[Constants.INVOICEADJ_EMAILLOGINNAME] = SPContext.Current.Web.CurrentUser.Name;

                        }
                        SPSecurity.RunWithElevatedPrivileges(delegate ()
                        {
                            bool unsafeUpdateState = web.AllowUnsafeUpdates;
                            web.AllowUnsafeUpdates = true;
                            itemToUpdate.Update();
                            web.AllowUnsafeUpdates = unsafeUpdateState;
                            actionresponse.Data = true;
                        });

                    }
                }

                actionresponse.ActionSuccessful = true;

            }
            catch (Exception ex)
            {
                InvoiceLogger.LogError("SaveApproverComment: Internal Error", ex);
                actionresponse.ActionSuccessful = false;
                actionresponse.ExceptionIfAny = ex;
            }
            return actionresponse;
        }

        public IInvoiceMethodResponse<bool> SaveInvoiceChanges(string itemID, List<KeyValuePair<string, string>> kvpList)
        {
            InvoiceMethodResponse<bool> actionresponse = new InvoiceMethodResponse<bool>();
            actionresponse.Data = false;
            try
            {
                SPWeb web = SPContext.Current.Web;

                SPListItem requestItem = GetRequestItemByID(web, itemID);
                if (requestItem != null)
                {
                    string network = getStringDataPart(requestItem[Constants.NETWORKFIELD].ToString(), '#', 1);
                    string prevFormStatus = requestItem[Constants.FORMPREVSTATUSFIELD].ToString();
                    string adjAmt = requestItem[Constants.ADJUSTMENTAMTFIELD].ToString();
                    string comment = "";
                    string newcomment = "";
                    string formstatus = string.Empty;
                    CurrentStatus currentStatus = GetCurrentRequestStatus(itemID).Data;
                    comment = requestItem[Constants.COMMENTSFIELD].ToString();
                    comment = comment.Substring(0, comment.IndexOf(']'));
                    newcomment = ",{" +
                        "\"UserName\":\"" + web.CurrentUser.Name + "\"," +
                        "\"Group\":\"" + currentStatus.CurrentStateGroup + "\"," +
                        "\"Comments\":\"Updated By SA\"," +
                        "\"Date\":\"" + DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fff") + "\"}]";
                    comment += newcomment;
                    bool isformEditable = true;
                    foreach (KeyValuePair<string, bool> kvp in IsFormEditable(itemID).Data) {
                        isformEditable = isformEditable && kvp.Value;
                    }
                    if (isformEditable)
                    {
                        bool isAdjAmtChanged = false;
                        string listName = string.Empty;
                        requestItem = web.Lists[Configuration[Constants.FORMSLISTNAMEKEY]].GetItemById(Int32.Parse(itemID));
                        foreach (KeyValuePair<string, string> kvp in kvpList)
                        {
                            if (kvp.Key == Constants.ADJUSTMENTAMTFIELD)
                            {
                                if(!kvp.Value.Equals(adjAmt)){
                                    isAdjAmtChanged = true;
                                }
                            }
                            requestItem[kvp.Key] = kvp.Value;
                        }
                        requestItem[Constants.FORMPREVSTATUSFIELD] = currentStatus.CurrentFormStatus;
                        if (isAdjAmtChanged)
                        {
                             string firstStage = new FirstStage().FirstStageValue;

                             if (firstStage != null)
                             {
                                 requestItem[Constants.FORMSTATUSFIELD] = firstStage;
                                 formstatus = firstStage;
                             }
                             else
                             {
                                 throw new Exception("First Stage can not be null or empty");
                             }
                        }
                        else
                        { 
                            requestItem[Constants.FORMSTATUSFIELD] = prevFormStatus;
                            formstatus = prevFormStatus;
                        }
                        InvoiceMethodResponse<string> res = new InvoiceAdjustmentInvoiceAdjustmentWorkflowStageSource().getCurrentStatusMessage(formstatus, adjAmt);
                        if (res.ActionSuccessful)
                        {
                            requestItem[Constants.CURRENTSTATUSMESSAGEFIELD] = res.Data;
                        }
                        else
                        {
                            throw res.ExceptionIfAny;
                        }
                        requestItem[Constants.PREVAPPROVALRESPONSEFIELD] = Constants.UPDATEDSTATUS;
                        requestItem[Constants.IASACRFIELD] = web.CurrentUser;
                        requestItem[Constants.IASACRDATETIMEFIELD] = DateTime.Now;
                        requestItem[Constants.MODIFYBYFIELD] = web.CurrentUser;
                        requestItem[Constants.COMMENTSFIELD] = comment;
                        requestItem[Constants.ISRESUBMITTEDFIELD] = true;
                        SPSecurity.RunWithElevatedPrivileges(delegate ()
                        {
                            bool unsafeUpdateState = web.AllowUnsafeUpdates;
                            web.AllowUnsafeUpdates = true;
                            requestItem.Update();
                            web.AllowUnsafeUpdates = unsafeUpdateState;
                            actionresponse.Data = true;
                        });
                    }
                }

                actionresponse.ActionSuccessful = true;
            }
            catch (Exception ex)
            {
                InvoiceLogger.LogError("SaveInvoiceChanges: Internal Error ", ex);
                actionresponse.ActionSuccessful = false;
                actionresponse.ExceptionIfAny = ex;
            }
            return actionresponse;
        }

        public IInvoiceMethodResponse<List<Brand>> GetBrandsForNetwork(string Network)
        {
            InvoiceMethodResponse<List<Brand>> actionresponse = new InvoiceMethodResponse<List<Brand>>();
            try
            {
                SQLDataSource sqldata = new SQLDataSource(Constants.CONNECTIONSTRINGKEY, "");
                SQLDataOperationParam dataparam = new SQLDataOperationParam();
                dataparam.SQLCommand = Constants.SP_GETBRANDS;
                dataparam.SQLCommandType = CommandType.StoredProcedure;
                dataparam.CommandParameters = new List<SqlParameter>();
                SqlParameter sqlparam1 = new SqlParameter(Constants.SP_PARA_NETWORKCALLSIGN, SqlDbType.VarChar);
                sqlparam1.Value = Network;
                dataparam.CommandParameters.Add(sqlparam1);
                List<DataRow> allrows = sqldata.GetData(dataparam);
                actionresponse.Data = new List<Brand>();
                foreach (DataRow rw in allrows)
                {
                    actionresponse.Data.Add(new Brand { BrandValue = rw["Brand"].ToString() });
                }
                actionresponse.ActionSuccessful = true;
            }
            catch (Exception ex)
            {
                InvoiceLogger.LogError("GetBrandsForNetwork: Internal Error", ex);
                actionresponse.ActionSuccessful = false;
                actionresponse.ExceptionIfAny = ex;
            }
            return actionresponse;
        }

        public IInvoiceMethodResponse<InvoiceDetails> GetInvoiceDetails(string invoiceID)
        {
            InvoiceMethodResponse<InvoiceDetails> actionresponse = new InvoiceMethodResponse<InvoiceDetails>();
            try
            {
                SQLDataSource sqldata = new SQLDataSource(Constants.CONNECTIONSTRINGKEY);
                SQLDataOperationParam dataparam = new SQLDataOperationParam();
                dataparam.SQLCommand = Constants.SP_GETINVOICEDETAILS;
                dataparam.SQLCommandType = CommandType.StoredProcedure;
                dataparam.CommandParameters = new List<SqlParameter>();
                SqlParameter sqlparam1 = new SqlParameter(Constants.SP_PARA_INVOICEID, SqlDbType.VarChar);
                sqlparam1.Value = invoiceID;
                dataparam.CommandParameters.Add(sqlparam1);
                List<DataRow> allrows = sqldata.GetData(dataparam);
                if (allrows.Count > 0)
                {
                    DataRow dr = allrows[0];
                    InvoiceDetails invoiceDetails = new InvoiceDetails();
                    invoiceDetails.Brand_Year = dr["Brand_Year"].ToString();
                    invoiceDetails.Network_Call_Sign = dr["Network_Call_Sign"].ToString();
                    invoiceDetails.Agency_Current_Invoicing = dr["Agency_Current_Invoicing"].ToString();
                    invoiceDetails.Advertiser_Current = dr["Advertiser_Current"].ToString();
                    invoiceDetails.Traffic_Deal_Code = dr["Traffic_Deal_Code"].ToString();
                    invoiceDetails.Invoice_ID = dr["Invoice_ID"].ToString();
                    invoiceDetails.Invoice_Rev_No = dr["Invoice_Rev_No"].ToString();
                    invoiceDetails.Brand = dr["Brand"].ToString();
                    invoiceDetails.Traffic_Deal_Start_Date = dr["Traffic_Deal_Start_Date"].ToString();
                    invoiceDetails.Traffic_Deal_End_Date = dr["Traffic_Deal_End_Date"].ToString();
                    actionresponse.Data = invoiceDetails;
                }
                actionresponse.ActionSuccessful = true;
            }
            catch (Exception ex)
            {
                InvoiceLogger.LogError("GetInvoiceDetails: Internal Error", ex);
                actionresponse.ActionSuccessful = false;
                actionresponse.ExceptionIfAny = ex;
            }
            return actionresponse;
        }


        public IInvoiceMethodResponse<List<string>> GetInvoicesForBrand(string network, string brand)
        {
            InvoiceMethodResponse<List<string>> actionresponse = new InvoiceMethodResponse<List<string>>();
            try
            {
                SQLDataSource sqldata = new SQLDataSource(Constants.CONNECTIONSTRINGKEY, string.Empty);
                SQLDataOperationParam dataparam = new SQLDataOperationParam();
                dataparam.SQLCommand = Constants.SP_GetINVOICEID;
                dataparam.SQLCommandType = CommandType.StoredProcedure;
                dataparam.CommandParameters = new List<SqlParameter>();
                SqlParameter sqlparam1 = new SqlParameter(Constants.SP_PARA_NETWORKCALLSIGN, SqlDbType.VarChar);
                sqlparam1.Value = network;
                SqlParameter sqlparam2 = new SqlParameter(Constants.SP_PARA_BRAND, SqlDbType.VarChar);
                sqlparam2.Value = brand;
                dataparam.CommandParameters.Add(sqlparam1);
                dataparam.CommandParameters.Add(sqlparam2);
                List<DataRow> allrows = sqldata.GetData(dataparam);
                actionresponse.Data = new List<string>();
                foreach (DataRow rw in allrows)
                {
                    actionresponse.Data.Add(rw["Invoice_ID"].ToString());
                }
                actionresponse.ActionSuccessful = true;
            }
            catch (Exception ex)
            {
                InvoiceLogger.LogError("GetInvoicesForBrand: Internal Error", ex);
                actionresponse.ActionSuccessful = false;
                actionresponse.ExceptionIfAny = ex;
            }
            return actionresponse;
        }


        public IInvoiceMethodResponse<List<IRole>> GetCurrentUserRoles(string NetWork)
        {
            InvoiceMethodResponse<List<IRole>> actionresponse = new InvoiceMethodResponse<List<IRole>>();
            actionresponse.Data = new List<IRole>();
            string networkkeyname = Constants.INVOICEADJ_CURRENTROLES + NetWork.ToUpper();
            try
            {

                List<IRole> _roles = new List<IRole>();
                string querystring = string.Concat(
                                               "<Where><Eq>",
                                                  "<FieldRef Name='", Constants.NETWORKNAMEFIELD, "'/>",
                                                  "<Value Type='Text'>", NetWork, "</Value>",
                                               "</Eq></Where>");

                SPListItemCollection usermappingItems = GetListItems(SPContext.Current.Web, Constants.USERSMAPPINGLIST, querystring, string.Empty);

                if (usermappingItems.Count > 0)
                {
                    SPListItem networkitem = usermappingItems[0];
                    List<IRole> allroles = new RoleSource().Roles;
                    foreach (IRole rl in allroles)
                    {
                        try
                        {
                            string linkedcol = (rl as Role).AssociatedListColumn;
                            SPFieldUser flduser = (SPFieldUser)networkitem.Fields.GetFieldByInternalName(linkedcol);
                            SPFieldUserValue flduserval = (SPFieldUserValue)flduser.GetFieldValue(networkitem[linkedcol].ToString());
                            if (flduserval.User == null)
                            {
                                if (SPContext.Current.Web.IsCurrentUserMemberOfGroup(flduserval.LookupId))
                                {
                                    _roles.Add(rl);
                                }
                            }

                        }
                        catch (Exception ex)
                        {
                            InvoiceLogger.LogError("GetCurrentUserRoles: Internal Error", ex);
                        }

                    }

                }


                actionresponse.Data = _roles;
                actionresponse.ActionSuccessful = true;
            }
            catch (Exception ex)
            {
                InvoiceLogger.LogError("GetCurrentUserRoles: Internal Error", ex);
                actionresponse.ActionSuccessful = false;
                actionresponse.ExceptionIfAny = ex;
            }
            return actionresponse;

        }

        public IInvoiceMethodResponse<string> GetCurrentStatusMessageByStage(string stageName, string adjAmount)
        {
            InvoiceMethodResponse<string> actionresponse = new InvoiceMethodResponse<string>();
            try
            {
                actionresponse = new InvoiceAdjustmentInvoiceAdjustmentWorkflowStageSource().getCurrentStatusMessage(stageName, adjAmount);
            }
            catch (Exception ex)
            {
                InvoiceLogger.LogError("GetCurrentStatusMessageByStage: Internal Error", ex);
                actionresponse.ActionSuccessful = false;
                actionresponse.ExceptionIfAny = ex;
            }
            return actionresponse;
        }
    }
}
