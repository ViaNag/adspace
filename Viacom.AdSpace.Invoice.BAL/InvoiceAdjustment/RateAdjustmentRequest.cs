﻿using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.SharePoint;
using System.Data;
using System.Data.SqlClient;
using Viacom.AdSpace.Invoice.BAL.Entities;
using Viacom.AdSpace.Invoice.BAL.Data;
using Viacom.AdSpace.Invoice.BAL.Workflows;

namespace Viacom.AdSpace.Invoice.BAL.InvoiceAdjustment
{
    public class RateAdjustmentRequest: InvoiceAdjustmentRequest
    {
        public IInvoiceMethodResponse<List<TrafficDeal>> GetTrafficDealByNetwork(string Network) {
            InvoiceMethodResponse<List<TrafficDeal>> actionresponse = new InvoiceMethodResponse<List<TrafficDeal>>();
            try
            {
                SQLDataSource sqldata = new SQLDataSource(Constants.CONNECTIONSTRINGKEY, "");
                SQLDataOperationParam dataparam = new SQLDataOperationParam();
                dataparam.SQLCommand = Constants.SP_GETTRAFFICDEALS;
                dataparam.SQLCommandType = CommandType.StoredProcedure;
                dataparam.CommandParameters = new List<SqlParameter>();
                SqlParameter sqlparam1 = new SqlParameter(Constants.SP_PARA_NETWORKCALLSIGN, SqlDbType.VarChar);
                sqlparam1.Value = Network;
                dataparam.CommandParameters.Add(sqlparam1);
                List<DataRow> allrows = sqldata.GetData(dataparam);
                actionresponse.Data = new List<TrafficDeal>();
                foreach (DataRow rw in allrows)
                {
                    if (rw[Constants.SQL_TRAFFFICDEAL] != null)
                        actionresponse.Data.Add(new TrafficDeal { TrafficDealValue = rw[Constants.SQL_TRAFFFICDEAL].ToString() });
                }
                actionresponse.ActionSuccessful = true;
            }
            catch (Exception ex)
            {
                InvoiceLogger.LogError("GetTrafficDealByNetwork: Internal Error", ex);
                actionresponse.ActionSuccessful = false;
                actionresponse.ExceptionIfAny = ex;
            }
            return actionresponse;
        }

        public IInvoiceMethodResponse<List<TrafficDealDetails>> GetDealDetailsByNetworkNDeal(string Network, string DealNo)
        {
            InvoiceMethodResponse<List<TrafficDealDetails>> actionresponse = new InvoiceMethodResponse<List<TrafficDealDetails>>();
            try
            {
                SQLDataSource sqldata = new SQLDataSource(Constants.CONNECTIONSTRINGKEY, "");
                SQLDataOperationParam dataparam = new SQLDataOperationParam();
                dataparam.SQLCommand = Constants.SP_GETTRAFFICDEALDETAILS;
                dataparam.SQLCommandType = CommandType.StoredProcedure;
                dataparam.CommandParameters = new List<SqlParameter>();
                SqlParameter sqlparam1 = new SqlParameter(Constants.SP_PARA_NETWORKCALLSIGN, SqlDbType.VarChar);
                sqlparam1.Value = Network;
                dataparam.CommandParameters.Add(sqlparam1);
                SqlParameter sqlparam2 = new SqlParameter(Constants.SP_PARA_DEALNO, SqlDbType.VarChar);
                sqlparam2.Value = DealNo;
                dataparam.CommandParameters.Add(sqlparam2);
                List<DataRow> allrows = sqldata.GetData(dataparam);
                actionresponse.Data = new List<TrafficDealDetails>();
                foreach (DataRow rw in allrows)
                {
                    if (rw[Constants.SQL_AGENCEY] != null && rw[Constants.SQL_ADVERTISER] != null)
                        actionresponse.Data.Add(new TrafficDealDetails {
                            Agency = rw[Constants.SQL_AGENCEY].ToString(),
                            Advertiser = rw[Constants.SQL_ADVERTISER].ToString()
                        });
                }
                actionresponse.ActionSuccessful = true;
            }
            catch (Exception ex)
            {
                InvoiceLogger.LogError("GetDealDetailsByNetworkNDeal: Internal Error", ex);
                actionresponse.ActionSuccessful = false;
                actionresponse.ExceptionIfAny = ex;
            }
            return actionresponse;
        }

        public IInvoiceMethodResponse<List<Brand>> GetBrandByNetworkNDeal(string Network, string DealNo)
        {
            InvoiceMethodResponse<List<Brand>> actionresponse = new InvoiceMethodResponse<List<Brand>>();
            try
            {
                SQLDataSource sqldata = new SQLDataSource(Constants.CONNECTIONSTRINGKEY, "");
                SQLDataOperationParam dataparam = new SQLDataOperationParam();
                dataparam.SQLCommand = Constants.SP_GETTRAFFICDEALBRANDS;
                dataparam.SQLCommandType = CommandType.StoredProcedure;
                dataparam.CommandParameters = new List<SqlParameter>();
                SqlParameter sqlparam1 = new SqlParameter(Constants.SP_PARA_NETWORKCALLSIGN, SqlDbType.VarChar);
                sqlparam1.Value = Network;
                dataparam.CommandParameters.Add(sqlparam1);
                SqlParameter sqlparam2 = new SqlParameter(Constants.SP_PARA_DEALNO, SqlDbType.VarChar);
                sqlparam2.Value = DealNo;
                dataparam.CommandParameters.Add(sqlparam2);
                List<DataRow> allrows = sqldata.GetData(dataparam);
                actionresponse.Data = new List<Brand>();
                foreach (DataRow rw in allrows)
                {
                    if(rw[Constants.SQL_BRAND] != null)
                        actionresponse.Data.Add(new Brand
                        {
                            BrandValue = rw[Constants.SQL_BRAND].ToString()
                        });
                }
                actionresponse.ActionSuccessful = true;
            }
            catch (Exception ex)
            {
                InvoiceLogger.LogError("GetBrandByNetworkNDeal: Internal Error", ex);
                actionresponse.ActionSuccessful = false;
                actionresponse.ExceptionIfAny = ex;
            }
            return actionresponse;
        }

        public IInvoiceMethodResponse<bool> SaveRateAdjChanges(string itemID, List<KeyValuePair<string, string>> kvpList, ArrayList childList)
        {
            InvoiceMethodResponse<bool> actionresponse = new InvoiceMethodResponse<bool>();
            actionresponse.Data = false;
            try
            {
                SPWeb web = SPContext.Current.Web;

                SPListItem requestItem = GetRequestItemByID(web, itemID);
                if (requestItem != null)
                {
                    string network = null;
                    string prevFormStatus = null;
                    string adjAmt = null;
                    string comment = "";
                    string newcomment = "";
                    string formstatus = string.Empty;
                    if (requestItem[Constants.NETWORKFIELD] != null)
                        network = getStringDataPart(requestItem[Constants.NETWORKFIELD].ToString(), '#', 1);
                    if (requestItem[Constants.FORMPREVSTATUSFIELD] != null)
                        prevFormStatus = requestItem[Constants.FORMPREVSTATUSFIELD].ToString();
                    if (requestItem[Constants.ADJUSTMENTAMTFIELD] != null)
                        adjAmt = requestItem[Constants.ADJUSTMENTAMTFIELD].ToString();
                    CurrentStatus currentStatus = GetCurrentRequestStatus(itemID).Data;
                    if (requestItem[Constants.COMMENTSFIELD] != null)
                        comment = requestItem[Constants.COMMENTSFIELD].ToString();
                    comment = comment.Substring(0, comment.IndexOf(']'));
                    newcomment = ",{" +
                        "\"UserName\":\"" + web.CurrentUser.Name + "\"," +
                        "\"Group\":\"" + currentStatus.CurrentStateGroup + "\"," +
                        "\"Comments\":\"Updated By SA\"," +
                        "\"Date\":\"" + DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fff") + "\"}]";
                    comment += newcomment;
                    bool isRequestor = false;
                    bool isChangeRequested = false;
                    foreach (KeyValuePair<string, bool> kvp in IsFormEditable(itemID).Data)
                    {
                        if (kvp.Key.Equals(Constants.ISREQUESTOR)) {
                            isRequestor = kvp.Value;
                        }
                        else if (kvp.Key.Equals(Constants.ISCHANGEREQUESTED)) {
                            isChangeRequested = kvp.Value;
                        }
                    }
                    if (isRequestor && isChangeRequested)
                    {
                        IInvoiceMethodResponse<bool> childRateChange = UpdateChildItems(itemID, childList);
                        if (!childRateChange.ActionSuccessful) {
                            if (childRateChange.ExceptionIfAny != null)
                            {
                                throw childRateChange.ExceptionIfAny;
                            }
                            else {
                                throw new Exception("There was some issue in updating child item");
                            }
                        }
                        string listName = string.Empty;
                        requestItem = web.Lists[Configuration[Constants.FORMSLISTNAMEKEY]].GetItemById(Int32.Parse(itemID));
                        foreach (KeyValuePair<string, string> kvp in kvpList)
                        {
                            requestItem[kvp.Key] = kvp.Value;
                        }
                        requestItem[Constants.FORMPREVSTATUSFIELD] = currentStatus.CurrentFormStatus;

                        if (childRateChange.Data)
                        {
                            string firstStage = new FirstStage().FirstStageValue;

                            if (firstStage != null)
                            {
                                requestItem[Constants.FORMSTATUSFIELD] = firstStage;
                                formstatus = firstStage;
                            }
                            else
                            {
                                throw new Exception("First Stage can not be null or empty");
                            }
                        }
                        else
                        {
                            requestItem[Constants.FORMSTATUSFIELD] = prevFormStatus;
                            formstatus = prevFormStatus;
                        }

                        InvoiceMethodResponse<string> res = new InvoiceAdjustmentInvoiceAdjustmentWorkflowStageSource().getCurrentStatusMessage(formstatus, adjAmt);
                        if (res.ActionSuccessful)
                        {
                            requestItem[Constants.CURRENTSTATUSMESSAGEFIELD] = res.Data;
                        }
                        else
                        {
                            throw res.ExceptionIfAny;
                        }
                        requestItem[Constants.PREVAPPROVALRESPONSEFIELD] = Constants.UPDATEDSTATUS;
                        requestItem[Constants.IASACRFIELD] = web.CurrentUser;
                        requestItem[Constants.IASACRDATETIMEFIELD] = DateTime.Now;
                        requestItem[Constants.MODIFYBYFIELD] = web.CurrentUser;
                        requestItem[Constants.COMMENTSFIELD] = comment;
                        requestItem[Constants.ISRESUBMITTEDFIELD] = true;
                        SPSecurity.RunWithElevatedPrivileges(delegate()
                        {
                            bool unsafeUpdateState = web.AllowUnsafeUpdates;
                            web.AllowUnsafeUpdates = true;
                            requestItem.Update();
                            web.AllowUnsafeUpdates = unsafeUpdateState;
                        });
                    }
                }
                actionresponse.ActionSuccessful = true;
                actionresponse.Data = true;                
            }
            catch (Exception ex)
            {
                InvoiceLogger.LogError("SaveRateAdjChanges: Internal Error ", ex);
                actionresponse.ActionSuccessful = false;
                actionresponse.ExceptionIfAny = ex;
            }
            return actionresponse;
        }

        private IInvoiceMethodResponse<bool> UpdateChildItems(string itemID, ArrayList childList) {
            InvoiceMethodResponse<bool> actionresponse = new InvoiceMethodResponse<bool>();
            actionresponse.Data = false;
            SPWeb web = SPContext.Current.Web;
            try
            {
                List<string> itemsToUpdate = new List<string>();
                List<string> itemsToDelete = new List<string>();                
                SPList list = web.Lists[Configuration[Constants.FORMCHILDLISTNAMEKEY]];
                SPFolder folder = list.RootFolder.SubFolders[itemID];
                SPQuery query = new SPQuery();
                query.Folder = folder;
                SPListItemCollection items = list.GetItems(query);
                if (items.Count > 0)
                {

                    foreach (List<KeyValuePair<string, string>> lstItem in childList)
                    {
                        foreach (KeyValuePair<string, string> kvp in lstItem)
                        {
                            if (kvp.Key == Constants.IDFIELD)
                            {
                                if (kvp.Value.Length > 0)
                                {
                                    itemsToUpdate.Add(kvp.Value);
                                }
                                break;
                            }
                        }
                    }
                    foreach (SPListItem item in items)
                    {
                        bool found = false;
                        foreach (string id in itemsToUpdate)
                        {
                            if (item[Constants.IDFIELD].ToString() == id)
                            {
                                found = true;
                                break;
                            }
                        }
                        if (!found)
                        {
                            itemsToDelete.Add(item[Constants.IDFIELD].ToString());
                        }
                    }
                }
                foreach (string id in itemsToDelete)
                {
                    SPListItem itemToDelete = list.GetItemById(Int32.Parse(id));
                    SPSecurity.RunWithElevatedPrivileges(delegate()
                    {
                        bool unsafeUpdate = web.AllowUnsafeUpdates;
                        web.AllowUnsafeUpdates = true;
                        itemToDelete.Delete();
                        web.AllowUnsafeUpdates = unsafeUpdate;
                        actionresponse.Data = true;
                    });
                }
                foreach (string id in itemsToUpdate)
                {
                    SPListItem itemToUpdate = list.GetItemById(Int32.Parse(id));
                    foreach (List<KeyValuePair<string, string>> lstItem in childList)
                    {
                        bool found = false;
                        foreach (KeyValuePair<string, string> kvp in lstItem)
                        {
                            if (kvp.Key == Constants.IDFIELD && kvp.Value == id)
                            {
                                found = true;
                                break;
                            }
                        }
                        if (found)
                        {
                            foreach (KeyValuePair<string, string> kvp in lstItem)
                            {
                                if (kvp.Key == Constants.IDFIELD || kvp.Key == Constants.TITLEFIELD)
                                {
                                    continue;
                                }
                                else
                                {
                                    if (kvp.Key == Constants.OLDRATEFIELD || kvp.Key == Constants.NEWRATEFIELD)
                                    {
                                        double amount = Double.Parse(itemToUpdate[kvp.Key].ToString());
                                        if (Double.Parse(kvp.Value) != amount)
                                        {
                                            actionresponse.Data = true;
                                        }
                                    }
                                    itemToUpdate[kvp.Key] = kvp.Value;
                                }
                            }
                            SPSecurity.RunWithElevatedPrivileges(delegate()
                            {
                                bool unsafeUpdate = web.AllowUnsafeUpdates;
                                web.AllowUnsafeUpdates = true;
                                itemToUpdate.Update();
                                web.AllowUnsafeUpdates = unsafeUpdate;
                            });
                        }
                    }
                }
                foreach (List<KeyValuePair<string, string>> lstItem in childList)
                {
                    bool create = false;
                    foreach (KeyValuePair<string, string> kvp in lstItem)
                    {
                        if (kvp.Key == Constants.IDFIELD)
                        {
                            if (kvp.Value.Length == 0)
                            {
                                create = true;
                                break;
                            }
                        }
                    }
                    if (create)
                    {
                        SPListItem newItem = list.Items.Add(folder.ServerRelativeUrl, SPFileSystemObjectType.File, null);
                        foreach (KeyValuePair<string, string> kvp in lstItem)
                        {
                            if (kvp.Key == Constants.IDFIELD)
                            {
                                continue;
                            }
                            else
                            {
                                newItem[kvp.Key] = kvp.Value;
                            }
                        }
                        SPSecurity.RunWithElevatedPrivileges(delegate()
                        {
                            bool unsafeUpdate = web.AllowUnsafeUpdates;
                            web.AllowUnsafeUpdates = true;
                            newItem.Update();
                            web.AllowUnsafeUpdates = unsafeUpdate;
                            actionresponse.Data = true;
                        });
                    }
                }
                actionresponse.ActionSuccessful = true;
            }
            catch (Exception ex)
            {
                InvoiceLogger.LogError("UpdateChildItems: Internal Error ", ex);
                actionresponse.ActionSuccessful = false;
                actionresponse.ExceptionIfAny = ex;
            }
            finally {
                web.AllowUnsafeUpdates = false;
            }
            return actionresponse;
        }
    }
}
