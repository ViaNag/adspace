﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint.Administration;
namespace Viacom.AdSpace.Invoice.BAL.Logging
{
    public class ULSLogger:ILogger
    {

        string _applicationname;
        public ULSLogger(string applicationname)
        {
            _applicationname = applicationname;
        }

        public ULSLogger()
        {
            _applicationname = "Viacom.AdSpace.Invoice";
        }

        SPDiagnosticsCategory catinfo
        {
            get 
            {
                return new SPDiagnosticsCategory(_applicationname, TraceSeverity.High, EventSeverity.Information);
            }
        }

        SPDiagnosticsCategory caterror
        {
            get
            {
                return new SPDiagnosticsCategory(_applicationname, TraceSeverity.Unexpected, EventSeverity.Error);
            }
        }
        public void LogInfo(string Infotext)
        {
            SPDiagnosticsService.Local.WriteEvent(0, catinfo, EventSeverity.Information, Infotext);
        }

        public void LogError(string error)
        {
            SPDiagnosticsService.Local.WriteEvent(0, caterror,EventSeverity.Error,error);            
        }

        public void LogError(string error, Exception ex)
        {
            SPDiagnosticsService.Local.WriteEvent(0, caterror, EventSeverity.Error, "Error: {0} Message: {1} Stacktrace: {2}", error ,ex.Message, ex.StackTrace);            
           
        }
    }
}
