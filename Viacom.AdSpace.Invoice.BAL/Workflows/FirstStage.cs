﻿using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Viacom.AdSpace.Invoice.BAL.Interfaces;

namespace Viacom.AdSpace.Invoice.BAL.Workflows
{
    public class FirstStage : Base,IFirstStage
    {
        public string FirstStageValue
        {
            get
            {
                try
                {
                    string querystring = string.Concat(
                                                       "<Where>",
                                                       "<Eq>",
                                                          "<FieldRef Name='", Constants.CONFIGKEYFIELD, "'/>",
                                                          "<Value Type='Text'>", Constants.FIRSTSTAGEKEY, "</Value>",
                                                       "</Eq>",
                                                       "</Where>");

                    SPWeb web = SPContext.Current.Web;
                    SPListItemCollection items = GetListItems(web, Constants.CONFIGLIST, querystring, null);
                    if (items != null && items.Count > 0)
                    {

                        SPListItem configItem = items[0];
                        if (configItem[Constants.CONFIGKEYFIELD] != null)
                        {
                            return configItem[Constants.CONFIGVALUEFIELD].ToString();

                        }
                    }
                }
                catch (Exception ex) { throw ex; }
                return null;
            }
        }
    }

}
