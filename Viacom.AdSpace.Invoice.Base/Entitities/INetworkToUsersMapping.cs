﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Viacom.AdSpace.Invoice.Base.Entitities
{
    public interface INetworkToUsersMapping
    {
        string NetworkName { get;}
        Dictionary<string, string> UserMapping { get; }
    }
}
