﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Viacom.AdSpace.Invoice.InvoiceAdjustment;
using Viacom.AdSpace.Invoice.InvoiceAdjustment.Entities;
using Viacom.AdSpace.Invoice.InvoiceAdjustment.Workflows;

using Microsoft.SharePoint;

namespace Viacom.AdSpace.Invoice.InvoiceAdjustment.Actions
{
    public class InvoiceAdjustmentActions : IInvoiceAdjustmentActions
    {        
        public SPListItemCollection getListItems(SPWeb web, string listname, string querystring, string viewstring)
        {
            SPListItemCollection items = null;
            // Build a query.
            try
            {
                if (!string.IsNullOrEmpty(listname) && !string.IsNullOrEmpty(querystring))
                {
                    SPQuery query = new SPQuery();
                    query.Query = querystring;
                    if (!string.IsNullOrEmpty(viewstring))
                    {
                        query.ViewFields = viewstring;

                        query.ViewFieldsOnly = true;
                    }
                    // Get data from a list.
                    string listUrl = web.ServerRelativeUrl + "/lists/" + listname;
                    SPList list = web.GetList(listUrl);
                    items = list.GetItems(query);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
            return items;
        }
        public string getNextStageGroupColumn(string currentStage, int adjAmt)
        {
            string nextStageGroupColumn = null;
            string nextStage = null;
            List<InvoiceAdjustmentWorkflowStage> stages = new InvoiceAdjustmentInvoiceAdjustmentWorkflowStageSource().Stages;//new InvoiceAdjustmentWorkflowStage().getWorkflowStages();
            foreach (InvoiceAdjustmentWorkflowStage stage in stages)
            {
                if (stage.ApprovalStage.Equals(currentStage))
                {
                    if (stage.ThresholdAmount > 0 && stage.ThresholdAmount <= adjAmt)
                    {
                        nextStage = stage.ThresholdBreachNextStage;
                    }
                    else
                    {
                        nextStage = stage.NextApprovalStage;
                    }
                    break;
                }
            }
            foreach (InvoiceAdjustmentWorkflowStage stage in stages)
            {
                if (stage.ApprovalStage.Equals(nextStage))
                {
                    nextStageGroupColumn = stage.StageGroupColumn;
                    break;
                }
            }
            return nextStageGroupColumn;
        }
        public SPListItemCollection getRequestItemByID(SPWeb web, string itemID)
        {
            SPListItemCollection requestItems = null;
            try
            {
                string querystring = string.Concat(
                                                       "<Where><Eq>",
                                                          "<FieldRef Name='", Constants.IDFIELD, "'/>",
                                                          "<Value Type='Text'>", itemID, "</Value>",
                                                       "</Eq></Where>");
                string viewstring = null;
                requestItems = getListItems(web, Constants.IAREQUESTLIST, querystring, viewstring);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
            return requestItems;
        }
        public string getStringDataPart(string text, char delimiter, int subStringPartNumber)
        {
            string result = null;
            result = text.Split(delimiter)[subStringPartNumber];
            return result;
        }
        public SPListItemCollection GetUserMappingByNetworknStageGrp(SPWeb web, string networkName, string nextStageGroupColumn)
        {
            SPListItemCollection usermappingItems = null;
            try
            {
                string querystring = string.Concat(
                                                   "<Where><Eq>",
                                                      "<FieldRef Name='", Constants.NETWORKNAMEFIELD, "'/>",
                                                      "<Value Type='Text'>", networkName, "</Value>",
                                                   "</Eq></Where>");

                if (!string.IsNullOrEmpty(nextStageGroupColumn))
                {
                    string viewstring = string.Concat(
                                                     "<FieldRef Name='", nextStageGroupColumn, "' />");
                    usermappingItems = getListItems(web, Constants.USERSMAPPINGLIST, querystring, viewstring);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
            return usermappingItems;
        }

        public Dictionary<string, string> GetConfigurationItems()
        {
            throw new NotImplementedException();
        }
    }
}
