﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Viacom.AdSpace.Invoice.InvoiceAdjustment.Actions
{
    public interface IActionResponse<T>
    {
        bool ActionSuccessful { get; set; }
        Exception ExceptionIfAny { get; set; }
        T Data { get; set; }
    }

    public class ActionResponse<T>:IActionResponse<T>
    {
        public bool ActionSuccessful
        {
            get;
            set;
        }

        public Exception ExceptionIfAny
        {
            get;
            set;
        }

        public T Data
        {
            get;
            set;
        }

    }

}
