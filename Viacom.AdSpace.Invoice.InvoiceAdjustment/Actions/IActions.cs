﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Viacom.AdSpace.Invoice.InvoiceAdjustment.Entities;

namespace Viacom.AdSpace.Invoice.InvoiceAdjustment.Actions
{
    public interface IInvoiceAdjustmentActions
    {
        Dictionary<string, string> GetConfigurationItems();

    }
}
