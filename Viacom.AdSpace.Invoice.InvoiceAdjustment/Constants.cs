﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Viacom.AdSpace.Invoice.InvoiceAdjustment
{
    public class Constants
    {
        public const string USERSMAPPINGLIST = "UsersMapping";
        public const string IAREQUESTLIST = "Invoice Adjustment Requests";
        public const string IAREQUESTDOCLIST = "Invoice Adjustment Request Documents";
        public const string IAAPPROVALSTAGESCONFIGLIST = "Approval Stages Configuration";
        public const string USERNETWORKVIEW = "User Networks";
        public const string NETWORKFIELD = "Network";
        public const string IDFIELD = "ID";
        public const string NETWORKNAMEFIELD = "NetworkName";
        public const string SAFIELD = "SA";
        public const string IASACRFIELD = "IA_SA_ChangeReq";
        public const string IASACRDATETIMEFIELD = "IA_SA_ChangeReqDateTime";
        public const string TITLEFIELD = "Title";
        public const string FORMSTATUSFIELD = "Form Status";
        public const string FORMPREVSTATUSFIELD = "IA_FormPreviousStatus";
        public const string COMMENTSFIELD = "IA_InvoiceCommentSummary";
        public const string MODIFYBYFIELD = "Editor";
        public const string ADJUSTMENTAMTFIELD = "IA_AdjustmentAmount";
        public const string APPROVERSFIELD = "IA_Approvers";
        public const string PREVAPPROVALRESPONSEFIELD = "IA_PreviousApprovalResponse";
        public const string ISRESUBMITTEDFIELD = "IA_IsResubmitted";
        public const string CONNECTIONSTRINGKEY = "AdSpaceInvoiceAdjustment";
        public const string SP_GETBRANDS = "SP_GetBrands";
        public const string SP_PARA_NETWORKCALLSIGN = "@NetworkCallSign";
        public const string SP_GetINVOICEID = "SP_GetInvoiceID";
        public const string SP_PARA_BRAND = "@Brand";
        public const string SP_GETINVOICEDETAILS = "SP_GetInvoicedetails";
        public const string SP_PARA_INVOICEID = "@InvoiceID";
        public const string APPROVEDSTATUS = "Approved";
        public const string REJECTSTATUS = "Rejected";
        public const string REQCHANGESTATUS = "RequestChange";
        public const string UPDATEDSTATUS = "Updated";
        public const string SUBMITTEDBYSASTATUS = "SubmittedBySA";
        public const string ITEMIDKEY = "itemID";
        public const string APPROVALSTATUSKEY = "approvalStatus";
        public const string COMMENTKEY = "comment";
        public const string INVOICEDATAKEY = "invoiceData";
    }

}
