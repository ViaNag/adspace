﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace Viacom.AdSpace.Invoice.InvoiceAdjustment.Data
{
    public class SQLDataOperationParam
    {
        public string SQLCommand { get; set; }
        public CommandType SQLCommandType { get; set; }
        public List<SqlParameter> CommandParameters { get; set; }
        public SqlCommand GetCommand(SqlConnection conn)
        {
            SqlCommand cmd = new SqlCommand(SQLCommand, conn);
            cmd.CommandType = SQLCommandType;
            foreach (SqlParameter sqlp1 in CommandParameters)
            {
                cmd.Parameters.Add(sqlp1);
            }
            return cmd;
        }
    }
}
