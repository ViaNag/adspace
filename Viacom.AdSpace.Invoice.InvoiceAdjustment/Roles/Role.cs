﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InvoiceBase = Viacom.AdSpace.Invoice.Base;

namespace Viacom.AdSpace.Invoice.InvoiceAdjustment.Roles
{
    public class Role:InvoiceBase.Roles.IRole
    {
        string _title;
        public string Title
        {
            get { return _title; }            
        }

        string _id;
        public string ID
        {
            get { return _id; }            
        }

        string _assoclistcolumn;
        public string AssociatedListColumn
        {
            get { return _assoclistcolumn; }
        }

        public Role(string title,string id,string asscolistcolumn)
        {
            _title = title;
            _id = id;
            _assoclistcolumn = asscolistcolumn;
        }

        
    }

    public class RoleSource : InvoiceBase.Roles.IRoleSource
    {
        public List<InvoiceBase.Roles.IRole> Roles
        {
            get
            {
                List<InvoiceBase.Roles.IRole> _roles=new List<InvoiceBase.Roles.IRole>();

                _roles.Add(new Role("SA", "SA", "SA"));
                _roles.Add(new Role("CSRManager", "CSRManager", "CSRManager"));
                _roles.Add(new Role("PnlManager", "PnlManager", "PnlManager"));
                _roles.Add(new Role("TrafficDirector", "TrafficDirector", "TrafficDirector"));
                _roles.Add(new Role("SRVP", "SRVP", "SRVP"));
                _roles.Add(new Role("BillingTeam", "BillingTeam", "BillingTeam"));
                _roles.Add(new Role("EVP", "EVP", "EVP"));
                _roles.Add(new Role("AR", "AR", "AR"));
                _roles.Add(new Role("BillingManager", "BillingManager", "BillingManager"));
                _roles.Add(new Role("BusinessAnalyst", "BusinessAnalyst", "BusinessAnalyst"));

                return _roles; 
            }
        }

        public Role GetRoleByName(string name)
        {
            return Roles.First(x => x.Title.ToLower() == name.ToLower()) as Role;
        }

        public Role GetRoleByListCol(string listcol)
        {
            return Roles.First(x => ((Role)x).AssociatedListColumn.ToLower() == listcol.ToLower()) as Role;
        }
    }

}
