﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InvoiceBase = Viacom.AdSpace.Invoice.Base;

namespace Viacom.AdSpace.Invoice.InvoiceAdjustment.Workflows
{
   public class InvoiceAdjustmentWorkflowStage:InvoiceBase.Workflows.IWorkflowStage
   {
       public string ApprovalStage { get; set; }
       public int ApprovalStageOrder { get; set; }
       public string NextApprovalStage { get; set; }
       public string StageGroupColumn { get; set; }
       public string IARequestListCol { get; set; }
       public string IARequestListDateCol { get; set; }
       public string ChangeRequestNextStage { get; set; }
       public int ThresholdAmount { get; set; }
       public string ThresholdBreachNextStage { get; set; }
       public bool IsBillingVisible { get; set; }
       public bool IsFinalStage { get; set; }
   }
}
