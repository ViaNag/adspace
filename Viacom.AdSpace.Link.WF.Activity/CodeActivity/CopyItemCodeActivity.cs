﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;

namespace Viacom.AdSpace.Link.WF.Activity
{
    public sealed class CopyItemCodeActivity : CodeActivity
    {
        /// <summary>
        /// Gets or sets a value for ListId
        /// </summary>
        public InArgument<string> SiteUrl { get; set; }

        /// <summary>
        /// Gets or sets a value for ListId
        /// </summary>
        public InArgument<string> ListName { get; set; }

        /// <summary>
        /// Gets or sets a value for ListItem
        /// </summary>
        public InArgument<string> ItemId { get; set; }

        /// <summary>
        /// Gets or sets a value for ListItem
        /// </summary>
        public OutArgument<string> OutputStatus { get; set; }

        // If your activity returns a value, derive from CodeActivity<TResult>
        // and return the value from the Execute method.
        protected override void Execute(CodeActivityContext context)
        {
            try
            {
                string siteUrl = context.GetValue<string>(this.SiteUrl);
                string listName = context.GetValue<string>(this.ListName);
                string itemID = context.GetValue<string>(this.ItemId);
                // validate
                if (siteUrl == null || listName == null || itemID == null)
                {
                    throw new Exception("Site url, list name, or ItemID cannot be null!", null);
                }
                else
                {
                    this.CopySPListItemInAnotherList(siteUrl, listName, itemID);
                    this.OutputStatus.Set(context, "Item successfully copied.");
                }
            }
            catch (Exception ex)
            {
                this.OutputStatus.Set(context, "Item copy failed: " + ex.Message);
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("AdSpace Workflow Error", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.StackTrace, null);
            }
        }

        /// <summary>
        /// promote link to parent link list
        /// </summary>
        /// <param name="siteUrl"></param>
        /// <param name="listName"></param>
        /// <param name="itemID"></param>
        private void CopySPListItemInAnotherList(string siteUrl, string listName, string itemID)
        {
            using (SPSite site = new SPSite(siteUrl))
            {
                if (site != null)
                {
                    // Get the web site
                    using (SPWeb webSub = site.OpenWeb())
                    {
                        if (webSub != null)
                        {
                            classEventFiring objclassEventFiring = new classEventFiring();
                            objclassEventFiring.DisableHandleEventFiring();
                            SPList listSub = webSub.Lists[listName];
                            SPListItem itemSub = listSub.GetItemById(Convert.ToInt32(itemID));
                            SPWeb webParent = webSub.ParentWeb;
                            if (webParent != null)
                            {
                                SPList listParent = webParent.Lists[listName];
                                SPQuery query = new SPQuery();
                                query.Query = "<Where><And><Eq><FieldRef Name='Title' /><Value Type='Text'>" + Convert.ToString(itemSub["Title"])
                                    + "</Value></Eq><Eq><FieldRef Name='LinkSource' /><Value Type='Text'>" + webSub.Title + "</Value></Eq></And></Where>";
                                //<Where><And><Eq><FieldRef Name='Title' /><Value Type='Text'>abc</Value></Eq><Eq><FieldRef Name='LinkSource' /><Value Type='Text'>xyz</Value></Eq></And></Where>
                                SPListItemCollection items = listParent.GetItems(query);
                                if (items != null && items.Count > 0)
                                {
                                    foreach (SPListItem updateItem in items)
                                    {
                                        updateItem["Title"] = itemSub["Title"];
                                        updateItem["LinkURL"] = itemSub["LinkURL"];
                                        updateItem["LinkAccessTo"] = itemSub["LinkAccessTo"];
                                        updateItem["IsFeaturedLink"] = itemSub["IsFeaturedLink"];
                                        updateItem["LinkSource"] = webSub.Title;
                                        updateItem["IsPromoted"] = itemSub["IsPromoted"];
                                        updateItem["IsHeaderLink"] = itemSub["IsHeaderLink"];
                                        updateItem["LinksCategory"] = itemSub["LinksCategory"];
                                        updateItem.SystemUpdate();
                                    }
                                }
                                else
                                {
                                    SPListItem newItem = listParent.AddItem();
                                    newItem["Title"] = itemSub["Title"];
                                    newItem["LinkURL"] = itemSub["LinkURL"];
                                    newItem["LinkAccessTo"] = itemSub["LinkAccessTo"];
                                    newItem["IsFeaturedLink"] = itemSub["IsFeaturedLink"];
                                    newItem["LinkSource"] = webSub.Title;
                                    newItem["IsPromoted"] = itemSub["IsPromoted"];
                                    newItem["IsHeaderLink"] = itemSub["IsHeaderLink"];
                                    newItem["LinksCategory"] = itemSub["LinksCategory"];
                                    newItem.SystemUpdate();
                                }
                            }
                            objclassEventFiring.EnableHandleEventFiring();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Unpromote link from parent link list
        /// </summary>
        /// <param name="siteUrl"></param>
        /// <param name="listName"></param>
        /// <param name="itemID"></param>
        private void DeleteSPListItemInAnotherList(string siteUrl, string listName, string itemID)
        {
            using (SPSite site = new SPSite(siteUrl))
            {
                if (site != null)
                {
                    // Get the web site
                    using (SPWeb webSub = site.OpenWeb())
                    {
                        if (webSub != null)
                        {
                            classEventFiring objclassEventFiring = new classEventFiring();
                            objclassEventFiring.DisableHandleEventFiring();
                           
                            SPList listSub = webSub.Lists[listName];
                            SPListItem itemSub = listSub.GetItemById(Convert.ToInt32(itemID));

                            SPWeb webParent = webSub.ParentWeb;
                            if (webParent != null)
                            {
                                SPList listParent = webParent.Lists[listName];
                                SPQuery query = new SPQuery();
                                query.Query = "<Where><And><Eq><FieldRef Name='Title' /><Value Type='Text'>" + Convert.ToString(itemSub["Title"])
                                    + "</Value></Eq><Eq><FieldRef Name='LinkSource' /><Value Type='Text'>" + webSub.Title + "</Value></Eq></And></Where>";
                                SPListItemCollection items = listParent.GetItems(query);

                                if (items != null && items.Count > 0)
                                {
                                    foreach (SPListItem Item in items)
                                    {                                        
                                        Item.Delete();
                                    }
                                }                               
                            }
                            objclassEventFiring.EnableHandleEventFiring();
                        }
                    }
                }
            }
        }
    }

    public class classEventFiring : SPItemEventReceiver
    {
        bool oldValue = true;
        public void DisableHandleEventFiring()
        {
            oldValue = this.EventFiringEnabled;
            this.EventFiringEnabled = false;
        }

        public void EnableHandleEventFiring()
        {
            this.EventFiringEnabled = oldValue;
        }
    }
}
