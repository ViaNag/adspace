﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;

namespace Viacom.AdSpace.Link.WF.Activity
{
    public sealed class DeleteItemCodeActivity : CodeActivity
    {

        /// <summary>
        /// Gets or sets a value for ListId
        /// </summary>
        public InArgument<string> UnpromoteSiteUrl { get; set; }

        /// <summary>
        /// Gets or sets a value for ListId
        /// </summary>
        public InArgument<string> UnpromoteListName { get; set; }

        /// <summary>
        /// Gets or sets a value for ListItem
        /// </summary>
        public InArgument<string> UnpromoteItemId { get; set; }

        /// <summary>
        /// Gets or sets a value for ListItem
        /// </summary>
        public OutArgument<string> OutputStatusForDelete { get; set; }

        // If your activity returns a value, derive from CodeActivity<TResult>
        // and return the value from the Execute method.
        protected override void Execute(CodeActivityContext context)
        {
            try
            {
                string siteUrl = context.GetValue<string>(this.UnpromoteSiteUrl);
                string listName = context.GetValue<string>(this.UnpromoteListName);
                string itemID = context.GetValue<string>(this.UnpromoteItemId);
                // validate
                if (siteUrl == null || listName == null || itemID == null)
                {
                    throw new Exception("Site url, list name, or ItemID cannot be null!", null);
                }
                else
                {
                    this.DeleteSPListItemInAnotherList(context, siteUrl, listName, itemID);                   
                }
            }
            catch (Exception ex)
            {
                this.OutputStatusForDelete.Set(context, "Item delete failed: " + ex.Message);
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("AdSpace Workflow Error", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.StackTrace, null);
            }
        }

        /// <summary>
        /// Unpromote link from parent link list
        /// </summary>
        /// <param name="siteUrl"></param>
        /// <param name="listName"></param>
        /// <param name="itemID"></param>
        private void DeleteSPListItemInAnotherList(CodeActivityContext context, string siteUrl, string listName, string itemID)
        {
            using (SPSite site = new SPSite(siteUrl))
            {
                if (site != null)
                {
                    // Get the web site
                    using (SPWeb webSub = site.OpenWeb())
                    {
                        if (webSub != null)
                        {
                            classEventFiring objclassEventFiring = new classEventFiring();
                            objclassEventFiring.DisableHandleEventFiring();

                            SPList listSub = webSub.Lists[listName];
                            SPListItem itemSub = listSub.GetItemById(Convert.ToInt32(itemID));

                            SPWeb webParent = webSub.ParentWeb;
                            if (webParent != null)
                            {
                                SPList listParent = webParent.Lists[listName];
                                SPQuery query = new SPQuery();
                                query.Query = "<Where><And><Eq><FieldRef Name='Title' /><Value Type='Text'>" + Convert.ToString(itemSub["Title"])
                                    + "</Value></Eq><Eq><FieldRef Name='LinkSource' /><Value Type='Text'>" + webSub.Title + "</Value></Eq></And></Where>";
                                SPListItemCollection items = listParent.GetItems(query);

                                if (items != null && items.Count > 0)
                                {
                                    foreach (SPListItem Item in items)
                                    {
                                        Item.Delete();

                                    }

                                    this.OutputStatusForDelete.Set(context, "Item successfully unpromoted from AdSpace root.");
                                }
                                else
                                {
                                    this.OutputStatusForDelete.Set(context, string.Empty);
                                }
                            }
                            objclassEventFiring.EnableHandleEventFiring();
                        }
                    }
                }
            }
        }
    }
   
}
