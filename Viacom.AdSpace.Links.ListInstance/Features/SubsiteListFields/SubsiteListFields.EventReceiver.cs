using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Microsoft.SharePoint;
using Viacom.AdSpace.Shared;

namespace Viacom.AdSpace.Links.ListInstance.Features.SubsiteListFields
{
    /// <summary>
    /// This class handles events raised during feature activation, deactivation, installation, uninstallation, and upgrade.
    /// </summary>
    /// <remarks>
    /// The GUID attached to this class may be used during packaging and should not be modified.
    /// </remarks>

    [Guid("6d9856d8-b635-4c32-b8be-e890a200e9b0")]
    public class SubsiteListFieldsEventReceiver : SPFeatureReceiver
    {
        private const string CONF_LST_NM = "FieldsConfigList";
        private const string KEY_FIELDPROPERTY_COL = "FieldProperty";
        private const string CONFIG_VALUE_FIELD = "Value1";

        public override void FeatureActivated(SPFeatureReceiverProperties properties)
        {
            SPWeb oWeb = (SPWeb)properties.Feature.Parent;
            UpdateFieldProp(oWeb, KEY_FIELDPROPERTY_COL);            
        }


        private void UpdateFieldProp(SPWeb oWeb, string fieldProperty)
        {
            string[] listName = null;
            string[] fieldName = null;
            string[] fieldProperties = null;
            string[] propertyName = null;
            string[] property = null;

            SPList configList = oWeb.Lists.TryGetList(CONF_LST_NM);
            if (configList != null)
            {
                SPQuery query = new SPQuery();
                query.Query = "<Where><Eq><FieldRef Name='Key'/><Value Type='Text'>" + fieldProperty + "</Value></Eq></Where>";
                SPListItemCollection itemcoll = configList.GetItems(query);

                if (itemcoll.Count == 1)
                {
                    SPItem item = itemcoll[0];
                    fieldProperties = item[CONFIG_VALUE_FIELD].ToString().Split(new Char[] { '|' });

                    foreach (string fieldProp in fieldProperties)
                    {
                        propertyName = fieldProp.Split(new Char[] { '~' });
                        property = propertyName[0].ToString().Split(new Char[] { '*' });
                        listName = propertyName[1].ToString().Split(new Char[] { '-' });
                        fieldName = listName[1].ToString().Split(new char[] { ',' });

                        SPList list = oWeb.Lists.TryGetList(listName[0]);

                        foreach (string fields in fieldName)
                        {
                            try
                            {
                                SPField field = list.Fields.GetFieldByInternalName(fields);
                                if (field != null)
                                {
                                    switch (property[0])
                                    {
                                        case "ShowInDisplayForm":
                                            {
                                                field.ShowInDisplayForm = Convert.ToBoolean(property[1]);
                                                break;
                                            }
                                        case "ShowInNewForm":
                                            {
                                                field.ShowInNewForm = Convert.ToBoolean(property[1]);
                                                break;
                                            }
                                        case "ShowInEditForm":
                                            {
                                                field.ShowInEditForm = Convert.ToBoolean(property[1]);
                                                break;
                                            }
                                        case "Hidden":
                                            {
                                                field.Hidden = Convert.ToBoolean(property[1]);
                                                break;
                                            }
                                        case "ReadOnly":
                                            {
                                                field.ReadOnlyField = Convert.ToBoolean(property[1]);
                                                break;
                                            }
                                    }

                                    field.Update();
                                }

                            }
                            catch (Exception ex)
                            {
                                ExceptionHandling.LogToSPDiagnostic(ex);
                            }

                        }
                    }
                }
            }
        }


        // Uncomment the method below to handle the event raised before a feature is deactivated.

        //public override void FeatureDeactivating(SPFeatureReceiverProperties properties)
        //{
        //}


        // Uncomment the method below to handle the event raised after a feature has been installed.

        //public override void FeatureInstalled(SPFeatureReceiverProperties properties)
        //{
        //}


        // Uncomment the method below to handle the event raised before a feature is uninstalled.

        //public override void FeatureUninstalling(SPFeatureReceiverProperties properties)
        //{
        //}

        // Uncomment the method below to handle the event raised when a feature is upgrading.

        //public override void FeatureUpgrading(SPFeatureReceiverProperties properties, string upgradeActionName, System.Collections.Generic.IDictionary<string, string> parameters)
        //{
        //}
    }
}
