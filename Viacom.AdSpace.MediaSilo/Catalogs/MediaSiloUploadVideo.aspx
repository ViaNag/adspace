﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>

<%@ Page Language="C#" Inherits="Viacom.AdSpace.MediaSilo.MediasiloProxy, $SharePoint.Project.AssemblyFullName$" %>

<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePointWebControls" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="PublishingWebControls" Namespace="Microsoft.SharePoint.Publishing.WebControls" Assembly="Microsoft.SharePoint.Publishing, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="PublishingNavigation" Namespace="Microsoft.SharePoint.Publishing.Navigation" Assembly="Microsoft.SharePoint.Publishing, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<asp:Content contentplaceholderid="PlaceHolderAdditionalPageHead" runat="server">
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    
    
    <script src="/_catalogs/masterpage/AdSpaceMasterPageAndLayouts/JS/JavaScript/Util.js"></script>
    <SharePointWebControls:CssRegistration ID="CssRegistration1" name="<% $SPUrl:~sitecollection/Style Library/~language/Themable/Core Styles/pagelayouts15.css %>" runat="server"/>
    <link rel="stylesheet" type="text/css" href="/_catalogs/masterpage/AdSpaceMasterPageAndLayouts/CSS/MediaSilo.css" />
	<PublishingWebControls:EditModePanel runat="server" id="editmodestyles">
		<!-- Styles for edit mode only-->
		<SharePointWebControls:CssRegistration ID="CssRegistration3" name="<% $SPUrl:~sitecollection/Style Library/~language/Themable/Core Styles/editmode15.css %>"
			After="<% $SPUrl:~sitecollection/Style Library/~language/Themable/Core Styles/pagelayouts15.css %>" runat="server"/>
	</PublishingWebControls:EditModePanel>
</asp:Content>
<asp:Content contentplaceholderid="PlaceHolderPageTitle" runat="server">
	<SharePointWebControls:FieldValue id="PageTitle" FieldName="Title" runat="server"/>
</asp:Content>
<asp:Content contentplaceholderid="PlaceHolderPageTitleInTitleArea" runat="server">
    <SharePoint:ProjectProperty ID="ProjectProperty1" Property="Title" runat="server" />
    -
	<SharePointWebControls:FieldValue ID="FieldValue1" FieldName="Title" runat="server"/>
    
</asp:Content>
<asp:Content contentplaceholderid="PlaceHolderMain" runat="server">
    
    <div class="VideoUpload">
        <div class="upload-file">
             <label> <asp:Literal ID="ltUploadFile" runat="server">Choose a file</asp:Literal></label>
                <input id="fileInput" name="videos[]" type="file" />
         </div>
        <div class="drag-drop-box">
                        <input id="getFile" onchange="Viacom.Mediasilo.Util.readfiles(this.files)" type="file" multiple="multiple">
                        Drag and Drop Multiple
                        <br>
                        video files here
         </div>
        <div class="col-md-9 drag-img-area">
            <div class="col-md-12 drag-img">
                <div class="row" id="docRow">
                </div>
            </div>
        </div>
        <div class="btn-group">
            <input type="button" ID="btnUploadFile" onclick="Viacom.Mediasilo.Util.uploadfile();" value="Ok" />
            <input type="button" ID="Cancel" onclick="Viacom.Mediasilo.Util.cancel();" value="Cancel" />
        </div>
        <asp:Literal ID="ltMessage" runat="server"></asp:Literal>
    <div id="LoadingArea" style="display:none">
        <img src="../../_layouts/15/images/Viacom.AdSpace.MediaSilo/spinner2.gif" />
        <p class="status"></p>
        <div id='progressbar'><label id="lblStatus"></label></div>
         
   </div>
   </div> 

</asp:Content>
<asp:Content contentplaceholderid="PlaceHolderTitleBreadcrumb" runat="server"> 
	<SharePointWebControls:ListSiteMapPath ID="ListSiteMapPath1" runat="server" SiteMapProviders="CurrentNavigationSwitchableProvider" RenderCurrentNodeAsLink="false" PathSeparator="" CssClass="s4-breadcrumb" NodeStyle-CssClass="s4-breadcrumbNode" CurrentNodeStyle-CssClass="s4-breadcrumbCurrentNode" RootNodeStyle-CssClass="s4-breadcrumbRootNode" NodeImageOffsetX=0 NodeImageOffsetY=289 NodeImageWidth=16 NodeImageHeight=16 NodeImageUrl="/_layouts/15/images/fgimg.png?rev=23" HideInteriorRootNodes="true" SkipLinkText=""/> </asp:Content>
<asp:Content contentplaceholderid="PlaceHolderPageImage" runat="server" />
<asp:Content contentplaceholderid="PlaceHolderNavSpacer" runat="server" />
