﻿
namespace Viacom.AdSpace.MediaSilo
{
    /// <summary>
    /// Configuration class details
    /// </summary>
    public class Configuration
    {
        private string deleteVideoPermission;
        public string DeleteVideoPermission
        {
            get { return deleteVideoPermission; }
            set { deleteVideoPermission = value; }
        }

        private string uploadVideoPermission;
        public string UploadVideoPermission
        {
            get { return uploadVideoPermission; }
            set { uploadVideoPermission = value; }
        }

        private string downloadVideoPermission;
        public string DownloadVideoPermission
        {
            get { return downloadVideoPermission; }
            set { downloadVideoPermission = value; }
        }

        private string baseAddress;
        public string BaseAddress
        {
            get { return baseAddress; }
            set { baseAddress = value; }
        }

        private string authorizationKey;
        public string AuthorizationKey
        {
            get { return authorizationKey; }
            set { authorizationKey = value; }
        }

        private string authorizationValue;
        public string AuthorizationValue
        {
            get { return authorizationValue; }
            set { authorizationValue = value; }
        }

        private string hostContextKey;
        public string HostContextKey
        {
            get { return hostContextKey; }
            set { hostContextKey = value; }
        }

        private string hostContextValue;
        public string HostContextValue
        {
            get { return hostContextValue; }
            set { hostContextValue = value; }
        }

        private string apiKey;
        public string APIKEY
        {
            get { return apiKey; }
            set { apiKey = value; }
        }

        private string apiValue;
        public string APIValue
        {
            get { return apiValue; }
            set { apiValue = value; }
        }

        private string applicationKey;
        public string ApplicationKey
        {
            get { return applicationKey; }
            set { applicationKey = value; }
        }

        private string applicationValue;
        public string ApplicationValue
        {
            get { return applicationValue; }
            set { applicationValue = value; }
        }

        private string projectID;
        public string ProjectID
        {
            get { return projectID; }
            set { projectID = value; }
        }

        private string uploadTicketURL;
        public string UploadTicketURL
        {
            get { return uploadTicketURL; }
            set { uploadTicketURL = value; }
        }

        private string assetURL;
        public string AssetURL
        {
            get { return assetURL; }
            set { assetURL = value; }
        }

        private string assetDetailsURL;
        public string AssetDetailsURL
        {
            get { return assetDetailsURL; }
            set { assetDetailsURL = value; }
        }

        private string apiURL;
        public string APIURL
        {
            get { return apiURL; }
            set { apiURL = value; }
        }

        private string metaDataURL;
        public string MetaDataURL
        {
            get { return metaDataURL; }
            set { metaDataURL = value; }
        }

        private string rootFolder;
        public string RootFolder
        {
            get { return rootFolder; }
            set { rootFolder = value; }
        }

        private string subFolderURL;
        public string SubFolderURL
        {
            get { return subFolderURL; }
            set { subFolderURL = value; }
        }

        private string mediaSiloThreadDelayTime;
        public string MediaSiloThreadDelayTime
        {
            get { return mediaSiloThreadDelayTime; }
            set { mediaSiloThreadDelayTime = value; }
        }
    }
}
