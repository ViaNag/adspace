﻿<?xml version="1.0" encoding="utf-8"?>
<feature xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="b97c2c07-efd3-4ecd-bf4a-c98d3ef3e9a0" activateOnDefault="false" description="Deploy MediaSilo components like custom actions, pages, mapped folders, etc." featureId="b97c2c07-efd3-4ecd-bf4a-c98d3ef3e9a0" imageUrl="" scope="Site" solutionId="00000000-0000-0000-0000-000000000000" title="Viacom.AdSpace.MediaSilo" version="" deploymentPath="$SharePoint.Project.FileNameWithoutExtension$_$SharePoint.Feature.FileNameWithoutExtension$" xmlns="http://schemas.microsoft.com/VisualStudio/2008/SharePointTools/FeatureModel">
  <projectItems>
    <projectItemReference itemId="cad53ca4-01c2-45d5-ad7a-314b881fba1a" />
    <projectItemReference itemId="42b68ff3-51a0-4252-b1ef-a4b197ac02ee" />
    <projectItemReference itemId="62977031-4285-4e00-a6e0-b37466a31859" />
    <projectItemReference itemId="6cd35e62-b5ff-4f91-8690-f86970203aa4" />
  </projectItems>
</feature>