﻿// Create a namespace for our functions so we don't collide with anything else
var Viacom = Viacom || {};

// Create a function for customizing the Field Rendering of our fields
Viacom.CustomizeFieldRendering = function () {
    var fieldJsLinkOverride = {};
    fieldJsLinkOverride.Templates = {};
    fieldJsLinkOverride.Templates.Fields =
    {
        // Make sure the Priority field view gets hooked up to the GetPriorityFieldIcon method defined below
        'DownloadServer': { 'View': Viacom.GetPriorityFieldIcon }
    };
    // Register the rendering template
    SPClientTemplates.TemplateManager.RegisterTemplateOverrides(fieldJsLinkOverride);
};

// Create a function for getting the Priority Field Icon value (called from the first method)
Viacom.GetPriorityFieldIcon = function (ctx) {
    var value = ctx.CurrentItem.DownloadServer;
    // In the following section we simply determine what the rendered html output should be. In my case I'm setting an icon.
    if (value != null && value != "") {
        var url = _spPageContextInfo.siteAbsoluteUrl + ctx.CurrentItem.FileRef;
        return "<a title=\"Download video from server\" href=\"javascript:void(0);\" onclick=\"Viacom.DownloadVideo('" + value + "','" + "DownloadServer" + "','" + url + "');return false\"><img src='/_layouts/15/images/Viacom.AdSpace.MediaSilo/download.png' /></a>";
    }
};

// Call the function. 
// We could've used a self-executing function as well but I think this simplifies the example
Viacom.CustomizeFieldRendering();