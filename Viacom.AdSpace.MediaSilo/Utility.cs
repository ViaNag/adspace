﻿using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Runtime.Caching;
using System.Web.Script.Serialization;

namespace Viacom.AdSpace.MediaSilo
{
    class Utility
    {
        public static void LogException(string exceptionLocation, Exception ex)
        {
            if (ex.InnerException != null)
                LogException(exceptionLocation, ex.InnerException);
            Log(exceptionLocation, ex.Message, ex.StackTrace);
        }
        public static void Log(string exceptionLocation, string Message, string StackTrace)
        {
            //log to ULS
            SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory(exceptionLocation, TraceSeverity.High, EventSeverity.ErrorCritical), TraceSeverity.Unexpected, Message, StackTrace);
        }

        /// <summary>
        /// Check we have cache object or not
        /// </summary>
        /// <returns>If cache object exist return cache object</returns>
        internal static Configuration GetConfigObject(string SiteCollectionUrl)
        {
            //Get Cache object Name
            string cacheObjectName = GetCacheObjectName(SiteCollectionUrl);
            // Do we have the result in the cache or not
            var result = MemoryCache.Default.Get(cacheObjectName) as Configuration;
            if (result != null)
            {
                return result;
            }
            else
            {
                Configuration oConfiguration = GetConfigValue(SiteCollectionUrl);
                result = oConfiguration;
                // Stores the result in the cache so that we yay the next time!
                MemoryCache.Default.Set(cacheObjectName, result, DateTimeOffset.Now.Add(new TimeSpan(Constants.CACHE_DAY, Constants.CACHE_HOUR, Constants.CACHE_MIN, Constants.CACHE_SEC)));
            }
            return result;
        }

        /// <summary>
        /// Function to get Cache Object Name
        /// </summary>
        /// <returns>Cache Object Name</returns>
        private static string GetCacheObjectName(string SiteCollectionUrl)
        {
            string cacheObjectName = string.Empty;
            SiteCollectionUrl = SiteCollectionUrl.Trim('"');
            Microsoft.SharePoint.SPSecurity.RunWithElevatedPrivileges(delegate()
            {
                using (SPSite oSite = new SPSite(SiteCollectionUrl))
                {
                    cacheObjectName = string.Format("{0}{1}", Constants.MS_CACHE_OBJECT_NAME, oSite.ID);
                }
            });

            return cacheObjectName;
        }

        /// <summary>
        /// Funcation to append header to web request
        /// </summary>
        /// <param name="client">web client object</param>
        /// <param name="oConfiguration">configuration object</param>
        /// <returns>web client object</returns>
        internal static WebClient GetConfigurationHeader(WebClient client, Configuration oConfiguration)
        {
            client.BaseAddress = oConfiguration.BaseAddress;
            client.Headers.Add(oConfiguration.AuthorizationKey, oConfiguration.AuthorizationValue);
            client.Headers.Add(oConfiguration.HostContextKey, oConfiguration.HostContextValue);
            client.Headers.Add(oConfiguration.APIKEY, oConfiguration.APIValue);
            client.Headers.Add(oConfiguration.ApplicationKey, oConfiguration.ApplicationValue);
            return client;
        }

        /// <summary>
        /// Funcation to get value from config list
        /// </summary>
        /// <returns>return object with config value</returns>
        private static Configuration GetConfigValue(string SiteCollectionUrl)
        {
            Configuration oConfiguration = new Configuration();
            try
            {
                Dictionary<string, string> keyValuePair = new Dictionary<string, string>();
                SiteCollectionUrl = SiteCollectionUrl.Trim('"');
                Microsoft.SharePoint.SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    using (SPSite oSite = new SPSite(SiteCollectionUrl))
                    {
                        using (SPWeb oWeb = oSite.RootWeb)
                        {
                            SPList oList = oWeb.Lists.TryGetList(Constants.MEDIASILO_CONFIGURATION_LIST);
                            if (oList != null)
                            {

                                //  SPQuery query = new SPQuery();
                                //query.Query = Constants.CONFIGURATION_LIST_QUERY;
                                SPListItemCollection itemColl = oList.Items;
                                foreach (SPItem oListItem in itemColl)
                                {
                                    // keyValuePair.Add(Convert.ToString(oListItem["Key"]),Convert.ToString(oListItem["Value1"]));

                                    //  if (!string.IsNullOrEmpty(Convert.ToString(oListItem[Constants.CONFIG_FIELD_VALUE])))
                                    // {
                                    //   JavaScriptSerializer serializer = new JavaScriptSerializer();
                                    //  oConfiguration = serializer.Deserialize<Configuration>(Convert.ToString(oListItem[Constants.CONFIG_FIELD_VALUE]));
                                    //}
                                    switch (Convert.ToString(oListItem["KEY"]))
                                    {
                                        case "BaseAddress":
                                            {
                                                oConfiguration.BaseAddress = Convert.ToString(oListItem[Constants.CONFIG_VALUE_FIELD]);
                                                break;
                                            }
                                        case "AuthorizationKey":
                                            {
                                                oConfiguration.AuthorizationKey = Convert.ToString(oListItem[Constants.CONFIG_VALUE_FIELD]);
                                                break;
                                            }
                                        case "HostContextKey":
                                            {
                                                oConfiguration.HostContextKey = Convert.ToString(oListItem[Constants.CONFIG_VALUE_FIELD]);
                                                break;
                                            }
                                        case "HostContextValue":
                                            {
                                                oConfiguration.HostContextValue = Convert.ToString(oListItem[Constants.CONFIG_VALUE_FIELD]);
                                                break;
                                            }
                                        case "APIKEY":
                                            {
                                                oConfiguration.APIKEY = Convert.ToString(oListItem[Constants.CONFIG_VALUE_FIELD]);
                                                break;
                                            }
                                        case "ApplicationKey":
                                            {
                                                oConfiguration.ApplicationKey = Convert.ToString(oListItem[Constants.CONFIG_VALUE_FIELD]);
                                                break;
                                            }
                                        case "ProjectID":
                                            {
                                                oConfiguration.ProjectID = Convert.ToString(oListItem[Constants.CONFIG_VALUE_FIELD]);
                                                break;
                                            }
                                        case "UploadTicketURL":
                                            {
                                                oConfiguration.UploadTicketURL = Convert.ToString(oListItem[Constants.CONFIG_VALUE_FIELD]);
                                                break;
                                            }
                                        case "AssetURL":
                                            {
                                                oConfiguration.AssetURL = Convert.ToString(oListItem[Constants.CONFIG_VALUE_FIELD]);
                                                break;
                                            }
                                        case "AssetDetailsURL":
                                            {
                                                oConfiguration.AssetDetailsURL = Convert.ToString(oListItem[Constants.CONFIG_VALUE_FIELD]);
                                                break;
                                            }
                                        case "APIURL":
                                            {
                                                oConfiguration.APIURL = Convert.ToString(oListItem[Constants.CONFIG_VALUE_FIELD]);
                                                break;
                                            }
                                        case "MetaDataURL":
                                            {
                                                oConfiguration.MetaDataURL = Convert.ToString(oListItem[Constants.CONFIG_VALUE_FIELD]);
                                                break;
                                            }
                                        case "RootFolder":
                                            {
                                                oConfiguration.RootFolder = Convert.ToString(oListItem[Constants.CONFIG_VALUE_FIELD]);
                                                break;
                                            }
                                        case "SubFolderURL":
                                            {
                                                oConfiguration.SubFolderURL = Convert.ToString(oListItem[Constants.CONFIG_VALUE_FIELD]);
                                                break;
                                            }
                                        case "UploadVideoPermission":
                                            {
                                                oConfiguration.UploadVideoPermission = Convert.ToString(oListItem[Constants.CONFIG_VALUE_FIELD]);
                                                break;
                                            }
                                        case "DeleteVideoPermission":
                                            {
                                                oConfiguration.DeleteVideoPermission = Convert.ToString(oListItem[Constants.CONFIG_VALUE_FIELD]);
                                                break;
                                            }
                                        case "DownloadVideoPermission":
                                            {
                                                oConfiguration.DownloadVideoPermission = Convert.ToString(oListItem[Constants.CONFIG_VALUE_FIELD]);
                                                break;
                                            }
                                        case "MediaSiloThreadDelayTime":
                                            {
                                                oConfiguration.MediaSiloThreadDelayTime = Convert.ToString(oListItem[Constants.CONFIG_VALUE_FIELD]);
                                                break;
                                            }
                                        default:
                                            break;
                                    }
                                }
                                oConfiguration.AuthorizationValue = Convert.ToString(ConfigurationManager.AppSettings[Constants.AURTHORIZATION_VALUE]);
                                oConfiguration.APIValue = Convert.ToString(ConfigurationManager.AppSettings[Constants.API_VALUE]);
                                oConfiguration.ApplicationValue = Convert.ToString(ConfigurationManager.AppSettings[Constants.APPLICATION_VALUE]);
                            }
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                Utility.Log("MediaSilouplaod.cs-GetConfigValue", ex.Message, ex.StackTrace);

            }
            return oConfiguration;
        }

        /// <summary>
        /// Funcation to provide Json string from dictionary object
        /// </summary>
        /// <param name="dictobj">Dictionary Object</param>
        /// <returns>Json string</returns>
        internal static string GetJson(Dictionary<string, object> dictobj)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = serializer.Serialize((object)dictobj);
            return jsonString;
        }
    }
}
