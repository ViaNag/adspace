﻿// Create a namespace for our functions so we don't collide with anything else
var Viacom = Viacom || {};

// Create a function for customizing the Field Rendering of our fields
Viacom.CustomizeFieldRendering = function () {
    var fieldJsLinkOverride = {};
    fieldJsLinkOverride.Templates = {};
    fieldJsLinkOverride.Templates.Fields =
    {
        // Make sure the Priority field view gets hooked up to the GetPriorityFieldIcon method defined below
        'Play': { 'View': Viacom.GetPriorityFieldIcon, 'DisplayForm': Viacom.GetPriorityFieldIcon }
    };
    // Register the rendering template
    SPClientTemplates.TemplateManager.RegisterTemplateOverrides(fieldJsLinkOverride);
};

$(document).ready(function () {
    JSRequest.EnsureSetup();
    if (JSRequest.QueryString["PlayVideo"] == 'true') {
        var playanchor = $('a[title="Play video"]');
        playanchor.click();
    }
});

// Create a function for getting the Priority Field Icon value (called from the first method)
Viacom.GetPriorityFieldIcon = function (ctx) {
    var value = ctx.CurrentItem.Play;


    // In the following section we simply determine what the rendered html output should be. In my case I'm setting an icon.
    if (value != null && value != "" && value != undefined) {

        var start_pos_actualValue = value.indexOf(">")
        var end_pos_actualValue = value.lastIndexOf("<")
        var actualValue = value;

        //If condition checks if the value variable contains any html element. Found to be encapsulated in Div on Item Display form.
        if (start_pos_actualValue > 0 && end_pos_actualValue > 0) {
            //Extracting innertext from the HTML encapsulated form
            var divElement = document.createElement("Div");
            divElement.innerHTML = ctx.CurrentItem.Play;
            if (divElement.innerText)
                actualValue = divElement.innerText;
            else if (divElement.innerContent)
                actualValue = divElement.innerContent;
        }

        if (ctx.CurrentItem.FileLeafRef.BaseName != undefined) {
            title = ctx.CurrentItem.FileLeafRef.BaseName
        }
        else {
            title = ctx.CurrentItem.FileLeafRef;
        }

        var url = _spPageContextInfo.siteAbsoluteUrl + "/" + title;
        return "<a title=\"Play video\" href=\"javascript:void(0);\" onclick=\"mediaSiloModal('" + actualValue + "','" + title + "','" + url + "');\"><img src='/_layouts/15/images/Viacom.MediaSilo.Video/play.png' /></a>";
    }
    else { return " "; }
};

function mediaSiloModal(assetID, vTitle, intVideoUrl) {
    $('#mediasilo-overlay,.mediasilo-modal').toggle();
    $('.ms-dlgTitleBtns').click(function () {
        $('#mediasilo-overlay,.mediasilo-modal').hide();
        jwplayer("mediasilo-video").remove();
        $('#mediasilo-video').text('Loading the Player...');
    });
    $('#videoTitle').text(vTitle);
    Viacom.GetVideoProperties(assetID, intVideoUrl);

}

Viacom.GetVideoProperties = function (astID, intVideoUrl) {
    try {
        $.ajax({
            type: "POST",
            url: _spPageContextInfo.siteAbsoluteUrl + "/_catalogs/masterpage/MediaSiloUploadVideo.aspx/" + "AssetDetails",
            data: JSON.stringify({ assetID: '"' + astID + '"', siteCollectionURL: '"' + _spPageContextInfo.siteAbsoluteUrl + '"' }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.d != null) {
                    var obj = jQuery.parseJSON(data.d);
                    var assetDetails = obj[0];
                    var derivative = assetDetails.derivatives[1];

                    vURL = derivative.url;
                    iURL = derivative.thumbnail;
                    jwplayer("mediasilo-video").setup({
                        autostart: true,
                        controlbar: "bottom",
                        file: vURL,
                        image: iURL,
                        duration: 26,
                        title: 'Nickelodeon\'s Kids\' Choice Sports 2014',
                        flashplayer: "/_catalogs/masterpage/JwPlayer6/jwplayer.flash.swf",
                        volume: 80,
                        width: 680,
                        height: 480
                    });
                    jwplayer().onPlay(jwplayer().setFullscreen('true'))
                    jwplayer().onReady(function (event) {
                        $('#mediasilo-video button').on('click', function (event) { return false; });
                    });

                    // Save video details
                    var IsDownload = "false";
                    if (Viacom.SaveVideoData) {
                        Viacom.SaveVideoData(intVideoUrl, IsDownload);
                    }
                }
            }
            ,
            error: function (err) {
                console.log('Asset Details Process failed');
            }
        });
    }
    catch (err) {
        console.log(err.message + 'Something went wrong during creation of detailsAsset');
    }
}

// Call the function. 
// We could've used a self-executing function as well but I think this simplifies the example
Viacom.CustomizeFieldRendering();