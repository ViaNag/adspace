﻿namespace Viacom.AdSpace.CustomPages
{

    using System;
    using System.Collections.Generic;
    using System.Web.Script.Serialization;
    using System.Web.Services;
    using System.Web.UI;
    using System.Linq;
    using Viacom.AdSpace.Shared;
    using System.ServiceModel.Syndication;

    public class Operations : Page
    {

        /// <summary>
        /// Log exception to SP diagnostic
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="page"></param>
        /// <param name="file"></param>
        /// <param name="method"></param>
        [WebMethod]
        public static void LogException(string msg, string page, string file, string method)
        {
            ExceptionHandling.LogToSPDiagnostic(msg, page, file, method);
        }

       /// <summary>
       /// Get RSS feed
       /// </summary>
       /// <param name="url"></param>
       /// <param name="pageSize"></param>
       /// <param name="pageCount"></param>
       /// <param name="outername"></param>
       /// <param name="attribute"></param>
       /// <returns></returns>
        [WebMethod]
        public static RSSFeedHelper.RSSFeedObject GetNewsFeed(string url, string pageSize, string pageCount, string outername, string attribute)
        {
            try
            {
                return new RSSFeedHelper().GetRSSFeed(url, pageSize, pageCount, outername, attribute);
            }
            catch (Exception ex)
            {

                ExceptionHandling.LogToSPDiagnostic(ex.Message, "external news", "externalnewshelper.js", "getnewsfeed");
                ExceptionHandling.LogExceptionToRSSFeedLogs(url, ex);
                RSSFeedHelper.RSSFeedObject obj = new RSSFeedHelper.RSSFeedObject();
                obj = null;
                return obj;
            } 
        }
    }
}
