﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ Page Language="C#" %>
<%@ Register tagprefix="SharePoint" namespace="Microsoft.SharePoint.WebControls" assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<html dir="ltr" xmlns="http://www.w3.org/1999/xhtml" xmlns:mso="urn:schemas-microsoft-com:office:office" xmlns:msdt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882">

<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Play Video</title>
<meta http-equiv="X-UA-Compatible" content="IE=10" />
<SharePoint:CssRegistration Name="default" runat="server"/>

<!--[if gte mso 9]>
<SharePoint:CTFieldRefs runat=server Prefix="mso:" FieldList="FileLeafRef,Comments,PublishingStartDate,PublishingExpirationDate,PublishingContactEmail,PublishingContactName,PublishingContactPicture,PublishingPageLayout,PublishingVariationGroupID,PublishingVariationRelationshipLinkFieldID,PublishingRollupImage,Audience,PublishingIsFurlPage,SeoBrowserTitle,SeoMetaDescription,SeoKeywords,RobotsNoIndex,gd55b52111464ed79bbc2d9a708ef2ba,TaxCatchAllLabel,CategoryDescription,_dlc_ExpireDateSaved,_dlc_ExpireDate,_dlc_DocId,_dlc_DocIdUrl,_dlc_DocIdPersistId"><xml>
	
	
	
	
	
	
	
	
<mso:CustomDocumentProperties>
<mso:ItemRetentionFormula msdt:dt="string">&lt;formula id=&quot;Microsoft.Office.RecordsManagement.PolicyFeatures.Expiration.Formula.BuiltIn&quot;&gt;&lt;number&gt;1&lt;/number&gt;&lt;property&gt;Expiry_x0020_date&lt;/property&gt;&lt;propertyId&gt;5b3357a0-af84-4655-8f1e-33340e5a329a&lt;/propertyId&gt;&lt;period&gt;days&lt;/period&gt;&lt;/formula&gt;</mso:ItemRetentionFormula>
<mso:_dlc_policyId msdt:dt="string">0x0101|1866761191</mso:_dlc_policyId>
<mso:_dlc_DocId msdt:dt="string">KZ5CDRNCJDVS-1-15</mso:_dlc_DocId>
<mso:_dlc_DocIdItemGuid msdt:dt="string">b7638faf-5e1c-4abd-a789-5d079a1e1809</mso:_dlc_DocIdItemGuid>
<mso:_dlc_DocIdUrl msdt:dt="string">http://166.77.196.72/_layouts/15/DocIdRedir.aspx?ID=KZ5CDRNCJDVS-1-15, KZ5CDRNCJDVS-1-15</mso:_dlc_DocIdUrl>
</mso:CustomDocumentProperties>
</xml></SharePoint:CTFieldRefs><![endif]-->
</head>

<body>
<link href="/_catalogs/masterpage/AdSpaceMasterPageAndLayouts/CSS/iPhoneCSS/menu-style.css"/>
<script type="text/javascript" src="/_catalogs/masterpage/AdSpaceMasterPageAndLayouts/jQuery/jquery-1.7.2.min.js"></script>
<script type='text/javascript' src='/_catalogs/masterpage/AdSpaceMasterPageAndLayouts/jQuery/JwPlayer/swfobject.js'></script>
<script type="text/javascript" src="/_catalogs/masterpage/AdSpaceMasterPageAndLayouts/jQuery/JwPlayer/jwplayer/jwplayer.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/AdSpaceMasterPageAndLayouts/jQuery/wmvplayer.js"></script>
<script type="text/javascript"> 
 /* $(document).ready(function() {
	    var Url = location.href;
		var QS = Url.split("=");
		//alert(QS[1].split("&")[0]);
		var so = new SWFObject('https://kit.viacom.com/sites/comedycentral/Teams/Marketing/JWPlayer/player.swf','mpl','500','400','9');
		so.addParam('allowfullscreen','true');
		so.addParam('allowscriptaccess','always');
		so.addParam('wmode','opaque');
		so.addVariable('file',QS[1].split("&")[0]);
		so.addVariable('stretching','fill');
		so.addVariable('autostart','true');
		so.write('mediaspace');
	  }); */
	  
	$(document).ready(function() {
	  
	    var Url = location.href;
		var QS = Url.split("=");

	  	//alert("file"+QS[1].split("&")[0]);
        jwplayer('mediaspace').setup({
        'file':QS[1].split("&")[0],
        'autostart':'true',
        // 'logo.file':'../Jquery/VIACOM_WTRMK.png',
        'logo.hide':'false',
        'logo.position':'bottom-right',
        'controlbar.position':'over',
       'controlbar.idlehide':'true',
       'stretching':'fill',
       'wmode':'opaque',
       'allowscriptaccess':'always',
       'allowfullscreen':'true',
       'width':'400',
       'height':'300',
       'modes': [
                    {type: 'html5'},
                    {type: 'flash', src: '/_catalogs/masterpage/AdSpaceMasterPageAndLayouts/jQuery/JwPlayer/jwplayer/jwplayer.flash.swf'}
                ]

       
        
        });

	  });

</script>
<table  cellpadding="0px" cellspacing="0px" border="0px" style="height:100%; width:100%">
<tr><td align="center" valign="middle" style="padding-top:10px"><div id='mediaspace' > div will be replaced</div>
</td></tr>
</table>

<form id="form1" runat="server">
</form>

</body>

</html>
