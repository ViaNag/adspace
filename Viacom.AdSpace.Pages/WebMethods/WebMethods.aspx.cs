﻿using System;
using System.Web.Services;
using System.Web.UI;
using System.Linq;
using Viacom.AdSpace.Shared;
using System.Data;
using System.Web.Script.Services;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;
using Microsoft.Office.DocumentManagement;
using System.Security.Principal;
using Newtonsoft.Json.Linq;

namespace Viacom.AdSpace.CustomPages
{

    public class ExtSharingItem
    {
        public string action { get; set; }
        public int ID { get; set; }
        public string listName { get; set; }
        public string type { get; set; }
        public string webSiteTitle { get; set; }
        public string itemName { get; set; }
        public string checkedItemURL { get; set; }
        public string listID { get; set; }
        public int itemID { get; set; }
    }

    public class WebMethods : Page
    {

        /// <summary>
        /// Log exception to SP diagnostic
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="page"></param>
        /// <param name="file"></param>
        /// <param name="method"></param>
        [WebMethod]
        public static void LogException(string msg, string page, string file, string method)
        {
            ExceptionHandling.LogToSPDiagnostic(msg, page, file, method);
        }

        /// <summary>
        /// Get Rss feed
        /// </summary>
        /// <param name="url"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageCount"></param>
        /// <param name="outername"></param>
        /// <param name="attribute"></param>
        /// <returns></returns>
        [WebMethod]
        public static RSSFeedHelper.RSSFeedObject GetNewsFeed(string url, string pageSize, string pageCount, string outername, string attribute)
        {
            try
            {
                return new RSSFeedHelper().GetRSSFeed(url, pageSize, pageCount, outername, attribute);
            }
            catch (Exception ex)
            {

                ExceptionHandling.LogToSPDiagnostic(ex.Message, "external news", "externalnewshelper.js", "getnewsfeed");
                RSSFeedHelper.RSSFeedObject obj = new RSSFeedHelper.RSSFeedObject();
                obj = null;
                return obj;
            }
        }

        #region Global Declarations
        string startTime;
        int i;
        string strRoleDefs;
        string endTime;
        #endregion


        #region Web Methods
        /// <summary>
        /// This method get the root child nodes
        /// </summary>
        /// <param name="nodeRootURL">Root site URL</param>
        /// <param name="nodeType">Root type ie ROOT</param>
        /// <param name="nodeId">root ID</param>
        /// <returns>JSON string containing root chilrdens</returns>
        /// conversion done
        [WebMethod]
        public static string GetChildNodes(string nodeRootURL, string nodeType, string nodeId)
        {
            string childNodesJSON = string.Empty;
            try
            {
                if (nodeRootURL != null && nodeRootURL.Length > 0)
                {
                    using (SPSite site = new SPSite(nodeRootURL))
                    {
                        using (SPWeb web = site.OpenWeb())
                        {
                            DataTable childItems = null;
                            childItems = GetRootChildItemsDT(web, nodeRootURL);
                            childNodesJSON = ComonOperations.GetJson(childItems);
                        }
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("External Sharing", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                });
            }
            return childNodesJSON;
        }

        /// <summary>
        /// This method gets all the items or folders for the list 
        /// </summary>
        /// <param name="siteURL">Subsite/Root URL</param>
        /// <param name="nodeId">List ID</param>
        /// <param name="nodeType">List Type</param>
        /// <param name="nodeParentId">site ID</param>
        /// <returns>JSON string containing list childrens</returns>
        /// conversion done
        [WebMethod]
        [ScriptMethod(UseHttpGet = false)]
        public static string GetListChildFolders(string siteURL, string nodeId, string nodeType, string nodeParentId)
        {
            string folderJSON = string.Empty;
            bool hasUniquePermission = false;
            try
            {

                using (SPSite site = new SPSite(siteURL))
                {
                    using (SPWeb web = site.OpenWeb())
                    {
                        if (nodeType.Equals("List"))
                        {
                            SPList list = web.GetList(nodeId);
                            SPQuery query = new SPQuery();
                            query.ViewFields = "";
                            SPListItemCollection items = list.GetItems(query);
                            if (items != null && items.Count > 0)
                            {
                                DataTable itemDT = new DataTable();//Create datatable
                                DataColumn itemDC = new DataColumn("ID");
                                itemDT.Columns.Add(itemDC);

                                itemDC = new DataColumn("Title");
                                itemDT.Columns.Add(itemDC);

                                itemDC = new DataColumn("Type");
                                itemDT.Columns.Add(itemDC);

                                itemDC = new DataColumn("Inheritance");
                                itemDT.Columns.Add(itemDC);

                                itemDC = new DataColumn("ItemURL");
                                itemDT.Columns.Add(itemDC);
                                itemDC = new DataColumn("MaxShareDuration");
                                itemDT.Columns.Add(itemDC);
                                itemDC = new DataColumn("Shareable");
                                itemDT.Columns.Add(itemDC);
                                itemDC = new DataColumn("IsArchived");
                                itemDT.Columns.Add(itemDC);
                                itemDT.AcceptChanges();

                                DataRow itemDR = null;
                                string title = "";
                                string itemPath = string.Empty;
                                string listPath = string.Empty;
                                string itemCTId = string.Empty;
                                string itemType = "Item";
                                string docCTPrefix = "0x0101";
                                string maxShareDuration = string.Empty;
                                string shareable = string.Empty;
                                string isArchived = string.Empty;

                                listPath = list.RootFolder.ServerRelativeUrl;
                                itemDR = itemDT.NewRow();

                                itemDR["ID"] = list.ID.ToString();//Store the list id in datatble row
                                itemDR["Title"] = list.Title;//Store the list title in datatble row
                                itemDR["Type"] = "List";//Store the list type in datatble row
                                hasUniquePermission = list.HasUniqueRoleAssignments;//Check whether list having unique permission
                                if (!hasUniquePermission)
                                {
                                    itemDR["Inheritance"] = "Inherited";
                                }
                                else
                                {
                                    itemDR["Inheritance"] = "Unique";
                                }
                                itemDR["ItemURL"] = listPath;//Store the list path in datatable row
                                itemDT.Rows.Add(itemDR);//add row in datatable

                                foreach (SPListItem item in items)//loop over item collection
                                {
                                    itemPath = (string)item["FileDirRef"];//Store the item path

                                    if (itemPath.Equals(listPath))
                                    {
                                        itemCTId = (string)item["ContentTypeId"].ToString();
                                        if (item.SortType.ToString() == "Folder")
                                        {
                                            itemType = "Folder";
                                        }
                                        else if (itemCTId.StartsWith(docCTPrefix))
                                        {
                                            itemType = "Document";
                                        }
                                        else
                                        {
                                            itemType = "Item";
                                        }
                                        if (itemType == "Document")
                                        {
                                            //User Story 48704 - Code commented out as per discussion with Gaurav, we are not using Title field anywhere
                                            //if (item["Title"] != null)//Check whether title field is empty
                                            //{
                                            //    title = (string)item["Title"];//Store the title 
                                            //}
                                            //else
                                            //{
                                            //    title = (string)item.File.Name;//Store the item file name 
                                            //}
                                            title = (string)item.File.Name;
                                            //maxShareDuration = GetItemDefaultColumnValue(item, "MaxShareDuration");
                                            //shareable = GetItemDefaultColumnValue(item, "Shareable");
                                            //isArchived = GetItemDefaultColumnValue(item, "IsArchived");
                                        }
                                        else if (itemType == "Folder")
                                        {
                                            title = (string)item.DisplayName;//Store the item display name in cae of folder

                                            //Line commented out to get Field level value rather than location based value
                                            //SPFolder folder = item.Folder;
                                            //MetadataDefaults columnDefaults = new MetadataDefaults(list);
                                            //maxShareDuration = GetLocationBasedDefaults(columnDefaults, folder, "MaxShareDuration");
                                            //shareable = GetLocationBasedDefaults(columnDefaults, folder, "Shareable");
                                            //isArchived = GetLocationBasedDefaults(columnDefaults, folder, "IsArchived");
                                            //maxShareDuration = GetItemDefaultColumnValue(item, "MaxShareDuration");
                                            //shareable = GetItemDefaultColumnValue(item, "Shareable");
                                            //isArchived = GetItemDefaultColumnValue(item, "IsArchived");
                                        }
                                        else
                                        {
                                            title = (string)item["FileLeafRef"];//Store the title 
                                            //maxShareDuration = GetItemDefaultColumnValue(item, "MaxShareDuration");
                                            //shareable = GetItemDefaultColumnValue(item, "Shareable");
                                            //isArchived = GetItemDefaultColumnValue(item, "IsArchived");
                                        }

                                        //User Story 48704 - common code for all above conditions
                                        maxShareDuration = GetItemDefaultColumnValue(item, "MaxShareDuration");
                                        shareable = GetItemDefaultColumnValue(item, "Shareable");
                                        isArchived = GetItemDefaultColumnValue(item, "IsArchived");
                                        itemDR = itemDT.NewRow();//Create row

                                        itemDR["ID"] = item.ID;//Store the item id in datatble row
                                        itemDR["Title"] = title;//Store the title in datatble row
                                        itemDR["Type"] = itemType;//Store the type in datatble row
                                        hasUniquePermission = item.HasUniqueRoleAssignments;
                                        if (!hasUniquePermission)
                                        {
                                            itemDR["Inheritance"] = "Inherited";
                                        }
                                        else
                                        {
                                            itemDR["Inheritance"] = "Unique";
                                        }
                                        itemDR["ItemURL"] = listPath;//Store the list path  in datatble row
                                        itemDR["MaxShareDuration"] = maxShareDuration;
                                        itemDR["Shareable"] = shareable;
                                        itemDR["IsArchived"] = isArchived;
                                        itemDT.Rows.Add(itemDR);//add row in datatable
                                    }
                                }
                                itemDT.AcceptChanges();

                                folderJSON = ComonOperations.GetJson(itemDT);

                            }
                            else
                            {
                                folderJSON = string.Empty;
                            }
                        }
                        else if (nodeType.Equals("Folder"))
                        {

                        }
                        else
                        {
                            folderJSON = string.Empty;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("External Sharing", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                });

            }
            return folderJSON;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteURL"></param>
        /// <param name="PollLookupValue"></param>
        /// <param name="listname"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        [WebMethod]
        [ScriptMethod(UseHttpGet = false)]
        public static string GetPollUsersInfo(string siteURL, string PollLookupValue, string listname, string userID, string indexCounter)
        {
            string folderJSON = string.Empty;
            Boolean IsCurrentUserPolled = false;
            string mypollAnswer = string.Empty;
            try
            {
                using (SPSite site = new SPSite(siteURL))
                {
                    using (SPWeb web = site.OpenWeb())
                    {
                        SPList list = web.Lists[listname];
                        SPQuery currentUserQuery = new SPQuery();
                        currentUserQuery.Query = "<Where><And><Eq><FieldRef Name='PollNameLookUp' /><Value Type='Lookup'>" + PollLookupValue + "</Value></Eq><Eq><FieldRef Name='Author' LookupId='TRUE'/><Value Type='Integer'>" + userID + "</Value></Eq></And></Where>";

                        SPListItemCollection currentUserItems = list.GetItems(currentUserQuery);
                        if (currentUserItems.Count > 0)
                        {
                            IsCurrentUserPolled = true;
                            foreach (SPListItem currentUserItem in currentUserItems)//loop over item collection
                            {
                                mypollAnswer = (string)currentUserItem["Response"];
                            }
                        }


                        SPQuery myQuery = new SPQuery();
                        myQuery.Query = "<Where><Eq><FieldRef Name='PollNameLookUp' /><Value Type='Lookup'>" + PollLookupValue + "</Value></Eq></Where><GroupBy><FieldRef Name='PollUserResponse' /></GroupBy>";
                        SPListItemCollection items = list.GetItems(myQuery);

                        DataTable itemDT = new DataTable();//Create datatable
                        DataColumn itemDC = new DataColumn("MyResponse");
                        itemDT.Columns.Add(itemDC);

                        itemDC = new DataColumn("PollName");
                        itemDT.Columns.Add(itemDC);

                        itemDC = new DataColumn("AnswerChoice");
                        itemDT.Columns.Add(itemDC);

                        itemDC = new DataColumn("Count");
                        itemDT.Columns.Add(itemDC);

                        itemDC = new DataColumn("Index");
                        itemDT.Columns.Add(itemDC);

                        DataRow itemDR = null;

                        if (items != null && items.Count > 0)
                        {

                            string pollAnswer = string.Empty;
                            int i = 0;

                            foreach (SPListItem item in items)//loop over item collection
                            {
                                if (i == 0)
                                {
                                    i++;
                                    pollAnswer = (string)item["Response"]; //Store the item path
                                    itemDR = itemDT.NewRow();//Create row
                                    if (IsCurrentUserPolled == true)
                                    {
                                        itemDR["MyResponse"] = mypollAnswer;    // set the selected value
                                    }
                                    else
                                    {
                                        itemDR["MyResponse"] = "NAN";   // set false when no value selected
                                    }

                                    itemDR["PollName"] = PollLookupValue;
                                    itemDR["AnswerChoice"] = pollAnswer;
                                    itemDR["Count"] = 1;
                                    itemDR["Index"] = Convert.ToInt32(indexCounter);

                                    itemDT.Rows.Add(itemDR);//add row in datatable
                                }
                                else
                                {
                                    if (pollAnswer == (string)item["Response"])
                                    {
                                        itemDT.Rows[i - 1]["Count"] = Convert.ToInt32(itemDT.Rows[i - 1]["Count"]) + 1;
                                    }
                                    else
                                    {
                                        i++;
                                        pollAnswer = (string)item["Response"]; //Store the item path
                                        itemDR = itemDT.NewRow();//Create row
                                        if (IsCurrentUserPolled == true)
                                        {
                                            itemDR["MyResponse"] = mypollAnswer;    // set the selected value
                                        }
                                        else
                                        {
                                            itemDR["MyResponse"] = "NAN";   // set false when no value selected
                                        }
                                        itemDR["PollName"] = PollLookupValue;
                                        itemDR["AnswerChoice"] = pollAnswer;
                                        itemDR["Count"] = 1;
                                        itemDR["Index"] = Convert.ToInt32(indexCounter);
                                        itemDT.Rows.Add(itemDR);//add row in datatable
                                    }
                                }

                            }
                            itemDT.AcceptChanges();

                            folderJSON = ComonOperations.GetJson(itemDT);

                        }
                        else
                        {

                            itemDR = itemDT.NewRow();
                            itemDR["MyResponse"] = "No item found.";
                            itemDR["Index"] = Convert.ToInt32(indexCounter);

                            itemDT.Rows.Add(itemDR);

                            folderJSON = ComonOperations.GetJson(itemDT);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Adspace Polls", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                });

            }

            return folderJSON;
        }


        /// <summary>
        /// This method gets all the items present in the folder. conversion done
        /// </summary>
        /// <param name="siteURL">Subsite/Root Site URl</param>
        /// <param name="folderId">Folder ID</param>
        /// <param name="nodeType">Folder type</param>
        /// <param name="parentListId">List ID</param>
        /// <returns></returns>
        [WebMethod]
        public static string GetFolderChildItems(string siteURL, string folderId, string nodeType, string parentListId)
        {
            string itemsJSON = string.Empty;//JSON string to store folder child items
            bool hasUniquePermission = false;
            try
            {
                using (SPSite site = new SPSite(siteURL))
                {
                    if (nodeType.Equals("Folder"))
                    {
                        using (SPWeb nodeWeb = site.OpenWeb())
                        {
                            SPList nodeParentList = nodeWeb.Lists.GetList(new Guid(parentListId), true);
                            SPListItem nodeFolderItem = nodeParentList.GetItemById(Convert.ToInt32(folderId));
                            SPFolder nodeFolder = nodeFolderItem.Folder;//get the folder
                            string nodeFolderPath = nodeFolder.ServerRelativeUrl;//store folder server relative URL
                            SPQuery itemQuery = new SPQuery();
                            itemQuery.Folder = nodeFolder;
                            SPListItemCollection nodeItemCollection = nodeParentList.GetItems(itemQuery);
                            if (nodeItemCollection != null && nodeItemCollection.Count > 0)
                            {
                                DataTable itemDT = new DataTable();//Create datatable
                                //Create columns in datatable
                                DataColumn itemDC = new DataColumn("ID");
                                itemDT.Columns.Add(itemDC);

                                itemDC = new DataColumn("Title");
                                itemDT.Columns.Add(itemDC);

                                itemDC = new DataColumn("Type");
                                itemDT.Columns.Add(itemDC);

                                itemDC = new DataColumn("Inheritance");
                                itemDT.Columns.Add(itemDC);

                                itemDC = new DataColumn("ItemURL");
                                itemDT.Columns.Add(itemDC);
                                itemDC = new DataColumn("MaxShareDuration");
                                itemDT.Columns.Add(itemDC);
                                itemDC = new DataColumn("Shareable");
                                itemDT.Columns.Add(itemDC);
                                itemDC = new DataColumn("IsArchived");
                                itemDT.Columns.Add(itemDC);
                                itemDT.AcceptChanges();

                                DataRow itemDR = null;
                                string title = "";
                                string itemPath = string.Empty;
                                string listPath = string.Empty;
                                string itemCTId = string.Empty;
                                string itemType = "Item";
                                string docCTPrefix = "0x0101";
                                string maxShareDuration = string.Empty;
                                string shareable = string.Empty;
                                string isArchived = string.Empty;

                                listPath = nodeFolderPath;

                                itemDR = itemDT.NewRow();//Creat row in datatable

                                itemDR["ID"] = nodeFolderItem.ID;//Add item id in ID column in datatable
                                //User Story 48704 - Code commented out as per discussion with Gaurav, we are not using Title field anywhere
                                //if (nodeFolderItem["Title"] != null)
                                //    itemDR["Title"] = (string)nodeFolderItem["Title"];//Add item title in title column in datatable
                                //else
                                //    itemDR["Title"] = nodeFolderItem.DisplayName;//Add item title in title column in datatable
                                itemDR["Title"] = nodeFolderItem.DisplayName; //Add folder name in title column in datatable
                                itemDR["Type"] = "Folder";//Add item type in type column in datatable
                                hasUniquePermission = nodeFolderItem.HasUniqueRoleAssignments;

                                if (!hasUniquePermission)
                                {
                                    itemDR["Inheritance"] = "Inherited";
                                }
                                else
                                {
                                    itemDR["Inheritance"] = "Unique";
                                }

                                itemDT.Rows.Add(itemDR);//add row in datatable

                                foreach (SPListItem item in nodeItemCollection)//loop over folder item collection
                                {
                                    itemPath = (string)item["FileDirRef"];
                                    if (itemPath.Equals(listPath))
                                    {
                                        itemCTId = (string)item["ContentTypeId"].ToString();
                                        //Based on content type decide the type
                                        if (item.SortType.ToString() == "Folder")
                                        {
                                            itemType = "Folder";
                                        }
                                        else if (itemCTId.StartsWith(docCTPrefix))
                                        {
                                            itemType = "Document";
                                        }
                                        else
                                        {
                                            itemType = "Item";
                                        }
                                        if (itemType == "Document")
                                        {
                                            //User Story 48704 - Code commented out as per discussion with Gaurav, we are not using Title field anywhere
                                            //if (item["Title"] != null)
                                            //{
                                            //    title = (string)item["Title"];
                                            //}
                                            //else
                                            //{
                                            //    title = (string)item.File.Name;
                                            //}

                                            title = (string)item.File.Name;
                                            //maxShareDuration = GetItemDefaultColumnValue(item, "MaxShareDuration");
                                            //shareable = GetItemDefaultColumnValue(item, "Shareable");
                                            //isArchived = GetItemDefaultColumnValue(item, "IsArchived");
                                        }
                                        else if (itemType == "Folder")
                                        {
                                            title = (string)item.DisplayName;
                                            //SPFolder folder = item.Folder;
                                            //MetadataDefaults columnDefaults = new MetadataDefaults(nodeParentList);
                                            //maxShareDuration = GetLocationBasedDefaults(columnDefaults, folder, "MaxShareDuration");
                                            //shareable = GetLocationBasedDefaults(columnDefaults, folder, "Shareable");
                                            //isArchived = GetLocationBasedDefaults(columnDefaults, folder, "IsArchived");
                                        }
                                        else
                                        {
                                            //title = (string)item["Title"];
                                            title = (string)item["FileLeafRef"];
                                            //maxShareDuration = GetItemDefaultColumnValue(item, "MaxShareDuration");
                                            //shareable = GetItemDefaultColumnValue(item, "Shareable");
                                            //isArchived = GetItemDefaultColumnValue(item, "IsArchived");
                                        }

                                        maxShareDuration = GetItemDefaultColumnValue(item, "MaxShareDuration");
                                        shareable = GetItemDefaultColumnValue(item, "Shareable");
                                        isArchived = GetItemDefaultColumnValue(item, "IsArchived");

                                        itemDR = itemDT.NewRow();//Create row
                                        itemDR["ID"] = item.ID;//Store the item id in datatble row
                                        itemDR["Title"] = title;//Store the title in datatble row
                                        itemDR["Type"] = itemType;//Store the type in datatble row
                                        hasUniquePermission = item.HasUniqueRoleAssignments;
                                        if (!hasUniquePermission)
                                        {
                                            itemDR["Inheritance"] = "Inherited";
                                        }
                                        else
                                        {
                                            itemDR["Inheritance"] = "Unique";
                                        }

                                        itemDR["ItemURL"] = listPath;
                                        itemDR["MaxShareDuration"] = maxShareDuration;
                                        itemDR["Shareable"] = shareable;
                                        itemDR["IsArchived"] = isArchived;
                                        itemDT.Rows.Add(itemDR);//add row in datatable
                                    }
                                }
                                itemDT.AcceptChanges();

                                itemsJSON = ComonOperations.GetJson(itemDT);//Convert datatable to JSON string
                            }
                            else
                            {
                                itemsJSON = string.Empty;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("External Sharing", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                });

            }
            return itemsJSON;//retun JSON string
        }

        /// <summary>
        /// This method gets all the lists and items for the particular subsite 
        /// </summary>
        /// <param name="siteURL">Subsite URL</param>
        /// <param name="nodeId">Subsite Node ID</param>
        /// <param name="nodeType">Subsite Type i.e. Site</param>
        /// <returns></returns>
        /// conversion done
        [WebMethod]
        [ScriptMethod(UseHttpGet = false)]
        public static string GetSiteChildren(string siteURL, string nodeId, string nodeType)
        {
            Guid appGuid = new Guid();


            string siteChildrenJSON = null;//JSON string to store site children 
            bool hasUniquePerms = false;
            try
            {
                if (siteURL != null && siteURL.Length > 0)
                {
                    using (SPSite site = new SPSite(siteURL))
                    {
                        using (SPWeb nodeRootWeb = site.OpenWeb())
                        {
                            SPWebCollection nodeChildWebs = nodeRootWeb.Webs;
                            SPListCollection nodeChildLists = nodeRootWeb.Lists;

                            DataTable rootChildrenDT = new DataTable();//Create datatable 
                            //Create columns in datatable
                            DataColumn rootChildrenDC = new DataColumn("ID");
                            rootChildrenDT.Columns.Add(rootChildrenDC);

                            rootChildrenDC = new DataColumn("Title");
                            rootChildrenDT.Columns.Add(rootChildrenDC);

                            rootChildrenDC = new DataColumn("Type");
                            rootChildrenDT.Columns.Add(rootChildrenDC);

                            rootChildrenDC = new DataColumn("Inheritance");
                            rootChildrenDT.Columns.Add(rootChildrenDC);

                            rootChildrenDC = new DataColumn("ItemURL");
                            rootChildrenDT.Columns.Add(rootChildrenDC);

                            rootChildrenDC = new DataColumn("IconURL");
                            rootChildrenDT.Columns.Add(rootChildrenDC);

                            rootChildrenDC = new DataColumn("ParentWebUrl");
                            rootChildrenDT.Columns.Add(rootChildrenDC);

                            rootChildrenDC = new DataColumn("MaxShareDuration");
                            rootChildrenDT.Columns.Add(rootChildrenDC);

                            rootChildrenDC = new DataColumn("Shareable");
                            rootChildrenDT.Columns.Add(rootChildrenDC);

                            rootChildrenDC = new DataColumn("IsArchived");
                            rootChildrenDT.Columns.Add(rootChildrenDC);

                            rootChildrenDT.AcceptChanges();
                            DataRow rootChildrenDR = null;
                            rootChildrenDR = rootChildrenDT.NewRow();//add row in datatable
                            rootChildrenDR["Id"] = nodeRootWeb.ID;//add rootweb id in ID column in datatable row
                            rootChildrenDR["Title"] = nodeRootWeb.Title;//add rootweb title in title column in datatable row
                            rootChildrenDR["Type"] = "Site";//add rootweb type in type column in datatable row
                            hasUniquePerms = nodeRootWeb.HasUniqueRoleAssignments;
                            if (!hasUniquePerms)
                            {
                                rootChildrenDR["Inheritance"] = "Inherited";
                            }
                            else
                            {
                                rootChildrenDR["Inheritance"] = "Unique";
                            }
                            rootChildrenDR["ItemURL"] = nodeRootWeb.Url;//add rootweb itemURL in itemURL column in datatable row
                            rootChildrenDR["IconURL"] = "";//add rootweb Icon URl in Icon URl  column in datatable row
                            rootChildrenDR["ParentWebUrl"] = siteURL;//add rootweb parentwebURL in parentwebURL column in datatable row
                            rootChildrenDR["MaxShareDuration"] = "";
                            rootChildrenDR["Shareable"] = "";
                            rootChildrenDR["IsArchived"] = "";
                            rootChildrenDT.Rows.Add(rootChildrenDR);//add row in datatable

                            foreach (SPWeb childWeb in nodeChildWebs)//loop over child webs
                            {
                                appGuid = childWeb.AppInstanceId;//get the app instance id of the web
                                if (appGuid == Guid.Parse("{00000000-0000-0000-0000-000000000000}"))//check whether web is an app
                                {
                                    rootChildrenDR = rootChildrenDT.NewRow();//Create a row
                                    rootChildrenDR["ID"] = childWeb.ID;//add childWeb id in ID column in datatable row
                                    rootChildrenDR["Title"] = childWeb.Title;//add childWeb title in title column in datatable row
                                    rootChildrenDR["Type"] = "Site";//add childWeb type in type column in datatable row
                                    hasUniquePerms = childWeb.HasUniqueRoleAssignments;
                                    if (!hasUniquePerms)
                                    {
                                        rootChildrenDR["Inheritance"] = "Inherited";
                                    }
                                    else
                                    {
                                        rootChildrenDR["Inheritance"] = "Unique";
                                    }
                                    rootChildrenDR["ItemURL"] = childWeb.Url;//add childWeb itemURL in itemURL column in datatable row
                                    rootChildrenDR["IconURL"] = "";//add childWeb Icon URl in Icon URl  column in datatable row
                                    rootChildrenDR["ParentWebUrl"] = siteURL;//add childWeb parentwebURL in parentwebURL column in datatable row
                                    rootChildrenDR["MaxShareDuration"] = "";
                                    rootChildrenDR["Shareable"] = "";
                                    rootChildrenDR["IsArchived"] = "";

                                    rootChildrenDT.Rows.Add(rootChildrenDR);//add row in datatable
                                }
                            }
                            bool isHidden = false;

                            foreach (SPList childList in nodeChildLists)//loop over list colection
                            {
                                if (childList.BaseType == SPBaseType.DocumentLibrary)
                                {
                                    SPContentTypeCollection SPCTCol = childList.ContentTypes;
                                    foreach (SPContentType CT in SPCTCol)
                                    {
                                        if (CT.Parent.Parent.Name == "AdSpace Document")
                                        {
                                            isHidden = childList.Hidden;
                                            if (!isHidden)
                                            {
                                                rootChildrenDR = rootChildrenDT.NewRow();//Create a row

                                                rootChildrenDR["ID"] = childList.ID;//add childList id in ID column in datatable row
                                                rootChildrenDR["Title"] = childList.Title;//add childList title in title column in datatable row
                                                rootChildrenDR["Type"] = "List";//add childList type in type column in datatable row
                                                hasUniquePerms = childList.HasUniqueRoleAssignments;
                                                if (!hasUniquePerms)
                                                {
                                                    rootChildrenDR["Inheritance"] = "Inherited";
                                                }
                                                else
                                                {
                                                    rootChildrenDR["Inheritance"] = "Unique";
                                                }
                                                rootChildrenDR["ItemURL"] = childList.RootFolder.ServerRelativeUrl;//add childList itemURL in itemURL column in datatable row
                                                rootChildrenDR["IconURL"] = site.Url.TrimEnd(new char[] { '/' }) + childList.ImageUrl;//add childList Icon URl in Icon URl  column in datatable row
                                                rootChildrenDR["ParentWebUrl"] = siteURL;//add childList parentwebURL in parentwebURL column in datatable row
                                                rootChildrenDR["MaxShareDuration"] = GetDefaultColumnValue(childList, "MaxShareDuration");//add Max Share Duration default value
                                                rootChildrenDR["Shareable"] = GetDefaultColumnValue(childList, "Shareable");//add Max Share Duration default value
                                                rootChildrenDR["IsArchived"] = GetDefaultColumnValue(childList, "Is Archived");
                                                rootChildrenDT.Rows.Add(rootChildrenDR);//add row in datatable
                                            }
                                        }
                                    }
                                }
                            }
                            rootChildrenDT.AcceptChanges();

                            if (rootChildrenDT != null && rootChildrenDT.Rows.Count > 0)
                            {
                                siteChildrenJSON = ComonOperations.GetJson(rootChildrenDT);//convert datatble to JSON string
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("External Sharing", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                });

            }
            return siteChildrenJSON;//retun JSON string
        }

        /// <summary>
        /// Get my last accessed items
        /// </summary>
        /// <param name="IsListing">Pass 1 for all data otherwiser pass 0</param>
        /// <returns></returns>
        [WebMethod]
        [ScriptMethod(UseHttpGet = false)]
        public static string GetMyLastAccessedURL(string IsListing)
        {
            string myLastAccessedUrlJSON = string.Empty;
            string conString = GetAnalyticsDBConnectionString();
            string userName = System.Web.HttpContext.Current.User.Identity.Name;
            SPSecurity.RunWithElevatedPrivileges(delegate()
            {
                WindowsImpersonationContext impersonatedContext = null;
                try
                {
                    WindowsIdentity spUserIdentity = WindowsIdentity.GetCurrent();
                    impersonatedContext = spUserIdentity.Impersonate();
                    DataSet dsMyLastAccessed = new DataSet();
                    using (SqlConnection sqlConnection = new SqlConnection(conString))
                    {
                        DataTable table = new DataTable();
                        table.Columns.Add("URL", typeof(string));
                        SPListItemCollection items = GetURLExceptionList();
                        if (items != null && items.Count > 0) {
                            foreach (SPListItem item in items) {
                                string exList = item[Constants.RootConfigValueField].ToString();
                                string[] exLists = exList.Split(';');
                                foreach (string exUrl in exLists) {
                                    if (exUrl != null && exUrl.Length > 0) {
                                        table.Rows.Add(exUrl);
                                    }
                                }
                            }
                        }
                        SqlCommand command = new SqlCommand("UspGetMyLastAccessedURL", sqlConnection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@IsListing", SqlDbType.Int).Value = Convert.ToInt32(IsListing);
                        command.Parameters.Add("@UserName", SqlDbType.VarChar).Value = userName;
                        command.Parameters.Add("@ExceptionList", SqlDbType.Structured).Value = table;
                        SqlDataAdapter dataAdapter = new SqlDataAdapter();
                        dataAdapter.SelectCommand = command;
                        dataAdapter.Fill(dsMyLastAccessed);
                        myLastAccessedUrlJSON = ComonOperations.GetJson(dsMyLastAccessed.Tables[0]);//Convert datatable to JSON string
                    }
                }
                catch (Exception ex)
                {
                    SPSecurity.RunWithElevatedPrivileges(delegate()
                    {
                        SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("AdSpace Analytics -- GetMyLastAccessedURL", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                    });
                }
            });
            return myLastAccessedUrlJSON;
        }

        /// <summary>
        /// Get my most viewed items
        /// </summary>
        /// <param name="IsListing">Pass 1 for all data otherwiser pass 0</param>
        /// <returns></returns>
        [WebMethod]
        [ScriptMethod(UseHttpGet = false)]
        public static string GetMyMostViewedURL(string IsListing)
        {
            string myMostViewedUrlJSON = string.Empty;
            string conString = GetAnalyticsDBConnectionString();
            string userName = System.Web.HttpContext.Current.User.Identity.Name;
            SPSecurity.RunWithElevatedPrivileges(delegate()
            {
                WindowsImpersonationContext impersonatedContext = null;
                try
                {
                    WindowsIdentity spUserIdentity = WindowsIdentity.GetCurrent();
                    impersonatedContext = spUserIdentity.Impersonate();
                    DataSet dsMyMostViewed = new DataSet();
                    using (SqlConnection sqlConnection = new SqlConnection(conString))
                    {
                        DataTable table = new DataTable();
                        table.Columns.Add("URL", typeof(string));
                        SPListItemCollection items = GetURLExceptionList();
                        if (items != null && items.Count > 0)
                        {
                            foreach (SPListItem item in items)
                            {
                                string exList = item[Constants.RootConfigValueField].ToString();
                                string[] exLists = exList.Split(';');
                                foreach (string exUrl in exLists)
                                {
                                    if (exUrl != null && exUrl.Length > 0)
                                    {
                                        table.Rows.Add(exUrl);
                                    }
                                }
                            }
                        }
                        SqlCommand command = new SqlCommand("UspGetMyMostViewedURL", sqlConnection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@IsListing", SqlDbType.Int).Value = Convert.ToInt32(IsListing);
                        command.Parameters.Add("@UserName", SqlDbType.VarChar).Value = userName;
                        command.Parameters.Add("@ExceptionList", SqlDbType.Structured).Value = table;
                        SqlDataAdapter dataAdapter = new SqlDataAdapter();
                        dataAdapter.SelectCommand = command;
                        dataAdapter.Fill(dsMyMostViewed);
                        myMostViewedUrlJSON = ComonOperations.GetJson(dsMyMostViewed.Tables[0]);//Convert datatable to JSON string
                    }
                }
                catch (Exception ex)
                {
                    SPSecurity.RunWithElevatedPrivileges(delegate()
                    {
                        SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("AdSpace Analytics -- GetMyMostViewedURL", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                    });
                }
            });
            return myMostViewedUrlJSON;
        }

        /// <summary>
        /// Get my last accessed items
        /// </summary>
        /// <param name="IsListing">Pass 1 for all data otherwiser pass 0</param>
        /// <returns></returns>
        [WebMethod]
        [ScriptMethod(UseHttpGet = false)]
        public static string[] GetAllUsers(string prefixText)
        {
            string[] prefix = prefixText.Split(',');
            int length = prefix.Count();
            List<string> users = new List<string>();

            try
            {
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    using (SqlConnection sqlConnection = new SqlConnection(GetExternalShareDBConnectionString()))
                    {
                        SqlCommand command = new SqlCommand("SELECT Email FROM dbo.aspnet_Membership WHERE Email Like '%' + @SearchText + '%'", sqlConnection);
                        command.Parameters.AddWithValue("@SearchText", prefix[length - 1]);
                        sqlConnection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            users.Add(Convert.ToString(reader["Email"]));
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("AdSpace External Sharing", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                });
            }
            foreach (string str in prefix)
            {
                if (users.Contains(str))
                {
                    users.Remove(str);
                }
            }
            return users.ToArray();
        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// This method gets all the subsites,list and items from the sitecollection
        /// </summary>
        /// <param name="siteURL">Site Collection URL</param>
        /// <returns>JSON String containing subsites,lists and items</returns>
        /// conversion done
        private static DataTable GetRootChildItemsDT(SPWeb web, string siteURL)
        {
            DataTable itemsDT = null;

            if (web != null && siteURL != null)
            {

                SPListCollection nodeChildLists = web.Lists;
                itemsDT = new DataTable();//Create datatable to store root children details
                bool hasUniquePerms = false;

                DataColumn dc = new DataColumn("Id");
                itemsDT.Columns.Add(dc);
                dc = new DataColumn("Title");
                itemsDT.Columns.Add(dc);
                dc = new DataColumn("Type");
                itemsDT.Columns.Add(dc);
                dc = new DataColumn("Inheritance");
                itemsDT.Columns.Add(dc);
                dc = new DataColumn("ItemURL");
                itemsDT.Columns.Add(dc);
                dc = new DataColumn("IconURL");
                itemsDT.Columns.Add(dc);
                dc = new DataColumn("ParentWebUrl");
                itemsDT.Columns.Add(dc);
                dc = new DataColumn("MaxShareDuration");
                itemsDT.Columns.Add(dc);
                dc = new DataColumn("Shareable");
                itemsDT.Columns.Add(dc);
                dc = new DataColumn("IsArchived");
                itemsDT.Columns.Add(dc);
                itemsDT.AcceptChanges();

                DataRow dr = null;

                dr = itemsDT.NewRow();//Create a new row

                dr["Id"] = web.ID;//Add the root id in ID column
                dr["Title"] = web.Title;//Add the root Title  in title column
                dr["Type"] = "Site";//Add the root type in type column
                hasUniquePerms = web.HasUniqueRoleAssignments;

                Guid appGuid = new Guid();


                if (!hasUniquePerms)
                {
                    dr["Inheritance"] = "Inherited";
                }
                else
                {
                    dr["Inheritance"] = "Unique";
                }
                dr["ItemURL"] = siteURL;//Add the site url in item URL column
                dr["IconURL"] = "";//Add the icon URL in ICon url column
                dr["ParentWebUrl"] = siteURL;//Add the root parent site url in parent Site URL column
                dr["MaxShareDuration"] = "";
                dr["Shareable"] = "";
                dr["IsArchived"] = "";

                itemsDT.Rows.Add(dr);//Add the root details in the row


                appGuid = web.AppInstanceId;

                if (appGuid == Guid.Parse("{00000000-0000-0000-0000-000000000000}"))//Check wehther web is an app.
                {
                    dr = itemsDT.NewRow();
                    dr["Id"] = web.ID;//Add the web id in ID column
                    dr["Title"] = web.Title; ;//Add the web Title  in title column
                    dr["Type"] = "Site";//Add the site type in type column
                    hasUniquePerms = web.HasUniqueRoleAssignments;

                    if (!hasUniquePerms)
                    {
                        dr["Inheritance"] = "Inherited";
                    }
                    else
                    {
                        dr["Inheritance"] = "Unique";
                    }
                    dr["ItemURL"] = web.Url;//Add the site url in item URL column
                    dr["IconURL"] = "";//Add the icon URL in ICon url column
                    dr["ParentWebUrl"] = siteURL;//Add the site parent site url in parent Site URL column
                    dr["MaxShareDuration"] = "";
                    dr["Shareable"] = "";
                    dr["IsArchived"] = "";

                    // dr["URL"] = ctx.Url.TrimEnd(new char[] { '/' }) + childWeb.ServerRelativeUrl;
                    itemsDT.Rows.Add(dr);//Add the web details in the row
                }

                foreach (SPList childList in nodeChildLists)//loop over list collection
                {
                    if (childList.BaseType == SPBaseType.DocumentLibrary)
                    {
                        SPContentTypeCollection SPCTCol = childList.ContentTypes;
                        foreach (SPContentType CT in SPCTCol)
                        {
                            if (CT.Parent.Parent.Name == "AdSpace Document")
                            {
                                bool isHidden = (bool)childList.Hidden;
                                if (!isHidden)
                                {
                                    dr = itemsDT.NewRow();
                                    dr["Id"] = childList.ID;//Add the web id in ID column
                                    dr["Title"] = childList.Title;//Add the list Title  in title column
                                    dr["Type"] = "List";//Add the list type in type column
                                    hasUniquePerms = childList.HasUniqueRoleAssignments;
                                    if (!hasUniquePerms)
                                    {
                                        dr["Inheritance"] = "Inherited";
                                    }
                                    else
                                    {
                                        dr["Inheritance"] = "Unique";
                                    }
                                    dr["ItemURL"] = childList.RootFolder.ServerRelativeUrl;//Add the site url in item URL column
                                    dr["IconURL"] = web.Url.TrimEnd(new char[] { '/' }) + childList.ImageUrl;//Add the icon URL in ICon url column
                                    dr["ParentWebUrl"] = siteURL;//Add the list parent site url in parent Site URL column
                                    dr["MaxShareDuration"] = GetDefaultColumnValue(childList, "MaxShareDuration");//add Max Share Duration default value
                                    dr["Shareable"] = GetDefaultColumnValue(childList, "Shareable");
                                    dr["IsArchived"] = GetDefaultColumnValue(childList, "IsArchived");//add Max Shareable default value

                                    //  dr["URL"] = childList.ParentWebUrl;
                                    // dr["URL"] = siteURL;
                                    itemsDT.Rows.Add(dr);//Add the list details in the row
                                }
                            }
                        }
                    }

                }
                itemsDT.AcceptChanges();//Commit all the changes made in datatable
            }

            return itemsDT;//return datatable
        }

        private static string GetAnalyticsDBConnectionString()
        {
            return Convert.ToString(ConfigurationManager.ConnectionStrings[Constants.AnalyticsDBConstants.ConnectionStringForAnalyticsDB_Windows]);
        }

        private static string GetExternalShareDBConnectionString()
        {
            return Convert.ToString(ConfigurationManager.ConnectionStrings["extuserdbstrcon"]);
        }

        #endregion

        /// <summary>
        /// conversion done from csom too server side
        /// </summary>
        /// <param name="submittedRequest"></param>
        [WebMethod(EnableSession = true)]
        public static void addItemsToAccessRequestMgmtList(string submittedRequest)
        {
            //****Decalarations******
            string[] str;
            string[] str3;
            string listids;
            string[] str7;
            int itemids;
            string[] str8;
            string action;
            string currentSitUrl;
            string selectedItemType = string.Empty;
            string folderName = string.Empty;
            string type = string.Empty;
            string expiryDate = string.Empty;

            try
            {
                str = submittedRequest.Split(',');
                type = str[1].Split('"')[3];
                str3 = str[3].Split('"');
                listids = Convert.ToString(str3[3]);
                str7 = str[7].Split('"');
                str8 = str[8].Split('"');
                action = Convert.ToString(str8[3]);
                currentSitUrl = str[5].Split('"')[3];
                expiryDate = Convert.ToString(str[9].Split('"')[3]);

                using (SPSite site = new SPSite(currentSitUrl))
                {
                    using (SPWeb web = site.OpenWeb())
                    {
                        if (site != null)
                        {
                            SPList oList = web.Lists[listids];
                            if (type == "List")
                            {
                                SetDefaultColumnValue(web, oList, action, expiryDate);
                                AddItemToList(web, currentSitUrl, type, listids, expiryDate, action, "");
                            }
                            if (type == "Folder")
                            {
                                folderName = str[4].Split('"')[3];
                                itemids = Convert.ToInt32(str7[3]);
                                SPListItem oListItem = oList.GetItemById(itemids);
                                SPFolder folder = oListItem.Folder;
                                MetadataDefaults columnDefaults = new MetadataDefaults(oList);
                                SetLocationBasedDefaults(web, columnDefaults, folder, action, expiryDate);
                                SetUpdateDateTimeColumn(web, folder, Constants.ExternalSharingupdateDateTime, action, expiryDate);
                                AddItemToList(web, currentSitUrl, type, listids, expiryDate, action, folder.Url);
                            }
                            if (type == "Item")
                            {
                                itemids = Convert.ToInt32(str7[3]);
                                SPListItem oListItem = oList.GetItemById(itemids);
                                UpdateColumn(web, oListItem, action, expiryDate);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("External Sharing", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                });
            }

        }

        /// <summary>
        /// conversion done from csom too server side
        /// </summary>
        /// <param name="submittedRequest"></param>
        [WebMethod(EnableSession = true)]
        public static void addItemsForArchival(string archivalSubmittedRequest)
        {
            //****Decalarations******

            string listName = string.Empty;
            int itemid;
            string[] requestArr;
            string archived = string.Empty;
            string currentSitUrl = string.Empty; ;
            string folderName = string.Empty;
            string type = string.Empty;
            string expiryDate = string.Empty;
            try
            {
                requestArr = archivalSubmittedRequest.Split(',');
                type = requestArr[1].Split('"')[3];
                listName = Convert.ToString(requestArr[3].Split('"')[3]);
                currentSitUrl = Convert.ToString(requestArr[5].Split('"')[3]);
                archived = Convert.ToString(requestArr[8].Split('"')[3]);

                using (SPSite site = new SPSite(currentSitUrl))
                {
                    using (SPWeb web = site.OpenWeb())
                    {
                        if (site != null)
                        {
                            SPList oList = web.Lists[listName];
                            if (type == Constants.ListType)
                            {
                                SetDefaultColumnValueForAchived(web, oList, archived);
                                AddItemToArchivalRequestsList(web, currentSitUrl, type, listName, archived, "");
                            }
                            if (type == Constants.FolderType)
                            {
                                folderName = requestArr[4].Split('"')[3];
                                itemid = Convert.ToInt32(requestArr[7].Split('"')[3]);
                                SPListItem oListItem = oList.GetItemById(itemid);
                                SPFolder folder = oListItem.Folder;
                                MetadataDefaults columnDefaults = new MetadataDefaults(oList);
                                SetLocationBasedDefaultsForAchived(web, columnDefaults, folder, archived);
                                UpdateDateTimeAndIsArchivedColumn(web, folder, archived);
                                AddItemToArchivalRequestsList(web, currentSitUrl, type, listName, archived, folder.Url);
                            }
                            if (type == Constants.ItemType)
                            {
                                itemid = Convert.ToInt32(requestArr[7].Split('"')[3]); ;
                                SPListItem oListItem = oList.GetItemById(itemid);
                                UpdateIsArchivedColumn(web, oListItem, archived);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Archival", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                });
            }

        }

        /// <summary>
        /// Updates each list item columns
        /// </summary>
        /// <param name="web"></param>
        /// <param name="items"></param>
        /// <param name="action"></param>
        /// <param name="expiryDate"></param>
        private static void UpdateItems(SPWeb web, SPListItemCollection items, string action, string expiryDate)
        {
            int itemToUpdate = 50;
            foreach (SPListItem item in items)
            {
                if (Convert.ToString(item.FileSystemObjectType) != "Folder")
                {
                    UpdateColumn(web, item, action, expiryDate);
                    itemToUpdate--;
                }
                if (itemToUpdate == 0)
                {
                    break;
                }
            }
        }


        /// <summary>
        /// Update isArchived and datetime for folders
        /// </summary>
        /// <param name="oWeb"></param>
        /// <param name="folder"></param>
        /// <param name="archived"></param>
        private static void UpdateDateTimeAndIsArchivedColumn(SPWeb oWeb, SPFolder folder, string archived)
        {
            if (folder != null)
            {
                SPListItem spitem = folder.Item;
                if (spitem != null)
                {
                    spitem[Constants.ArchivalSettingUpdateDateTime] = DateTime.Now;
                    if (archived == Constants.Archived)
                    {
                        spitem[Constants.ArchievedField] = 1;
                    }
                    else
                    {
                        spitem[Constants.ArchievedField] = 0;
                    }
                    bool unsafeUpdate = oWeb.AllowUnsafeUpdates;
                    try
                    {                       
                        oWeb.AllowUnsafeUpdates = true;
                        spitem.SystemUpdate();
                    }
                    catch (Exception ex)
                    {
                        SPSecurity.RunWithElevatedPrivileges(delegate()
                        {
                            SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Add Items For Archival", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                        });
                    }
                    finally
                    {
                        oWeb.AllowUnsafeUpdates = unsafeUpdate;
                    }
                }
            }
        }

        /// <summary>
        /// updates list item column value
        /// </summary>
        /// <param name="web"></param>
        /// <param name="item"></param>
        /// <param name="action"></param>
        /// <param name="expiryDate"></param>
        private static void UpdateColumn(SPWeb web, SPListItem item, string action, string expiryDate)
        {
            if (action == "Enable")
            {
                item["Shareable"] = 1;
            }
            else
            {
                item["Shareable"] = 0;
            }
            if (expiryDate != "-")
            {
                item["MaxShareDuration"] = Convert.ToInt32(expiryDate);
            }
            item[Constants.ExternalSharingupdateDateTime] = DateTime.Now;
            web.AllowUnsafeUpdates = true;
            //item.Update();
            item.SystemUpdate();
            web.AllowUnsafeUpdates = false;
        }

        /// <summary>
        /// Set location based defaults for library folders
        /// </summary>
        /// <param name="web"></param>
        /// <param name="columnDefaults"></param>
        /// <param name="folder"></param>
        /// <param name="action"></param>
        /// <param name="expiryDate"></param>
        private static void SetLocationBasedDefaults(SPWeb web, MetadataDefaults columnDefaults, SPFolder folder, string action, string expiryDate)
        {
            if (action == "Enable")
            {
                columnDefaults.SetFieldDefault(folder, "Shareable", "1");
            }
            else
            {
                columnDefaults.SetFieldDefault(folder, "Shareable", "0");
            }
            if (expiryDate != "-")
            {
                columnDefaults.SetFieldDefault(folder, "MaxShareDuration", expiryDate);
            }
            web.AllowUnsafeUpdates = true;
            columnDefaults.Update();
            web.AllowUnsafeUpdates = false;
        }

        /// <summary>
        /// Sets Library field default value
        /// </summary>
        /// <param name="web"></param>
        /// <param name="oList"></param>
        /// <param name="action"></param>
        /// <param name="expiryDate"></param>
        private static void SetDefaultColumnValue(SPWeb web, SPList oList, string action, string expiryDate)
        {
            SPField fldSharable = oList.Fields["Shareable"];
            SPField fldMaxShare = oList.Fields.GetField("MaxShareDuration");
            if (action == "Enable")
            {
                fldSharable.DefaultValue = "1";
            }
            else
            {
                fldSharable.DefaultValue = "0";
            }
            if (expiryDate != "-")
            {
                fldMaxShare.DefaultValue = expiryDate;
            }
            web.AllowUnsafeUpdates = true;
            fldSharable.Update();
            fldMaxShare.Update();
            oList.Update();
            web.AllowUnsafeUpdates = false;
        }

        /// <summary>
        /// get location based defaults for library folders
        /// </summary>
        /// <param name="columnDefaults"></param>
        /// <param name="folder"></param>
        /// <param name="column"></param>
        /// <returns></returns>
        private static string GetLocationBasedDefaults(MetadataDefaults columnDefaults, SPFolder folder, string column)
        {
            string defaultValue = columnDefaults.GetFieldDefault(folder, column);
            if (String.IsNullOrEmpty(defaultValue))
            {
                if (column.Equals("MaxShareDuration"))
                {
                    defaultValue = "14";
                }
                if (column.Equals("Shareable"))
                {
                    defaultValue = "0";
                }
                if (column.Equals("IsArchived"))
                {
                    defaultValue = "0";
                }
            }
            return defaultValue;
        }

        /// <summary>
        /// Get column default value
        /// </summary>
        /// <param name="item"></param>
        /// <param name="column"></param>
        /// <returns></returns>
        private static string GetItemDefaultColumnValue(SPListItem item, string column)
        {
            string defaultValue = Convert.ToString(item[column]);
            if (String.IsNullOrEmpty(defaultValue))
            {
                if (column.Equals("MaxShareDuration"))
                {
                    defaultValue = "14";
                }
                if (column.Equals("Shareable"))
                {
                    defaultValue = "0";
                }
                if (column.Equals("IsArchived"))
                {
                    defaultValue = "0";
                }
            }
            return defaultValue;
        }

        private static void SetUpdateDateTimeColumn(SPWeb oWeb, SPFolder folder, string columnName, string action, string expiryDate)
        {
            if (folder != null)
            {
                SPListItem spitem = folder.Item;
                if (spitem != null)
                {
                    spitem[columnName] = DateTime.Now;
                    if (action == "Enable")
                    {
                        spitem["Shareable"] = 1;
                    }
                    else
                    {
                        spitem["Shareable"] = 0;
                    }
                    if (expiryDate != "-")
                    {
                        spitem["MaxShareDuration"] = Convert.ToInt32(expiryDate);
                    }
                    bool unsafeUpdate = oWeb.AllowUnsafeUpdates;
                    try
                    {                       
                        oWeb.AllowUnsafeUpdates = true;
                        spitem.SystemUpdate();
                    }
                    catch (Exception ex)
                    {
                        SPSecurity.RunWithElevatedPrivileges(delegate()
                        {
                            SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Add Items For Archival", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                        });
                    }
                    finally
                    {
                        oWeb.AllowUnsafeUpdates = unsafeUpdate;
                    }
                }
            }
        }


        /// <summary>
        /// Get library field default value
        /// </summary>
        /// <param name="oList"></param>
        /// <param name="column"></param>
        /// <returns></returns>
        private static string GetDefaultColumnValue(SPList oList, string column)
        {
            SPField field = oList.Fields.GetField(column);
            string defaultValue = Convert.ToString(field.DefaultValue);
            if (String.IsNullOrEmpty(defaultValue))
            {
                if (column.Equals("MaxShareDuration"))
                {
                    defaultValue = "14";
                }
                if (column.Equals("Shareable"))
                {
                    defaultValue = "0";
                }
                if (column.Equals("Is Archived"))
                {
                    defaultValue = "0";
                }
            }
            return defaultValue;
        }

        /// <summary>
        /// Add Item to External Share Configuration Requests List
        /// </summary>
        /// <param name="web"></param>
        /// <param name="currentSitUrl"></param>
        /// <param name="type"></param>
        /// <param name="listname"></param>
        /// <param name="expiryDate"></param>
        /// <param name="action"></param>
        /// <param name="folderName"></param>
        private static void AddItemToList(SPWeb web, string currentSitUrl, string type, string listname, string expiryDate, string action, string folderName)
        {
            try
            {
                string currentMonth = DateTime.Now.Month.ToString();
                string currentYear = DateTime.Now.Year.ToString();
                string folder = currentMonth + "-" + currentYear;
                using (SPSite oSite = web.Site)
                {
                    using (SPWeb oWeb = oSite.RootWeb)
                    {
                        SPList oList = oWeb.Lists.TryGetList(Constants.ExtShareConfigListName);
                        if (oList != null)
                        {
                            SPFolder listFolder = oWeb.GetFolder(String.Format("{0}/{1}", oList.RootFolder.ServerRelativeUrl, folder));
                            if (!listFolder.Exists)
                            {
                                SPListItem newFolder = oList.Items.Add(oList.RootFolder.ServerRelativeUrl, SPFileSystemObjectType.Folder);
                                newFolder["Title"] = folder;
                                oWeb.AllowUnsafeUpdates = true;
                                newFolder.SystemUpdate();
                                oList.Update();
                                oWeb.AllowUnsafeUpdates = false;
                            }
                            string itemUrl = "";
                            string isEnable = "";
                            if (action == "Enable")
                            {
                                isEnable = "1";
                            }
                            else
                            {
                                isEnable = "0";
                            }
                            if (folderName == "")
                            {
                                itemUrl = currentSitUrl + "/" + listname;
                            }
                            else
                            {
                                itemUrl = currentSitUrl + "/" + folderName;
                            }

                            foreach (SPListItem item in oList.Folders)
                            {
                                if (item.Title == folder)
                                {
                                    SPListItem NewItem = oList.AddItem(item.Folder.ServerRelativeUrl, SPFileSystemObjectType.File);
                                    NewItem[Constants.ListColumns.ConfigListColumns.SharedItemType] = type;
                                    NewItem[Constants.ListColumns.ConfigListColumns.SiteURL] = currentSitUrl;
                                    NewItem[Constants.ListColumns.ConfigListColumns.LibraryName] = listname;
                                    NewItem[Constants.ListColumns.ConfigListColumns.ItemURL] = itemUrl;
                                    NewItem[Constants.ListColumns.ConfigListColumns.SharingEnabled] = isEnable;
                                    NewItem[Constants.ListColumns.ConfigListColumns.SharingDuration] = Convert.ToInt32(expiryDate);
                                    NewItem[Constants.ListColumns.ConfigListColumns.JobStatus] = Constants.NewJobStatus;
                                    oWeb.AllowUnsafeUpdates = true;
                                    NewItem.SystemUpdate();
                                    oWeb.AllowUnsafeUpdates = false;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("External Sharing Add items to External Share Configuration Requests", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                });
            }
        }

        /// <summary>
        /// Add Item to External Share Configuration Requests List
        /// </summary>
        /// <param name="web"></param>
        /// <param name="currentSitUrl"></param>
        /// <param name="type"></param>
        /// <param name="listname"></param>
        /// <param name="expiryDate"></param>
        /// <param name="action"></param>
        /// <param name="folderName"></param>
        private static void AddItemToArchivalRequestsList(SPWeb web, string currentSitUrl, string type, string listname, string archived, string folderName)
        {
            try
            {
                string currentMonth = DateTime.Now.Month.ToString();
                string currentYear = DateTime.Now.Year.ToString();
                string folder = currentMonth + "-" + currentYear;
                using (SPSite oSite = web.Site)
                {
                    using (SPWeb oWeb = oSite.RootWeb)
                    {
                        SPList oList = oWeb.Lists.TryGetList(Constants.ArchivalRequestsListName);
                        if (oList != null)
                        {
                            SPFolder listFolder = oWeb.GetFolder(String.Format("{0}/{1}", oList.RootFolder.ServerRelativeUrl, folder));
                            if (!listFolder.Exists)
                            {
                                SPListItem newFolder = oList.Items.Add(oList.RootFolder.ServerRelativeUrl, SPFileSystemObjectType.Folder);
                                newFolder["Title"] = folder;
                                oWeb.AllowUnsafeUpdates = true;
                                newFolder.SystemUpdate();
                                oList.Update();
                                oWeb.AllowUnsafeUpdates = false;
                            }
                            string itemUrl = "";
                            string isArchive = "";
                            if (archived == "Archived")
                            {
                                isArchive = "1";
                            }
                            else
                            {
                                isArchive = "0";
                            }
                            if (folderName == "")
                            {
                                itemUrl = currentSitUrl + "/" + listname;
                            }
                            else
                            {
                                itemUrl = currentSitUrl + "/" + folderName;
                            }

                            foreach (SPListItem item in oList.Folders)
                            {
                                if (item.Title == folder)
                                {
                                    SPListItem NewItem = oList.AddItem(item.Folder.ServerRelativeUrl, SPFileSystemObjectType.File);
                                    NewItem[Constants.ListColumns.ConfigListColumns.SharedItemType] = type;
                                    NewItem[Constants.ListColumns.ConfigListColumns.SiteURL] = currentSitUrl;
                                    NewItem[Constants.ListColumns.ConfigListColumns.LibraryName] = listname;
                                    NewItem[Constants.ListColumns.ConfigListColumns.ItemURL] = itemUrl;
                                    NewItem[Constants.ListColumns.ConfigListColumns.ArchivalEnabled] = isArchive;
                                    NewItem[Constants.ListColumns.ConfigListColumns.JobStatus] = Constants.NewJobStatus;
                                    oWeb.AllowUnsafeUpdates = true;
                                    NewItem.SystemUpdate();
                                    oWeb.AllowUnsafeUpdates = false;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Add items to Archive Configuration Requests", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="web"></param>
        /// <param name="item"></param>
        /// <param name="archived"></param>
        private static void UpdateIsArchivedColumn(SPWeb web, SPListItem item, string archived)
        {
            if (archived == Constants.Archived)
            {
                item[Constants.ArchievedField] = 1;
                item[Constants.ArchivalSettingUpdateDateTime] = DateTime.Now;
            }
            else
            {
                item[Constants.ArchievedField] = 0;
                item[Constants.ArchivalSettingUpdateDateTime] = null;
            }
            
            web.AllowUnsafeUpdates = true;
            item.SystemUpdate();
            web.AllowUnsafeUpdates = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="web"></param>
        /// <param name="columnDefaults"></param>
        /// <param name="folder"></param>
        /// <param name="archived"></param>
        private static void SetLocationBasedDefaultsForAchived(SPWeb web, MetadataDefaults columnDefaults, SPFolder folder, string archived)
        {
            if (archived == Constants.Archived)
            {
                columnDefaults.SetFieldDefault(folder, "IsArchived", "1");
            }
            else
            {
                columnDefaults.SetFieldDefault(folder, "IsArchived", "0");
            }
            web.AllowUnsafeUpdates = true;
            columnDefaults.Update();
            web.AllowUnsafeUpdates = false;
        }

        /// <summary>
        /// Sets Library field default value
        /// </summary>
        /// <param name="web"></param>
        /// <param name="oList"></param>
        /// <param name="action"></param>
        /// <param name="expiryDate"></param>
        private static void SetDefaultColumnValueForAchived(SPWeb web, SPList oList, string archived)
        {
            SPField fldArchival = oList.Fields["Is Archived"];
            if (archived == Constants.Archived)
            {
                fldArchival.DefaultValue = "1";
            }
            else
            {
                fldArchival.DefaultValue = "0";
            }

            web.AllowUnsafeUpdates = true;
            fldArchival.Update();
            oList.Update();
            web.AllowUnsafeUpdates = false;
        }

        /// <summary>
        /// conversion done from csom too server side
        /// </summary>
        /// <param name="submittedRequest"></param>
        [WebMethod(EnableSession = true)]
        public static bool addItemsToPostFeedbackList(string feedbackFormSubmittedRequest)
        {
            //****Decalarations******
            string currentSiteUrl = string.Empty;
            string name = string.Empty;
            string emailAddress = string.Empty;
            string title = string.Empty;
            string categoryTitle = string.Empty;
            int categoryId;
            string comments = string.Empty;
            bool isSuccess = false;
            int newItemID = 0;
            try
            {
                JObject results = JObject.Parse(feedbackFormSubmittedRequest);
                currentSiteUrl = Convert.ToString(results[Constants.SiteUrl]);
                title = Convert.ToString(results[Constants.Title]);
                name = Convert.ToString(results[Constants.Name]);
                emailAddress = Convert.ToString(results[Constants.EmailAddress]);
                categoryId = Convert.ToInt32(results[Constants.CategoryId]);
                categoryTitle = Convert.ToString(results[Constants.CategoryTitle]);
                comments = Convert.ToString(results[Constants.Comments]);
                SPFieldUserValueCollection usercollection = new SPFieldUserValueCollection();
                using (SPSite site = new SPSite(currentSiteUrl))
                {
                    using (SPWeb web = site.OpenWeb())
                    {
                        if (web != null)
                        {
                            SPSite rootSite = web.Site;
                            SPListItemCollection items = GetFeedBackCategoryListItem(rootSite, categoryTitle);
                            if (items != null && items.Count > 0)
                            {
                                SPFieldUser userField = (SPFieldUser)items[0].Fields.GetField(Constants.ListColumns.VlogPostFeedback.Notifications);
                                SPFieldUserValueCollection userFieldValueCollection = (SPFieldUserValueCollection)userField.GetFieldValue(items[0][Constants.ListColumns.VlogPostFeedback.Notifications].ToString());
                                SPFieldUserValue userValue = new SPFieldUserValue();
                                foreach (SPFieldUserValue userFieldValue in userFieldValueCollection)
                                {
                                    if (userFieldValue.User != null)
                                    {
                                        SPUser user = userFieldValue.User;
                                        userValue = new SPFieldUserValue(web, user.ID, user.LoginName);
                                    }
                                    //if the field contain group
                                    else
                                    {
                                        SPGroup group = web.SiteGroups.GetByID(userFieldValue.LookupId);
                                        userValue = new SPFieldUserValue(web, group.ID, group.LoginName);
                                    }
                                    usercollection.Add(userValue);
                                }
                                SPList oList = web.Lists.TryGetList(Constants.VlogPostFeedbackListName);
                                if (oList != null)
                                {
                                    SPListItem NewItem = oList.Items.Add();
                                    NewItem[Constants.ListColumns.VlogPostFeedback.Title] = title;
                                    NewItem[Constants.ListColumns.VlogPostFeedback.UsersName] = name;
                                    NewItem[Constants.ListColumns.VlogPostFeedback.EmailAddress] = emailAddress;
                                    NewItem[Constants.ListColumns.VlogPostFeedback.Comments] = comments;
                                    NewItem[Constants.ListColumns.VlogPostFeedback.FeedbackQuestionCategory] = new SPFieldLookupValue(categoryId, categoryTitle);
                                    NewItem[Constants.ListColumns.VlogPostFeedback.Notifications] = usercollection;

                                    bool unsafeUpdates = web.AllowUnsafeUpdates;
                                    web.AllowUnsafeUpdates = true;
                                    NewItem.Update();
                                    newItemID = NewItem.ID;
                                    web.AllowUnsafeUpdates = unsafeUpdates;
                                    isSuccess = true;
                                }
                                SPSecurity.RunWithElevatedPrivileges(delegate()
                                {
                                    SPListItem newlyCreatedListItem = oList.GetItemById(newItemID);
                                    SPUser CUser = web.EnsureUser(Constants.SystemAccount);
                                    newlyCreatedListItem[Constants.ListColumns.VlogPostFeedback.CreatedBy] = CUser;
                                    newlyCreatedListItem[Constants.ListColumns.VlogPostFeedback.ModifiedBy] = CUser;
                                    bool unsafeUpdates = web.AllowUnsafeUpdates;
                                    web.AllowUnsafeUpdates = true;
                                    newlyCreatedListItem.Update();
                                    web.AllowUnsafeUpdates = unsafeUpdates;
                                });
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                isSuccess = false;
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Feedback", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                });
            }
            return isSuccess;
        }

        private static SPListItemCollection GetFeedBackCategoryListItem(SPSite oSite, string categoryTitle)
        {
            SPListItemCollection items = null;
            try
            {
                using (SPWeb web = oSite.RootWeb)
                {
                    SPList oList = web.Lists.TryGetList(Constants.VlogPostFeedbackCategoryListName);
                    if (oList != null)
                    {
                        string Query = string.Format(Constants.FeedbackCategoryListQuery, categoryTitle);
                        SPQuery query = new SPQuery();
                        query.Query = Query;
                        items = oList.GetItems(query);
                    }
                }
            }
            catch (Exception ex)
            {
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Feedback", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                });
            }
            return items;
        }

        private static SPListItemCollection GetURLExceptionList() {
            SPListItemCollection result = null;
            try
            {
                SPWeb web = SPContext.Current.Web.Site.RootWeb;
                SPList list = web.Lists.TryGetList(Constants.RootConfigList);
                if (list != null)
                {
                    SPQuery query = new SPQuery();
                    query.Query = string.Format(Constants.RootConfigListQuery, Constants.AnalyticsExceptionKey);
                    result = list.GetItems(query);
                }
            }
            catch (Exception ex) {
                SPSecurity.RunWithElevatedPrivileges(delegate() {
                    SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("AnalyticsDB", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                });
            }
            return result;
        }
    }
}
