﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Viacom.AdSpace.Shared
{
    public class Constants
    {
        public struct ListColumns
        {
            public struct Navigation
            {
                public const string HeaderTitle = "HeaderTitle";
                public const string CategoryTitle = "CategoryTitle";
                public const string HeaderUrl = "HeaderUrl";
                public const string LinkHeader = "LinkHeader";
                public const string LinkCategory = "LinkCategory";
                public const string LinkCategoryUrl = "LinkCategoryUrl";
                public const string LinkName = "LinkName";
                public const string LinkUrl = "LinkUrl";
                public const string AccessTo = "AccessTo";
                public const string OrderCategories = "OrderCategories";
                public const string ContentType = "Content Type";
            }

            public struct ConfigListColumns
            {
                public const string SharedItemType = "SharedItemType";
                public const string SiteURL = "SiteURL";
                public const string LibraryName = "LibraryName";
                public const string ItemURL = "ItemURL";
                public const string SharingEnabled = "SharingEnabled";
                public const string SharingDuration = "SharingDuration";
                public const string JobStatus = "JobStatus";
                public const string CreatedField = "Created";
                public const string ArchivalEnabled = "ArchivalEnabled";   
            }

            public struct VlogPostFeedback
            {
                public const string EmailAddress = "EmailAddress";
                public const string FeedbackQuestionCategory = "FeedbackQuestionCategory";
                public const string UsersName = "UsersName";
                public const string Comments = "Comments";
                public const string Title = "Title";
                public const string Notifications = "Notifications";
                public const string CreatedBy = "Created By";
                public const string ModifiedBy = "Editor";
            } 

        }

        public struct ContentType
        {
            public struct Navigation
            {
                public const string NavigationHeader = "Navigation Header";
                public const string NavigationCategory = "Navigation Category";
                public const string NavigationLink = "Navigation Link";
            }
        }

        public struct AnalyticsDBConstants
        {
            public const string ConnectionStringForAnalyticsDB = "AdSpaceAnalytics";
            public const string ConnectionStringForAnalyticsDB_Windows = "AdSpaceAnalytics_windows";
        }
        public const string ArchievedField = "IsArchived";
        public const string Enabled = "Enable";
        public const string Archived = "Archived";
        public const string ExternalSharingupdateDateTime = "ExternalSharingupdateDateTime";
        public const string ArchivalSettingUpdateDateTime = "ArchivalSettingupdateDateTime";
        public const string SiteUrl = "siteUrl";
        public const string Title = "title";
        public const string Name = "name";
        public const string EmailAddress = "emailAddress";
        public const string CategoryId = "categoryId";
        public const string CategoryTitle = "categoryTitle";
        public const string Comments = "comments";
        public const string NavigationListName = "Navigation";
        public const string MyNavigationListView = "My Navigation";
        public const string NewJobStatus = "New";
        public const string ExtShareConfigListName = "External Share Configuration Requests";
        public const string ArchivalRequestsListName = "Archival Requests";
        public const string VlogPostFeedbackListName = "Vlog Post Feedback";
        public const string VlogPostFeedbackCategoryListName = "Vlog Post Feedback Category";
        public const string ListType = "List";
        public const string FolderType = "Folder";
        public const string ItemType = "Item";
        public const string SystemAccount = "Sharepoint\\System";
        public const string FeedbackCategoryListQuery = @" <Where>
                                                              <Eq>
                                                                 <FieldRef Name='CategoryName' />
                                                                 <Value Type='Text'>{0}</Value>
                                                              </Eq>
                                                           </Where>";
        
        public const string RootConfigList = "ConfigurationList";
        public const string RootConfigListQuery = @" <Where>
                                                              <Eq>
                                                                 <FieldRef Name='Key' />
                                                                 <Value Type='Text'>{0}</Value>
                                                              </Eq>
                                                           </Where>";
        public const string RootConfigValueField = "Value1";
        //Analytics Exception List Constants
        public const string AnalyticsExceptionKey = "AnalyticsExceptionKey";
        //Treeview Content Type Keys
        public const string NotRequiredCTKey = "NotRequired";
        public const string ShareDocToVaultCTKey = "ShareDocToVaultCTKey";
        public const string ShareLinkToVaultCTKey = "ShareLinkToVaultCTKey";
        public const string OTToVaultCTKey = "OTtoVaultCTKey";

    }
}
