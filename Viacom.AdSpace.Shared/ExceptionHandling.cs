﻿using Microsoft.SharePoint.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint;

namespace Viacom.AdSpace.Shared
{
    /// <summary>
    ///  Exception Handing class for handing exception and creating logs
    /// </summary>
    public class ExceptionHandling
    {
        #region Constants

        /// <summary>
        /// Log category
        /// </summary>
        private const string C_CATEGORY = "AdSPace Error Log";
        private const string C_RSSCATEGORY = "AdSPace RSS Feed Error Log";


        /// <summary>
        /// Exception format
        /// </summary>
        private const string C_EXCEPTION_FOR_JS_MSG = "Exception: {0}. occurred on page {1}. while processing file:{2}. method:{3}";

        /// <summary>
        /// Exception list for RSS Feed Logging
        /// </summary>
        private const string C_RSSLOGLISTNAME = "RSS Feed Logs";

        /// <summary>
        /// RSS Feed Logging list columns
        /// </summary>
        private const string C_TITLE = "Title";
        private const string C_EXCEPTION = "Exception";
        private const string C_STACKTRACE = "Stacktrace";
        private const string C_USERNAME = "UserName";
        #endregion

        #region Methods


        /// <summary>
        /// Log to Sp diagnostic
        /// </summary>
        /// <param name="ex"></param>
        public static void LogToSPDiagnostic(Exception ex)
        {
            ////Getting an instance of SPDiagnostic service.
            SPDiagnosticsService diagSvc = SPDiagnosticsService.Local;
            ////Logging message into sharepoint logs.
            

             diagSvc.WriteTrace(0,new SPDiagnosticsCategory(C_CATEGORY, TraceSeverity.Unexpected,EventSeverity.Error),TraceSeverity.Unexpected, ex.InnerException.ToString(),ex.StackTrace.ToString());
        }

        /// <summary>
        /// Log to Sp diagnostic
        /// </summary>
        /// <param name="category"></param>
        /// <param name="ex"></param>
        public static void LogToSPDiagnostic(string category, Exception ex)
        {
            ////Getting an instance of SPDiagnostic service.
            SPDiagnosticsService diagSvc = SPDiagnosticsService.Local;
            ////Logging message into sharepoint logs.


            diagSvc.WriteTrace(0, new SPDiagnosticsCategory(category, TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.InnerException.ToString(), ex.StackTrace.ToString());
        }

        /// <summary>
        /// Log Exception to SP diagnostic service
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="page"></param>
        /// <param name="file"></param>
        /// <param name="method"></param>
        public static void LogToSPDiagnostic(string msg,string page,string file,string method)
        {
            ////Getting an instance of SPDiagnostic service.
            SPDiagnosticsService diagSvc = SPDiagnosticsService.Local;
            ////Logging message into sharepoint logs.

            string logText = string.Format(C_EXCEPTION_FOR_JS_MSG, msg, page, file, method);

            diagSvc.WriteTrace(0, new SPDiagnosticsCategory(C_CATEGORY, TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, logText, logText);
        }

        /// <summary>
        /// Log Exception to RSS Log List at Root site
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="feedUrl"></param>
        public static void LogExceptionToRSSFeedLogs(string feedUrl, Exception ex) {
            try
            {
                // Get Current site from context
                SPSite site = SPContext.Current.Web.Site;

                SPUser user = SPContext.Current.Web.CurrentUser;
                using (SPWeb web = site.OpenWeb()) {
                    SPSecurity.RunWithElevatedPrivileges(delegate() {
                        SPListItemCollection listItems = web.Lists[C_RSSLOGLISTNAME].Items;
                        bool unsafeUpdate = web.AllowUnsafeUpdates;
                        web.AllowUnsafeUpdates = true;
                        SPListItem item = listItems.Add();

                        if (!string.IsNullOrEmpty(feedUrl))
                        {
                            item[C_TITLE] = feedUrl;
                        }
                        else
                        {
                            item[C_TITLE] = "Empty Url";
                        }
                        if (!string.IsNullOrEmpty(ex.Message))
                        {
                            item[C_EXCEPTION] = ex.Message;
                        }
                        else
                        {
                            item[C_EXCEPTION] = "";
                        }
                        if (!string.IsNullOrEmpty(ex.StackTrace))
                        {
                            item[C_STACKTRACE] = ex.StackTrace;
                        }
                        else 
                        {
                            item[C_STACKTRACE] = "";
                        }
                        if (!string.IsNullOrEmpty(user.Name))
                        {
                            item[C_USERNAME] = user.Name;
                        }
                        else
                        {
                            item[C_USERNAME] = user.LoginName;
                        }
                        item.Update();
                        web.AllowUnsafeUpdates = unsafeUpdate;
                    });

                }
            }
            catch (Exception e) {
                LogToSPDiagnostic(C_RSSCATEGORY, e);
            }
        }
        #endregion

    }
}
