﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel.Syndication;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Viacom.AdSpace.Shared
{
    /// <summary>
    /// Class to  help rss feed operations
    /// </summary>
    public class RSSFeedHelper
    {


        /// <summary>
        /// Get RSS feed
        /// </summary>
        /// <param name="url"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageCount"></param>
        /// <returns></returns>
        public RSSFeedObject GetRSSFeed(string url, string pageSize, string pageCount, string feed_outerName, string feed_attribute)
        {

            RSSFeedObject obj = new RSSFeedObject();
            try
            {
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                XmlReader reader = XmlReader.Create(url);

                IList<SyndicationItem> items = null;
                List<NewsFeedItem> newsFeedItems = new List<NewsFeedItem>();

                SyndicationFeed feed = SyndicationFeed.Load(reader);

                if (Convert.ToInt32(pageCount) == 0)
                {
                    items = feed.Items.Take(Convert.ToInt32(pageSize)).ToList();
                }


                //RSSFeedObject obj = new RSSFeedObject();

                // get news feed item from syndication item
                foreach (SyndicationItem synItem in items)
                {
                    NewsFeedItem feedItem = new NewsFeedItem(synItem, feed_outerName, feed_attribute);
                    newsFeedItems.Add(feedItem);
                }


                obj.Items = newsFeedItems;
                obj.Source = feed.Title.Text;
                if (obj.Items.Count == 0) {
                    ExceptionHandling.LogExceptionToRSSFeedLogs(url, new Exception("No item returned from the feed"));
                }
                return obj;
            }
            catch (Exception Ex)
            {
                obj = null;
                ExceptionHandling.LogToSPDiagnostic(Ex.Message,"external news","externalnewshelper.js","getnewsfeed");
                ExceptionHandling.LogExceptionToRSSFeedLogs(url, Ex);
                return obj;
            }




        }

        /// <summary>
        /// Rss feed object to return.
        /// </summary>
        public class RSSFeedObject
        {
            /// <summary>
            /// Source of the site
            /// </summary>
            public string Source { get; set; }

            /// <summary>
            /// Source of the site
            /// </summary>
            public string ImageUrl { get; set; }

            /// <summary>
            /// Items
            /// </summary>
            public IList<NewsFeedItem> Items { get; set; }
        }

        /// <summary>
        /// News item details
        /// </summary>
        public class NewsFeedItem
        {

            public SyndicationItem Item { get; set; }

            public string ImageUrl { get; set; }

            //Constructer
            public NewsFeedItem(SyndicationItem synItem, string outerName, string attribute)
            {
                this.Item = synItem;
                this.SetThumbnailImageSource(outerName, attribute);
            }

            //Set the image url property
            private void SetThumbnailImageSource(string outerName, string attribute)
            {
                try
                {
                    
                    var outerNameArray = outerName.Split(',');
                    var attributeArray = attribute.Split(',');
                    string img1 = string.Empty;
                    string img2 = string.Empty;

                    if (Item != null)
                    {
                        foreach (SyndicationElementExtension synEx in Item.ElementExtensions)
                        {

                            var index = Array.IndexOf(outerNameArray, synEx.OuterName);
                            if (index >= 0)
                            {
                                XmlReader r = synEx.GetReader();
                                img1 = r.GetAttribute(attributeArray[index]);
                                while (r.Read())
                                {
                                    if (r.NodeType == XmlNodeType.Element)
                                    {
                                        img2 = r.GetAttribute(attributeArray[index]);
                                        break;
                                    }

                                }

                            }
                        }
                    }

                    //Use img1 if img2 not available.
                    this.ImageUrl = img1;
                    if (!string.IsNullOrEmpty(img2))
                    {
                        this.ImageUrl = img2;
                    }


                }
                catch (Exception ex) {
                    throw ex;
                }
            }
            

        }




    }
}
