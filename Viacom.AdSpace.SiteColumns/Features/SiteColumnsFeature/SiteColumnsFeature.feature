﻿<?xml version="1.0" encoding="utf-8"?>
<feature xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="c56c18f1-4af8-404f-ac72-8e7e01333af8" description="Feature contains viacom adspace common site columns" featureId="c56c18f1-4af8-404f-ac72-8e7e01333af8" imageUrl="" scope="Site" solutionId="00000000-0000-0000-0000-000000000000" title="Viacom.AdSpace.CommonSiteColumns.SiteColumnsFeature" version="" deploymentPath="$SharePoint.Project.FileNameWithoutExtension$_$SharePoint.Feature.FileNameWithoutExtension$" xmlns="http://schemas.microsoft.com/VisualStudio/2008/SharePointTools/FeatureModel">
  <projectItems>
    <projectItemReference itemId="ce1a005e-2d9a-46d5-9395-d16cad262f68" />
    <projectItemReference itemId="32279d7a-bdc2-4011-bb9a-69ebf09de326" />
    <projectItemReference itemId="2569c766-20a0-4b43-8c49-c709cc38cb22" />
    <projectItemReference itemId="309f0013-5a94-40d9-a7a8-283d3809bc12" />
    <projectItemReference itemId="39542f46-1fc4-4239-9e37-e6c5d31834f1" />
    <projectItemReference itemId="b679cad6-e17a-4bc5-89f7-7e10c10753ee" />
    <projectItemReference itemId="dbb29168-ac2e-48de-93d1-02ca8195a122" />
    <projectItemReference itemId="97229454-4824-4477-b18f-f7289a9edd8e" />
    <projectItemReference itemId="3fa45429-3d4c-4f76-a7d0-e21f5dc35021" />
    <projectItemReference itemId="4d1d9333-6dca-4af0-a42b-202ee1fd2ae7" />
    <projectItemReference itemId="2d8cfb60-4ed8-4450-a96c-e7a5548050b2" />
    <projectItemReference itemId="2b6eaf59-52c4-4168-8b67-71be8689178a" />
    <projectItemReference itemId="b06a7f36-7abe-417e-a4cd-8d3809fc7312" />
    <projectItemReference itemId="b5773159-135f-49da-8e69-a72185898f58" />
    <projectItemReference itemId="d0333f24-331d-4ba7-95e6-87405ffaa085" />
    <projectItemReference itemId="e2936530-01ff-4fff-b2b0-aaca240642a0" />
    <projectItemReference itemId="e113f2fb-2ead-4aa0-a136-2c2dc34d5a7e" />
    <projectItemReference itemId="1611ff91-5192-402d-9750-576c082e36b5" />
    <projectItemReference itemId="10fbf526-36c7-4ed2-a00d-05435c9b3881" />
    <projectItemReference itemId="3dde4c33-7f22-42f1-af4c-3b263e8cb8a0" />
    <projectItemReference itemId="25dd15ff-1112-45af-9020-a3a8fc2e1a96" />
    <projectItemReference itemId="8a5a791c-8265-4c8b-a806-9cacd7abfa80" />
    <projectItemReference itemId="0e1fddb9-34e0-4c6b-905b-a7d58bf54e61" />
    <projectItemReference itemId="346c5879-d530-49ea-87a8-207f3cd8c54e" />
    <projectItemReference itemId="33fadaea-8dc2-4565-ad3f-e184572e2038" />
    <projectItemReference itemId="b77ca690-3ca6-4b59-91d4-7aad277047fa" />
    <projectItemReference itemId="bbc0fad5-8958-4358-aaef-6f53fd7f2b5c" />
    <projectItemReference itemId="fbdb4bac-effe-4f3b-85a4-f03f52d85216" />
    <projectItemReference itemId="785e9472-9f75-48e4-8abf-c49f90a884c5" />
    <projectItemReference itemId="1e07b111-c60d-4199-aed9-707ecc4220bf" />
    <projectItemReference itemId="541b16ea-e0d8-44a3-a26f-7f8e9398f116" />
  </projectItems>
</feature>