﻿function getListItems(url, listname, query, complete, failure) {
    $.ajax({
        url: _spPageContextInfo.siteAbsoluteUrl + "/_api/web/lists/getbytitle('" + listname + "')/items" + query,
        method: "GET",
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {
            complete(data.d.results); 
        },
        error: function (data) {
            failure(data.d.results);
        }
    });
}

/**Then you could retrieve unique values from JSON array as demonstrated in answer Get unique results from JSON array using jQuery:***/

function groupBy(items,propertyName)
{
    var result = [];
    $.each(items, function(index, item) {
       if ($.inArray(item[propertyName], result)==-1) {
          result.push(item[propertyName]);
       }
    });
    return result;
}

/*The usage*/

getListItems('','Externally shared Content','?select=Content Group',function(items){    var taskNames = groupBy(items,'Content Group');alert(taskNames);},function(error){alert(JSON.stringify(error));});
