var ContentCatFeed = {
    RegisterRenderer: function () {// Method to register the jslink
        var overrideCtx = {};
        overrideCtx.Templates = {};
        overrideCtx.BaseViewID = 2;
        overrideCtx.ListTemplateType = 100;
        overrideCtx.Templates.Fields = {
            'AdSpaceContentCategory': { 'View': ContentCatFeed.CustomView}
        };
        SPClientTemplates.TemplateManager.RegisterTemplateOverrides(overrideCtx);
    },
    CustomView: function (ctx) { // method to change the view of the field
        if (ctx != null && ctx.CurrentItem != null) {

            var categories = ctx.CurrentItem["AdSpaceContentCategory"];

            // get current sub iste url.

            var siteUrl = window.location.protocol + "//" + window.location.host + L_Menu_BaseUrl;

            var categoryUrl = 'k=ContentCategory:' + categories.Label;

            var ret = "<a href='/sites/adspace/SearchCenter/Pages/ContentCategoryFeed.aspx?ud=" + encodeURIComponent(siteUrl) + "&" + encodeURI(categoryUrl) + "' class='navToSearchCat'>" + categories.Label + "</a>";

            return ret;
        }
    }
};

ExecuteOrDelayUntilScriptLoaded(ContentCatFeed.RegisterRenderer, 'clienttemplates.js');
