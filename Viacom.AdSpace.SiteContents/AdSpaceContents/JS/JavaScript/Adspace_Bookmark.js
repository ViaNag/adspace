if (!window.console) {
    console = { log: function () { } };
}

var Viacom = Viacom || {};
Viacom.AdSpace = Viacom.AdSpace || {};
Viacom.AdSpace.CustomAction = Viacom.AdSpace.CustomAction || {};

Viacom.AdSpace.CustomAction.Constants = function () {
    return {
        itemArr: null,
        selectedItems: null,
        bookmarkLst: null,
        bookmarkLstColNm: null,
        configurationLstNm: null,
        listContentTypes: null,
        grpCTName: null,
        bookmarkLstCTName: null,
        noOfItems: null,
        existingGrpItems: null,
        addBookmarkToGrp: null,
        addBookmarkToGrpId: null,
        itemsLimit: null
    }
};

$(document).ready(function () {
    SP.SOD.executeFunc('sp.js', 'SP.ClientContext', Viacom.AdSpace.CustomAction.Initialize);
});

Viacom.AdSpace.CustomAction.Initialize = function () {
    Viacom.AdSpace.CustomAction.Constants.itemArr = [];
    Viacom.AdSpace.CustomAction.Constants.selectedItems = [];
    Viacom.AdSpace.CustomAction.Constants.bookmarkLst = "Bookmark List";
    Viacom.AdSpace.CustomAction.Constants.configurationLstNm = "ConfigurationList";
    Viacom.AdSpace.CustomAction.Constants.listContentTypes = null;
    Viacom.AdSpace.CustomAction.Constants.grpCTName = "BookmarkGroups";
    Viacom.AdSpace.CustomAction.Constants.bookmarkLstCTName = "Bookmark List";
    Viacom.AdSpace.CustomAction.Constants.noOfItems = 0;
    Viacom.AdSpace.CustomAction.Constants.addBookmarkToGrp = null;
    Viacom.AdSpace.CustomAction.Constants.addBookmarkToGrpId = null;
    Viacom.AdSpace.CustomAction.Constants.existingGrpItems = 0;
    Viacom.AdSpace.CustomAction.Constants.bookmarkLstColNm = {};
    Viacom.AdSpace.CustomAction.Constants.bookmarkLstColNm.UserGrp = "UserGroup";
    Viacom.AdSpace.CustomAction.Constants.bookmarkLstColNm.CTId = "ContentTypeId";
    Viacom.AdSpace.CustomAction.Constants.bookmarkLstColNm.Title = "Title";
    Viacom.AdSpace.CustomAction.Constants.bookmarkLstColNm.Url = "Url";
    Viacom.AdSpace.CustomAction.Constants.bookmarkLstColNm.GrpNm = "GroupName";
    Viacom.AdSpace.CustomAction.Constants.bookmarkLstColNm.GroupLkpId = "GroupNameId";
    Viacom.AdSpace.CustomAction.Constants.itemsLimit = {};
    Viacom.AdSpace.CustomAction.Constants.itemsLimit.maxBookmarkLinkInGroup = 10;
    Viacom.AdSpace.CustomAction.Constants.itemsLimit.maxUserBookmarkLinks = 30;
    var query = "<View><Query><Where><And><Eq><FieldRef Name='Title' /><Value Type='Text'>BookMarkMaxLinkForGroup</Value></Eq><Eq><FieldRef Name='Key'/><Value Type='Text'>MaxLinks</Value></Eq></And></Where></Query></View>";
    Viacom.AdSpace.CustomAction.getConfigurationLstData(query, Viacom.AdSpace.CustomAction.Constants.configurationLstNm, Viacom.AdSpace.CustomAction.Constants.itemsLimit);
    $("#ddlCustomActionGroup").unbind('change').change(Viacom.AdSpace.CustomAction.AdSpaceItemGrpOnChange);

};

Viacom.AdSpace.CustomAction.getConfigurationLstData = function (query, lstNm, setglobalValue) {
    try {
        var configCtx = SP.ClientContext.get_current();
        var rootWeb = configCtx.get_site().get_rootWeb();
        var oList = rootWeb.get_lists().getByTitle(lstNm);
        var camlQuery = new SP.CamlQuery();
        camlQuery.set_viewXml(query);
        var collListItem = oList.getItems(camlQuery);
        configCtx.load(collListItem);
        configCtx.executeQueryAsync(
              function (sender, args) {
                  var itemsCount = collListItem.get_count();
                  if (itemsCount == 1) {
                      var listItemEnumerator = collListItem.getEnumerator();
                      var value = null;
                      while (listItemEnumerator.moveNext()) {
                          var oListItem = listItemEnumerator.get_current();
                          setglobalValue.maxBookmarkLinkInGroup = oListItem.get_item("Value1");
                      }
                  }
                  else {
                      return null;
                  }
              }, function (sender, args) {
                  //alert("Error while fetching data from Configuration list.");
                  console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                  console.log("Error while fetching data from Configuration list.");
              });
    }
    catch (e) {
        //alert("Exception while fetching data from Configuration list.");
        console.log("Exception while fetching data from Configuration list.");
    }
};

Viacom.AdSpace.CustomAction.AdSpaceItemGrpOnChange = function () {
    try {
        var select_group_val = $("#ddlCustomActionGroup").find("option:selected").attr("value");
        if (select_group_val === "AddNewGroup") {
            $("#ddlCustomActionGroup").parents('.edit-bookmark-form').children('.add-group-field').show();
            $("#customActionGrpDiv").show();
        }
        else {
            $('.add-group-field').hide();
            $("#customActionValidationMsg").empty();
        }
        return false;
    }
    catch (e) {
        $("#dialog-add-doc").dialog("close");
        $('.add-group-field').hide();
        $("#bookmarkNewGrp").val("");
        //alert("Exception while onchange event of Group Dropdown");
        console.log("Exception while onchange event of Group Dropdown.");
        Viacom.AdSpace.CustomAction.CommonDialogAlertBox("Failed to to select different group.");
    }
};

Viacom.AdSpace.CustomAction.CreateListItemWithDetails = function (bookMarkType, grpName, id) {
    try {
        var clientContext = SP.ClientContext.get_current();
        var rootWeb = clientContext.get_site().get_rootWeb();
        var oList = rootWeb.get_lists().getByTitle(Viacom.AdSpace.CustomAction.Constants.bookmarkLst);
        Viacom.AdSpace.CustomAction.Constants.listContentTypes = null;
        Viacom.AdSpace.CustomAction.Constants.listContentTypes = oList.get_contentTypes();
        clientContext.load(Viacom.AdSpace.CustomAction.Constants.listContentTypes);
        var item = oList.addItem();

        if (Viacom.AdSpace.CustomAction.Constants.selectedItems[Viacom.AdSpace.CustomAction.Constants.noOfItems].Title !== null && Viacom.AdSpace.CustomAction.Constants.selectedItems[Viacom.AdSpace.CustomAction.Constants.noOfItems].Title !== undefined && Viacom.AdSpace.CustomAction.Constants.selectedItems[Viacom.AdSpace.CustomAction.Constants.noOfItems].Title != '') {
            item.set_item(Viacom.AdSpace.CustomAction.Constants.bookmarkLstColNm.Title, Viacom.AdSpace.CustomAction.Constants.selectedItems[Viacom.AdSpace.CustomAction.Constants.noOfItems].Title);
        }
        else if (Viacom.AdSpace.CustomAction.Constants.selectedItems[Viacom.AdSpace.CustomAction.Constants.noOfItems].Name !== null && Viacom.AdSpace.CustomAction.Constants.selectedItems[Viacom.AdSpace.CustomAction.Constants.noOfItems].Name !== undefined && Viacom.AdSpace.CustomAction.Constants.selectedItems[Viacom.AdSpace.CustomAction.Constants.noOfItems].Name != '') {
            item.set_item(Viacom.AdSpace.CustomAction.Constants.bookmarkLstColNm.Title, Viacom.AdSpace.CustomAction.Constants.selectedItems[Viacom.AdSpace.CustomAction.Constants.noOfItems].Name);
        }
        else {
            item.set_item(Viacom.AdSpace.CustomAction.Constants.bookmarkLstColNm.Title, "");
        }

        var bookmarkUrl = new SP.FieldUrlValue();
        bookmarkUrl.set_url(Viacom.AdSpace.CustomAction.Constants.selectedItems[Viacom.AdSpace.CustomAction.Constants.noOfItems].Url);
        bookmarkUrl.set_description(Viacom.AdSpace.CustomAction.Constants.selectedItems[Viacom.AdSpace.CustomAction.Constants.noOfItems].Url);
        item.set_item(Viacom.AdSpace.CustomAction.Constants.bookmarkLstColNm.Url, bookmarkUrl);

        switch (bookMarkType) {
            case "NewGroup": {
                var grpLookupId = new SP.FieldLookupValue();
                grpLookupId.set_lookupId(id);
                item.set_item(Viacom.AdSpace.CustomAction.Constants.bookmarkLstColNm.GrpNm, grpLookupId);
                break;
            }
            case "NoGroup": {
                break;
            }
            case "WithGroup": {
                var grpLookupId = new SP.FieldLookupValue();
                grpLookupId.set_lookupId(id);
                item.set_item(Viacom.AdSpace.CustomAction.Constants.bookmarkLstColNm.GrpNm, grpLookupId);
                break;
            }
        }
        item.update();
        clientContext.load(item);
        clientContext.executeQueryAsync(function () {
            var ct_enumerator = Viacom.AdSpace.CustomAction.Constants.listContentTypes.getEnumerator();
            while (ct_enumerator.moveNext()) {
                var ct = ct_enumerator.get_current();
                if (ct.get_name().toString() === Viacom.AdSpace.CustomAction.Constants.bookmarkLstCTName) {
                    item.set_item(Viacom.AdSpace.CustomAction.Constants.bookmarkLstColNm.CTId, ct.get_id().toString());
                    item.update();
                    clientContext.executeQueryAsync(function () {
                        console.log("New item created");
                        Viacom.AdSpace.CustomAction.Constants.noOfItems++;
                        if (Viacom.AdSpace.CustomAction.Constants.noOfItems < Viacom.AdSpace.CustomAction.Constants.selectedItems.length) {
                            Viacom.AdSpace.CustomAction.CreateListItemWithDetails(bookMarkType, Viacom.AdSpace.CustomAction.Constants.addBookmarkToGrp, Viacom.AdSpace.CustomAction.Constants.addBookmarkToGrpId);
                        }
                        else {
                            //alert("Bookmarks created successfully");
                            $("#dialog-add-doc").dialog("close");
                            $('.add-group-field').hide();
                            $("#bookmarkNewGrp").val("");
                            Viacom.AdSpace.CustomAction.CommonDialogAlertBox("Bookmarks created successfully");
                            //location.reload();
                        }
                    }, function (sender, args) {
                        $("#dialog-add-doc").dialog("close");
                        //alert("Error in while updating list item");
                        $('.add-group-field').hide();
                        $("#bookmarkNewGrp").val("");
                        console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                        console.log("Error in while updating list item");
                        Viacom.AdSpace.CustomAction.CommonDialogAlertBox("Failed to add Bookmark");
                    });
                }
            }
        }, function (sender, args) {
            $("#dialog-add-doc").dialog("close");
            //alert("Error in creating item in 'Bookmark' list");
            $('.add-group-field').hide();
            $("#bookmarkNewGrp").val("");
            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
            console.log("Error in creating item in 'Bookmark' list");
            Viacom.AdSpace.CustomAction.CommonDialogAlertBox("Failed to add Bookmark");
        });
    }
    catch (e) {
        $("#dialog-add-doc").dialog("close");
        $('.add-group-field').hide();
        $("#bookmarkNewGrp").val("");
        //alert("Exception while creating item in Bookmark list");
        console.log("Exception while creating item in Bookmark list");
        Viacom.AdSpace.CustomAction.CommonDialogAlertBox("Failed to add Bookmark");
    }
};

Viacom.AdSpace.CustomAction.AdSpaceItemCreateGrp = function (listID, itemID) {
    try {
        var bookMarkGroupValue = $("#ddlCustomActionGroup option:selected").val();
        switch (bookMarkGroupValue) {
            case "AddNewGroup": {
                var bookMarkNewGroup = $("#bookmarkNewGrp").val();
                if (!(bookMarkNewGroup === "" || bookMarkNewGroup === null || bookMarkNewGroup === undefined)) {
                    Viacom.AdSpace.CustomAction.Constants.addBookmarkToGrp = bookMarkNewGroup;
                    var groupCtx = SP.ClientContext.get_current();
                    var rootWeb = groupCtx.get_site().get_rootWeb();
                    var oList = rootWeb.get_lists().getByTitle(Viacom.AdSpace.CustomAction.Constants.bookmarkLst);
                    Viacom.AdSpace.CustomAction.Constants.listContentTypes = oList.get_contentTypes();
                    groupCtx.load(Viacom.AdSpace.CustomAction.Constants.listContentTypes);
                    var item = oList.addItem();
                    item.set_item(Viacom.AdSpace.CustomAction.Constants.bookmarkLstColNm.UserGrp, bookMarkNewGroup);
                    item.update();
                    groupCtx.load(item);
                    groupCtx.executeQueryAsync(function () {
                        var ct_enumerator = Viacom.AdSpace.CustomAction.Constants.listContentTypes.getEnumerator();
                        while (ct_enumerator.moveNext()) {
                            var ct = ct_enumerator.get_current();
                            if (ct.get_name().toString() === Viacom.AdSpace.CustomAction.Constants.grpCTName) {
                                item.set_item(Viacom.AdSpace.CustomAction.Constants.bookmarkLstColNm.CTId, ct.get_id().toString());
                                item.update();
                                groupCtx.executeQueryAsync(function () {
                                    console.log("New group created");
                                    Viacom.AdSpace.CustomAction.Constants.noOfItems = 0;
                                    Viacom.AdSpace.CustomAction.Constants.addBookmarkToGrpId = item.get_id();
                                    Viacom.AdSpace.CustomAction.CreateListItemWithDetails("NewGroup", Viacom.AdSpace.CustomAction.Constants.addBookmarkToGrp, Viacom.AdSpace.CustomAction.Constants.addBookmarkToGrpId);
                                }, function (sender, args) {
                                    $("#dialog-add-doc").dialog("close");
                                    //alert("Error in while updating list item");
                                    $('.add-group-field').hide();
                                    $("#bookmarkNewGrp").val("");
                                    console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                                    console.log("Error in while updating list item");
                                    Viacom.AdSpace.CustomAction.CommonDialogAlertBox("Failed to add Bookmark. Failed to create group " + Viacom.AdSpace.CustomAction.Constants.addBookmarkToGrpId+".");
                                });
                            }
                        }
                    }, function (sender, args) {
                        $("#dialog-add-doc").dialog("close");
                        //alert("Error while making call to funtion to make entry in BookMark list for new group");
                        $('.add-group-field').hide();
                        $("#bookmarkNewGrp").val("");
                        console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                        console.log("Error while making call to funtion to make entry in BookMark list for new group");
                        Viacom.AdSpace.CustomAction.CommonDialogAlertBox("Failed to add Bookmark");
                    });
                }
                else {
                    $("#customActionValidationMsg").empty();
                    $("#customActionValidationMsg").append("<br/>Empty group cannot be added.");
                }
                break;
            }
            case "AddUniqueBookmark": {
                Viacom.AdSpace.CustomAction.CreateListItemWithDetails("NoGroup", null, null);
                break;
            }
            default: {
                Viacom.AdSpace.CustomAction.Constants.addBookmarkToGrp = $("#ddlCustomActionGroup option:selected").text();
                Viacom.AdSpace.CustomAction.Constants.addBookmarkToGrpId = bookMarkGroupValue;
                Viacom.AdSpace.CustomAction.CreateListItemWithDetails("WithGroup", Viacom.AdSpace.CustomAction.Constants.addBookmarkToGrp, bookMarkGroupValue);
            }
        }
    }
    catch (e) {
        $("#dialog-add-doc").dialog("close");
        //alert("Exception while adding new group in Bookmark list");
        console.log("Exception while adding new group in Bookmark list");
        $('.add-group-field').hide();
        $("#bookmarkNewGrp").val("");
        Viacom.AdSpace.CustomAction.CommonDialogAlertBox("Failed to add Bookmark");
    }
};

Viacom.AdSpace.CustomAction.AdSpaceItemAddGrp = function (listID, itemID) {
    try {
        var bookMarkGroupValue = $("#ddlCustomActionGroup option:selected").val();
        var arrItems = itemID.split(",");
        var noOfItems = arrItems.length;
        var grpToValidate = null;
        if (bookMarkGroupValue === "AddNewGroup") {
            grpToValidate = $("#bookmarkNewGrp").val();
        }
        if (bookMarkGroupValue !== "AddNewGroup" && bookMarkGroupValue !== "AddUniqueBookmark") {
            grpToValidate = $("#ddlCustomActionGroup option:selected").text();
        }
        Viacom.AdSpace.CustomActionValidateUserBookmark(_spPageContextInfo.userId, Viacom.AdSpace.CustomAction.Constants.bookmarkLstCTName, grpToValidate, Viacom.AdSpace.CustomAction.Constants.bookmarkLst, noOfItems, listID, itemID)
    }
    catch (e) {
        $("#dialog-add-doc").dialog("close");
        //alert("Exception while validation for items in Bookmark list");
        console.log("Exception while validation for items in Bookmark list");
        $('.add-group-field').hide();
        $("#bookmarkNewGrp").val("");
        Viacom.AdSpace.CustomAction.CommonDialogAlertBox("Failed to add Bookmark");
    }
};

Viacom.AdSpace.CustomAction.AdSpaceItemAddButton = function (listID, itemID) {
    try {
        var bookMarkGroupValue = $("#ddlCustomActionGroup option:selected").val();
        switch (bookMarkGroupValue) {
            case "AddNewGroup": {
                var bookMarkNewGroup = $("#bookmarkNewGrp").val();
                if (!(bookMarkNewGroup === "" || bookMarkNewGroup == null || bookMarkNewGroup == undefined)) {
                    Viacom.AdSpace.CustomActionCheckGrpExists(_spPageContextInfo.userId, bookMarkNewGroup, Viacom.AdSpace.CustomAction.Constants.bookmarkLst, listID, itemID);
                }
                else {
                    //$("#dialog-add-doc").dialog("close");
                    //alert("Group name is either empty or null.");
                    console.log("Group name cannot be empty.");
                    $("#customActionValidationMsg").empty();
                    $("#customActionValidationMsg").append("<br/>Group name cannot be empty.");
                }
                break;
            }
            case "AddUniqueBookmark": {
                Viacom.AdSpace.CustomAction.AdSpaceItemAddGrp(listID, itemID);
                break;
            }
            default: {
                Viacom.AdSpace.CustomAction.AdSpaceItemAddGrp(listID, itemID);
            }
        }
    }
    catch (e) {
        $("#dialog-add-doc").dialog("close");
        //alert("Exception while adding new group in Bookmark list");
        console.log("Exception while adding new group in Bookmark list");
        $('.add-group-field').hide();
        $("#bookmarkNewGrp").val("");
        Viacom.AdSpace.CustomAction.CommonDialogAlertBox("Failed to add Bookmark");
    }
};

Viacom.AdSpace.CustomActionValidateUserBookmark = function (user, contentTypeName, grpToValidate, lstNm, noOfItems, listID, itemID) {
    try {
        var bookMarkGroupValue = $("#ddlCustomActionGroup option:selected").val();
        Viacom.AdSpace.CustomAction.Constants.existingGrpItems = 0;
        var grpValidateCtx = SP.ClientContext.get_current();
        var rootWeb = grpValidateCtx.get_site().get_rootWeb();
        var oList = rootWeb.get_lists().getByTitle(lstNm);
        var camlQuery = new SP.CamlQuery();
        var query = "<View><Query><Where><And><Eq><FieldRef Name='Author' LookupId='True'/><Value Type='Lookup'>" + user + "</Value></Eq><Eq><FieldRef Name='ContentType'/><Value Type='Computed'>" + contentTypeName + "</Value></Eq></And></Where><GroupBy Collapse='TRUE'><FieldRef Name='GroupName'/></GroupBy></Query></View>";
        camlQuery.set_viewXml(query);
        var collListItem = oList.getItems(camlQuery);
        grpValidateCtx.load(collListItem);
        grpValidateCtx.executeQueryAsync(
              function (sender, args) {
                  var itemsCount = collListItem.get_count();
                  if (Math.abs(itemsCount + noOfItems) > Viacom.AdSpace.CustomAction.Constants.itemsLimit.maxUserBookmarkLinks) {
                      //$("#dialog-add-doc").dialog("close");
                      //alert("Already " + itemsCount + " item exists as bookmark exist for current user. So user cannot add " + noOfItems + " items as bookmark");
                      console.log("Already " + itemsCount + " item as bookmark exist for current user. So user cannot " + noOfItems + " items as bookmark");
                      $("#customActionValidationMsg").empty();
                      //$("#customActionValidationMsg").append("<br/>Already " + itemsCount + " item exists as bookmark for current user. So user cannot add" + noOfItems + " items as bookmark");
                      $("#customActionValidationMsg").append("<br/>You've added the maximum number of Bookmarks. Please delete older links and try adding the Bookmark again");
                  }
                  else {
                      var listItemEnumerator = collListItem.getEnumerator();
                      try {
                          if (grpToValidate != null) {
                              while (listItemEnumerator.moveNext()) {
                                  var oListItem = listItemEnumerator.get_current();
                                  var grpNm = oListItem.get_item('GroupName');
                                  if (grpNm != null) {
                                      var grpLkpValue = grpNm.get_lookupValue();
                                      if (grpLkpValue != null) {
                                          if (grpLkpValue.toLowerCase() === grpToValidate.toLowerCase()) {
                                              Viacom.AdSpace.CustomAction.Constants.existingGrpItems++;
                                          }
                                      }
                                  }
                              }
                          }
                          if ((Math.abs(Viacom.AdSpace.CustomAction.Constants.existingGrpItems + noOfItems) > Viacom.AdSpace.CustomAction.Constants.itemsLimit.maxBookmarkLinkInGroup) && bookMarkGroupValue != 'AddUniqueBookmark') {
                              //$("#dialog-add-doc").dialog("close");
                              //alert("'" + grpToValidate + "' already contain " + Viacom.AdSpace.CustomAction.Constants.existingGrpItems + " bookmark. So user cannot add more than " + Math.abs(Viacom.AdSpace.CustomAction.Constants.itemsLimit.maxBookmarkLinkInGroup - Viacom.AdSpace.CustomAction.Constants.existingGrpItems) + " item as bookmark");
                              console.log("'" + grpToValidate + "' already contain " + Viacom.AdSpace.CustomAction.Constants.existingGrpItems + " bookmark. So user cannot add more than " + Math.abs(Viacom.AdSpace.CustomAction.Constants.itemsLimit.maxBookmarkLinkInGroup - Viacom.AdSpace.CustomAction.Constants.existingGrpItems) + " item as bookmark");
                              $("#customActionValidationMsg").empty();
                              //$("#customActionValidationMsg").append("<br/>'" + grpToValidate + "' already contain " + Viacom.AdSpace.CustomAction.Constants.existingGrpItems + " bookmark. So user cannot add more than " + Math.abs(Viacom.AdSpace.CustomAction.Constants.itemsLimit.maxBookmarkLinkInGroup - Viacom.AdSpace.CustomAction.Constants.existingGrpItems) + " item as bookmark");
                              $("#customActionValidationMsg").append("<br/>'The selected Group is at maximum capacity. Please create a new Group, or add these links to another Group");
                          }
                          else {
                              Viacom.AdSpace.CustomAction.AdSpaceItemCreateGrp(listID, itemID);
                          }
                      }
                      catch (e) {
                          $("#dialog-add-doc").dialog("close");
                          //alert("Error: Exception while iterating on items fetched from Bookmark list.");
                          console.log("Error: Exception while iterating on items fetched from Bookmark list.");
                          $('.add-group-field').hide();
                          $("#bookmarkNewGrp").val("");
                          Viacom.AdSpace.CustomAction.CommonDialogAlertBox("Failed to add Bookmark");
                      }
                  }
              }, function (sender, args) {
                  $("#dialog-add-doc").dialog("close");
                  //alert("Error while fetching data from Bookmark list for validation");
                  console.log("Error while fetching data from Bookmark list for validation");
                  $('.add-group-field').hide();
                  $("#bookmarkNewGrp").val("");
                  console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                  Viacom.AdSpace.CustomAction.CommonDialogAlertBox("Failed to add Bookmark");
              });
    }
    catch (e) {
        $("#dialog-add-doc").dialog("close");
        //alert("Error: Exception while validating no of item for a particular user");
        console.log("Error: Exception while validating no of item for a particular user");
        $('.add-group-field').hide();
        $("#bookmarkNewGrp").val("");
        Viacom.AdSpace.CustomAction.CommonDialogAlertBox("Failed to add Bookmark");
    }
};

Viacom.AdSpace.CustomActionCheckGrpExists = function (user, grpName, lstNm, listID, itemID) {
    try {
        var grpValidateCtx = SP.ClientContext.get_current();
        var rootWeb = grpValidateCtx.get_site().get_rootWeb();
        var oList = rootWeb.get_lists().getByTitle(lstNm);
        var camlQuery = new SP.CamlQuery();
        var query = "<View><Query><Where><And><Eq><FieldRef Name='UserGroup' /><Value Type='Text'>" + grpName + "</Value></Eq><Eq><FieldRef Name='Author' LookupId='True'/><Value Type='Lookup'>" + user + "</Value></Eq></And></Where></Query></View>";
        camlQuery.set_viewXml(query);
        var collListItem = oList.getItems(camlQuery);
        grpValidateCtx.load(collListItem);
        grpValidateCtx.executeQueryAsync(
              function (sender, args) {
                  if (collListItem.get_count() == 0) {
                      Viacom.AdSpace.CustomAction.AdSpaceItemAddGrp(listID, itemID);
                  }
                  else {
                      //alert("A group with this name '" + grpName + "' already exists ");
                      console.log("A group with this name '" + grpName + "' already exists ");
                      $("#customActionValidationMsg").empty();
                      $("#customActionValidationMsg").append("<br/>Group with this name '" + grpName + "' already exists.");
                      //location.reload();
                  }
              }, function (sender, args) {
                  $("#dialog-add-doc").dialog("close");
                  //alert("Error while fetching data from Bookmark list for validation");
                  console.log("Error while fetching data from Bookmark list for validation");
                  $('.add-group-field').hide();
                  $("#bookmarkNewGrp").val("");
                  console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                  Viacom.AdSpace.CustomAction.CommonDialogAlertBox("Failed to add Bookmark");
              });
    }
    catch (e) {
        $("#dialog-add-doc").dialog("close");
        $('.add-group-field').hide();
        $("#bookmarkNewGrp").val("");
        //alert("Error: Exception while fetching groups from Bookmark List for validation");
        console.log("Error: Exception while fetching groups from Bookmark List for validation");
        Viacom.AdSpace.CustomAction.CommonDialogAlertBox("Failed to add Bookmark");
    }
};

Viacom.AdSpace.CustomAction.CreateHtmlAdSpaceItem = function (listID, itemID) {
    try {
        var tblBody = $('#tblBodyBookmark');
        var AdSpaceItemCtx = SP.ClientContext.get_current();
        var list = AdSpaceItemCtx.get_web().get_lists().getById(listID);
        AdSpaceItemCtx.load(list);
        var itemCollIds = itemID.split(",");
        for (var i = 0; i < itemCollIds.length; i++) {
            Viacom.AdSpace.CustomAction.Constants.itemArr[i] = list.getItemById(itemCollIds[i]);
            AdSpaceItemCtx.load(Viacom.AdSpace.CustomAction.Constants.itemArr[i]);
        }
        AdSpaceItemCtx.executeQueryAsync(function () {
            tblBody.empty();
            Viacom.AdSpace.CustomAction.Constants.selectedItems.length = 0;
            for (var i = 0; i < Viacom.AdSpace.CustomAction.Constants.itemArr.length; i++) {
                Viacom.AdSpace.CustomAction.Constants.selectedItems[Viacom.AdSpace.CustomAction.Constants.selectedItems.length] = {
                    "Title": Viacom.AdSpace.CustomAction.Constants.itemArr[i].get_item('Title'),
                    "Name": Viacom.AdSpace.CustomAction.Constants.itemArr[i].get_item('FileLeafRef'),
                    "Url": window.location.protocol + "//" + window.location.host + Viacom.AdSpace.CustomAction.Constants.itemArr[i].get_item('FileRef')
                };
                if (Viacom.AdSpace.CustomAction.Constants.itemArr[i].get_item('Title') !== null && Viacom.AdSpace.CustomAction.Constants.itemArr[i].get_item('Title') !== undefined && Viacom.AdSpace.CustomAction.Constants.itemArr[i].get_item('Title') != '') {
                    tblBody.append("<tr><td class='user-name'>" + Viacom.AdSpace.CustomAction.Constants.itemArr[i].get_item('Title') + "</td></tr>");
                }
                else if (Viacom.AdSpace.CustomAction.Constants.itemArr[i].get_item('FileLeafRef') !== null && Viacom.AdSpace.CustomAction.Constants.itemArr[i].get_item('FileLeafRef') !== undefined && Viacom.AdSpace.CustomAction.Constants.itemArr[i].get_item('FileLeafRef') != '') {
                    tblBody.append("<tr><td class='user-name'>" + Viacom.AdSpace.CustomAction.Constants.itemArr[i].get_item('FileLeafRef') + "</td></tr>");
                }
                else {
                    tblBody.append("<tr><td class='user-name'></td></tr>");
                }
            }
            $('.add-group-field').hide();
            $("#bookmarkNewGrp").val("");
            $("#customActionValidationMsg").empty();
            $("#dialog-add-doc").dialog({
                width: 450,
                autoOpen: true,
                modal: true,
                draggable: false,
                buttons: {
                    Add: function () {
                        Viacom.AdSpace.CustomAction.AdSpaceItemAddButton(listID, itemID)
                    },
                    Cancel: function () {
                        $(this).dialog("close");
                        $('.add-group-field').hide();
                        $("#bookmarkNewGrp").val("");
                    }
                }
            });
        },
        function (sender, args) {
            $("#dialog-add-doc").dialog("close");
            $('.add-group-field').hide();
            $("#bookmarkNewGrp").val("");
            //alert('Request failed. \nError: ' + args.get_message() + '\nStackTrace: ' + args.get_stackTrace());
            console.log('Request failed. \nError: ' + args.get_message() + '\nStackTrace: ' + args.get_stackTrace());
            Viacom.AdSpace.CustomAction.CommonDialogAlertBox("Error while opening dialog");
        });
    }
    catch (e) {
        $("#dialog-add-doc").dialog("close");
        $('.add-group-field').hide();
        $("#bookmarkNewGrp").val("");
        //alert("Error: Exception while creating html for items in popup");
        console.log("Error: Exception while creating html for items in popup");
        Viacom.AdSpace.CustomAction.CommonDialogAlertBox("Error while opening dialog");
    }
};

Viacom.AdSpace.CustomAction.ddlPopulateGrp = function (dropdown, listID, itemID) {
    try {
        $(dropdown).length = 0;
        $.ajax({
            type: "GET",
            url: _spPageContextInfo.siteAbsoluteUrl + "/_api/web/lists/getbytitle('" + Viacom.AdSpace.CustomAction.Constants.bookmarkLst + "')/items?" +
                "$select ContentTypeId,Author/ID,UserGroup&$expand=ContentType&$filter=((Author/ID eq " + _spPageContextInfo.userId + ") and (ContentType eq 'BookmarkGroups'))",
            headers: {
                accept: "application/json;odata=verbose"
            },
            async: false,
            success: function (e) {
                if (e.d.results.length > 0) {
                    $(dropdown).html("<option value='AddUniqueBookmark'>Add unique bookmark (no group)</option>"
                    + "<option value='AddNewGroup'>Add New Group</option>");
                    for (i = 0; i < e.d.results.length; i++) {
                        $(dropdown).append($('<option>', {
                            value: e.d.results[i].ID,
                            text: e.d.results[i].UserGroup
                        }));
                    }
                    Viacom.AdSpace.CustomAction.CreateHtmlAdSpaceItem(listID, itemID);
                }
                else {
                    $(dropdown).append("<option value='AddUniqueBookmark'>Add unique bookmark (no group)</option><option value='AddNewGroup'>Add New Group</option>");
                    Viacom.AdSpace.CustomAction.CreateHtmlAdSpaceItem(listID, itemID);
                }

            },
            error: function (e) {
                //alert("Error while fetching groups from Bookmark List");
                console.log("Error while fetching groups from Bookmark List");
                Viacom.AdSpace.CustomAction.CommonDialogAlertBox("Error while opening dialog");
            }
        })
    }
    catch (ex) {
        //alert("Error: Exception while fetching groups from Bookmark List");
        console.log("Error: Exception while fetching groups from Bookmark List");
        Viacom.AdSpace.CustomAction.CommonDialogAlertBox("Error while opening dialog");
    }
};

Viacom.AdSpace.CustomAction.AdSpaceItem = function (listID, itemID) {
    $('#tblBodyBookmark').empty();
    Viacom.AdSpace.CustomAction.Constants.itemArr.length = 0;
    Viacom.AdSpace.CustomAction.Constants.selectedItems.length = 0;
    Viacom.AdSpace.CustomAction.Constants.listContentTypes = null;
    Viacom.AdSpace.CustomAction.Constants.noOfItems = 0;
    Viacom.AdSpace.CustomAction.Constants.addBookmarkToGrp = null;
    Viacom.AdSpace.CustomAction.Constants.addBookmarkToGrpId = null;
    Viacom.AdSpace.CustomAction.Constants.existingGrpItems = 0;
    Viacom.AdSpace.CustomAction.ddlPopulateGrp('#ddlCustomActionGroup', listID, itemID);
};

Viacom.AdSpace.CustomAction.AddUrl = function (listID, itemID) {
    try {
        ascommon.openAddBookmarkCustomAction();
    }
    catch (e) {
        // alert("Error: Exception while opening dialog.");
        console.log("Error: Exception while opening dialog.");
    }
};

Viacom.AdSpace.CustomAction.CommonDialogAlertBox = function (msg) {
    try {
        $("#commonDlgAlertBox").html(msg);
        $(".dialog-alert").dialog({
            width: 450,
            autoOpen: true,
            modal: true,
            draggable: false,
            buttons: {
                Ok: function () {
                    $(this).dialog("close");
                }
            }
        });
    }
    catch (e) {
        $(".dialog-alert").dialog("close");
        console.log("Error: Exception while opening dialog.");
    }
};
