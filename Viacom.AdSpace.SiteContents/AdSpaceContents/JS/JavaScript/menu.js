var asmenu = {
	equalMenuHeight : function(){
		$( ".submenu-level1" ).each(function() {
		
		$('.submenu-level1 > ul> li').mouseover(function(){
			var level1_height = $(this).parents('.submenu-level1').outerHeight();
			$(this).find('.submenu-level2').css('min-height',level1_height);		
		});
		
		var level1_height = $(this).outerHeight();
		$(this).find('.submenu-level2').css('min-height',level1_height);
		
		var container = $('.split-list'),
		listItem = 'li';
			
		container.each(function() {
        	var html = '<ul>';
			var sum = 0;
			var colCounter = 0;	
			var items = $(this).find(listItem);
			items.each(function() {
				
				sum += $(this).outerHeight();			
				
				if(sum > (level1_height - 24))
				{
					colCounter++;
					html+= '</ul><ul>';
					$(this).insertAfter('</ul><ul>');
					sum = $(this).outerHeight();
				}
				html+= '<li>' + $(this).html() + '</li>';
			});
			
			html+= '</ul>';
			$(this).parent().html(html).addClass('submenu-level2 submenu-col' + (colCounter + 1));
		});
	});
	$(".submenu-level1,.submenu-level2").hide();
	$(".submenu-level1").css('visibility','visible');	
	}
	
};


$( document ).ready(function() {
	asmenu.equalMenuHeight();	
});