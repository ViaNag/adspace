﻿ /*********USES REST API and CALLOUTS***************************/			 
var AllAnnouncements = {
/**********Declarations********************/
	structure: " ",
	_announcementTitle:'',
	_announcementDesc:'' ,
	_announcementID:'' ,
	_announcementType:'' ,
	_announcementSourceSite:'',
	_announcementStartdate:'' ,
	_announcementEnddate:'',	
	iconURL:'',
	iconAbsURL:'',
	myDate:'',
	months : new Array(12),
	months  : ['January','February','March','April','May','June','July','August','September','October','November','December']	,
	day :'',
	month:'',
	year:'',
	_formattedDate:'',
	todayDate: new Date(),
	_announcementSourceSite:'',
	
	 /****************Method to get Icon for announcement type****/
    getIcon: function (_announcementType) {
    	$.ajax({
    	    url: _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getbytitle('Announcement Icons')/items?$select=EncodedAbsUrl,IconAnnouncementType,ID&$filter=IconAnnouncementType eq '" + _announcementType + "'",
		        method: "GET",
		        async: false,
		        headers: { "Accept": "application/json; odata=verbose" },
		        success: function (data) 
		        {
					AllAnnouncements.iconAbsURL=data.d.results[0].EncodedAbsUrl;//EncodedAbsUrl to get img url
		        },
		        error: function (jqXHR, textStatus, errorThrown) 
		        {
					alert(jqXHR.responseText );
		        }
			  });
			        
		return AllAnnouncements.iconAbsURL;    	
    },
   
   /****************Method to create Callout on announcement title****/
   CreateCallOutPopup:function (targetElement, _announcementID, _announcementTitle, _announcementDescFull,_formattedDate,_announcementType){
   		var calloutOptions = new CalloutOptions();3
	    calloutOptions.ID = 'notificationcallout' + _announcementID;
	    calloutOptions.launchPoint = targetElement;
	    calloutOptions.beakOrientation = 'topBottom';//'leftRight';topBottom
	    calloutOptions.title = _announcementTitle;
	    calloutOptions.contentWidth=610;
	    calloutOptions.content = "<div class=\"ms-soften\" style=\"margin-top:13px;\"><hr/></div>"
	                           + "<div class=\"ms-soften\" style=\"margin-top:13px;\"><b>Date</b>: " + _formattedDate + "</div>"
	                           + "<div class=\"ms-soften\" style=\"margin-top:13px;\"><b>Type</b>: " + _announcementType + "</div>"
	                           + "<div class=\"callout-section\" style=\"margin-top:13px;\">" + _announcementDescFull+ "</div>";    
	                           
	    var displayedPopup = CalloutManager.createNewIfNecessary(calloutOptions);
	    displayedPopup.set({ openOptions: { event: "click" } });

	}, 
   	
	
}
/********************Get top 5 Announcements by start Date*************************************/
	$.ajax({ 
	    url: _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getbytitle('AdSpaceAnnouncements')/Items?$select=Body,SubSiteNameID,Title,AnnouncementType/IconAnnouncementType,AdSpaceStartDate,Expires,AnnouncementsCalcExpiry,ID&$orderby=AdSpaceStartDate desc&$expand=AnnouncementType/IconAnnouncementType",
		   type: "GET", 
		   async: false,
		   headers: {"accept": "application/json;odata=verbose"}, 
		   success: function (data) 
		   { 
		      if (data.d.results) 
		      	{ 
			       for(i=0;i<data.d.results.length;i++)//data.d.results.length
			       	{ 
			       		var _announcementDescFull="";
			       		
			        	AllAnnouncements._announcementStartdate =new Date(data.d.results[i].AdSpaceStartDate);
			        	AllAnnouncements._announcementEnddate =new Date(data.d.results[i].AnnouncementsCalcExpiry);						

			        	var calculateExpiry=AllAnnouncements._announcementEnddate.toString();
			        	if(data.d.results[i].SubSiteNameID!=null)
						{
						AllAnnouncements._announcementSourceSite =data.d.results[i].SubSiteNameID;
						AllAnnouncements._announcementSourceSite=AllAnnouncements._announcementSourceSite.split('-')[0];
						}
						else
						AllAnnouncements._announcementSourceSite="Global";
			        		
			        	if(AllAnnouncements._announcementStartdate<=AllAnnouncements.todayDate  && (AllAnnouncements.todayDate<=AllAnnouncements._announcementEnddate || (calculateExpiry.indexOf("1889") >= 0)))
			        	{
			        		AllAnnouncements._announcementTitle= data.d.results[i].Title;
			        					        		
			        		if(data.d.results[i].Body!=null)
								_announcementDescFull = data.d.results[i].Body;
							
							if(_announcementDescFull.indexOf("</div>") > 0)
								AllAnnouncements._announcementDesc=$(_announcementDescFull).text();
							else
								AllAnnouncements._announcementDesc=_announcementDescFull;
							
							AllAnnouncements._announcementDesc= AllAnnouncements._announcementDesc.substring(0,100)+"..";
							AllAnnouncements._announcementID = data.d.results[i].ID;
							AllAnnouncements._announcementType = data.d.results[i].AnnouncementType.IconAnnouncementType;
								
							AllAnnouncements.myDate = AllAnnouncements._announcementStartdate;
							AllAnnouncements.day = AllAnnouncements.myDate.getDate();				

							AllAnnouncements.month = AllAnnouncements.myDate.getMonth();							

							AllAnnouncements.year = AllAnnouncements.myDate.getFullYear();						

							AllAnnouncements._formattedDate=AllAnnouncements.months[AllAnnouncements.month] + " " + AllAnnouncements.day + ", " + AllAnnouncements.year;
								
							/***************call iconGet to get icon for announcement***/				
							AllAnnouncements.iconAbsURL=AllAnnouncements.getIcon(AllAnnouncements._announcementType);
							
							/***************HTML strusture for announcement widget***/					
			      			AllAnnouncements.structure= "<tr><td style='vertical-align: top;'>"
			      					  +"<img height=15px width=15px href='"+AllAnnouncements.iconAbsURL+"' src='"+AllAnnouncements.iconAbsURL+"'/></td>"
			      					  +"<td style='padding-bottom: 15px;'><p class='pTdAnnouncements' id=AnnTitleCall"+AllAnnouncements._announcementID+" style=''>"+AllAnnouncements._announcementTitle+"</p>"
			      					  +"<p class='pAnnouncementSourceSite' style=''>"+AllAnnouncements._announcementSourceSite+", "+AllAnnouncements._formattedDate+"</p>"
			      					  +"<p class='pAnnouncementDesc' style=''>"+AllAnnouncements._announcementDesc+"</p></td></tr>";
			      			$('#Announcetable').append(AllAnnouncements.structure);
			      			
			      			/***************call callout method*************************/
							oDivId = "AnnTitleCall"+AllAnnouncements._announcementID;// P element that holds announcement title
						    var targetElement = document.getElementById(oDivId);
						    AllAnnouncements.CreateCallOutPopup(targetElement,AllAnnouncements._announcementID, AllAnnouncements._announcementTitle, _announcementDescFull,AllAnnouncements._formattedDate,AllAnnouncements._announcementType);
						    
			        	} //if date check 
			      	}//for loop
		       } //if (data.d.results) 
		   }, 
		    error: function (jqXHR, textStatus, errorThrown) 
	        {
				alert(jqXHR.responseText );
	        }	 
	});