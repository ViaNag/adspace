﻿$(document).ready(function () {
    AllBookmarks.AdSpaceUserName = _spPageContextInfo.userId;
    AllBookmarks.loadBookMark("BookMarkSection");
    AllBookmarks.retrieveKeyValue();
 
});
var AllBookmarks = {
    AdSpaceUserName: "",
    structure: "",
    individualStructure: "",
    bookMarkTitle: "",
    bookMarkUrl: "",
    bookMarkGroupValue: "",
    bookMarkGroupName: "",
    bookMarkNewGroup: "",
    bookMarkList: 'Bookmark List',
    bookMarkGroupCTid: '',
    bookMarkGroupLookUpId: 0,
    controlBookMarkSection: 'BookMarkSection',
    controlBookMarkListing: 'BookmarkListing',
    deleteImage: '/_catalogs/masterpage/AdSpaceMasterPageAndLayouts/images/deleteCross.jpg',
    editImage: '/_catalogs/masterpage/AdSpaceMasterPageAndLayouts/images/EditPencil.png',
    validationMsgTitle: 'Please provide a Bookmark display name',
    validationMsgUrl: 'The URL field cannot be left blank',
    validationMsgGrpEmpty: 'Please provide a name for the Bookmark group',
    validationMsgGrpDuplicate: 'A group with this name already exists',
    validationMsgs: '',
    checkgrp: '',
    bookmarkcount: 0,
    targetclass: '',
    maxGroupCount: 0,
    successmsg: 'Bookmark added successfully',
    failuremsg: 'Failed to add Bookmark',
    maxBookmarkmsg: 'You’ve added the maximum number of Bookmarks.  Please delete older links and try adding the Bookmark again',
    maxGroupmsg: 'The selected Group is at maximum capacity.  Please create a new Group, or add these links to another Group',
    flag:true,
    loadBookMark: function (control) {

        AllBookmarks.structure = " <div class='add-bookmark-link'> <a href='#' class='add-bookmark'>+ Add Bookmark</a></div>";

        AllBookmarks.getAllBookmarks(control);
    },
    getAllBookmarks: function (control) {
        $.ajax({
            type: "GET",
            url: _spPageContextInfo.siteAbsoluteUrl + "/_api/web/lists/getbytitle('" + AllBookmarks.bookMarkList + "')/Items?" +
                "$select=Title,Url,ID,Author/ID,GroupName/UserGroup,GroupName/ID&$expand=Author/ID,GroupName/UserGroup,GroupName/ID&$filter=((Author/ID eq "
                + AllBookmarks.AdSpaceUserName + ") and (ContentType eq 'Bookmark List'))&$orderby=GroupName/UserGroup,Title",
            headers: {
                accept: "application/json;odata=verbose"
            },
            async: false,
            success: function (e) {

                AllBookmarks.loadUserBookmark(e, control);
                if (control == AllBookmarks.controlBookMarkSection) {
                    $('.submenu-level1.bookmark-container').append(AllBookmarks.structure);
                    $('.submenu-level1.bookmark-container > .bookmark-box').each(function (index) {
                        var element = $(this);
                        var linkcount = $(this).find('li').length;                        
                        $(element).attr("linkcount", linkcount);
                    });
                }
                else {
                    $('#manageBookmarkSection').append(AllBookmarks.structure);
                    $('#manageBookmarkSection > .bookmark-box').each(function (index) {
                        var element = $(this);
                        var linkcount = $(this).find('li').length;
                        $(element).attr("linkcount", linkcount);
                    });
                }
            },


            error: function (e) {
                $('#dialog-add').dialog("close");               
                ascommon.CommonDialogAlertBox(AllBookmarks.failuremsg);
                
            }
        })
    },
    loadUserBookmark: function (e, control) {

        var linkurl = '#';
        var title = '';
        var groupName = '';
        var render = false;
        var count = 0;
        AllBookmarks.individualStructure = "";
        if (e.d.results.length > 0) {
            AllBookmarks.bookmarkcount=e.d.results.length ;
            for (var i = 0; i < e.d.results.length; i++) {


                if (e.d.results[i].Url != undefined) {
                    linkurl = e.d.results[i].Url.Url;
                }
                if (e.d.results[i].Title != undefined) {
                    title = e.d.results[i].Title;
                }
                if (e.d.results[i].GroupName.UserGroup != '' && e.d.results[i].GroupName.UserGroup != undefined) {
                    if (groupName != e.d.results[i].GroupName.UserGroup) {                                                                    
                        groupName = e.d.results[i].GroupName.UserGroup;
                        count = count + 1;
                        render = true;
                       
                    }
                    else {
                        render = false;
                    }
                    if (render == true && count > 1) {
                        if (control == AllBookmarks.controlBookMarkSection) {
                            AllBookmarks.structure = AllBookmarks.structure + "</ul></div><div class='bookmark-box' id='" + AllBookmarks.controlBookMarkSection + e.d.results[i].GroupName.UserGroup + "' linkcount=0><div class='bookmark-head'><h4>" +
                           "<span class='toggle-arrow'></span> " +
                           e.d.results[i].GroupName.UserGroup + "</h4></div><ul>";
                        }
                        else {
                            AllBookmarks.structure = AllBookmarks.structure + "</ul></div><div class='bookmark-box' id='" + AllBookmarks.controlBookMarkListing + e.d.results[i].GroupName.UserGroup + "' linkcount=0><div class='bookmark-head'>" +
                                                            "<h4><span class='toggle-arrow'></span> " + e.d.results[i].GroupName.UserGroup + "</h4><a href='javascript:void(0)' class='group-del'" +
                                                            " title='Delete Group' onclick='Viacom.AdSpace.ManageBookMark.DeleteBookmarkGrp(\"" + e.d.results[i].GroupName.UserGroup + "\")'><img src='" + AllBookmarks.deleteImage + "' alt=''/></a><a href='javascript:void(0)'  class='group-edit'" +
                                                            " title='Edit Group' onclick='Viacom.AdSpace.ManageBookMark.EditBookmarkGrp(\"" + e.d.results[i].GroupName.UserGroup + "\")' ><img src='" + AllBookmarks.editImage + "' alt='' /></a></div><ul>";
                        }

                    }
                    else if (render == true && count == 1) {
                        if (control == AllBookmarks.controlBookMarkSection) {
                            AllBookmarks.structure = AllBookmarks.structure + "<div class='bookmark-box' id='" + AllBookmarks.controlBookMarkSection+e.d.results[i].GroupName.UserGroup + "' linkcount=0><div class='bookmark-head'><h4>" +
                             "<span class='toggle-arrow'></span> " +
                             e.d.results[i].GroupName.UserGroup + "</h4></div><ul>";
                        }
                        else {
                            AllBookmarks.structure = AllBookmarks.structure + "<div class='bookmark-box' id='" + AllBookmarks.controlBookMarkListing + e.d.results[i].GroupName.UserGroup + "' linkcount=0><div class='bookmark-head'>" +
                                "<h4><span class='toggle-arrow'></span> " + e.d.results[i].GroupName.UserGroup + "</h4><a href='javascript:void(0)' class='group-del'" +
                                " title='Delete Group' onclick='Viacom.AdSpace.ManageBookMark.DeleteBookmarkGrp(\"" + e.d.results[i].GroupName.UserGroup + "\")' ><img src='" + AllBookmarks.deleteImage + "' alt=''/></a><a href='javascript:void(0)' class='group-edit'" +
                                " title='Edit Group' onclick='Viacom.AdSpace.ManageBookMark.EditBookmarkGrp(\"" + e.d.results[i].GroupName.UserGroup + "\")' ><img src='" + AllBookmarks.editImage + "' alt='' /></a></div><ul>";
                        }
                    }

                    if (control == AllBookmarks.controlBookMarkSection) {
                        AllBookmarks.structure = AllBookmarks.structure + "<li><a href='" + linkurl + "' target='_blank'>"
                            + title + "</a></li>";
                    }
                    else {
                        AllBookmarks.structure = AllBookmarks.structure + "<li><a href='" + linkurl + "' target='_blank'>" + title + "</a>" +
                            "<a href='javascript:void(0)' class='bookmark-del' onclick='Viacom.AdSpace.ManageBookMark.DeleteBookmarkLink(\"" + e.d.results[i].ID + "\",\"" + title + "\",\"" + linkurl + "\",\"" + e.d.results[i].GroupName.UserGroup + "\")'" +
                            "title='Delete Bookmark' ><img src='" + AllBookmarks.deleteImage + "' alt=''/>" +
                            "</a><a href='javascript:void(0)'  onclick='Viacom.AdSpace.ManageBookMark.EditBookmarkLink(\"" + e.d.results[i].ID + "\",\"" + title + "\",\"" + linkurl + "\",\"" + e.d.results[i].GroupName.UserGroup + "\")' class='bookmark-edit' title='Edit Bookmark' ><img src='" + AllBookmarks.editImage + "' alt='' /></a></li>";

                    }             

                }
                else {
                    if (control == AllBookmarks.controlBookMarkSection) {

                        AllBookmarks.individualStructure = AllBookmarks.individualStructure + "<div class='bookmark-box individual-links'>" +
                      "<ul><li><a href='" + linkurl + "' target='_blank'>" + title + "</a></li></ul></div>";
                    }
                    else {
                        AllBookmarks.individualStructure = AllBookmarks.individualStructure + " <div class='bookmark-box individual-links'>" +
            "<ul><li><a href='" + linkurl + "' target='_blank'>" + title + " </a><a href='javascript:void(0)' onclick='Viacom.AdSpace.ManageBookMark.DeleteBookmarkLink(\"" + e.d.results[i].ID + "\",\"" + title + "\",\"" + linkurl + "\",\"\")' class='bookmark-del' title='Delete Bookmark' id='" + e.d.results[i].ID + "'>" +
            "<img src='" + AllBookmarks.deleteImage + "' alt=''/></a><a href='javascript:void(0)' onclick='Viacom.AdSpace.ManageBookMark.EditBookmarkLink(\"" + e.d.results[i].ID + "\",\"" + title + "\",\"" + linkurl + "\",\"\")' class='bookmark-edit' title='Edit Bookmark' id='" + e.d.results[i].ID + "'>" +
            "<img src='" + AllBookmarks.editImage + "' alt='' /></a></li></ul></div>";

                    }


                }
            }
        }

        if (AllBookmarks.structure != '') {
            AllBookmarks.structure = AllBookmarks.structure + "</ul></div>" + AllBookmarks.individualStructure;
        }
        else {
            AllBookmarks.structure = AllBookmarks.individualStructure;
        }
        if (control == AllBookmarks.controlBookMarkSection) {
            AllBookmarks.structure = AllBookmarks.structure + " <div class='manage-bookmark-link'> " +
               "<a href='" + _spPageContextInfo.siteAbsoluteUrl + "/Pages/ManageBookmarks.aspx' class='manage-bookmark'>Manage Bookmarks</a> </div></div>";
        }
        AllBookmarks.structure=AllBookmarks.structure+"<div id=BookMarkGroupCount count='"+AllBookmarks.bookmarkcount+"'/>";
    },
    AddBookMark: function (e) {
     
        var valid = AllBookmarks.checkValidation(e);
        if (valid) {
            AllBookmarks.bookMarkGroupValue = $("#BookMarkGroup option:selected").val();
            AllBookmarks.bookMarkTitle = $("#BookMarkTitle input").val();
            AllBookmarks.bookMarkUrl = $("#BookMarkUrl input").val();
            AllBookmarks.bookMarkGroupLookUpId = 0;          
            switch (AllBookmarks.bookMarkGroupValue) {
                case "0":                   
                    AllBookmarks.bookMarkGroupName = '';
                    var BookMark = { Title: AllBookmarks.bookMarkTitle, Url: AllBookmarks.bookMarkUrl };
                    AllBookmarks.CreateListItemWithDetails(AllBookmarks.bookMarkList, BookMark, "addBookMarkNoGroup", function (entity) {
                        console.log('New task ' + entity.d.ID + ' has been created');
                    },
                       function (error) {
                           console.log(JSON.stringify(error));
                       });
                    break;
                case "1":
                    AllBookmarks.bookMarkNewGroup = $("#BookMarkNewGroup input").val();
                    var BookMark = { Title: AllBookmarks.bookMarkTitle, Url: AllBookmarks.bookMarkUrl, group: AllBookmarks.bookMarkNewGroup };
                    AllBookmarks.CreateListItemWithDetails(AllBookmarks.bookMarkList, BookMark, "newgroup", function (entity) {
                        AllBookmarks.bookMarkGroupLookUpId = entity.d.ID;


                        console.log('New task ' + entity.d.ID + ' has been created');
                    },
                        function (error) {
                            console.log(JSON.stringify(error));
                            $('#dialog-add').dialog("close");
                            ascommon.CommonDialogAlertBox(AllBookmarks.failuremsg);
                        });
                    AllBookmarks.CreateListItemWithDetails(AllBookmarks.bookMarkList, BookMark, "addBookMark", function (entity) {
                        console.log('New task ' + entity.d.ID + ' has been created');
                    },
                        function (error) {
                            console.log(JSON.stringify(error));
                            $('#dialog-add').dialog("close");
                            ascommon.CommonDialogAlertBox(AllBookmarks.failuremsg);
                        });
                    break;
                default:
                    AllBookmarks.bookMarkGroupName = $("#BookMarkGroup option:selected").text();
                    AllBookmarks.bookMarkGroupLookUpId = $("#BookMarkGroup option:selected").val();
                    var element = AllBookmarks.targetclass;
                    if (element == "manage-bookmark-container")
                    {
                        var object = "#" + AllBookmarks.controlBookMarkListing + AllBookmarks.bookMarkGroupName;
                    }
                    else
                    {
                        var object = "#" + AllBookmarks.controlBookMarkSection + AllBookmarks.bookMarkGroupName;
                    }
                    
                    var bookmarkCount = $(object).attr('linkcount');
                    if (bookmarkCount < AllBookmarks.maxGroupCount) {
                        var BookMark = { Title: AllBookmarks.bookMarkTitle, Url: AllBookmarks.bookMarkUrl, group: AllBookmarks.bookMarkGroupValue };
                        AllBookmarks.CreateListItemWithDetails(AllBookmarks.bookMarkList, BookMark, "addBookMark", function (entity) {
                            console.log('New task ' + entity.d.ID + ' has been created');
                        },
                           function (error) {
                               console.log(JSON.stringify(error));
                               $('#dialog-add').dialog("close");
                               ascommon.CommonDialogAlertBox(AllBookmarks.failuremsg);
                           });
                    }
                    else {
                        ascommon.CommonDialogAlertBox(AllBookmarks.maxGroupmsg);
                        AllBookmarks.flag = false;
                    }
                    break;
            }
            //Loads the bookmark section again and register the functions
            AllBookmarks.loadBookMarkSection();
            $('#dialog-add').dialog("close");
            if (AllBookmarks.flag == true) {
                ascommon.CommonDialogAlertBox(AllBookmarks.successmsg);
            }
            AllBookmarks.flag = true;
        }

    },
    loadBookMarkSection: function () {
        $('.submenu-level1.bookmark-container').html('');
        $('#manageBookmarkSection').html('');
        AllBookmarks.loadBookMark("BookMarkSection");
        AllBookmarks.loadBookMark("BookmarkListing");
        ascommon.bookmarkAccordion();
        ascommon.bookmarkAddDialog();
      
    },
    checkValidation: function (e) {
        $('#validationMsg').html('');
        AllBookmarks.validationMsgs = '';
        AllBookmarks.checkgrp = '';
        AllBookmarks.bookMarkGroupValue = $("#BookMarkGroup option:selected").val();
        AllBookmarks.bookMarkNewGroup = $("#BookMarkNewGroup input").val();
        var isValid = true;
        if ($('#BookMarkTitle input').val() == '') {
            isValid = false;
            AllBookmarks.validationMsgs = AllBookmarks.validationMsgs + AllBookmarks.validationMsgTitle + "<br/>";

        }
        if ($('#BookMarkUrl input').val() == 'http://') {
            isValid = false;
            AllBookmarks.validationMsgs = AllBookmarks.validationMsgs + AllBookmarks.validationMsgUrl + "<br/>";

        }
        if (AllBookmarks.bookMarkGroupValue == 1) {
            if (AllBookmarks.bookMarkNewGroup == '') {

                AllBookmarks.validationMsgs = AllBookmarks.validationMsgs + AllBookmarks.validationMsgGrpEmpty + "<br/>";
                isValid = false;
            }
            else {
                 
                AllBookmarks.CheckGrpExists(AllBookmarks.bookMarkNewGroup);
                if (AllBookmarks.checkgrp == false) {
                    isValid = false;
                    AllBookmarks.validationMsgs = AllBookmarks.validationMsgs + AllBookmarks.validationMsgGrpDuplicate + "<br/>";
                }
            }
        }
        $('#validationMsg').html(AllBookmarks.validationMsgs);

        return isValid;

    },
    // CREATE Operation
    // listName: The name of the list you want to get items from
    // weburl: The url of the web that the list is in. 
    // newItemTitle: New Item title.
    // success: The function to execute if the call is sucesfull
    // failure: The function to execute if the call fails
    CreateListItemWithDetails: function (listName, newBookMark, flag, success, failure) {

        if (flag == "newgroup") {
            var item = {
                "__metadata": { "type": "SP.Data.Bookmark_x0020_ListListItem" },
                "UserGroup": newBookMark.group,
                "ContentTypeId": AllBookmarks.bookMarkGroupCTid
            };
        }
        if (flag == "addBookMark") {
            var item = {
                "__metadata": { "type": "SP.Data.Bookmark_x0020_ListListItem" },
                "Title": newBookMark.Title,
                "Url": {
                    __metadata: { "type": "SP.FieldUrlValue" },
                    Url: newBookMark.Url,
                    Description: newBookMark.Url
                },
                "GroupNameId": AllBookmarks.bookMarkGroupLookUpId

            };

        }
        if (flag == "addBookMarkNoGroup") {
            var item = {
                "__metadata": { "type": "SP.Data.Bookmark_x0020_ListListItem" },
                "Title": newBookMark.Title,
                "Url": {
                    __metadata: { "type": "SP.FieldUrlValue" },
                    Url: newBookMark.Url,
                    Description: newBookMark.Url
                },

            }
        }

        $.ajax({
            url: _spPageContextInfo.siteAbsoluteUrl + "/_api/web/lists/getbytitle('" + AllBookmarks.bookMarkList + "')/Items",
            type: "POST",
            contentType: "application/json;odata=verbose",
            data: JSON.stringify(item),
            async: false,
            headers: {
                "Accept": "application/json;odata=verbose",
                "X-RequestDigest": $("#__REQUESTDIGEST").val()
            },
            success: function (data) {
                success(data);
            },
            error: function (data) {
                AllBookmarks.flag = false;
                failure(data);
            }
        });
    },
    GetContentTypeId: function (dropdown) {
        $.ajax({
            type: "GET",
            url: _spPageContextInfo.siteAbsoluteUrl + "/_api/web/lists/getbytitle('" + AllBookmarks.bookMarkList + "')/items?" +
                "$select ContentTypeId,Author/ID,UserGroup&$expand=ContentType&$filter=((Author/ID eq " + AllBookmarks.AdSpaceUserName + ") and (ContentType eq 'BookmarkGroups'))",
            headers: {
                accept: "application/json;odata=verbose"
            },
            async: false,
            success: function (e) {
                if (e.d.results.length > 0) {
                    $(dropdown).html("<option value='0'>Add unique bookmark (no group)</option>"
                    + "<option value='1'>Add New Group</option>");
                    //AllBookmarks.bookMarkGroupCTid = e.d.results[0].ContentTypeId;
                    for (i = 0; i < e.d.results.length; i++) {
                        $(dropdown).append($('<option>', {
                            value: e.d.results[i].ID,
                            text: e.d.results[i].UserGroup
                        }));
                    }
                }
            },


            error: function (e) {
                $('#dialog-add').dialog("close");
                ascommon.CommonDialogAlertBox(AllBookmarks.failuremsg);
            }
        })

    },
    GetContentTypeIdbyName: function (contenttypename) {
        $.ajax({
            type: "GET",
            url: _spPageContextInfo.siteAbsoluteUrl + "/_api/web/lists/getbytitle('" + AllBookmarks.bookMarkList + "')/contenttypes",
            headers: {
                accept: "application/json;odata=verbose"
            },
            async: false,
            success: function (e) {
                if (e.d.results.length > 0) {
                    for (i = 0; i < e.d.results.length; i++) {
                        if (e.d.results[i].Name == contenttypename) {
                            AllBookmarks.bookMarkGroupCTid = e.d.results[i].StringId;
                        }
                    }

                }
            },


            error: function (e) {
                $('#dialog-add').dialog("close");
                ascommon.CommonDialogAlertBox(AllBookmarks.failuremsg);

            }
        })
    },
    CheckGrpExists: function (grpName) {
        try {
            $.ajax({
                url: _spPageContextInfo.siteAbsoluteUrl + "/_api/web/lists/getbytitle('" + AllBookmarks.bookMarkList + "')/Items?&$filter=((Author/ID eq " + AllBookmarks.AdSpaceUserName + ") and (ContentType eq 'BookmarkGroups') and (UserGroup eq '" + grpName + "'))",
                type: "GET",
                headers: {
                    accept: "application/json;odata=verbose"
                },
                async: false,               
                success: function (data) {                  
                    if (data.d.results.length > 0)
                    {
                      
                        AllBookmarks.checkgrp = false;
                    }
                    else
                    {
                        AllBookmarks.checkgrp = true;
                    }
                },
                error: function (data) {
                    
                }
            });

        }
        catch (e) {
            $('#dialog-add').dialog("close");
            ascommon.CommonDialogAlertBox(AllBookmarks.failuremsg);

        }
    },
    retrieveKeyValue: function () {

        $.ajax({
            url: _spPageContextInfo.siteAbsoluteUrl + "/_api/web/lists/getbytitle('ConfigurationList')/items?$select=Title,Key,Value1&$filter=(Title eq 'BookMarkMaxLinkForGroup') and (key eq 'MaxLinks')",
            method: "GET",
            async: false,
            headers: { "Accept": "application/json; odata=verbose" },
            success: function (data) {
                if (data.d.results.length > 0) {
                                           
                        var value = _spPageContextInfo.siteAbsoluteUrl + "/" + data.d.results[0].Value1;
                        AllBookmarks.maxGroupCount = value;
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {

            }
        });
    }
}