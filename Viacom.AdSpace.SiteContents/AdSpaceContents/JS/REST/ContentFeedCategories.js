﻿var ContentFeedCategories = {
    currentSite: null,
    currentSiteIndex: null,
    displayContentCategories: function () {
        $.ajax({
            url: _spPageContextInfo.siteAbsoluteUrl + "/_api/search/query?querytext='*'&refiners='RefinableString04'",
            type: "GET",
            async: false,
            headers: {
                accept: "application/json;odata=verbose"
            },
            success: function (e) {
                if (e.d.query.PrimaryQueryResult.RefinementResults.Refiners.results) {
                    for (var t = 0; t < e.d.query.PrimaryQueryResult.RefinementResults.Refiners.results.length; t++) {
                        var n = e.d.query.PrimaryQueryResult.RefinementResults.Refiners.results[t].Name;
                        if (n == "RefinableString04") {
                            $("div#ContentFeed").append("<div class='divContentCategory' style=''></div>");
                            if (e.d.query.PrimaryQueryResult.RefinementResults.Refiners.results[t].Entries.results) {
                                for (var r = 0; r < e.d.query.PrimaryQueryResult.RefinementResults.Refiners.results[t].Entries.results.length - 1; r++) {
                                    var i = e.d.query.PrimaryQueryResult.RefinementResults.Refiners.results[t].Entries.results[r].RefinementName;
                                    if (i) {
                                        if (ContentFeedCategories.currentSite == "adspace") var s = _spPageContextInfo.siteAbsoluteUrl + "/SearchCenter/Pages/ContentFeedResults.aspx#k=RefinableString04%3A" + i;
                                        if (ContentFeedCategories.currentSite == "musicandentertainment") var s = _spPageContextInfo.siteAbsoluteUrl + "/SearchCenter/Pages/ContentFeedResults.aspx#k=RefinableString04%3A" + i;
                                        if (ContentFeedCategories.currentSite == "international") var s = _spPageContextInfo.siteAbsoluteUrl + "/SearchCenter/Pages/ContentFeedResults.aspx#k=RefinableString04%3A" + i;
                                        if (ContentFeedCategories.currentSite == "nickelodeon") var s = _spPageContextInfo.siteAbsoluteUrl + "/SearchCenter/Pages/ContentFeedResults.aspx#k=RefinableString04%3A" + i;
                                        if (ContentFeedCategories.currentSite == "corporatereports") var s = _spPageContextInfo.siteAbsoluteUrl + "/SearchCenter/Pages/ContentFeedResults.aspx#k=RefinableString04%3A" + i;
                                        $(".divContentCategory").append("<div class='ContentCategoryRefinerDIV'><img style='margin-right: 3px;' src='" + _spPageContextInfo.siteAbsoluteUrl + "/SiteAssets/images/arrowTrans.jpg' complete='complete'><li class='ContentCategoryRefinerLI' style=''><a class='aContentCategoryRefinerLI' target='_blank' style='' href='" + s + "'>" + i + "</a></li></div>")
                                    }
                                }
                            }
                        }
                    }
                }
            },
            error: function (e) {
                alert(e.status + ": " + e.responseText)
            }
        })
    }
};
$(document).ready(function () {
    ContentFeedCategories.currentSite = _spPageContextInfo.webAbsoluteUrl;
    ContentFeedCategories.currentSiteIndex = ContentFeedCategories.currentSite.lastIndexOf("/");
    ContentFeedCategories.currentSite = ContentFeedCategories.currentSite.substring(currentSiteIndex + 1).toLowerCase();
    ContentFeedCategories.displayContentCategories()
})