var PerformOnLoad = {
	lexis: "disabled",
	getQuerystring: function(e) {
		e = e.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
		var t = new RegExp("[\\?&]" + e + "=([^&#]*)");
		var n = t.exec(window.location.href);
		if (n == null) return null;
		else return n[1]
	},
	HighlightCurrentSiteNav: function() {
		var e = _spPageContextInfo.siteAbsoluteUrl;
		var t = _spPageContextInfo.webAbsoluteUrl;
		currentSiteIndex = t.lastIndexOf("/");
		t = t.substring(currentSiteIndex + 1).toLowerCase();
		$(".aHomeLink").attr("href", e);
		if (t == "AdSpace") $(".aHomeLink").css("background-color", "#EA8525");
		if (t == "musicandentertainment") $("a[id*='music']").css("background-color", "#EA8525");
		if (t == "international") $("a[id*='international']").css("background-color", "#EA8525");
		if (t == "nickelodeon") $("a[id*='nickelodeon']").css("background-color", "#EA8525");
		if (t == "corporatereports") $("a[id*='corporatereports']").css("background-color", "#EA8525");
		$("ul.ulCategoryNav").each(function() {
			if ($(this).children().length <= 1) {
				$(this).remove()
			}
		})
	},
	iframeResize: function() {
		iFrameResize({
			autoResize: true,
			enablePublicMethods: true,
			checkOrigin: false,
			sizeWidth: false
		})
	}
};
$(document).ready(function() {
    $(".ms-cui-ctl-large").each(function(index,alg)
	{ 
	    if(index==3)
	    {
	       
	       $(this).hide();
	    } 
	})
	
	$(".ms-webpart-chrome.ms-webpart-chrome-vertical ").removeAttr("style");
	PerformOnLoad.HighlightCurrentSiteNav();
	$("#ctl00_PlaceHolderSearchArea_SmallSearchInputBox1_csr_sboxdiv").append('<a id="searchMe" style="display:none"><img id="searchImg"  src="' + _spPageContextInfo.siteAbsoluteUrl + '/SiteAssets/images/searchIcon.png" /></a>');
	//$("div#verticalSearch ul.ms-srchnav-list").append('<li class="ms-srchnav-item ms-verticalAlignTop"><h2 class="ms-displayInline"><a id="lexisNexis" href="javascript:{}" onclick="" class="ms-srchnav-link" title="Search Lexis Nexis">Lexis Nexis</a></h2></li>');
	
	if ($('.ms-core-listMenu-verticalBox:contains("Site Contents")')) {}
	iFrameResize({
		autoResize: true,
		enablePublicMethods: true,
		checkOrigin: false,
		sizeWidth: false
	})
	
	

})