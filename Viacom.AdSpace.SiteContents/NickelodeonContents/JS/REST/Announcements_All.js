﻿/**********Declarations********************/
	var structure;
	var _announcementTitle;
	var _announcementDesc ;
	var _announcementID ;
	var _announcementType ;
	var _announcementSourceSite;	
	var _announcementStartdate ;	
	var iconURL="";
	var iconAbsURL="";
	var myDate;
	var months = new Array(12);
	months  = ['January','February','March','April','May','June','July','August','September','October','November','December'];		
	var day ;
	var month;
	var year;
	var _formattedDate;
	var todayDate= new Date();
	
	/********************Get all AdSpaceAnnouncements by start Date*************************************/
	$.ajax({ 
	
	    url: _spPageContextInfo.siteAbsoluteUrl + "/_api/web/lists/getbytitle('AdSpaceAnnouncements')/Items?$select=Body,Title,AnnouncementType/IconAnnouncementType,AdSpaceStartDate,Expires,ID&$orderby=AdSpaceStartDate&$expand=AnnouncementType/IconAnnouncementType",
		    type: "GET", 
		   async: false,
		   headers: {"accept": "application/json;odata=verbose"}, 
		   success: function (data) 
		   { 
		      if (data.d.results) 
		      	{ 
			       for(i=0;i<data.d.results.length;i++)
			       	{ 
			       		
			        	_announcementStartdate =new Date(data.d.results[i].AdSpaceStartDate);
			        	if(_announcementStartdate<=todayDate)
			        	{
			        		_announcementTitle= data.d.results[i].Title;
							var _announcementDescFull = data.d.results[i].Body;
							_announcementDesc=$(_announcementDescFull).text();
							_announcementDesc= _announcementDesc.substring(0,250)+"..";
							_announcementID = data.d.results[i].ID;
							_announcementType = data.d.results[i].AnnouncementType.IconAnnouncementType;
							
							myDate = _announcementStartdate;
							day = myDate.getDate();
							month = myDate.getMonth();
							year = myDate.getFullYear();

							_formattedDate=months[month] + " " + day + ", " + year;
								
							/***************call iconGet to get icon for announcement***/				
							iconAbsURL=getIcon(_announcementType);
							
							/***************HTML strusture for announcement widget***/					
			      			structure= "<tr><td style='vertical-align: top;'><img href='"+iconAbsURL+"' src='"+iconAbsURL+"'/></td><td><p id=AnnTitleCall"+_announcementID+" style='cursor:pointer;font-weight:bold;color:#2CB6C0;margin:2px;font-family:Verdana, Arial, sans-serif;font-size:13px;'>"+_announcementTitle+"</p><p style='font-weight:bold;color:grey;margin:2px;font-family:Verdana, Arial, sans-serif;font-size:11px;'>"+_formattedDate+"</p><p style='font-size:11px!important;margin:2px!important;font-family:Verdana, Arial, sans-serif!important'>"+_announcementDesc+"</p></td></tr>";
			      			$('#Announcetable').append(structure);
			      			
			      			/***************call callout method*************************/
							oDivId = "AnnTitleCall"+_announcementID;// P element that holds announcement title
						    var targetElement = document.getElementById(oDivId);
						    CreateCallOutPopup(targetElement, _announcementID, _announcementTitle, _announcementDescFull,_formattedDate,_announcementType);
						    
			        	} //if date check 
			      	}//for loop
		       } //if (data.d.results) 
		   }, 
		    error: function (jqXHR, textStatus, errorThrown) 
	        {
				alert(jqXHR.responseText );
	        }	 
	}); 
	
	/****************Method to get Icon for announcement type****/
	function getIcon(_announcementType)
	{
		$.ajax({
		    url: _spPageContextInfo.siteAbsoluteUrl + "/_api/web/lists/getbytitle('Announcement Icons')/items?$select=EncodedAbsUrl,IconAnnouncementType,ID&$filter=IconAnnouncementType eq '" + _announcementType + "'",
		        method: "GET",
		        async: false,
		        headers: { "Accept": "application/json; odata=verbose" },
		        success: function (data) 
		        {
					iconAbsURL=data.d.results[0].EncodedAbsUrl;//EncodedAbsUrl to get img url
		        },
		        error: function (jqXHR, textStatus, errorThrown) 
		        {
					alert(jqXHR.responseText );
		        }
			  });
			        
		return iconAbsURL;
	}
	
	/****************Method to create Callout on announcement title****/
	function CreateCallOutPopup(targetElement, _announcementID, _announcementTitle, _announcementDescFull,_formattedDate,_announcementType) 
	{
	    var calloutOptions = new CalloutOptions();
	    calloutOptions.ID = 'notificationcallout' + _announcementID;
	    calloutOptions.launchPoint = targetElement;
	    calloutOptions.beakOrientation = 'topBottom';//'leftRight';topBottom
	    calloutOptions.title = _announcementTitle;
	    calloutOptions.contentWidth=610;
	    calloutOptions.content = "<div class=\"ms-soften\" style=\"margin-top:13px;\"><hr/></div>"
	                           + "<div class=\"ms-soften\" style=\"margin-top:13px;\"><b>Date</b>: " + _formattedDate + "</div>"
	                           + "<div class=\"ms-soften\" style=\"margin-top:13px;\"><b>Type</b>: " + _announcementType + "</div>"
	                           + "<div class=\"callout-section\" style=\"margin-top:13px;\">" + _announcementDescFull+ "</div>";    
	                           
	    var displayedPopup = CalloutManager.createNewIfNecessary(calloutOptions);
	    displayedPopup.set({ openOptions: { event: "click" } });
	}

