﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Viacom.AdSpace.SiteProvisioning.BLL
{
    /// <summary>
    /// Description			: Interface for Site Operation actions
    /// System				: Viacom - Ad Space
    /// 
    /// </summary>
    /// <ModificationLog>
    /// Ver	|Date			|Author			|Change Desc.
    /// ---------------------------------------------------------------------------------------------------
    /// 1	|06/17/2015		|Ateet 		|Initial Version
    /// </ModificationLog>
    public interface ISiteProvisioningAction
    {
       void ExecuteAction();
       void ExecuteAction(string rootSiteCollectionURL);
    }
}
