﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Viacom.AdSpace.SiteProvisioning.BLL;
using Microsoft.SharePoint.Administration;

namespace Viacom.AdSpace.SiteProvisioning.BLL
{
    /// <summary>
    /// Description			: class for starting site creation as a site opertion actions
    /// System				: Viacom - Ad Space
    /// 
    /// </summary>
    /// <ModificationLog>
    /// Ver	|Date			|Author			|Change Desc.
    /// ---------------------------------------------------------------------------------------------------
    /// 1	|06/17/2015		|Ateet 		|Initial Version
    /// </ModificationLog>
    public class CreateSiteAction : SiteAction, ISiteProvisioningAction
    {
        public CreateSiteAction(ISiteProvisioningRequest siteRequest)
            : base(siteRequest)
        {

        }
        public void ExecuteAction()
        {
            throw new NotImplementedException();
        }
        public void ExecuteAction(string clientSiteCollectionURL)
        {
            try
            {
                string successMsg = string.Empty;
                string templateName = string.Empty;
               
                    templateName = siteRequest.GetWebTemplate(clientSiteCollectionURL, siteRequest);
                if (!string.IsNullOrEmpty(templateName))
                {
                    successMsg = siteRequest.CreateSite(clientSiteCollectionURL, siteRequest, templateName);
                }
                if (successMsg.Equals(Constants.SUCESS_MSG))
                {
                    successMsg = siteRequest.InsertMasterData(clientSiteCollectionURL, siteRequest);
                }
                if (!string.IsNullOrEmpty(templateName) && successMsg.Equals(Constants.SUCESS_MSG))
                {
                    successMsg = siteRequest.ActivateFeature(siteRequest.SiteURL);
                }
                if (successMsg.Equals(Constants.SUCESS_MSG))
                {
                    siteRequest.UpdatedListItems(clientSiteCollectionURL, siteRequest, Constants.STATUS_COMPLETED, Constants.SUCESS_MSG);
                }
                else
                {
                    siteRequest.UpdatedListItems(clientSiteCollectionURL, siteRequest, Constants.STATUS_FAILED, successMsg);
                }
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Ad Space Site Provisioning-CreateSiteAction.cs-ExecuteMethod", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
            }
        }
    }
}
