﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Viacom.AdSpace.SiteProvisioning.BLL;
using Microsoft.SharePoint.Administration;

namespace Viacom.AdSpace.SiteProvisioning.BLL
{
    /// <summary>
    /// Description			: class for editing site as a site opertion actions
    /// System				: Viacom - Ad Space
    /// 
    /// </summary>
    /// <ModificationLog>
    /// Ver	|Date			|Author			|Change Desc.
    /// ---------------------------------------------------------------------------------------------------
    /// 1	|08/26/2015		|Ateet 		|Initial Version
    /// </ModificationLog>
    public class EditSiteAction : SiteAction, ISiteProvisioningAction
    {
        public EditSiteAction(ISiteProvisioningRequest siteRequest)
            : base(siteRequest)
        {

        }
        public void ExecuteAction()
        {
            throw new NotImplementedException();
        }
        public void ExecuteAction(string clientSiteCollectionURL)
        {
            try
            {
                string successMsg = string.Empty;
                successMsg = siteRequest.DeleteListItems(siteRequest.SiteURL, siteRequest);
                if (successMsg.Equals(Constants.SUCESS_MSG))
                {
                    successMsg = siteRequest.InsertMasterData(clientSiteCollectionURL, siteRequest);
                    if (successMsg.Equals(Constants.SUCESS_MSG))
                    {
                        successMsg = siteRequest.UpdateSiteName(siteRequest.SiteURL, siteRequest);
                        if (successMsg.Equals(Constants.SUCESS_MSG))
                        {
                            successMsg = siteRequest.ReactivateFeature(siteRequest.SiteURL, siteRequest);
                        }
                    }
                }
                
                if (successMsg.Equals(Constants.SUCESS_MSG))
                {
                    siteRequest.UpdatedListItems(clientSiteCollectionURL, siteRequest, Constants.STATUS_COMPLETED, Constants.SUCESS_MSG);
                }
                else
                {
                    successMsg = "Error:EditSiteAction " + successMsg;
                    siteRequest.UpdatedListItems(clientSiteCollectionURL, siteRequest, Constants.STATUS_FAILED, successMsg);
                }
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Ad Space Site Provisioning-EditSiteAction.cs-ExecuteMethod", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
            }
        }
    }
}
