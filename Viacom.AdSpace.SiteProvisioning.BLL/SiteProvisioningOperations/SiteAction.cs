﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Viacom.AdSpace.SiteProvisioning.BLL;

namespace Viacom.AdSpace.SiteProvisioning.BLL
{
    /// <summary>
    /// Description			: class for site opertion actions
    /// System				: Viacom - Ad Space
    /// 
    /// </summary>
    /// <ModificationLog>
    /// Ver	|Date			|Author			|Change Desc.
    /// ---------------------------------------------------------------------------------------------------
    /// 1	|06/17/2015		|Ateet 		|Initial Version
    /// </ModificationLog>
    public class SiteAction
    {
        public ISiteProvisioningRequest siteRequest;
        public SiteAction(ISiteProvisioningRequest siteRequest)
        {
            this.siteRequest = siteRequest;
        }
      
    }
}
