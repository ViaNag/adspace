﻿using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using Microsoft.SharePoint.Taxonomy;
using System.Reflection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSOM = Microsoft.SharePoint.Client;
using CTAX = Microsoft.SharePoint.Client.Taxonomy;
using System.Net;

namespace Viacom.AdSpace.SiteProvisioning.BLL
{
    /// <summary>
    /// Description			: class for defining database operation
    /// System				: Viacom - Ad Space
    /// 
    /// </summary>
    /// <ModificationLog>
    /// Ver	|Date			|Author			|Change Desc.
    /// ---------------------------------------------------------------------------------------------------
    /// 1	|06/17/2015		|Ateet 		|Initial Version
    /// </ModificationLog>
    public class Util
    {
        string Message = string.Empty;

        #region Create Action


        /// <summary>
        /// Function to create items in list
        /// </summary>
        /// <param name="siteURL">Url of site where lists are present</param>
        /// <param name="listTitle">List name where item will be created</param>
        /// <param name="fieldInfo">Collection of field information</param>
        /// <returns>String message which specify whether opeartion excuted successfully or not</returns>
        internal string InsertMasterData(string siteURL, string listTitle, List<object[]> fieldInfo)
        {
            try
            {
                using (SPSite oSPsite = new SPSite(siteURL))
                {
                    using (SPWeb oSPWeb = oSPsite.OpenWeb())
                    {
                        bool allowUnsafeupdates = oSPWeb.AllowUnsafeUpdates;
                        oSPWeb.AllowUnsafeUpdates = true;
                        /* get the SPList object by list name*/
                        SPList lst = oSPWeb.Lists.TryGetList(listTitle);
                        SPListItem oSPListItem = lst.Items.Add();
                        for (int i = 0; i < fieldInfo.Count; i++)
                        {
                            if (Convert.ToString(fieldInfo[i][1]).Equals(Constants.CREATE_ITEM_URL))
                            {
                                SPFieldUrlValue hyper = new SPFieldUrlValue();
                                hyper.Description = Convert.ToString(fieldInfo[i][2]).Split(new char[] { ',' }).Last();
                                hyper.Url = Convert.ToString(fieldInfo[i][2]).Split(new char[] { ',' }).First();
                                oSPListItem[Convert.ToString(fieldInfo[i][0])] = hyper;
                            }
                            if (Convert.ToString(fieldInfo[i][1]).Equals(Constants.CREATE_ITEM_MULTIUSER))
                            {
                                SPFieldUserValueCollection usercollection = new SPFieldUserValueCollection();
                                usercollection = fieldInfo[i][2] as SPFieldUserValueCollection;
                                oSPListItem[Convert.ToString(fieldInfo[i][0])] = usercollection;
                            }
                            if (Convert.ToString(fieldInfo[i][1]).Equals(Constants.CREATE_ITEM_USER))
                            {
                                SPFieldUserValue userValue = fieldInfo[i][2] as SPFieldUserValue;
                                oSPListItem[Convert.ToString((fieldInfo[i][0]))] = userValue;
                            }
                            if (Convert.ToString(fieldInfo[i][1]).Equals(Constants.CREATE_ITEM_TAX) && (!string.IsNullOrEmpty(Convert.ToString(fieldInfo[i][2]))))
                            {
                                TaxonomyField oField = (TaxonomyField)lst.Fields.GetFieldByInternalName(Convert.ToString(fieldInfo[i][0]));
                                string termGUID = Convert.ToString(fieldInfo[i][2]).Split(new char[] { '|' }).Last();
                                Term oTerm = GetTermValue(oSPsite, termGUID);
                                if (oTerm != null)
                                {
                                    TaxonomyFieldValue taxonomyFieldValue = new TaxonomyFieldValue(oField);
                                    taxonomyFieldValue.TermGuid = oTerm.Id.ToString();
                                    taxonomyFieldValue.Label = oTerm.Name;
                                    oSPListItem[Convert.ToString(fieldInfo[i][0])] = taxonomyFieldValue;
                                }
                                else
                                {
                                    StringBuilder sbExitMsg = new StringBuilder();
                                    return Convert.ToString(sbExitMsg.AppendFormat("{0}{1}{2}{3}", Constants.FAIL_MSG, "CreateListItem", Constants.FAIL_EX_MSG, Constants.FAIL_NULL_EX_MSG));
                                }
                            }
                            if (Convert.ToString(fieldInfo[i][1]).Equals(Constants.CREATE_ITEM_SLT))
                            {
                                oSPListItem[Convert.ToString(fieldInfo[i][0])] = Convert.ToString(fieldInfo[i][2]);
                            }
                        }
                        /* finally update list */
                        oSPListItem.Update();
                        lst.Update();
                        oSPWeb.AllowUnsafeUpdates = allowUnsafeupdates;
                    }
                }
                return Constants.SUCESS_MSG;
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Ad Space Site Provisioning-Util.cs-CreateListItem", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                StringBuilder sbFailMsg = new StringBuilder();
                return Convert.ToString(sbFailMsg.AppendFormat("{0}{1}{2}{3}", Constants.FAIL_MSG, "CreateListItem", Constants.FAIL_EX_MSG, ex.Message));
            }
        }


        /// <summary>
        /// Function to get veirfy is site exists or not
        /// </summary>
        /// <param name="rootSiteCollectionURL">Site collection url</param>
        /// <param name="request">Interface object</param>
        /// <returns>string</returns>
        public string CheckSiteExists(string clientSiteCollectionURL, string url)
        {
            try
            {
                using (SPSite site = new SPSite(clientSiteCollectionURL))
                {
                    using (SPWeb web = site.OpenWeb(url, true))
                    {
                        return Constants.SUCESS_MSG;
                    }
                }
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Ad Space Site Provisioning-Util.cs-CheckSiteExists", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                StringBuilder sbFailMsg = new StringBuilder();
                return Convert.ToString(sbFailMsg.AppendFormat("{0}{1}{2}{3}", Constants.FAIL_MSG, "CheckSiteExists", Constants.FAIL_EX_MSG, ex.Message));
            }
        }

        /// <summary>
        /// Funtion to get web template name (name include web template guid) from site collection
        /// </summary>
        /// <param name="rootSiteCollectionURL"> Site collection url</param>
        /// <param name="siteType">Type of site for which web template need to fetched</param>
        /// <returns> Web Template name</returns>
        internal string GetWebTemplate(string clientSiteCollectionURL, string siteType)
        {
            try
            {
                using (SPSite site = new SPSite(clientSiteCollectionURL))
                {
                    string templateName = null;
                    string templateTitle = null;
                    SPWebTemplateCollection coll = site.GetWebTemplates(1033);
                    switch (siteType)
                    {
                        case Constants.CLIENT_SITE_TYPE:
                            {
                                templateTitle = Constants.CLIENT_WEB_TEMPLATE;
                                break;
                            }
                    }
                    foreach (SPWebTemplate template in coll)
                    {

                        if (template.Title.Equals(templateTitle, StringComparison.CurrentCultureIgnoreCase))
                        {
                            templateName = template.Name;
                            break;
                        }
                    }
                    return templateName;
                }
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Ad Space Site Provisioning-Util.cs-GetWebTemplate", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                StringBuilder sbFailMsg = new StringBuilder();
                return Convert.ToString(sbFailMsg.AppendFormat("{0}{1}{2}{3}", Constants.FAIL_MSG, "GetWebTemplate", Constants.FAIL_EX_MSG, ex.Message));
            }
        }

        /// <summary>
        /// Function to create site
        /// </summary>
        /// <param name="clientSiteCollectionURL">Site collection url</param>
        /// <param name="newWebObject">Dictionary object conating all keys required for new web creation</param>
        /// <param name="LCID">value: 1033</param>
        /// <param name="breakPermission">true if break new site permission else false</param>
        /// <returns>String message whether operation performed successfully or not</returns>
        internal string CreateSite(string clientSiteCollectionURL, Dictionary<string, string> newWebObject, uint LCID, bool breakPermission)
        {
            try
            {
                using (SPSite site = new SPSite(clientSiteCollectionURL))
                {
                    using (SPWeb parentWeb = site.OpenWeb())
                    {
                        bool allowUnsafeupdates = parentWeb.AllowUnsafeUpdates;
                        parentWeb.AllowUnsafeUpdates = true;
                        SPWeb newSite = parentWeb.Webs.Add(newWebObject[Constants.DICT_NEWWEB_KEY], newWebObject[Constants.DICT_NEWWEB_TITLE_KEY], newWebObject[Constants.DICT_NEWWEB_DESC_KEY], LCID, newWebObject[Constants.DICT_TEMPLATENAME_KEY], breakPermission, false);
                        newSite.Update();
                        parentWeb.AllowUnsafeUpdates = allowUnsafeupdates;
                    }
                }
                return Constants.SUCESS_MSG;
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Ad Space Site Provisioning-Util.cs-CreateSite", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                StringBuilder sbFailMsg = new StringBuilder();
                return Convert.ToString(sbFailMsg.AppendFormat("{0}{1}{2}{3}", Constants.FAIL_MSG, "CreateSite", Constants.FAIL_EX_MSG, ex.Message));
            }
        }

        /// <summary>
        /// Function to update list item
        /// </summary>
        /// <param name="siteURL">site collection url</param>
        /// <param name="listName">List name which need to update</param>
        /// <param name="query">Query to update list item</param>
        /// <param name="fieldToUpdate">Field which need to udpate[Only text type or choice field]</param>
        /// <param name="fieldValues">Value array with which field need to update</param>
        /// <returns>status massage</returns>
        internal string UpdateListItem(string siteURL, string listName, string query, string[] fieldToUpdate, string[] fieldValues, string fieldToCompare, string compareValue)
        {
            string URL = string.Empty;
            try
            {
                using (SPSite oSite = new SPSite(siteURL))
                {
                    using (SPWeb oWeb = oSite.OpenWeb())
                    {
                        SPList list = oWeb.Lists.TryGetList(listName);
                        if (list != null)
                        {
                            SPQuery oQuery = new SPQuery();
                            oQuery.Query = query;
                            SPListItemCollection itemcoll = list.GetItems(oQuery);
                            foreach (SPListItem item in itemcoll)
                            {
                                TaxonomyFieldValue value = item[fieldToCompare] as TaxonomyFieldValue;
                                string valueToCompare = value.Label;
                                if (valueToCompare.ToLower().Equals(compareValue.ToLower()))
                                {
                                    for (int i = 0; i < fieldToUpdate.Length; i++)
                                    {
                                        item[fieldToUpdate[i]] = Convert.ToString(fieldValues[i]);
                                    }
                                    item.Update();
                                }
                            }
                            list.Update();
                        }
                    }
                }
                return Constants.SUCESS_MSG;
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Ad Space Site Provisioning-Util.cs-UpdateListItem", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                StringBuilder sbFailMsg = new StringBuilder();
                return Convert.ToString(sbFailMsg.AppendFormat("{0}{1}{2}{3}", Constants.FAIL_MSG, "UpdateListItem", Constants.FAIL_EX_MSG, ex.Message));
            }

        }

        /// <summary>
        /// Fucntion to activate feature on web
        /// </summary>
        /// <param name="webURL">Url of web where feature will be activated</param>
        /// <param name="featureID">Collection of feature GUIDs</param>
        /// <returns>string message whether operation executed succesfully or not</returns>
        internal string ActivateFeature(string webURL, string featureID)
        {
            string featureGUID = string.Empty;
            try
            {
                using (SPSite oSite = new SPSite(webURL))
                {
                    using (SPWeb oWeb = oSite.OpenWeb())
                    {
                        if (oWeb != null)
                        {
                            bool allowUnsafeupdates = oWeb.AllowUnsafeUpdates;
                            oWeb.AllowUnsafeUpdates = true;
                            featureGUID = featureID;
                            Guid featureGuid = new Guid(featureID);
                            SPFeature feature = oWeb.Features.SingleOrDefault(f => f.DefinitionId == new Guid(featureID));
                            if (feature != null)//if feature is activated
                            {
                                oWeb.Features.Remove(feature.DefinitionId);//deactivate feature
                                feature = oWeb.Features.SingleOrDefault(f => f.DefinitionId == new Guid(featureID));
                            }
                            if (feature == null)//if feature is not activated
                            {
                                oWeb.Features.Add(featureGuid);//activate feature
                            }
                            oWeb.Update();
                            oWeb.AllowUnsafeUpdates = allowUnsafeupdates;

                        }
                        return Constants.SUCESS_MSG;
                    }
                }
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Ad Space Site Provisioning-Util.cs-ActivateFeature", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                StringBuilder sbFailMsg = new StringBuilder();
                return Convert.ToString(sbFailMsg.AppendFormat("{0}{1}{2}{3}{4}", Constants.FAIL_MSG, "ActivateFeature failed for ID - ", featureGUID, Constants.FAIL_EX_MSG, ex.Message));
            }
        }

        /// <summary>
        /// Funcation to deactivate feature on web
        /// </summary>
        /// <param name="webURL">Url of web where feature will be deactivated</param>
        /// <param name="featureID">Collection of feature GUIDs</param>
        /// <returns>string message whether operation executed succesfully or not</returns>
        internal string DeActivateFeature(string webURL, string featureID)
        {
            string featureGUID = string.Empty;
            try
            {
                using (SPSite oSite = new SPSite(webURL))
                {
                    using (SPWeb oWeb = oSite.OpenWeb())
                    {
                        if (oWeb != null)
                        {
                            bool allowUnsafeupdates = oWeb.AllowUnsafeUpdates;
                            oWeb.AllowUnsafeUpdates = true;
                            featureGUID = featureID;
                            Guid featureGuid = new Guid(featureID);
                            SPFeature feature = oWeb.Features.SingleOrDefault(f => f.DefinitionId == new Guid(featureID));
                            if (feature != null)//if feature is activated
                            {
                                oWeb.Features.Remove(feature.DefinitionId);//deactivate feature
                            }
                            oWeb.Update();
                            oWeb.AllowUnsafeUpdates = allowUnsafeupdates;

                        }
                        return Constants.SUCESS_MSG;
                    }
                }
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Ad Space Site Provisioning-Util.cs-ActivateFeature", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                StringBuilder sbFailMsg = new StringBuilder();
                return Convert.ToString(sbFailMsg.AppendFormat("{0}{1}{2}{3}{4}", Constants.FAIL_MSG, "ActivateFeature failed for ID - ", featureGUID, Constants.FAIL_EX_MSG, ex.Message));
            }
        }


        /// <summary>
        /// Funtion to create item in team site for client site
        /// </summary>
        /// <param name="adspaceReg">Interface object to contain values</param>
        /// <returns></returns>
        internal string UpdateListItemCOM(string teamSiteURL, string listName, string query, string[] fieldToUpdate, string[] fieldValues, string fieldToCompare, string compareValue)
        {
            string libraryTitle = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(teamSiteURL))
                {
                    CSOM.ClientContext context = new CSOM.ClientContext(teamSiteURL);
                    context.Credentials = CredentialCache.DefaultNetworkCredentials;
                    CSOM.Web oWeb = context.Web;
                    CSOM.List oList = oWeb.Lists.GetByTitle(listName);
                    CSOM.CamlQuery oquery = new CSOM.CamlQuery();
                    oquery.ViewXml = query;
                    CSOM.ListItemCollection collListItem = oList.GetItems(oquery);

                    context.Load(collListItem);

                    context.ExecuteQuery();

                    foreach (CSOM.ListItem oListItem in collListItem)
                    {
                        for (int i = 0; i < fieldToUpdate.Length; i++)
                        {
                            if (listName.Equals(Constants.CLIENT_DATA_LIST_NAME))
                            {
                                oListItem[fieldToUpdate[i]] = Convert.ToBoolean(Convert.ToString(fieldValues[i]));
                            }
                            else
                            {
                                oListItem[fieldToUpdate[i]] = Convert.ToString(fieldValues[i]);
                            }
                        }

                        oListItem.Update();
                    }
                    oList.Update();
                    context.ExecuteQuery();
                }
                return Constants.SUCESS_MSG;
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Ad Space Site Provisioning-Util.cs-UpdateListItemCOM", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                StringBuilder sbFailMsg = new StringBuilder();
                return Convert.ToString(sbFailMsg.AppendFormat("{0}{1}{2}{3}", Constants.FAIL_MSG, "UpdateListItemCOM", Constants.FAIL_EX_MSG, ex.Message));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteURL"></param>
        /// <param name="listTitle"></param>
        /// <returns></returns>
        internal string DeleteMasterData(string siteURL, string listTitle)
        {
            try
            {
                using (SPSite oSPsite = new SPSite(siteURL))
                {
                    using (SPWeb oSPWeb = oSPsite.OpenWeb())
                    {
                        bool allowUnsafeupdates = oSPWeb.AllowUnsafeUpdates;
                        oSPWeb.AllowUnsafeUpdates = true;
                        /* get the SPList object by list name*/
                        SPList lst = oSPWeb.Lists.TryGetList(listTitle);
                        if (lst != null && lst.Items.Count > 0)
                        {
                            for (int i = 0; i < lst.Items.Count;i++ )
                            {
                                SPListItem item = lst.Items[i];
                                item.Delete();
                            }   
                            lst.Update();
                        }
                        oSPWeb.AllowUnsafeUpdates = allowUnsafeupdates;
                    }
                }
                return Constants.SUCESS_MSG;
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Ad Space Site Provisioning-Util.cs-CreateListItem", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                StringBuilder sbFailMsg = new StringBuilder();
                return Convert.ToString(sbFailMsg.AppendFormat("{0}{1}{2}{3}", Constants.FAIL_MSG, "CreateListItem", Constants.FAIL_EX_MSG, ex.Message));
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteURL"></param>
        /// <param name="listTitle"></param>
        /// <returns></returns>
        internal string UpdateSiteName(string siteURL, string siteName)
        {
            try
            {
                siteName = siteName.Split(new char[] { '|' }).First();
                using (SPSite oSPsite = new SPSite(siteURL))
                {
                    using (SPWeb oSPWeb = oSPsite.OpenWeb())
                    {
                        bool allowUnsafeupdates = oSPWeb.AllowUnsafeUpdates;
                        oSPWeb.AllowUnsafeUpdates = true;
                        oSPWeb.Title = siteName;
                        oSPWeb.Update();
                        oSPWeb.AllowUnsafeUpdates = allowUnsafeupdates;
                    }
                }
                return Constants.SUCESS_MSG;
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Ad Space Site Provisioning-Util.cs-CreateListItem", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                StringBuilder sbFailMsg = new StringBuilder();
                return Convert.ToString(sbFailMsg.AppendFormat("{0}{1}{2}{3}", Constants.FAIL_MSG, "CreateListItem", Constants.FAIL_EX_MSG, ex.Message));
            }
        }

        #endregion

        #region common methods
        /// <summary>
        /// Funtion to fetch term from termstore
        /// </summary>
        /// <param name="osite">SPSite object</param>
        /// <param name="termName">Termnam whose object is need to be fetched</param>
        /// <returns>Term object containing term</returns>
        internal Term GetTermValue(SPSite osite, string termGUID)
        {
            try
            {
                TaxonomySession session = new TaxonomySession(osite);
                TermStore oTermStore = session.DefaultSiteCollectionTermStore;
                Term term = oTermStore.GetTerm(new Guid(termGUID));
                return term;
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Ad Sales Site Provisioning-Util.cs-GetTermValue", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                StringBuilder sbFailMsg = new StringBuilder();
                return null;
            }
        }
        #endregion

    }
}