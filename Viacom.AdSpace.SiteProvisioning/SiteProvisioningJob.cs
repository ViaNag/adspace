﻿using Microsoft.SharePoint.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint;
using Viacom.AdSpace.SiteProvisioning.BLL;
using ClientCtx = Microsoft.SharePoint.Client;
using System.Net;
using System.Web.Configuration;
using Microsoft.SharePoint.Client.Taxonomy;


namespace Viacom.AdSpace.SiteProvisioning
{
    /// <summary>
    /// Description			: class for Creating Site Provisioning tiemr job
    /// System				: Viacom - Site Provisioning
    /// 
    /// </summary>
    /// <ModificationLog>
    /// Ver	|Date			|Author			|Change Desc.
    /// ---------------------------------------------------------------------------------------------------
    /// 1	|06/17/2014		|Ateet Agarwal		|Initial Version
    /// </ModificationLog>
    public class SiteProvisioningJob : SPJobDefinition
    {
        public SiteProvisioningJob() : base() { }

        public SiteProvisioningJob(string jobName, SPService service)
            : base(jobName, service, null, SPJobLockType.Job)
        {
            this.Title = Constants.SITEPROVISIONING_TIMERJOBNAME;
        }

        public SiteProvisioningJob(string jobName, SPWebApplication webapp)
            : base(jobName, webapp, null, SPJobLockType.Job)
        {
            this.Title = Constants.SITEPROVISIONING_TIMERJOBNAME;
        }

        private string teamSiteURL = string.Empty;

        /// <summary>
        /// Funcation to perform action for timer job
        /// </summary>
        /// <param name="targetInstanceId">Target Web application Guid </param>
        public override void Execute(Guid targetInstanceId)
        {

            SPWebApplication webApp = this.Parent as SPWebApplication;
            string rootSiteCollectionUrl = webApp.GetResponseUri(SPUrlZone.Default).AbsoluteUri;
            string clientSiteCollUrl = rootSiteCollectionUrl + Constants.CLIENT_RELATIVE_URL;
            ISiteProvisioningAction provisioningAction = null;
            teamSiteURL = GetTeamSiteUrl(clientSiteCollUrl);
            try
            {
                ClientCtx.ClientContext targetSiteContext = new ClientCtx.ClientContext(teamSiteURL);
                targetSiteContext.Credentials = CredentialCache.DefaultNetworkCredentials;
                //targetSiteContext.Credentials = new NetworkCredential("testone", "newuser1", "viacom_corp");
                ClientCtx.Web oTargetWeb = targetSiteContext.Web;
                ClientCtx.List oTargetList = oTargetWeb.Lists.GetByTitle(Constants.SITEPRO_LIST_TITLE);
                ClientCtx.CamlQuery camlQuery = new ClientCtx.CamlQuery();
                camlQuery.ViewXml = Constants.SITEPRO_LIST_QUERY;
                ClientCtx.ListItemCollection collListItem = oTargetList.GetItems(camlQuery);

                targetSiteContext.Load(collListItem);

                targetSiteContext.ExecuteQuery();

                foreach (ClientCtx.ListItem oListItem in collListItem)
                {
                    try
                    {

                        if (oListItem[Constants.CLIENT_DATA_CLIENTNAME] != null)
                        {
                            // get client name
                            string clientName = (oListItem[Constants.CLIENT_DATA_CLIENTNAME] as TaxonomyFieldValue).Label;

                            // get sequence number
                            string sequeneNumber = GetSequeneNumber(targetSiteContext, oTargetWeb, clientName);
                            if (!string.IsNullOrEmpty(sequeneNumber))
                            {
                                provisioningAction = GetSiteprovisioningActionObject(oListItem, rootSiteCollectionUrl, clientSiteCollUrl, "ascs" + sequeneNumber);
                                if (provisioningAction != null)
                                {
                                    SiteProvisioningFacade.SingleInstance.ExecuteSiteProvisioningRequest(provisioningAction, clientSiteCollUrl);
                                }
                            }
                        }
                    }

                    catch (Exception ex)
                    {
                        SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Ad Space Site Provisioning Job-SiteProvisioningJob.cs-Execute-For Loop", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                    }
                }
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Ad Sales retention Policy-Retention.cs-Execute", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
            }
        }


        /// <summary>
        /// Funcation ot get Site Provisioning Action Object
        /// </summary>
        /// <param name="oItem">SPListItem object</param>
        /// <returns>Site Provisioning Action Object</returns>
        public ISiteProvisioningAction GetSiteprovisioningActionObject(ClientCtx.ListItem oItem, string rootSiteCollectionUrl, string clientSiteCollUrl, string sequenceNumber)
        {
            ISiteProvisioningRequest SiteRequest = null;
            ISiteProvisioningAction provisioningAction = null;
            //try
            //{
            SiteRequest = new ClientSiteProvisioningRequest();
            TaxonomyFieldValue clientNameFieldValue = oItem[Constants.CLIENT_DATA_CLIENTNAME] as TaxonomyFieldValue;
            SiteRequest.ClientName = string.Format("{0}{1}{2}", clientNameFieldValue.Label, "|", clientNameFieldValue.TermGuid);
            TaxonomyFieldValue clientSegmentFieldValue = oItem[Constants.CLIENT_DATA_SEGMENT] as TaxonomyFieldValue;
            SiteRequest.ClientSegment = string.Format("{0}{1}{2}", clientSegmentFieldValue.Label, "|", clientSegmentFieldValue.TermGuid);
            TaxonomyFieldValue clientCategoryFieldValue = oItem[Constants.CLIENT_DATA_CATEGORY] as TaxonomyFieldValue;
            SiteRequest.ClientCategory = string.Format("{0}{1}{2}", clientCategoryFieldValue.Label, "|", clientCategoryFieldValue.TermGuid);
            string url = Convert.ToString(oItem[Constants.CLIENT_DATA_SITEURL]).Split(new string[] { Constants.TEAMSITE_PATH }, StringSplitOptions.None).Last();
            SiteRequest.SiteURL = string.Format("{0}{1}{2}", rootSiteCollectionUrl, Constants.CLIENTSPACE_PATHWITHSLASH, sequenceNumber);
            SiteRequest.RDMClientID = Convert.ToString(oItem[Constants.CLIENT_DATA_RDMID]);
            SiteRequest.GabrielClientID = Convert.ToString(oItem[Constants.CLIENT_DATA_GABRIELID]);
            SiteRequest.WideOrbitClientID = Convert.ToString(oItem[Constants.CLIENT_DATA_WIDEID]);
            SiteRequest.AdFrontClientID = Convert.ToString(oItem[Constants.CLIENT_DATA_ADFRONTID]);
            SiteRequest.RequestStatus = Convert.ToString(oItem[Constants.CLIENT_DATA_REQSTATUS]);
            SiteRequest.SiteType = Viacom.AdSpace.SiteProvisioning.BLL.SiteTypes.ClientSite;
            SiteRequest.TeamSiteURL = GetTeamSiteUrl(clientSiteCollUrl);
            SiteRequest.ClientSiteURL = Constants.CLIENT_SITE_POSTFIX + sequenceNumber;

            if (Convert.ToString(oItem[Constants.SITE_PROVISIONING_DATA_ACTIONTYPE]).Equals(Convert.ToString(ActionTypes.CreateSite)))
            {
                SiteRequest.Action = ActionTypes.CreateSite;
                provisioningAction = new CreateSiteAction(SiteRequest);
            }
            if (Convert.ToString(oItem[Constants.SITE_PROVISIONING_DATA_ACTIONTYPE]).Equals(Convert.ToString(ActionTypes.EditSite)))
            {
                SiteRequest.Action = ActionTypes.EditSite;
                provisioningAction = new EditSiteAction(SiteRequest);
            }
            //}
            //catch (Exception ex)
            //{
            //    SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Ad Space Site Provisioning Job-SiteProvisioningJob.cs-GetSiteprovisioningActionObject", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
            //}
            return provisioningAction;
        }




        /// <summary>
        /// Get the sequence number from client data list.
        /// </summary>
        /// <param name="targetSiteContext"></param>
        /// <param name="oTargetWeb"></param>
        /// <param name="oListItem"></param>
        /// <returns></returns>
        internal string GetSequeneNumber(ClientCtx.ClientContext targetSiteContext, ClientCtx.Web oTargetWeb, string clientName)
        {
            string sequencNumber = string.Empty;

            ClientCtx.List oTargetList = oTargetWeb.Lists.GetByTitle(Constants.CLIENT_DATA_LIST_NAME);
            ClientCtx.CamlQuery camlQuery = new ClientCtx.CamlQuery();
            camlQuery.ViewXml = string.Format(Constants.DATA_LIST_QUERY, clientName);
            ClientCtx.ListItemCollection collListItem = oTargetList.GetItems(camlQuery);

            targetSiteContext.Load(collListItem);

            targetSiteContext.ExecuteQuery();

            if (collListItem.Count > 0)
            {

                ClientCtx.ListItem item = collListItem[0];
                sequencNumber += Convert.ToString(item[Constants.CLIENT_DATA_SEQUENCE]);
            }
            return sequencNumber;
        }

        internal string GetTeamSiteUrl(string rootSiteUrl)
        {
            string url = string.Empty;
            try
            {
                using (SPSite oSite = new SPSite(rootSiteUrl))
                {
                    using (SPWeb oWeb = oSite.OpenWeb())
                    {
                        SPList list = oWeb.Lists[Constants.CONFIGURATION_LISTNAME];
                        SPQuery query = new SPQuery();
                        query.Query = Constants.TEAMSITE_QUERY;
                        SPListItemCollection itemColl = list.GetItems(query);
                        foreach (SPListItem item in itemColl)
                        {
                            if (item != null)
                            {
                                if (Convert.ToString(item[Constants.CONFIGLIST_FIELDKEY_NAME]) == Constants.TEAMSITE_URL_KEY)
                                {
                                    if (!string.IsNullOrEmpty(Convert.ToString(item[Constants.CONFIGLIST_FIELDKVALUE_NAME])))
                                    {
                                        url = Convert.ToString(item[Constants.CONFIGLIST_FIELDKVALUE_NAME]);
                                    }
                                }
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Ad Sales Site Provisioning-Util.cs-GetAdSpaceCLientSpaceUrl", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
            }
            return url;
        }
    }
}
