using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Taxonomy;
using Viacom.AdSpace.TeamSiteSharedContent;


namespace Viacom.AdSpace.TeamSiteSharedContent.Features.SiteColumns
{
    /// <summary>
    /// This class handles events raised during feature activation, deactivation, installation, uninstallation, and upgrade.
    /// </summary>
    /// <remarks>
    /// The GUID attached to this class may be used during packaging and should not be modified.
    /// </remarks>

    [Guid("71633f90-338d-4796-a242-b7aa9573d8e7")]
    public class SiteColumnsEventReceiver : SPFeatureReceiver
    {
        // Uncomment the method below to handle the event raised after a feature has been activated.

        public override void FeatureActivated(SPFeatureReceiverProperties properties)
        {
            //SPSite site = properties.Feature.Parent as SPSite;
            //ConnectTaxonomyField(site, new Guid("{B654D984-187A-471B-8738-F08F3356CFDA}"), "Continents", "Countries");
        }


        // Uncomment the method below to handle the event raised before a feature is deactivated.

        //public override void FeatureDeactivating(SPFeatureReceiverProperties properties)
        //{
        //}


        // Uncomment the method below to handle the event raised after a feature has been installed.

        //public override void FeatureInstalled(SPFeatureReceiverProperties properties)
        //{
        //}


        // Uncomment the method below to handle the event raised before a feature is uninstalled.

        //public override void FeatureUninstalling(SPFeatureReceiverProperties properties)
        //{
        //}

        // Uncomment the method below to handle the event raised when a feature is upgrading.

        //public override void FeatureUpgrading(SPFeatureReceiverProperties properties, string upgradeActionName, System.Collections.Generic.IDictionary<string, string> parameters)
        //{
        //}

        //public static void ConnectTaxonomyField(SPSite site, Guid fieldId, String termGroup, string termSetName)
        //{
        //    //Check if field exists in the site.
        //    if (site.RootWeb.Fields.Contains(fieldId))
        //    {
        //        //Create a Taxonomy Session
        //        TaxonomySession session = new TaxonomySession(site);
        //        if (session.DefaultKeywordsTermStore != null)
        //        {
        //            //get the default metadata service application
        //            var termStore = session.DefaultKeywordsTermStore;
        //            //var group = termStore.Groups.GetByName(termGroup);
        //            //var termSet = group.TermSets.GetByName(termSetName);
        //                                var group = Extensions.GetByName(termGroup
        //            var termSet = group.TermSets.GetByName(termSetName);
        //            TaxonomyField field = site.RootWeb.Fields[fieldId] as TaxonomyField;
        //            field.SspId = termSet.TermStore.Id;
        //            field.TermSetId = termSet.Id;
        //            field.TargetTemplate = string.Empty;
        //            field.AnchorId = Guid.Empty;
        //            field.Update();
        //        }
        //        else
        //        {
        //            throw new Exception(string.Format("DefaultKeyWordTermStore not found in this site {0}", site.Url));
        //        }
        //    }
        //    else
        //    {
        //        throw new ArgumentException(string.Format("Field {0} not found in site {1}", fieldId, site.Url));
        //    }
        //}
    }
}
