﻿using Microsoft.Office.DocumentManagement;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using Microsoft.SharePoint.Taxonomy;
using System;
using System.Linq;
using System.Text;
using System.Web.Services;
using Microsoft.Office.Server.UserProfiles;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using Viacom.AdSpace.Shared;
using System.Security.Principal;

namespace Viacom.AdSpace.AdSpaceWebService
{
    public class TargetWSShareDocuments : Microsoft.SharePoint.Publishing.PublishingLayoutPage
    {

        /// <summary>
        /// Web Method to Check permission
        /// </summary>
        /// <param name="Name">List Name</param>
        /// <param name="URL">URL of current site</param>
        /// <param name="CurrentUser">Current user Login Name</param>
        /// <param name="Type">List or folder</param>
        /// <returns>"True" or "False"</returns>
        [WebMethod]
        public static string TargetWSCheckPermission(string Name, string URL, string CurrentUser, string Type, string Permission)
        {
            try
            {
                using (SPSite osite = new SPSite(URL, GetUserToken(URL, CurrentUser)))
                {
                    using (SPWeb oTargetWeb = osite.OpenWeb())
                    {
                        if (Type.Equals("Lib"))
                        {
                            SPList oList = oTargetWeb.Lists.TryGetList(Name);
                            if (oList.DoesUserHavePermissions(SPBasePermissions.ViewListItems) ||
                                oList.DoesUserHavePermissions(SPBasePermissions.AddListItems))
                            {
                                return "TRUE";
                            }
                            else
                            {
                                return "FALSE";
                            }
                        }
                        else
                        {
                            SPItem oItem = oTargetWeb.GetListItem(URL);
                            if (oItem.DoesUserHavePermissions(SPBasePermissions.ViewListItems) ||
                                oItem.DoesUserHavePermissions(SPBasePermissions.AddListItems))
                            {
                                return "TRUE";
                            }
                            else
                            {
                                return "FALSE";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("TargetWSShareDocuments.cs-TargetWSCheckPermission", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                throw ex;
            }

        }


        /// <summary>
        /// Funcation to get site hirarchy xml
        /// </summary>
        /// <param name="Name">Nmae of list</param>
        /// <param name="URL">URL of current site</param>
        /// <param name="CurrentUser">Current user Login Nmae</param>
        /// <param name="Type">subsite or lib or folder</param>
        /// <param name="oFolderURL">Folder URL</param>
        /// <returns></returns>
        [WebMethod]
        public static string TargetWSGetTargetLocations(string Name, string URL, string CurrentUser, string Type, string oFolderURL, string Permission)
        {
            StringBuilder sbXML = new StringBuilder();
            string absoluteweburl = string.Empty;
            string treeStrPermissionLevel = string.Empty;
            string result = GetConfigValueFromAdSpaceRoot("ttyestest");
            if (string.IsNullOrEmpty(Permission))
            {
                treeStrPermissionLevel = "Read";
            }
            else
            {
                treeStrPermissionLevel = Permission;
            }
            try
            {

                using (SPSite osite = new SPSite(URL, GetUserToken(URL, CurrentUser)))
                {
                    using (SPWeb oTargetWeb = osite.OpenWeb((URL.Replace(osite.Url, "")).TrimStart('/')))
                    {

                        switch (Type)
                        {
                            case "SubSite":
                                {

                                    if (oTargetWeb.Exists)
                                    {
                                        sbXML.Append("<Site Type='SubSite'><Webs>");

                                        SPWebCollection oWebcoll = oTargetWeb.GetSubwebsForCurrentUser();
                                        foreach (SPWeb oweb in oWebcoll)
                                        {

                                            sbXML.AppendFormat("<Web Url='{0}' Type='{1}'>{2}</Web>", oweb.Url, "SubSite", oweb.Title);

                                            #region PERMISSION_LEVEL_COMMENTED
                                            //SPRoleDefinitionCollection roleDefinations = oweb.RoleDefinitions;


                                            //SPRoleDefinition roleDefination = roleDefinations.Cast<SPRoleDefinition>().FirstOrDefault(r => r.Name == treeStrPermissionLevel);
                                            //if (roleDefination != null)
                                            //{

                                            //    SPRoleDefinitionBindingCollection userRoles = oweb.AllRolesForCurrentUser;
                                            //    if (userRoles.Contains(roleDefination))
                                            //    {
                                            //        sbXML.AppendFormat("<Web Url='{0}' Type='{1}'>{2}</Web>", oweb.Url, "SubSite", oweb.Title);
                                            //    }
                                            //}
                                            #endregion
                                        }
                                        sbXML.Append("</Webs><Lists>");

                                        SPListCollection oListcoll = oTargetWeb.Lists;
                                        foreach (SPList olist in oListcoll)
                                        {
                                            if (olist.BaseType.ToString() == "DocumentLibrary" && !Constant.OOTB_LIB_NAME.Any(s => olist.Title.Contains(s)))
                                            {

                                                sbXML.AppendFormat("<List Url='{0}' Type='{1}'>{2}</List>", oTargetWeb.Url + "/" + olist.EntityTypeName, "Lib", olist.Title);


                                                #region PERMISSION_LEVEL_COMMENTED
                                                //SPRoleDefinitionCollection roleDefinations = olist.ParentWeb.RoleDefinitions;
                                                //SPRoleDefinition roleDefination = roleDefinations.Cast<SPRoleDefinition>().FirstOrDefault(r => r.Name == treeStrPermissionLevel);
                                                //if (roleDefination != null)
                                                //{

                                                //    SPRoleDefinitionBindingCollection userRoles = olist.AllRolesForCurrentUser;
                                                //    if (userRoles.Contains(roleDefination))
                                                //    {
                                                //        sbXML.AppendFormat("<List Url='{0}' Type='{1}'>{2}</List>", oTargetWeb.Url + "/" + olist.EntityTypeName, "Lib", olist.Title);
                                                //    }
                                                //}
                                                #endregion
                                            }
                                        }
                                        sbXML.Append("</Lists></Site>");

                                    }
                                    else
                                    {
                                        sbXML.Append(string.Format("{0}{1}{2}", "<Site Type='Message'><Message>Target site with URL: ", URL, " does not exists</Message></Site>"));
                                    }
                                    break;
                                }
                            case "Lib":
                                {
                                    sbXML.Append("<Site Type='Lib'><Folders>");
                                    SPList oList = oTargetWeb.Lists.TryGetList(Name);
                                    SPFolderCollection oFolderColl = oList.RootFolder.SubFolders;
                                    foreach (SPFolder oFolder in oFolderColl)
                                    {
                                        if (!Constant.OOTB_FOLDER_NAME.Any(s => oFolder.Name.Contains(s)))
                                        {

                                            sbXML.AppendFormat("<Folder Url='{0}' Type='{1}'>{2}</Folder>", oTargetWeb.Url + "/" + oFolder.Url, "Folder", oFolder.Name);

                                            #region PERMISSION_LEVEL_COMMENTED
                                            //SPRoleDefinitionCollection roleDefinations = oFolder.ParentWeb.RoleDefinitions;
                                            //SPRoleDefinition roleDefination = roleDefinations.Cast<SPRoleDefinition>().FirstOrDefault(r => r.Name == treeStrPermissionLevel);
                                            //if (roleDefination != null)
                                            //{
                                            //    SPRoleDefinitionBindingCollection userRoles = oFolder.Item.AllRolesForCurrentUser;
                                            //    if (userRoles.Contains(roleDefination))
                                            //    {
                                            //        sbXML.AppendFormat("<Folder Url='{0}' Type='{1}'>{2}</Folder>", oTargetWeb.Url + "/" + oFolder.Url, "Folder", oFolder.Name);
                                            //    }
                                            //}
                                            #endregion
                                        }
                                    }
                                    sbXML.Append("</Folders></Site>");
                                    break;
                                }
                            case "Folder":
                                {
                                    if (!string.IsNullOrEmpty(oFolderURL))
                                        sbXML.Append("<Site Type='SubFolder'><Folders>");
                                    SPFolder oTargetFolder = oTargetWeb.GetFolder(oFolderURL);
                                    SPFolderCollection oFolderColl = oTargetFolder.SubFolders;
                                    foreach (SPFolder oFolder in oFolderColl)
                                    {
                                        if (!Constant.OOTB_FOLDER_NAME.Any(s => oFolder.Name.Contains(s)))
                                        {
                                            sbXML.AppendFormat("<Folder Url='{0}' Type='{1}'>{2}</Folder>", oTargetWeb.Url + "/" + oFolder.Url, "Folder", oFolder.Name);

                                            #region PERMISSION_LEVEL_COMMENTED
                                            //SPRoleDefinitionCollection roleDefinations = oFolder.ParentWeb.RoleDefinitions;
                                            //SPRoleDefinition roleDefination = roleDefinations.Cast<SPRoleDefinition>().FirstOrDefault(r => r.Name == treeStrPermissionLevel);
                                            //if (roleDefination != null)
                                            //{

                                            //    SPRoleDefinitionBindingCollection userRoles = oFolder.Item.AllRolesForCurrentUser;
                                            //    if (userRoles.Contains(roleDefination))
                                            //    {
                                            //        sbXML.AppendFormat("<Folder Url='{0}' Type='{1}'>{2}</Folder>", oTargetWeb.Url + "/" + oFolder.Url, "Folder", oFolder.Name);
                                            //    }
                                            //}
                                            #endregion
                                        }
                                    }
                                    sbXML.Append("</Folders></Site>");
                                    break;
                                }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("TargetWSShareDocuments.cs-TargetWSGetTargetLocations", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                throw ex;
            }
            return Convert.ToString(sbXML);
        }

        [WebMethod]
        public static string ValidateContentTypeAndPermission(string URL, string CurrentUser, string ContentTypeKey, string LibraryName)
        {
            string ret = string.Empty;
            bool isContentTypeAdded = false;
            string contentType = string.Empty;
            try
            {
                using (SPSite osite = new SPSite(URL, GetUserToken(URL, CurrentUser)))
                {
                    using (SPWeb oTargetWeb = osite.OpenWeb((URL.Replace(osite.Url, "")).TrimStart('/')))
                    {
                        SPList oList = oTargetWeb.Lists.TryGetList(LibraryName);

                        if (ContentTypeKey.Equals(Constants.ShareDocToVaultCTKey, StringComparison.InvariantCultureIgnoreCase))
                        {
                            contentType = GetConfigValueFromAdSpaceRoot(Constants.ShareDocToVaultCTKey);
                        }
                        else if (ContentTypeKey.Equals(Constants.OTToVaultCTKey, StringComparison.InvariantCultureIgnoreCase))
                        {
                            contentType = GetConfigValueFromAdSpaceRoot(Constants.OTToVaultCTKey);
                        }
                        else if (ContentTypeKey.Equals(Constants.ShareLinkToVaultCTKey, StringComparison.InvariantCultureIgnoreCase))
                        {
                            contentType = GetConfigValueFromAdSpaceRoot(Constants.ShareLinkToVaultCTKey);
                        }

                        if (contentType.Equals(Constants.NotRequiredCTKey))
                        {
                            isContentTypeAdded = true;
                        }
                        else if (!string.IsNullOrEmpty(contentType))
                        {
                            foreach (SPContentType ct in oList.ContentTypes)
                            {
                                if (ct.Name.Equals(contentType, StringComparison.InvariantCultureIgnoreCase))
                                {
                                    isContentTypeAdded = true;
                                    break;
                                }
                            }
                        }
                        if (isContentTypeAdded)
                        {
                            SPBasePermissions permissions = oList.GetUserEffectivePermissions(CurrentUser);

                            bool canAdd = (permissions & SPBasePermissions.AddListItems) != 0;
                            bool canEdit = (permissions & SPBasePermissions.EditListItems) != 0;

                            if (canAdd || canEdit)
                            {
                                ret = "Success";
                            }
                            else
                            {
                                ret = "NoPermission";
                            }
                        }
                        else
                        {
                            ret = "NoContentType";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("TargetWSShareDocuments.cs-ValidateContentTypeAndPermission", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                throw ex;

            }



            return ret;
        }


        [WebMethod]
        //TargetWebURL,LinkFileURL,CurrentUser,SourcePath,SourceSiteName,SourceLibraryName,SourceFolderName
        //public static string UpdatetargetListitem(string webURL, string LinkFileURL, string CurrentUser)
        public static string UpdatetargetListitem(string TargetWebURL, string LinkFileURL, string CurrentUser, string SourcePath, string SourceSiteName, string SourceLibraryName, string SourceFolderName)
        {
            try
            {
                using (SPSite osite = new SPSite(TargetWebURL))
                {
                    using (SPWeb oweb = osite.OpenWeb())
                    {
                        string rootSiteCollURL = oweb.Site.WebApplication.GetResponseUri(SPUrlZone.Default).AbsoluteUri;
                        string oSharedColumnName = GetConfigValueByKey(rootSiteCollURL, Constant.SHARED_COLUMN_KEY);
                        if (!string.IsNullOrEmpty(oSharedColumnName))
                        {
                            string defaultValue = string.Empty;
                            bool valueUnsafeUpdate = oweb.AllowUnsafeUpdates;
                            oweb.AllowUnsafeUpdates = true;
                            string[] SHARED_COLUMN = oSharedColumnName.Split(',');
                            SPListItem newItem = oweb.GetListItem(LinkFileURL);
                            SPFolder oFolder = newItem.File.ParentFolder;
                            SPList olist = newItem.ParentList;
                            MetadataDefaults metadata = new MetadataDefaults(olist);
                            SPUser user = oweb.EnsureUser(CurrentUser);
                            SPFieldUserValue oUser = new SPFieldUserValue(oweb, user.ID, user.LoginName);

                            newItem["Author"] = oUser;
                            newItem["Editor"] = oUser;
                            foreach (SPField oField in newItem.Fields)
                            {
                                try
                                {
                                    if (SHARED_COLUMN.Contains(oField.InternalName))
                                    {
                                        defaultValue = string.Empty;
                                        defaultValue = metadata.GetFieldDefault(oFolder, oField.InternalName);
                                        if (oField is TaxonomyField)
                                        {
                                            TaxonomyField taxonomyField = oField as TaxonomyField;
                                            if (string.IsNullOrEmpty(defaultValue))
                                            {
                                                defaultValue = string.IsNullOrEmpty(defaultValue) ? taxonomyField.DefaultValue : defaultValue;
                                            }
                                            TaxonomyFieldValue taxValue = null;
                                            if (string.IsNullOrEmpty(defaultValue))
                                            {
                                                taxValue = new TaxonomyFieldValue(taxonomyField);
                                                taxonomyField.SetFieldValue(newItem, taxValue);
                                            }
                                            else
                                            {
                                                if (taxonomyField.AllowMultipleValues)
                                                {
                                                    TaxonomyFieldValueCollection taxValueColl = new TaxonomyFieldValueCollection(defaultValue);
                                                    taxonomyField.SetFieldValue(newItem, taxValueColl);
                                                }
                                                else
                                                {
                                                    taxValue = new TaxonomyFieldValue(defaultValue);
                                                    taxonomyField.SetFieldValue(newItem, taxValue);
                                                }
                                            }
                                        }
                                        else if (oField is SPFieldDateTime || oField is SPFieldUser)
                                        {
                                            newItem[oField.InternalName] = null;
                                            if (string.IsNullOrEmpty(defaultValue))
                                            {
                                                defaultValue = newItem.Fields[oField.InternalName].DefaultValue;
                                            }
                                            if (!string.IsNullOrEmpty(defaultValue))
                                            {
                                                newItem[oField.InternalName] = defaultValue;
                                            }
                                        }
                                        else
                                        {
                                            newItem[oField.InternalName] = string.Empty;
                                            if (string.IsNullOrEmpty(defaultValue))
                                            {
                                                defaultValue = newItem.Fields[oField.InternalName].DefaultValue;
                                            }
                                            if (!string.IsNullOrEmpty(defaultValue))
                                            {
                                                newItem[oField.InternalName] = defaultValue;
                                            }
                                        }

                                    }
                                }
                                catch (Exception ex)
                                {
                                    SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("AdSales Sharefun-FieldNotExist", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                                }

                            }
                            //Save request in analytics database
                            CaptureShareToAdSpaceInfo(osite, oweb, newItem, LinkFileURL, user.LoginName, CurrentUser, SourcePath, SourceSiteName, SourceLibraryName, SourceFolderName);

                            if (newItem.File.CheckOutType != SPFile.SPCheckOutType.None)
                            {
                                newItem.File.CheckIn("Record Doc check in.", SPCheckinType.MajorCheckIn);
                            }
                            newItem.UpdateOverwriteVersion();
                            oweb.AllowUnsafeUpdates = valueUnsafeUpdate;
                        }
                    }
                }
                return "Success";
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("AdSales Sharefun-UpdateLinkItemProp", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                return "False";
                throw ex;
            }
        }

        #region Private Method
        /// <summary>
        /// function to get user token
        /// </summary>
        /// <param name="url">URL of site</param>
        /// <param name="CurrentUser">Current User Login</param>
        /// <returns>User Token</returns>
        private static SPUserToken GetUserToken(string url, string CurrentUser)
        {
            SPUserToken oToken = null;
            using (SPSite osite = new SPSite(url))
            {
                using (SPWeb oWeb = osite.OpenWeb())
                {
                    oToken = oWeb.EnsureUser(CurrentUser).UserToken;
                }
            }
            return oToken;
        }

        /// <summary>
        /// Get Configuration List Value for given key
        /// </summary>
        /// <param name="rootSiteCollectionUrl">Root SiteCollection Url</param>
        /// <param name="key">Key for configuration list query</param>
        /// <returns>value</returns>
        private static string GetConfigValueByKey(string rootSiteCollectionUrl, string key)
        {
            string value = null;
            // string key = "SharedColumn";
            try
            {
                // elevate privileges of inserting data in DropFolder Configuration List
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    using (SPSite rootSite = new SPSite(rootSiteCollectionUrl))
                    {
                        using (SPWeb rootWeb = rootSite.OpenWeb())
                        {
                            SPList configList = rootWeb.GetList(rootWeb.Url + Constant.CONFIG_LIST_URL);

                            SPQuery query = new SPQuery();
                            query.Query = string.Format(Constant.QueryForTitleEqualValue, key);
                            SPListItemCollection itemcoll = configList.GetItems(query);
                            if (itemcoll != null && itemcoll.Count > 0)
                            {
                                SPListItem item = itemcoll[0];
                                if (!string.IsNullOrEmpty(Convert.ToString(item[Constant.CONFIG_LIST_FIELD_NAME])))
                                {
                                    value = Convert.ToString(item[Constant.CONFIG_LIST_FIELD_NAME]);
                                }

                            }
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("AdSales Sharefun-GetConfigListValue", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
            }

            return value;
        }

        /// <summary>
        /// Save ShareToAdSpace document information into SQL DB
        /// </summary>
        /// <param name="site"></param>
        /// <param name="web"></param>
        /// <param name="listItem"></param>
        /// <param name="LinkFileURL"></param>
        /// <param name="sharedBy"></param>
        /// <param name="currentUser"></param>
        /// <param name="sourceItem"></param>
        private static void CaptureShareToAdSpaceInfo(SPSite site, SPWeb web, SPListItem listItem, string LinkFileURL, string sharedBy,
            string currentUser, string SourcePath, string SourceSiteName, string SourceLibraryName, string SourceFolderName)
        {
            SPSecurity.RunWithElevatedPrivileges(delegate()
            {
                WindowsImpersonationContext impersonatedContext = null;
                try
                {
                    SPFolder oFolder = listItem.File.ParentFolder;
                    SPList olist = listItem.ParentList;
                    SPServiceContext context = SPServiceContext.GetContext(site);
                    UserProfileManager upm = new UserProfileManager(context);
                    UserProfile profile = upm.GetUserProfile(currentUser);
                    string firstName = profile[PropertyConstants.FirstName].Value != null ? profile[PropertyConstants.FirstName].Value.ToString() : string.Empty;
                    string lastName = profile[PropertyConstants.LastName].Value != null ? profile[PropertyConstants.LastName].Value.ToString() : string.Empty;
                    string targetSiteName = web.Title;
                    string targetPath = web.Url + LinkFileURL.Substring(0, LinkFileURL.LastIndexOf(('/')));
                    string fileName = listItem.Name;
                    string shareType = site.RootWeb.Title;
                    string targetLibraryName = olist.Title;
                    string targetFolderName = oFolder.Name;


                    string spName = Convert.ToString(ConfigurationManager.AppSettings[WebMethodConstants.SpLogShareToAdSpaceInfo]);
                    string connectionString = Convert.ToString(ConfigurationManager.ConnectionStrings[Constants.AnalyticsDBConstants.ConnectionStringForAnalyticsDB_Windows]);
                    SqlConnection sqlConnection;
                    sqlConnection = new SqlConnection(connectionString);
                    sqlConnection.Open();
                    SqlCommand command = new SqlCommand(spName.Trim(), sqlConnection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@SharedBy", SqlDbType.VarChar).Value = sharedBy;
                    command.Parameters.Add("@SharedByFN", SqlDbType.VarChar).Value = firstName;
                    command.Parameters.Add("@SharedByLN", SqlDbType.VarChar).Value = lastName;
                    command.Parameters.Add("@FileName", SqlDbType.VarChar).Value = fileName;
                    command.Parameters.Add("@TargetPath", SqlDbType.VarChar).Value = targetPath;
                    command.Parameters.Add("@TargetSiteName", SqlDbType.VarChar).Value = targetSiteName;
                    command.Parameters.Add("@TargetLibraryName", SqlDbType.VarChar).Value = targetLibraryName;
                    command.Parameters.Add("@TargetFolderName", SqlDbType.VarChar).Value = targetFolderName;
                    command.Parameters.Add("@ShareType", SqlDbType.VarChar).Value = shareType;
                    command.Parameters.Add("@SourcePath", SqlDbType.VarChar).Value = SourcePath;
                    command.Parameters.Add("@SourceSiteName", SqlDbType.VarChar).Value = SourceSiteName;
                    command.Parameters.Add("@SourceLibraryName", SqlDbType.VarChar).Value = SourceLibraryName;
                    command.Parameters.Add("@SourceFolderName", SqlDbType.VarChar).Value = SourceFolderName;
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("AdSales -- CaptureShareToAdSpaceInfo", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                }
                finally
                {
                    if (impersonatedContext != null) { impersonatedContext.Undo(); }
                }
            });
        }

        private static string GetConfigValueFromAdSpaceRoot(string key){
            string result = null;
            try {
                if (!string.IsNullOrEmpty(key))
                {
                    SPSecurity.RunWithElevatedPrivileges(delegate()
                    {
                        string rootSiteUrl = SPContext.Current.Web.Site.WebApplication.GetResponseUri(SPUrlZone.Default).AbsoluteUri;
                        using (SPSite site = new SPSite(rootSiteUrl))
                        {
                            using (SPWeb web = site.OpenWeb())
                            {
                                SPList configList = web.Lists.TryGetList(Constants.RootConfigList);

                                SPQuery query = new SPQuery();
                                query.Query = string.Format(Constants.RootConfigListQuery, key);
                                SPListItemCollection itemcoll = configList.GetItems(query);
                                if (itemcoll != null && itemcoll.Count > 0)
                                {
                                    SPListItem item = itemcoll[0];
                                    if (!string.IsNullOrEmpty(Convert.ToString(item[Constants.RootConfigValueField])))
                                    {
                                        result = Convert.ToString(item[Constants.RootConfigValueField]);
                                    }

                                }
                            }
                        }
                    });
                }
            }
            catch (Exception ex) { 
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("TargetWSSShareDocument - GetConfigValueFromAdSpaceRoot",TraceSeverity.Unexpected,EventSeverity.Error)
                    ,TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                throw ex;
            } 
            return result;
        }
        #endregion
    }
}