﻿using Microsoft.Office.DocumentManagement;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using Microsoft.SharePoint.Taxonomy;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using Viacom.AdSpace.Shared;

namespace Viacom.AdSpace.AdSpaceWebService
{
    /// <summary>
    /// Web method class for AdSpace/CS/AdSpace external. Please activate feature in all site collections
    /// </summary>
    public class WebMethods : Microsoft.SharePoint.Publishing.PublishingLayoutPage
    {

        string userName = string.Empty;
        static SqlConnection sqlConnection;

        /// <summary>
        /// Web Method to insert Play video information in analytics
        /// </summary>
        /// <param name="Url"></param>
        /// <returns></returns>
        [WebMethod]
        public static void CaptureVideosInfo(string url, string isDownloaded)
        {
            SPSecurity.RunWithElevatedPrivileges(delegate()
            {
                try
                {
                    SaveVideosInfo(url, isDownloaded);
                }
                catch (Exception ex)
                {
                    SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("WebMethods.cs-CaptureVideosInfo", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                    throw ex;
                }
            });
        }

        /// <summary>
        /// Web Method to insert Play and Download video information in analytics
        /// </summary>
        /// <param name="Url"></param>
        /// <returns></returns>
        [WebMethod]
        public static void CaptureVideosAnalytics(string url, string isDownloaded, string docTitle, string originalUrl)
        {
            SPSecurity.RunWithElevatedPrivileges(delegate()
            {
                try
                {
                    SaveVideosInfo(url, isDownloaded, docTitle, originalUrl);
                }
                catch (Exception ex)
                {
                    SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("WebMethods.cs-CaptureVideosAnalytics", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                    throw ex;
                }
            });
        }
        

        #region Private

        private static void SaveVideosInfo(string url, string isDownloaded) {
            try
            {
                string vaultUrl = Convert.ToString(ConfigurationManager.AppSettings[WebMethodConstants.VaultRootWebUrl]);
                using (SPSite site = new SPSite(vaultUrl))
                {
                    using (SPWeb web = site.RootWeb)
                    {
                        string querystring = string.Concat(
                                                   "<Where>",
                                                   "<Eq>",
                                                      "<FieldRef Name='", WebMethodConstants.ConfigKeyField, "'/>",
                                                      "<Value Type='Text'>", WebMethodConstants.VideoFormatKey, "</Value>",
                                                   "</Eq>",
                                                   "</Where>");
                        SPListItemCollection items = GetListItems(web, WebMethodConstants.ConfigList, querystring, null);
                        SPListItem configItem = null;
                        configItem = items[0];
                        bool isVideo = false;
                        if (configItem != null)
                        {
                            string videoFormats = configItem[WebMethodConstants.ConfigValueField].ToString();
                            string extension = null;
                            string[] splitString = url.Split('.');
                            for (int i = 0; i < splitString.Length; i++)
                            {
                                extension = splitString[i];
                            }
                            extension = extension.Replace("_z", "");
                            if (videoFormats.Contains(extension))
                            {
                                isVideo = true;
                            }
                        }
                        bool IsValidUser = true;
                        string userName = System.Web.HttpContext.Current.User.Identity.Name;
                        bool IsDownloaded = false;
                        if (string.Compare(isDownloaded, "true", true) == 0)
                        {
                            IsDownloaded = true;
                        }
                        string excludeUserAccounts = Convert.ToString(ConfigurationManager.AppSettings[WebMethodConstants.ExcludeUserAccounts]);
                        string[] arrExcludeUserAccounts = excludeUserAccounts.Split(';');
                        if (arrExcludeUserAccounts != null && arrExcludeUserAccounts.Length > 0)
                        {
                            int userExistCount = Array.IndexOf(arrExcludeUserAccounts, userName);
                            if (userExistCount > -1)
                            {
                                IsValidUser = false;
                            }
                        }
                        if (IsValidUser)
                        {
                            string appSource = Convert.ToString(ConfigurationManager.AppSettings[WebMethodConstants.AppSourceType]);
                            url = HttpContext.Current.Server.UrlDecode(url);
                            SaveVideosInfoInAnalyticsDB(url,
                                IsDownloaded,
                                userName,
                                DateTime.Now,
                                GetUserIP(System.Web.HttpContext.Current),
                                System.Web.HttpContext.Current.Request.Browser.Browser,
                                isVideo,
                                HttpUtility.UrlDecode(url.Substring(url.LastIndexOf('/') + 1).Replace("." + url.Substring(url.LastIndexOf('.') + 1), string.Empty)),
                                url,
                                appSource);
                        }
                    }
                }
            }
            catch (Exception ex) {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("WebMethods.cs-SaveVideosInfo(url,isDownloaded)", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                throw ex;
            }
        }

        private static void SaveVideosInfo(string url, string isDownloaded, string docTitle, string originalUrl)
        {
            try
            {
                url = HttpContext.Current.Server.UrlDecode(url);
                originalUrl = HttpContext.Current.Server.UrlDecode(originalUrl);
                docTitle = HttpContext.Current.Server.UrlDecode(docTitle);
                string vaultUrl = Convert.ToString(ConfigurationManager.AppSettings[WebMethodConstants.VaultRootWebUrl]);
                using (SPSite site = new SPSite(vaultUrl))
                {
                    using (SPWeb web = site.RootWeb)
                    {
                        string querystring = string.Concat(
                                                   "<Where>",
                                                   "<Eq>",
                                                      "<FieldRef Name='", WebMethodConstants.ConfigKeyField, "'/>",
                                                      "<Value Type='Text'>", WebMethodConstants.VideoFormatKey, "</Value>",
                                                   "</Eq>",
                                                   "</Where>");
                        SPListItemCollection items = GetListItems(web, WebMethodConstants.ConfigList, querystring, null);
                        SPListItem configItem = null;
                        configItem = items[0];
                        bool isVideo = false;
                        if (configItem != null)
                        {
                            string videoFormats = configItem[WebMethodConstants.ConfigValueField].ToString();
                            string extension = null;
                            string[] splitString = url.Split('.');
                            for (int i = 0; i < splitString.Length; i++)
                            {
                                extension = splitString[i];
                            }
                            extension = extension.Replace("_z", "");
                            if (videoFormats.Contains(extension))
                            {
                                isVideo = true;
                            }
                        }
                        bool IsValidUser = true;
                        string userName = System.Web.HttpContext.Current.User.Identity.Name;
                        bool IsDownloaded = false;
                        if (string.Compare(isDownloaded, "true", true) == 0)
                        {
                            IsDownloaded = true;
                        }
                        string excludeUserAccounts = Convert.ToString(ConfigurationManager.AppSettings[WebMethodConstants.ExcludeUserAccounts]);
                        string[] arrExcludeUserAccounts = excludeUserAccounts.Split(';');
                        if (arrExcludeUserAccounts != null && arrExcludeUserAccounts.Length > 0)
                        {
                            int userExistCount = Array.IndexOf(arrExcludeUserAccounts, userName);
                            if (userExistCount > -1)
                            {
                                IsValidUser = false;
                            }
                        }
                        if (IsValidUser)
                        {
                            string appSource = Convert.ToString(ConfigurationManager.AppSettings[WebMethodConstants.AppSourceType]);
                            
                            SaveVideosInfoInAnalyticsDB(url,
                                IsDownloaded,
                                userName,
                                DateTime.Now,
                                GetUserIP(System.Web.HttpContext.Current),
                                System.Web.HttpContext.Current.Request.Browser.Browser,
                                isVideo,
                                docTitle,
                                originalUrl,
                                appSource);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("WebMethods.cs-SaveVideosInfo(4 parameters)", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                throw ex;
            }
        }

        private static void SaveVideosInfoInAnalyticsDB(string url, 
            bool IsDownloaded, 
            string userName, 
            DateTime timestamp, 
            string userIP, 
            string browser, 
            bool isVideo, 
            string docTitle, 
            string originalURL, 
            string appSourceType) {
            try
            {
                string spName = Convert.ToString(ConfigurationManager.AppSettings[WebMethodConstants.SpLogVideosInfo]);                
                string conString = Convert.ToString(ConfigurationManager.ConnectionStrings[Constants.AnalyticsDBConstants.ConnectionStringForAnalyticsDB_Windows]);
                if (!string.IsNullOrEmpty(url) 
                    && !string.IsNullOrEmpty(conString)
                    && !string.IsNullOrEmpty(docTitle)
                    && !string.IsNullOrEmpty(originalURL))
                {                    
                    sqlConnection = new SqlConnection(conString);
                    sqlConnection.Open();
                    SqlCommand command = new SqlCommand(spName.Trim(), sqlConnection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@UserName", SqlDbType.VarChar).Value = userName;
                    command.Parameters.Add("@URL", SqlDbType.VarChar).Value = url;
                    command.Parameters.Add("@IsDownloaded", SqlDbType.Bit).Value = IsDownloaded;
                    command.Parameters.Add("@Timestamp", SqlDbType.DateTime).Value = timestamp;
                    command.Parameters.Add("@UserIP", SqlDbType.VarChar).Value = userIP;
                    command.Parameters.Add("@Browser", SqlDbType.VarChar).Value = browser;
                    if (isVideo)
                    {
                        command.Parameters.Add("@Type", SqlDbType.VarChar).Value = WebMethodConstants.TypeVideo;
                    }
                    else
                    {
                        command.Parameters.Add("@Type", SqlDbType.VarChar).Value = WebMethodConstants.TypeDocument;
                    }
                    command.Parameters.Add("@DocTitle", SqlDbType.VarChar).Value = docTitle;
                    command.Parameters.Add("@OriginalURL", SqlDbType.VarChar).Value = originalURL;
                    command.Parameters.Add("@AppSourceType", SqlDbType.VarChar).Value = appSourceType;
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception ex) {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("WebMethods.cs-SaveVideosInfoInAnalyticsDB", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                throw ex;
            }
        }

        private static SPListItemCollection GetListItems(SPWeb web, string listname, string querystring, string viewstring)
        {
            SPListItemCollection items = null;
            // Build a query.
            try
            {
                if (!string.IsNullOrEmpty(listname) && !string.IsNullOrEmpty(querystring))
                {
                    SPQuery query = new SPQuery();
                    query.Query = querystring;
                    if (!string.IsNullOrEmpty(viewstring))
                    {
                        query.ViewFields = viewstring;

                        query.ViewFieldsOnly = true;
                    }
                    // Get data from a list.
                    string listUrl = web.ServerRelativeUrl + "lists/" + listname;
                    SPList list = web.GetList(listUrl);
                    items = list.GetItems(query);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
            return items;
        }
        private static string GetUserIP(HttpContext context)
        {
            var ip = (context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null
            && context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != "")
            ? context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"]
            : context.Request.ServerVariables["REMOTE_ADDR"];
            if (ip.Contains(","))
                ip = ip.Split(',').First().Trim();
            return ip;
        }
        #endregion
    }

    /// <summary>
    /// Constants of WebMethods class
    /// </summary>
    public static class WebMethodConstants
    {
        public const string ExcludeUserAccounts = "AdSpaceAnalyticsExcludeUserAccounts";
        public const string SpLogVideosInfo = "SpLogVideosInfo";
        public const string AppSourceType = "AppSourceType";
        public const string TypeVideo = "Video";
        public const string TypeDocument = "Document";
        public const string SpLogShareToAdSpaceInfo = "SpLogShareToAdSpaceInfo";
        public const string VaultRootWebUrl = "OTRootWebUrl";
        public const string ConfigList = "ConfigurationList";
        public const string ConfigKeyField = "Key";
        public const string ConfigValueField = "Value1";
        public const string VideoFormatKey = "OTVideoFormats";
    }

}
