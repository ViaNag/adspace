﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Viacom.Adspace.HttpModule
{
    public class CacheManager
    {
        #region Singleton Implementation

        private static volatile CacheManager instance;
        private static object syncLock = new object();

        /// <summary>
        /// gets the single instance.
        /// </summary>
        /// <value>The single instance.</value>
        public static CacheManager SingleInstance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncLock)
                    {
                        if (instance == null)
                        {
                            instance = new CacheManager();
                        }
                    }
                }
                return instance;
            }
        }

        /// <summary>
        /// Adds to the cache
        /// </summary>
        /// <param name="value">value</param>
        /// <param name="cacheKey">cache key</param>
        public void AddCache(object value, string cacheKey)
        {
            lock (syncLock)
            {
                HttpRuntime.Cache.Insert(cacheKey,
                          value,
                          null,
                          DateTime.MaxValue, TimeSpan.FromMinutes(15),
                          System.Web.Caching.CacheItemPriority.Default,
                          null);

            }
        }

        /// <summary>
        /// Gets the cache
        /// </summary>
        /// <param name="cacheKey">cache key</param>
        /// <returns>returns value</returns>
        public object GetCache(string cacheKey)
        {
            return HttpRuntime.Cache[cacheKey];
        }

        /// <summary>
        /// Checks if the key exists
        /// </summary>
        /// <param name="cacheKey">cache key</param>
        /// <returns>true if exists</returns>
        public bool KeyExists(string cacheKey)
        {
            return HttpRuntime.Cache[cacheKey] != null ? true : false;
        }

        /// <summary>
        /// Updates the cache
        /// </summary>
        /// <param name="value">value</param>
        /// <param name="cacheKey">cache key</param>
        public void UpdateCache(object value, string cacheKey)
        {
            lock (syncLock)
            {
                HttpRuntime.Cache.Remove(cacheKey);
                AddCache(value, cacheKey);
            }
        }

        /// <summary>
        /// Adds/Updates the cache key and value
        /// </summary>
        /// <param name="value">value</param>
        /// <param name="cacheKey">cache key</param>
        public void Add(object value, string cacheKey)
        {
            if (KeyExists(cacheKey))
            {
                AddCache(value, cacheKey);
            }
            else
            {
                UpdateCache(value, cacheKey);
            }
        }


        /// <summary>
        /// Removes the cacke key
        /// </summary>
        /// <param name="cacheKey">cache key</param>
        public void RemoveCache(string cacheKey)
        {
            lock (syncLock)
            {
                HttpRuntime.Cache.Remove(cacheKey);
            }
        }

        #endregion
    }
}
