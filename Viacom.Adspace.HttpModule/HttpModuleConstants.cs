﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Viacom.Adspace.HttpModule
{
    public static class HttpModuleConstants
    {
        #region RestrictedIpConstants
        public const string INTER_NETWORK = "InterNetwork";
        public const string SITEURL = "SiteUrl";
        public const string QUERY_ONACTIVE = "<Where><Eq><FieldRef Name='Active' /><Value Type='Boolean'>1</Value></Eq></Where>";
        public const string TITLE = "Title";
        public const string IPADDRESS_STARTRANGE="IPAddressStartRange";
        public const string IPADDRESS_ENDRANGE="IPAddressEndRange";
        public const string RESTRICTED_MODULES="add[@name='RestrictedIPHttpModule']";
        public const string MODULE_PATH="configuration/system.webServer/modules";
        public const string ASSEMBLY_PATH = @"<add name=""RestrictedIPHttpModule"" type=""Viacom.Adspace.HttpModule.RestrictedIP, {0}"" />";
        public const string RESTRICTED_URL = "RestrictedUrl";
        public const string RESTRICTED_IP = "RestrictedIp";

        #endregion

        #region ListNames
        public const string RESTRICTED_URLS_LIST="RestrictedUrls";
        public const string IPCONFIGURATION_LIST= "IpConfigurationList";
        #endregion
    }
}
